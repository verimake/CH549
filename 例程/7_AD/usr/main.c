/**
  ******************************************************************
  * @file    main.c
  * @author  甜鱼酱，xy
  * @version V1.0
  * @date    2021-5-19
  * @brief   SPI
  ******************************************************************
  * @attention
  * verimake 用于
  * 在 oled 屏幕上显示 p0.7 的电压值
  ******************************************************************
  */
#include <CH549_sdcc.h>  //ch549的头文件，其中定义了单片机的一些特殊功能寄存器
#include <CH549_OLED.h>  //其中有驱动屏幕使用的函数
#include <CH549_BMP.h>   //用于显示图片的头文件
#include <CH549_DEBUG.h> //CH549官方提供库的头文件，定义了一些关于主频，延时，串口设置，看门口，赋值设置等基础函数
#include <CH549_SPI.h>   //CH549官方提供库的头文件，定义了一些关于SPI初始化，传输数据等函数
#include <CH549_ADC.h>   //CH549官方提供库的头文件，定义了一些关于ADC初始化，采集数据等函数
/********************************************************************
* TIPS:
* adc各通道对应引脚关系
* P1.1       AIN1
* P1.2       AIN2
* P1.3       AIN3
* P1.4       AIN4
* P1.5       AIN5
* P1.6       AIN6
* P1.7       AIN7
* P0.0       AIN8
* P0.1       AIN9
* P0.2       AIN10  
* P0.3       AIN11 
* P0.4       AIN12 
* P0.5       AIN13 
* P0.6       AIN14 
* P0.7       AIN15 
*********************************************************************/
int oled_colum;
int oled_row;
/********************************************************************
* 函 数 名       : setCursor
* 函数功能		   : 设置printf到屏幕上的字符串起始位置
* 输    入       : 行坐标 列坐标
* 输    出    	 : 无
********************************************************************/
void setCursor(int column,int row)
{
oled_colum = column;
oled_row = row;
}
/********************************************************************
* 函 数 名       : main
* 函数功能		 : 主函数
* 输    入       : 无
* 输    出    	 : 无
*********************************************************************/
void main(void)
{
  UINT8 ch;
  float Voltage;
  CfgFsys(); //CH549时钟选择配置
  mDelaymS(20);
  /* OLED的初始化程序 */
  SPIMasterModeSet(3); //SPI主机模式设置，模式3
  SPI_CK_SET(12);      //12分频
/* 初始化OLED */
  OLED_Init();
  OLED_Clear();
  /* ADC的初始化配置 */
  ADC_ExInit(3); //ADC初始化,选择采样时钟
  /* 主循环 */
  while (1)
  {
    ch = 15;
    ADC_ChSelect(ch);  //选择通道
    ADC_StartSample(); //启动采样
    while ((ADC_CTRL & bADC_IF) == 0)
    {
      ; //查询等待标志置位
    }
    ADC_CTRL = bADC_IF; //清标志
    Voltage = (ADC_DAT / 4095.0)*5.0 ;
    /*
    adc采集到的值为ADC_DAT，由于ch549是12位ADC模数转换器因此ADC_DAT为0-4095之间的数，它表达的含义是电压值的相对高低
    因此 实际电压值的算法为ADC_DAT除以4095乘以ch549所使用的电压5v
    */
    setFontSize(16); //设置显示在oled上屏幕的字号
    setCursor(0,0);//设置printf到屏幕上的字符串起始位置
	  printf_fast_f("AD CJ %fV",Voltage);//显示数据到屏幕上 CJ含义为caiji（采集）
    mDelaymS(300);
  }
}
/********************************************************************
* 函 数 名       : putchar
* 函数功能       : 将printf映射到oled屏幕输出上
* 输    入      : 字符串
* 输    出    	: 字符串
********************************************************************/
int putchar( int a)
{      
  OLED_ShowChar(oled_colum,oled_row,a);
  oled_colum+=8;
  if (oled_colum>120){oled_colum=0;oled_row+=2;}
  return(a);
}