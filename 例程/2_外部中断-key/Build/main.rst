                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _Int0
                                     12 	.globl _main
                                     13 	.globl _Int0Init
                                     14 	.globl _delay
                                     15 	.globl _UIF_BUS_RST
                                     16 	.globl _UIF_DETECT
                                     17 	.globl _UIF_TRANSFER
                                     18 	.globl _UIF_SUSPEND
                                     19 	.globl _UIF_HST_SOF
                                     20 	.globl _UIF_FIFO_OV
                                     21 	.globl _U_SIE_FREE
                                     22 	.globl _U_TOG_OK
                                     23 	.globl _U_IS_NAK
                                     24 	.globl _S0_R_FIFO
                                     25 	.globl _S0_T_FIFO
                                     26 	.globl _S0_FREE
                                     27 	.globl _S0_IF_BYTE
                                     28 	.globl _S0_IF_FIRST
                                     29 	.globl _S0_IF_OV
                                     30 	.globl _S0_FST_ACT
                                     31 	.globl _CP_RL2
                                     32 	.globl _C_T2
                                     33 	.globl _TR2
                                     34 	.globl _EXEN2
                                     35 	.globl _TCLK
                                     36 	.globl _RCLK
                                     37 	.globl _EXF2
                                     38 	.globl _CAP1F
                                     39 	.globl _TF2
                                     40 	.globl _RI
                                     41 	.globl _TI
                                     42 	.globl _RB8
                                     43 	.globl _TB8
                                     44 	.globl _REN
                                     45 	.globl _SM2
                                     46 	.globl _SM1
                                     47 	.globl _SM0
                                     48 	.globl _IT0
                                     49 	.globl _IE0
                                     50 	.globl _IT1
                                     51 	.globl _IE1
                                     52 	.globl _TR0
                                     53 	.globl _TF0
                                     54 	.globl _TR1
                                     55 	.globl _TF1
                                     56 	.globl _XI
                                     57 	.globl _XO
                                     58 	.globl _P4_0
                                     59 	.globl _P4_1
                                     60 	.globl _P4_2
                                     61 	.globl _P4_3
                                     62 	.globl _P4_4
                                     63 	.globl _P4_5
                                     64 	.globl _P4_6
                                     65 	.globl _RXD
                                     66 	.globl _TXD
                                     67 	.globl _INT0
                                     68 	.globl _INT1
                                     69 	.globl _T0
                                     70 	.globl _T1
                                     71 	.globl _CAP0
                                     72 	.globl _INT3
                                     73 	.globl _P3_0
                                     74 	.globl _P3_1
                                     75 	.globl _P3_2
                                     76 	.globl _P3_3
                                     77 	.globl _P3_4
                                     78 	.globl _P3_5
                                     79 	.globl _P3_6
                                     80 	.globl _P3_7
                                     81 	.globl _PWM5
                                     82 	.globl _PWM4
                                     83 	.globl _INT0_
                                     84 	.globl _PWM3
                                     85 	.globl _PWM2
                                     86 	.globl _CAP1_
                                     87 	.globl _T2_
                                     88 	.globl _PWM1
                                     89 	.globl _CAP2_
                                     90 	.globl _T2EX_
                                     91 	.globl _PWM0
                                     92 	.globl _RXD1
                                     93 	.globl _PWM6
                                     94 	.globl _TXD1
                                     95 	.globl _PWM7
                                     96 	.globl _P2_0
                                     97 	.globl _P2_1
                                     98 	.globl _P2_2
                                     99 	.globl _P2_3
                                    100 	.globl _P2_4
                                    101 	.globl _P2_5
                                    102 	.globl _P2_6
                                    103 	.globl _P2_7
                                    104 	.globl _AIN0
                                    105 	.globl _CAP1
                                    106 	.globl _T2
                                    107 	.globl _AIN1
                                    108 	.globl _CAP2
                                    109 	.globl _T2EX
                                    110 	.globl _AIN2
                                    111 	.globl _AIN3
                                    112 	.globl _AIN4
                                    113 	.globl _UCC1
                                    114 	.globl _SCS
                                    115 	.globl _AIN5
                                    116 	.globl _UCC2
                                    117 	.globl _PWM0_
                                    118 	.globl _MOSI
                                    119 	.globl _AIN6
                                    120 	.globl _VBUS
                                    121 	.globl _RXD1_
                                    122 	.globl _MISO
                                    123 	.globl _AIN7
                                    124 	.globl _TXD1_
                                    125 	.globl _SCK
                                    126 	.globl _P1_0
                                    127 	.globl _P1_1
                                    128 	.globl _P1_2
                                    129 	.globl _P1_3
                                    130 	.globl _P1_4
                                    131 	.globl _P1_5
                                    132 	.globl _P1_6
                                    133 	.globl _P1_7
                                    134 	.globl _AIN8
                                    135 	.globl _AIN9
                                    136 	.globl _AIN10
                                    137 	.globl _RXD_
                                    138 	.globl _AIN11
                                    139 	.globl _TXD_
                                    140 	.globl _AIN12
                                    141 	.globl _RXD2
                                    142 	.globl _AIN13
                                    143 	.globl _TXD2
                                    144 	.globl _AIN14
                                    145 	.globl _RXD3
                                    146 	.globl _AIN15
                                    147 	.globl _TXD3
                                    148 	.globl _P0_0
                                    149 	.globl _P0_1
                                    150 	.globl _P0_2
                                    151 	.globl _P0_3
                                    152 	.globl _P0_4
                                    153 	.globl _P0_5
                                    154 	.globl _P0_6
                                    155 	.globl _P0_7
                                    156 	.globl _IE_SPI0
                                    157 	.globl _IE_INT3
                                    158 	.globl _IE_USB
                                    159 	.globl _IE_UART2
                                    160 	.globl _IE_ADC
                                    161 	.globl _IE_UART1
                                    162 	.globl _IE_UART3
                                    163 	.globl _IE_PWMX
                                    164 	.globl _IE_GPIO
                                    165 	.globl _IE_WDOG
                                    166 	.globl _PX0
                                    167 	.globl _PT0
                                    168 	.globl _PX1
                                    169 	.globl _PT1
                                    170 	.globl _PS
                                    171 	.globl _PT2
                                    172 	.globl _PL_FLAG
                                    173 	.globl _PH_FLAG
                                    174 	.globl _EX0
                                    175 	.globl _ET0
                                    176 	.globl _EX1
                                    177 	.globl _ET1
                                    178 	.globl _ES
                                    179 	.globl _ET2
                                    180 	.globl _E_DIS
                                    181 	.globl _EA
                                    182 	.globl _P
                                    183 	.globl _F1
                                    184 	.globl _OV
                                    185 	.globl _RS0
                                    186 	.globl _RS1
                                    187 	.globl _F0
                                    188 	.globl _AC
                                    189 	.globl _CY
                                    190 	.globl _UEP1_DMA_H
                                    191 	.globl _UEP1_DMA_L
                                    192 	.globl _UEP1_DMA
                                    193 	.globl _UEP0_DMA_H
                                    194 	.globl _UEP0_DMA_L
                                    195 	.globl _UEP0_DMA
                                    196 	.globl _UEP2_3_MOD
                                    197 	.globl _UEP4_1_MOD
                                    198 	.globl _UEP3_DMA_H
                                    199 	.globl _UEP3_DMA_L
                                    200 	.globl _UEP3_DMA
                                    201 	.globl _UEP2_DMA_H
                                    202 	.globl _UEP2_DMA_L
                                    203 	.globl _UEP2_DMA
                                    204 	.globl _USB_DEV_AD
                                    205 	.globl _USB_CTRL
                                    206 	.globl _USB_INT_EN
                                    207 	.globl _UEP4_T_LEN
                                    208 	.globl _UEP4_CTRL
                                    209 	.globl _UEP0_T_LEN
                                    210 	.globl _UEP0_CTRL
                                    211 	.globl _USB_RX_LEN
                                    212 	.globl _USB_MIS_ST
                                    213 	.globl _USB_INT_ST
                                    214 	.globl _USB_INT_FG
                                    215 	.globl _UEP3_T_LEN
                                    216 	.globl _UEP3_CTRL
                                    217 	.globl _UEP2_T_LEN
                                    218 	.globl _UEP2_CTRL
                                    219 	.globl _UEP1_T_LEN
                                    220 	.globl _UEP1_CTRL
                                    221 	.globl _UDEV_CTRL
                                    222 	.globl _USB_C_CTRL
                                    223 	.globl _ADC_PIN
                                    224 	.globl _ADC_CHAN
                                    225 	.globl _ADC_DAT_H
                                    226 	.globl _ADC_DAT_L
                                    227 	.globl _ADC_DAT
                                    228 	.globl _ADC_CFG
                                    229 	.globl _ADC_CTRL
                                    230 	.globl _TKEY_CTRL
                                    231 	.globl _SIF3
                                    232 	.globl _SBAUD3
                                    233 	.globl _SBUF3
                                    234 	.globl _SCON3
                                    235 	.globl _SIF2
                                    236 	.globl _SBAUD2
                                    237 	.globl _SBUF2
                                    238 	.globl _SCON2
                                    239 	.globl _SIF1
                                    240 	.globl _SBAUD1
                                    241 	.globl _SBUF1
                                    242 	.globl _SCON1
                                    243 	.globl _SPI0_SETUP
                                    244 	.globl _SPI0_CK_SE
                                    245 	.globl _SPI0_CTRL
                                    246 	.globl _SPI0_DATA
                                    247 	.globl _SPI0_STAT
                                    248 	.globl _PWM_DATA7
                                    249 	.globl _PWM_DATA6
                                    250 	.globl _PWM_DATA5
                                    251 	.globl _PWM_DATA4
                                    252 	.globl _PWM_DATA3
                                    253 	.globl _PWM_CTRL2
                                    254 	.globl _PWM_CK_SE
                                    255 	.globl _PWM_CTRL
                                    256 	.globl _PWM_DATA0
                                    257 	.globl _PWM_DATA1
                                    258 	.globl _PWM_DATA2
                                    259 	.globl _T2CAP1H
                                    260 	.globl _T2CAP1L
                                    261 	.globl _T2CAP1
                                    262 	.globl _TH2
                                    263 	.globl _TL2
                                    264 	.globl _T2COUNT
                                    265 	.globl _RCAP2H
                                    266 	.globl _RCAP2L
                                    267 	.globl _RCAP2
                                    268 	.globl _T2MOD
                                    269 	.globl _T2CON
                                    270 	.globl _T2CAP0H
                                    271 	.globl _T2CAP0L
                                    272 	.globl _T2CAP0
                                    273 	.globl _T2CON2
                                    274 	.globl _SBUF
                                    275 	.globl _SCON
                                    276 	.globl _TH1
                                    277 	.globl _TH0
                                    278 	.globl _TL1
                                    279 	.globl _TL0
                                    280 	.globl _TMOD
                                    281 	.globl _TCON
                                    282 	.globl _XBUS_AUX
                                    283 	.globl _PIN_FUNC
                                    284 	.globl _P5
                                    285 	.globl _P4_DIR_PU
                                    286 	.globl _P4_MOD_OC
                                    287 	.globl _P4
                                    288 	.globl _P3_DIR_PU
                                    289 	.globl _P3_MOD_OC
                                    290 	.globl _P3
                                    291 	.globl _P2_DIR_PU
                                    292 	.globl _P2_MOD_OC
                                    293 	.globl _P2
                                    294 	.globl _P1_DIR_PU
                                    295 	.globl _P1_MOD_OC
                                    296 	.globl _P1
                                    297 	.globl _P0_DIR_PU
                                    298 	.globl _P0_MOD_OC
                                    299 	.globl _P0
                                    300 	.globl _ROM_CTRL
                                    301 	.globl _ROM_DATA_HH
                                    302 	.globl _ROM_DATA_HL
                                    303 	.globl _ROM_DATA_HI
                                    304 	.globl _ROM_ADDR_H
                                    305 	.globl _ROM_ADDR_L
                                    306 	.globl _ROM_ADDR
                                    307 	.globl _GPIO_IE
                                    308 	.globl _INTX
                                    309 	.globl _IP_EX
                                    310 	.globl _IE_EX
                                    311 	.globl _IP
                                    312 	.globl _IE
                                    313 	.globl _WDOG_COUNT
                                    314 	.globl _RESET_KEEP
                                    315 	.globl _WAKE_CTRL
                                    316 	.globl _CLOCK_CFG
                                    317 	.globl _POWER_CFG
                                    318 	.globl _PCON
                                    319 	.globl _GLOBAL_CFG
                                    320 	.globl _SAFE_MOD
                                    321 	.globl _DPH
                                    322 	.globl _DPL
                                    323 	.globl _SP
                                    324 	.globl _A_INV
                                    325 	.globl _B
                                    326 	.globl _ACC
                                    327 	.globl _PSW
                                    328 ;--------------------------------------------------------
                                    329 ; special function registers
                                    330 ;--------------------------------------------------------
                                    331 	.area RSEG    (ABS,DATA)
      000000                        332 	.org 0x0000
                           0000D0   333 _PSW	=	0x00d0
                           0000E0   334 _ACC	=	0x00e0
                           0000F0   335 _B	=	0x00f0
                           0000FD   336 _A_INV	=	0x00fd
                           000081   337 _SP	=	0x0081
                           000082   338 _DPL	=	0x0082
                           000083   339 _DPH	=	0x0083
                           0000A1   340 _SAFE_MOD	=	0x00a1
                           0000B1   341 _GLOBAL_CFG	=	0x00b1
                           000087   342 _PCON	=	0x0087
                           0000BA   343 _POWER_CFG	=	0x00ba
                           0000B9   344 _CLOCK_CFG	=	0x00b9
                           0000A9   345 _WAKE_CTRL	=	0x00a9
                           0000FE   346 _RESET_KEEP	=	0x00fe
                           0000FF   347 _WDOG_COUNT	=	0x00ff
                           0000A8   348 _IE	=	0x00a8
                           0000B8   349 _IP	=	0x00b8
                           0000E8   350 _IE_EX	=	0x00e8
                           0000E9   351 _IP_EX	=	0x00e9
                           0000B3   352 _INTX	=	0x00b3
                           0000B2   353 _GPIO_IE	=	0x00b2
                           008584   354 _ROM_ADDR	=	0x8584
                           000084   355 _ROM_ADDR_L	=	0x0084
                           000085   356 _ROM_ADDR_H	=	0x0085
                           008F8E   357 _ROM_DATA_HI	=	0x8f8e
                           00008E   358 _ROM_DATA_HL	=	0x008e
                           00008F   359 _ROM_DATA_HH	=	0x008f
                           000086   360 _ROM_CTRL	=	0x0086
                           000080   361 _P0	=	0x0080
                           0000C4   362 _P0_MOD_OC	=	0x00c4
                           0000C5   363 _P0_DIR_PU	=	0x00c5
                           000090   364 _P1	=	0x0090
                           000092   365 _P1_MOD_OC	=	0x0092
                           000093   366 _P1_DIR_PU	=	0x0093
                           0000A0   367 _P2	=	0x00a0
                           000094   368 _P2_MOD_OC	=	0x0094
                           000095   369 _P2_DIR_PU	=	0x0095
                           0000B0   370 _P3	=	0x00b0
                           000096   371 _P3_MOD_OC	=	0x0096
                           000097   372 _P3_DIR_PU	=	0x0097
                           0000C0   373 _P4	=	0x00c0
                           0000C2   374 _P4_MOD_OC	=	0x00c2
                           0000C3   375 _P4_DIR_PU	=	0x00c3
                           0000AB   376 _P5	=	0x00ab
                           0000AA   377 _PIN_FUNC	=	0x00aa
                           0000A2   378 _XBUS_AUX	=	0x00a2
                           000088   379 _TCON	=	0x0088
                           000089   380 _TMOD	=	0x0089
                           00008A   381 _TL0	=	0x008a
                           00008B   382 _TL1	=	0x008b
                           00008C   383 _TH0	=	0x008c
                           00008D   384 _TH1	=	0x008d
                           000098   385 _SCON	=	0x0098
                           000099   386 _SBUF	=	0x0099
                           0000C1   387 _T2CON2	=	0x00c1
                           00C7C6   388 _T2CAP0	=	0xc7c6
                           0000C6   389 _T2CAP0L	=	0x00c6
                           0000C7   390 _T2CAP0H	=	0x00c7
                           0000C8   391 _T2CON	=	0x00c8
                           0000C9   392 _T2MOD	=	0x00c9
                           00CBCA   393 _RCAP2	=	0xcbca
                           0000CA   394 _RCAP2L	=	0x00ca
                           0000CB   395 _RCAP2H	=	0x00cb
                           00CDCC   396 _T2COUNT	=	0xcdcc
                           0000CC   397 _TL2	=	0x00cc
                           0000CD   398 _TH2	=	0x00cd
                           00CFCE   399 _T2CAP1	=	0xcfce
                           0000CE   400 _T2CAP1L	=	0x00ce
                           0000CF   401 _T2CAP1H	=	0x00cf
                           00009A   402 _PWM_DATA2	=	0x009a
                           00009B   403 _PWM_DATA1	=	0x009b
                           00009C   404 _PWM_DATA0	=	0x009c
                           00009D   405 _PWM_CTRL	=	0x009d
                           00009E   406 _PWM_CK_SE	=	0x009e
                           00009F   407 _PWM_CTRL2	=	0x009f
                           0000A3   408 _PWM_DATA3	=	0x00a3
                           0000A4   409 _PWM_DATA4	=	0x00a4
                           0000A5   410 _PWM_DATA5	=	0x00a5
                           0000A6   411 _PWM_DATA6	=	0x00a6
                           0000A7   412 _PWM_DATA7	=	0x00a7
                           0000F8   413 _SPI0_STAT	=	0x00f8
                           0000F9   414 _SPI0_DATA	=	0x00f9
                           0000FA   415 _SPI0_CTRL	=	0x00fa
                           0000FB   416 _SPI0_CK_SE	=	0x00fb
                           0000FC   417 _SPI0_SETUP	=	0x00fc
                           0000BC   418 _SCON1	=	0x00bc
                           0000BD   419 _SBUF1	=	0x00bd
                           0000BE   420 _SBAUD1	=	0x00be
                           0000BF   421 _SIF1	=	0x00bf
                           0000B4   422 _SCON2	=	0x00b4
                           0000B5   423 _SBUF2	=	0x00b5
                           0000B6   424 _SBAUD2	=	0x00b6
                           0000B7   425 _SIF2	=	0x00b7
                           0000AC   426 _SCON3	=	0x00ac
                           0000AD   427 _SBUF3	=	0x00ad
                           0000AE   428 _SBAUD3	=	0x00ae
                           0000AF   429 _SIF3	=	0x00af
                           0000F1   430 _TKEY_CTRL	=	0x00f1
                           0000F2   431 _ADC_CTRL	=	0x00f2
                           0000F3   432 _ADC_CFG	=	0x00f3
                           00F5F4   433 _ADC_DAT	=	0xf5f4
                           0000F4   434 _ADC_DAT_L	=	0x00f4
                           0000F5   435 _ADC_DAT_H	=	0x00f5
                           0000F6   436 _ADC_CHAN	=	0x00f6
                           0000F7   437 _ADC_PIN	=	0x00f7
                           000091   438 _USB_C_CTRL	=	0x0091
                           0000D1   439 _UDEV_CTRL	=	0x00d1
                           0000D2   440 _UEP1_CTRL	=	0x00d2
                           0000D3   441 _UEP1_T_LEN	=	0x00d3
                           0000D4   442 _UEP2_CTRL	=	0x00d4
                           0000D5   443 _UEP2_T_LEN	=	0x00d5
                           0000D6   444 _UEP3_CTRL	=	0x00d6
                           0000D7   445 _UEP3_T_LEN	=	0x00d7
                           0000D8   446 _USB_INT_FG	=	0x00d8
                           0000D9   447 _USB_INT_ST	=	0x00d9
                           0000DA   448 _USB_MIS_ST	=	0x00da
                           0000DB   449 _USB_RX_LEN	=	0x00db
                           0000DC   450 _UEP0_CTRL	=	0x00dc
                           0000DD   451 _UEP0_T_LEN	=	0x00dd
                           0000DE   452 _UEP4_CTRL	=	0x00de
                           0000DF   453 _UEP4_T_LEN	=	0x00df
                           0000E1   454 _USB_INT_EN	=	0x00e1
                           0000E2   455 _USB_CTRL	=	0x00e2
                           0000E3   456 _USB_DEV_AD	=	0x00e3
                           00E5E4   457 _UEP2_DMA	=	0xe5e4
                           0000E4   458 _UEP2_DMA_L	=	0x00e4
                           0000E5   459 _UEP2_DMA_H	=	0x00e5
                           00E7E6   460 _UEP3_DMA	=	0xe7e6
                           0000E6   461 _UEP3_DMA_L	=	0x00e6
                           0000E7   462 _UEP3_DMA_H	=	0x00e7
                           0000EA   463 _UEP4_1_MOD	=	0x00ea
                           0000EB   464 _UEP2_3_MOD	=	0x00eb
                           00EDEC   465 _UEP0_DMA	=	0xedec
                           0000EC   466 _UEP0_DMA_L	=	0x00ec
                           0000ED   467 _UEP0_DMA_H	=	0x00ed
                           00EFEE   468 _UEP1_DMA	=	0xefee
                           0000EE   469 _UEP1_DMA_L	=	0x00ee
                           0000EF   470 _UEP1_DMA_H	=	0x00ef
                                    471 ;--------------------------------------------------------
                                    472 ; special function bits
                                    473 ;--------------------------------------------------------
                                    474 	.area RSEG    (ABS,DATA)
      000000                        475 	.org 0x0000
                           0000D7   476 _CY	=	0x00d7
                           0000D6   477 _AC	=	0x00d6
                           0000D5   478 _F0	=	0x00d5
                           0000D4   479 _RS1	=	0x00d4
                           0000D3   480 _RS0	=	0x00d3
                           0000D2   481 _OV	=	0x00d2
                           0000D1   482 _F1	=	0x00d1
                           0000D0   483 _P	=	0x00d0
                           0000AF   484 _EA	=	0x00af
                           0000AE   485 _E_DIS	=	0x00ae
                           0000AD   486 _ET2	=	0x00ad
                           0000AC   487 _ES	=	0x00ac
                           0000AB   488 _ET1	=	0x00ab
                           0000AA   489 _EX1	=	0x00aa
                           0000A9   490 _ET0	=	0x00a9
                           0000A8   491 _EX0	=	0x00a8
                           0000BF   492 _PH_FLAG	=	0x00bf
                           0000BE   493 _PL_FLAG	=	0x00be
                           0000BD   494 _PT2	=	0x00bd
                           0000BC   495 _PS	=	0x00bc
                           0000BB   496 _PT1	=	0x00bb
                           0000BA   497 _PX1	=	0x00ba
                           0000B9   498 _PT0	=	0x00b9
                           0000B8   499 _PX0	=	0x00b8
                           0000EF   500 _IE_WDOG	=	0x00ef
                           0000EE   501 _IE_GPIO	=	0x00ee
                           0000ED   502 _IE_PWMX	=	0x00ed
                           0000ED   503 _IE_UART3	=	0x00ed
                           0000EC   504 _IE_UART1	=	0x00ec
                           0000EB   505 _IE_ADC	=	0x00eb
                           0000EB   506 _IE_UART2	=	0x00eb
                           0000EA   507 _IE_USB	=	0x00ea
                           0000E9   508 _IE_INT3	=	0x00e9
                           0000E8   509 _IE_SPI0	=	0x00e8
                           000087   510 _P0_7	=	0x0087
                           000086   511 _P0_6	=	0x0086
                           000085   512 _P0_5	=	0x0085
                           000084   513 _P0_4	=	0x0084
                           000083   514 _P0_3	=	0x0083
                           000082   515 _P0_2	=	0x0082
                           000081   516 _P0_1	=	0x0081
                           000080   517 _P0_0	=	0x0080
                           000087   518 _TXD3	=	0x0087
                           000087   519 _AIN15	=	0x0087
                           000086   520 _RXD3	=	0x0086
                           000086   521 _AIN14	=	0x0086
                           000085   522 _TXD2	=	0x0085
                           000085   523 _AIN13	=	0x0085
                           000084   524 _RXD2	=	0x0084
                           000084   525 _AIN12	=	0x0084
                           000083   526 _TXD_	=	0x0083
                           000083   527 _AIN11	=	0x0083
                           000082   528 _RXD_	=	0x0082
                           000082   529 _AIN10	=	0x0082
                           000081   530 _AIN9	=	0x0081
                           000080   531 _AIN8	=	0x0080
                           000097   532 _P1_7	=	0x0097
                           000096   533 _P1_6	=	0x0096
                           000095   534 _P1_5	=	0x0095
                           000094   535 _P1_4	=	0x0094
                           000093   536 _P1_3	=	0x0093
                           000092   537 _P1_2	=	0x0092
                           000091   538 _P1_1	=	0x0091
                           000090   539 _P1_0	=	0x0090
                           000097   540 _SCK	=	0x0097
                           000097   541 _TXD1_	=	0x0097
                           000097   542 _AIN7	=	0x0097
                           000096   543 _MISO	=	0x0096
                           000096   544 _RXD1_	=	0x0096
                           000096   545 _VBUS	=	0x0096
                           000096   546 _AIN6	=	0x0096
                           000095   547 _MOSI	=	0x0095
                           000095   548 _PWM0_	=	0x0095
                           000095   549 _UCC2	=	0x0095
                           000095   550 _AIN5	=	0x0095
                           000094   551 _SCS	=	0x0094
                           000094   552 _UCC1	=	0x0094
                           000094   553 _AIN4	=	0x0094
                           000093   554 _AIN3	=	0x0093
                           000092   555 _AIN2	=	0x0092
                           000091   556 _T2EX	=	0x0091
                           000091   557 _CAP2	=	0x0091
                           000091   558 _AIN1	=	0x0091
                           000090   559 _T2	=	0x0090
                           000090   560 _CAP1	=	0x0090
                           000090   561 _AIN0	=	0x0090
                           0000A7   562 _P2_7	=	0x00a7
                           0000A6   563 _P2_6	=	0x00a6
                           0000A5   564 _P2_5	=	0x00a5
                           0000A4   565 _P2_4	=	0x00a4
                           0000A3   566 _P2_3	=	0x00a3
                           0000A2   567 _P2_2	=	0x00a2
                           0000A1   568 _P2_1	=	0x00a1
                           0000A0   569 _P2_0	=	0x00a0
                           0000A7   570 _PWM7	=	0x00a7
                           0000A7   571 _TXD1	=	0x00a7
                           0000A6   572 _PWM6	=	0x00a6
                           0000A6   573 _RXD1	=	0x00a6
                           0000A5   574 _PWM0	=	0x00a5
                           0000A5   575 _T2EX_	=	0x00a5
                           0000A5   576 _CAP2_	=	0x00a5
                           0000A4   577 _PWM1	=	0x00a4
                           0000A4   578 _T2_	=	0x00a4
                           0000A4   579 _CAP1_	=	0x00a4
                           0000A3   580 _PWM2	=	0x00a3
                           0000A2   581 _PWM3	=	0x00a2
                           0000A2   582 _INT0_	=	0x00a2
                           0000A1   583 _PWM4	=	0x00a1
                           0000A0   584 _PWM5	=	0x00a0
                           0000B7   585 _P3_7	=	0x00b7
                           0000B6   586 _P3_6	=	0x00b6
                           0000B5   587 _P3_5	=	0x00b5
                           0000B4   588 _P3_4	=	0x00b4
                           0000B3   589 _P3_3	=	0x00b3
                           0000B2   590 _P3_2	=	0x00b2
                           0000B1   591 _P3_1	=	0x00b1
                           0000B0   592 _P3_0	=	0x00b0
                           0000B7   593 _INT3	=	0x00b7
                           0000B6   594 _CAP0	=	0x00b6
                           0000B5   595 _T1	=	0x00b5
                           0000B4   596 _T0	=	0x00b4
                           0000B3   597 _INT1	=	0x00b3
                           0000B2   598 _INT0	=	0x00b2
                           0000B1   599 _TXD	=	0x00b1
                           0000B0   600 _RXD	=	0x00b0
                           0000C6   601 _P4_6	=	0x00c6
                           0000C5   602 _P4_5	=	0x00c5
                           0000C4   603 _P4_4	=	0x00c4
                           0000C3   604 _P4_3	=	0x00c3
                           0000C2   605 _P4_2	=	0x00c2
                           0000C1   606 _P4_1	=	0x00c1
                           0000C0   607 _P4_0	=	0x00c0
                           0000C7   608 _XO	=	0x00c7
                           0000C6   609 _XI	=	0x00c6
                           00008F   610 _TF1	=	0x008f
                           00008E   611 _TR1	=	0x008e
                           00008D   612 _TF0	=	0x008d
                           00008C   613 _TR0	=	0x008c
                           00008B   614 _IE1	=	0x008b
                           00008A   615 _IT1	=	0x008a
                           000089   616 _IE0	=	0x0089
                           000088   617 _IT0	=	0x0088
                           00009F   618 _SM0	=	0x009f
                           00009E   619 _SM1	=	0x009e
                           00009D   620 _SM2	=	0x009d
                           00009C   621 _REN	=	0x009c
                           00009B   622 _TB8	=	0x009b
                           00009A   623 _RB8	=	0x009a
                           000099   624 _TI	=	0x0099
                           000098   625 _RI	=	0x0098
                           0000CF   626 _TF2	=	0x00cf
                           0000CF   627 _CAP1F	=	0x00cf
                           0000CE   628 _EXF2	=	0x00ce
                           0000CD   629 _RCLK	=	0x00cd
                           0000CC   630 _TCLK	=	0x00cc
                           0000CB   631 _EXEN2	=	0x00cb
                           0000CA   632 _TR2	=	0x00ca
                           0000C9   633 _C_T2	=	0x00c9
                           0000C8   634 _CP_RL2	=	0x00c8
                           0000FF   635 _S0_FST_ACT	=	0x00ff
                           0000FE   636 _S0_IF_OV	=	0x00fe
                           0000FD   637 _S0_IF_FIRST	=	0x00fd
                           0000FC   638 _S0_IF_BYTE	=	0x00fc
                           0000FB   639 _S0_FREE	=	0x00fb
                           0000FA   640 _S0_T_FIFO	=	0x00fa
                           0000F8   641 _S0_R_FIFO	=	0x00f8
                           0000DF   642 _U_IS_NAK	=	0x00df
                           0000DE   643 _U_TOG_OK	=	0x00de
                           0000DD   644 _U_SIE_FREE	=	0x00dd
                           0000DC   645 _UIF_FIFO_OV	=	0x00dc
                           0000DB   646 _UIF_HST_SOF	=	0x00db
                           0000DA   647 _UIF_SUSPEND	=	0x00da
                           0000D9   648 _UIF_TRANSFER	=	0x00d9
                           0000D8   649 _UIF_DETECT	=	0x00d8
                           0000D8   650 _UIF_BUS_RST	=	0x00d8
                                    651 ;--------------------------------------------------------
                                    652 ; overlayable register banks
                                    653 ;--------------------------------------------------------
                                    654 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        655 	.ds 8
                                    656 ;--------------------------------------------------------
                                    657 ; overlayable bit register bank
                                    658 ;--------------------------------------------------------
                                    659 	.area BIT_BANK	(REL,OVR,DATA)
      000020                        660 bits:
      000020                        661 	.ds 1
                           008000   662 	b0 = bits[0]
                           008100   663 	b1 = bits[1]
                           008200   664 	b2 = bits[2]
                           008300   665 	b3 = bits[3]
                           008400   666 	b4 = bits[4]
                           008500   667 	b5 = bits[5]
                           008600   668 	b6 = bits[6]
                           008700   669 	b7 = bits[7]
                                    670 ;--------------------------------------------------------
                                    671 ; internal ram data
                                    672 ;--------------------------------------------------------
                                    673 	.area DSEG    (DATA)
                                    674 ;--------------------------------------------------------
                                    675 ; overlayable items in internal ram 
                                    676 ;--------------------------------------------------------
                                    677 	.area	OSEG    (OVR,DATA)
                                    678 ;--------------------------------------------------------
                                    679 ; Stack segment in internal ram 
                                    680 ;--------------------------------------------------------
                                    681 	.area	SSEG
      000021                        682 __start__stack:
      000021                        683 	.ds	1
                                    684 
                                    685 ;--------------------------------------------------------
                                    686 ; indirectly addressable internal ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area ISEG    (DATA)
                                    689 ;--------------------------------------------------------
                                    690 ; absolute internal ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area IABS    (ABS,DATA)
                                    693 	.area IABS    (ABS,DATA)
                                    694 ;--------------------------------------------------------
                                    695 ; bit data
                                    696 ;--------------------------------------------------------
                                    697 	.area BSEG    (BIT)
                                    698 ;--------------------------------------------------------
                                    699 ; paged external ram data
                                    700 ;--------------------------------------------------------
                                    701 	.area PSEG    (PAG,XDATA)
                                    702 ;--------------------------------------------------------
                                    703 ; external ram data
                                    704 ;--------------------------------------------------------
                                    705 	.area XSEG    (XDATA)
                                    706 ;--------------------------------------------------------
                                    707 ; absolute external ram data
                                    708 ;--------------------------------------------------------
                                    709 	.area XABS    (ABS,XDATA)
                                    710 ;--------------------------------------------------------
                                    711 ; external initialized ram data
                                    712 ;--------------------------------------------------------
                                    713 	.area XISEG   (XDATA)
                                    714 	.area HOME    (CODE)
                                    715 	.area GSINIT0 (CODE)
                                    716 	.area GSINIT1 (CODE)
                                    717 	.area GSINIT2 (CODE)
                                    718 	.area GSINIT3 (CODE)
                                    719 	.area GSINIT4 (CODE)
                                    720 	.area GSINIT5 (CODE)
                                    721 	.area GSINIT  (CODE)
                                    722 	.area GSFINAL (CODE)
                                    723 	.area CSEG    (CODE)
                                    724 ;--------------------------------------------------------
                                    725 ; interrupt vector 
                                    726 ;--------------------------------------------------------
                                    727 	.area HOME    (CODE)
      000000                        728 __interrupt_vect:
      000000 02 00 09         [24]  729 	ljmp	__sdcc_gsinit_startup
      000003 02 00 94         [24]  730 	ljmp	_Int0
                                    731 ;--------------------------------------------------------
                                    732 ; global & static initialisations
                                    733 ;--------------------------------------------------------
                                    734 	.area HOME    (CODE)
                                    735 	.area GSINIT  (CODE)
                                    736 	.area GSFINAL (CODE)
                                    737 	.area GSINIT  (CODE)
                                    738 	.globl __sdcc_gsinit_startup
                                    739 	.globl __sdcc_program_startup
                                    740 	.globl __start__stack
                                    741 	.globl __mcs51_genXINIT
                                    742 	.globl __mcs51_genXRAMCLEAR
                                    743 	.globl __mcs51_genRAMCLEAR
                                    744 	.area GSFINAL (CODE)
      000062 02 00 06         [24]  745 	ljmp	__sdcc_program_startup
                                    746 ;--------------------------------------------------------
                                    747 ; Home
                                    748 ;--------------------------------------------------------
                                    749 	.area HOME    (CODE)
                                    750 	.area HOME    (CODE)
      000006                        751 __sdcc_program_startup:
      000006 02 00 8F         [24]  752 	ljmp	_main
                                    753 ;	return from main will return to caller
                                    754 ;--------------------------------------------------------
                                    755 ; code
                                    756 ;--------------------------------------------------------
                                    757 	.area CSEG    (CODE)
                                    758 ;------------------------------------------------------------
                                    759 ;Allocation info for local variables in function 'delay'
                                    760 ;------------------------------------------------------------
                                    761 ;i                         Allocated to registers 
                                    762 ;------------------------------------------------------------
                                    763 ;	usr/main.c:26: void delay(long int i)
                                    764 ;	-----------------------------------------
                                    765 ;	 function delay
                                    766 ;	-----------------------------------------
      000065                        767 _delay:
                           000007   768 	ar7 = 0x07
                           000006   769 	ar6 = 0x06
                           000005   770 	ar5 = 0x05
                           000004   771 	ar4 = 0x04
                           000003   772 	ar3 = 0x03
                           000002   773 	ar2 = 0x02
                           000001   774 	ar1 = 0x01
                           000000   775 	ar0 = 0x00
      000065 AC 82            [24]  776 	mov	r4,dpl
      000067 AD 83            [24]  777 	mov	r5,dph
      000069 AE F0            [24]  778 	mov	r6,b
      00006B FF               [12]  779 	mov	r7,a
                                    780 ;	usr/main.c:28: while(i--);	
      00006C                        781 00101$:
      00006C 8C 00            [24]  782 	mov	ar0,r4
      00006E 8D 01            [24]  783 	mov	ar1,r5
      000070 8E 02            [24]  784 	mov	ar2,r6
      000072 8F 03            [24]  785 	mov	ar3,r7
      000074 1C               [12]  786 	dec	r4
      000075 BC FF 09         [24]  787 	cjne	r4,#0xff,00111$
      000078 1D               [12]  788 	dec	r5
      000079 BD FF 05         [24]  789 	cjne	r5,#0xff,00111$
      00007C 1E               [12]  790 	dec	r6
      00007D BE FF 01         [24]  791 	cjne	r6,#0xff,00111$
      000080 1F               [12]  792 	dec	r7
      000081                        793 00111$:
      000081 E8               [12]  794 	mov	a,r0
      000082 49               [12]  795 	orl	a,r1
      000083 4A               [12]  796 	orl	a,r2
      000084 4B               [12]  797 	orl	a,r3
      000085 70 E5            [24]  798 	jnz	00101$
                                    799 ;	usr/main.c:29: }
      000087 22               [24]  800 	ret
                                    801 ;------------------------------------------------------------
                                    802 ;Allocation info for local variables in function 'Int0Init'
                                    803 ;------------------------------------------------------------
                                    804 ;	usr/main.c:46: void Int0Init()
                                    805 ;	-----------------------------------------
                                    806 ;	 function Int0Init
                                    807 ;	-----------------------------------------
      000088                        808 _Int0Init:
                                    809 ;	usr/main.c:49: IT0=1;  //跳变沿触发方式（下降沿）
                                    810 ;	assignBit
      000088 D2 88            [12]  811 	setb	_IT0
                                    812 ;	usr/main.c:50: EX0=1;  //打开INT0的中断允许。	
                                    813 ;	assignBit
      00008A D2 A8            [12]  814 	setb	_EX0
                                    815 ;	usr/main.c:51: EA=1;   //打开总中断	
                                    816 ;	assignBit
      00008C D2 AF            [12]  817 	setb	_EA
                                    818 ;	usr/main.c:52: }
      00008E 22               [24]  819 	ret
                                    820 ;------------------------------------------------------------
                                    821 ;Allocation info for local variables in function 'main'
                                    822 ;------------------------------------------------------------
                                    823 ;	usr/main.c:74: void main()
                                    824 ;	-----------------------------------------
                                    825 ;	 function main
                                    826 ;	-----------------------------------------
      00008F                        827 _main:
                                    828 ;	usr/main.c:76: Int0Init();  //设置外部中断0（P3.2）
      00008F 12 00 88         [24]  829 	lcall	_Int0Init
                                    830 ;	usr/main.c:77: while(1);    //死循环
      000092                        831 00102$:
                                    832 ;	usr/main.c:78: }
      000092 80 FE            [24]  833 	sjmp	00102$
                                    834 ;------------------------------------------------------------
                                    835 ;Allocation info for local variables in function 'Int0'
                                    836 ;------------------------------------------------------------
                                    837 ;	usr/main.c:92: void Int0()	__interrupt 0		//外部中断0的中断函数
                                    838 ;	-----------------------------------------
                                    839 ;	 function Int0
                                    840 ;	-----------------------------------------
      000094                        841 _Int0:
      000094 C0 20            [24]  842 	push	bits
      000096 C0 E0            [24]  843 	push	acc
      000098 C0 F0            [24]  844 	push	b
      00009A C0 82            [24]  845 	push	dpl
      00009C C0 83            [24]  846 	push	dph
      00009E C0 07            [24]  847 	push	(0+7)
      0000A0 C0 06            [24]  848 	push	(0+6)
      0000A2 C0 05            [24]  849 	push	(0+5)
      0000A4 C0 04            [24]  850 	push	(0+4)
      0000A6 C0 03            [24]  851 	push	(0+3)
      0000A8 C0 02            [24]  852 	push	(0+2)
      0000AA C0 01            [24]  853 	push	(0+1)
      0000AC C0 00            [24]  854 	push	(0+0)
      0000AE C0 D0            [24]  855 	push	psw
      0000B0 75 D0 00         [24]  856 	mov	psw,#0x00
                                    857 ;	usr/main.c:94: delay(120000);	 //延时消抖
      0000B3 90 D4 C0         [24]  858 	mov	dptr,#0xd4c0
      0000B6 75 F0 01         [24]  859 	mov	b,#0x01
      0000B9 E4               [12]  860 	clr	a
      0000BA 12 00 65         [24]  861 	lcall	_delay
                                    862 ;	usr/main.c:95: IE0=0;
                                    863 ;	assignBit
      0000BD C2 89            [12]  864 	clr	_IE0
                                    865 ;	usr/main.c:96: if(key1==0)
      0000BF E5 B0            [12]  866 	mov	a,_P3
      0000C1 60 02            [24]  867 	jz	00103$
                                    868 ;	usr/main.c:98: led=!led;    //LED状态取反
      0000C3 B2 A2            [12]  869 	cpl	_P2_2
      0000C5                        870 00103$:
                                    871 ;	usr/main.c:100: }
      0000C5 D0 D0            [24]  872 	pop	psw
      0000C7 D0 00            [24]  873 	pop	(0+0)
      0000C9 D0 01            [24]  874 	pop	(0+1)
      0000CB D0 02            [24]  875 	pop	(0+2)
      0000CD D0 03            [24]  876 	pop	(0+3)
      0000CF D0 04            [24]  877 	pop	(0+4)
      0000D1 D0 05            [24]  878 	pop	(0+5)
      0000D3 D0 06            [24]  879 	pop	(0+6)
      0000D5 D0 07            [24]  880 	pop	(0+7)
      0000D7 D0 83            [24]  881 	pop	dph
      0000D9 D0 82            [24]  882 	pop	dpl
      0000DB D0 F0            [24]  883 	pop	b
      0000DD D0 E0            [24]  884 	pop	acc
      0000DF D0 20            [24]  885 	pop	bits
      0000E1 32               [24]  886 	reti
                                    887 	.area CSEG    (CODE)
                                    888 	.area CONST   (CODE)
                                    889 	.area XINIT   (CODE)
                                    890 	.area CABS    (ABS,CODE)
