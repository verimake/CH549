                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_DEBUG
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _CH549UART0Alter
                                     12 	.globl _UIF_BUS_RST
                                     13 	.globl _UIF_DETECT
                                     14 	.globl _UIF_TRANSFER
                                     15 	.globl _UIF_SUSPEND
                                     16 	.globl _UIF_HST_SOF
                                     17 	.globl _UIF_FIFO_OV
                                     18 	.globl _U_SIE_FREE
                                     19 	.globl _U_TOG_OK
                                     20 	.globl _U_IS_NAK
                                     21 	.globl _S0_R_FIFO
                                     22 	.globl _S0_T_FIFO
                                     23 	.globl _S0_FREE
                                     24 	.globl _S0_IF_BYTE
                                     25 	.globl _S0_IF_FIRST
                                     26 	.globl _S0_IF_OV
                                     27 	.globl _S0_FST_ACT
                                     28 	.globl _CP_RL2
                                     29 	.globl _C_T2
                                     30 	.globl _TR2
                                     31 	.globl _EXEN2
                                     32 	.globl _TCLK
                                     33 	.globl _RCLK
                                     34 	.globl _EXF2
                                     35 	.globl _CAP1F
                                     36 	.globl _TF2
                                     37 	.globl _RI
                                     38 	.globl _TI
                                     39 	.globl _RB8
                                     40 	.globl _TB8
                                     41 	.globl _REN
                                     42 	.globl _SM2
                                     43 	.globl _SM1
                                     44 	.globl _SM0
                                     45 	.globl _IT0
                                     46 	.globl _IE0
                                     47 	.globl _IT1
                                     48 	.globl _IE1
                                     49 	.globl _TR0
                                     50 	.globl _TF0
                                     51 	.globl _TR1
                                     52 	.globl _TF1
                                     53 	.globl _XI
                                     54 	.globl _XO
                                     55 	.globl _P4_0
                                     56 	.globl _P4_1
                                     57 	.globl _P4_2
                                     58 	.globl _P4_3
                                     59 	.globl _P4_4
                                     60 	.globl _P4_5
                                     61 	.globl _P4_6
                                     62 	.globl _RXD
                                     63 	.globl _TXD
                                     64 	.globl _INT0
                                     65 	.globl _INT1
                                     66 	.globl _T0
                                     67 	.globl _T1
                                     68 	.globl _CAP0
                                     69 	.globl _INT3
                                     70 	.globl _P3_0
                                     71 	.globl _P3_1
                                     72 	.globl _P3_2
                                     73 	.globl _P3_3
                                     74 	.globl _P3_4
                                     75 	.globl _P3_5
                                     76 	.globl _P3_6
                                     77 	.globl _P3_7
                                     78 	.globl _PWM5
                                     79 	.globl _PWM4
                                     80 	.globl _INT0_
                                     81 	.globl _PWM3
                                     82 	.globl _PWM2
                                     83 	.globl _CAP1_
                                     84 	.globl _T2_
                                     85 	.globl _PWM1
                                     86 	.globl _CAP2_
                                     87 	.globl _T2EX_
                                     88 	.globl _PWM0
                                     89 	.globl _RXD1
                                     90 	.globl _PWM6
                                     91 	.globl _TXD1
                                     92 	.globl _PWM7
                                     93 	.globl _P2_0
                                     94 	.globl _P2_1
                                     95 	.globl _P2_2
                                     96 	.globl _P2_3
                                     97 	.globl _P2_4
                                     98 	.globl _P2_5
                                     99 	.globl _P2_6
                                    100 	.globl _P2_7
                                    101 	.globl _AIN0
                                    102 	.globl _CAP1
                                    103 	.globl _T2
                                    104 	.globl _AIN1
                                    105 	.globl _CAP2
                                    106 	.globl _T2EX
                                    107 	.globl _AIN2
                                    108 	.globl _AIN3
                                    109 	.globl _AIN4
                                    110 	.globl _UCC1
                                    111 	.globl _SCS
                                    112 	.globl _AIN5
                                    113 	.globl _UCC2
                                    114 	.globl _PWM0_
                                    115 	.globl _MOSI
                                    116 	.globl _AIN6
                                    117 	.globl _VBUS
                                    118 	.globl _RXD1_
                                    119 	.globl _MISO
                                    120 	.globl _AIN7
                                    121 	.globl _TXD1_
                                    122 	.globl _SCK
                                    123 	.globl _P1_0
                                    124 	.globl _P1_1
                                    125 	.globl _P1_2
                                    126 	.globl _P1_3
                                    127 	.globl _P1_4
                                    128 	.globl _P1_5
                                    129 	.globl _P1_6
                                    130 	.globl _P1_7
                                    131 	.globl _AIN8
                                    132 	.globl _AIN9
                                    133 	.globl _AIN10
                                    134 	.globl _RXD_
                                    135 	.globl _AIN11
                                    136 	.globl _TXD_
                                    137 	.globl _AIN12
                                    138 	.globl _RXD2
                                    139 	.globl _AIN13
                                    140 	.globl _TXD2
                                    141 	.globl _AIN14
                                    142 	.globl _RXD3
                                    143 	.globl _AIN15
                                    144 	.globl _TXD3
                                    145 	.globl _P0_0
                                    146 	.globl _P0_1
                                    147 	.globl _P0_2
                                    148 	.globl _P0_3
                                    149 	.globl _P0_4
                                    150 	.globl _P0_5
                                    151 	.globl _P0_6
                                    152 	.globl _P0_7
                                    153 	.globl _IE_SPI0
                                    154 	.globl _IE_INT3
                                    155 	.globl _IE_USB
                                    156 	.globl _IE_UART2
                                    157 	.globl _IE_ADC
                                    158 	.globl _IE_UART1
                                    159 	.globl _IE_UART3
                                    160 	.globl _IE_PWMX
                                    161 	.globl _IE_GPIO
                                    162 	.globl _IE_WDOG
                                    163 	.globl _PX0
                                    164 	.globl _PT0
                                    165 	.globl _PX1
                                    166 	.globl _PT1
                                    167 	.globl _PS
                                    168 	.globl _PT2
                                    169 	.globl _PL_FLAG
                                    170 	.globl _PH_FLAG
                                    171 	.globl _EX0
                                    172 	.globl _ET0
                                    173 	.globl _EX1
                                    174 	.globl _ET1
                                    175 	.globl _ES
                                    176 	.globl _ET2
                                    177 	.globl _E_DIS
                                    178 	.globl _EA
                                    179 	.globl _P
                                    180 	.globl _F1
                                    181 	.globl _OV
                                    182 	.globl _RS0
                                    183 	.globl _RS1
                                    184 	.globl _F0
                                    185 	.globl _AC
                                    186 	.globl _CY
                                    187 	.globl _UEP1_DMA_H
                                    188 	.globl _UEP1_DMA_L
                                    189 	.globl _UEP1_DMA
                                    190 	.globl _UEP0_DMA_H
                                    191 	.globl _UEP0_DMA_L
                                    192 	.globl _UEP0_DMA
                                    193 	.globl _UEP2_3_MOD
                                    194 	.globl _UEP4_1_MOD
                                    195 	.globl _UEP3_DMA_H
                                    196 	.globl _UEP3_DMA_L
                                    197 	.globl _UEP3_DMA
                                    198 	.globl _UEP2_DMA_H
                                    199 	.globl _UEP2_DMA_L
                                    200 	.globl _UEP2_DMA
                                    201 	.globl _USB_DEV_AD
                                    202 	.globl _USB_CTRL
                                    203 	.globl _USB_INT_EN
                                    204 	.globl _UEP4_T_LEN
                                    205 	.globl _UEP4_CTRL
                                    206 	.globl _UEP0_T_LEN
                                    207 	.globl _UEP0_CTRL
                                    208 	.globl _USB_RX_LEN
                                    209 	.globl _USB_MIS_ST
                                    210 	.globl _USB_INT_ST
                                    211 	.globl _USB_INT_FG
                                    212 	.globl _UEP3_T_LEN
                                    213 	.globl _UEP3_CTRL
                                    214 	.globl _UEP2_T_LEN
                                    215 	.globl _UEP2_CTRL
                                    216 	.globl _UEP1_T_LEN
                                    217 	.globl _UEP1_CTRL
                                    218 	.globl _UDEV_CTRL
                                    219 	.globl _USB_C_CTRL
                                    220 	.globl _ADC_PIN
                                    221 	.globl _ADC_CHAN
                                    222 	.globl _ADC_DAT_H
                                    223 	.globl _ADC_DAT_L
                                    224 	.globl _ADC_DAT
                                    225 	.globl _ADC_CFG
                                    226 	.globl _ADC_CTRL
                                    227 	.globl _TKEY_CTRL
                                    228 	.globl _SIF3
                                    229 	.globl _SBAUD3
                                    230 	.globl _SBUF3
                                    231 	.globl _SCON3
                                    232 	.globl _SIF2
                                    233 	.globl _SBAUD2
                                    234 	.globl _SBUF2
                                    235 	.globl _SCON2
                                    236 	.globl _SIF1
                                    237 	.globl _SBAUD1
                                    238 	.globl _SBUF1
                                    239 	.globl _SCON1
                                    240 	.globl _SPI0_SETUP
                                    241 	.globl _SPI0_CK_SE
                                    242 	.globl _SPI0_CTRL
                                    243 	.globl _SPI0_DATA
                                    244 	.globl _SPI0_STAT
                                    245 	.globl _PWM_DATA7
                                    246 	.globl _PWM_DATA6
                                    247 	.globl _PWM_DATA5
                                    248 	.globl _PWM_DATA4
                                    249 	.globl _PWM_DATA3
                                    250 	.globl _PWM_CTRL2
                                    251 	.globl _PWM_CK_SE
                                    252 	.globl _PWM_CTRL
                                    253 	.globl _PWM_DATA0
                                    254 	.globl _PWM_DATA1
                                    255 	.globl _PWM_DATA2
                                    256 	.globl _T2CAP1H
                                    257 	.globl _T2CAP1L
                                    258 	.globl _T2CAP1
                                    259 	.globl _TH2
                                    260 	.globl _TL2
                                    261 	.globl _T2COUNT
                                    262 	.globl _RCAP2H
                                    263 	.globl _RCAP2L
                                    264 	.globl _RCAP2
                                    265 	.globl _T2MOD
                                    266 	.globl _T2CON
                                    267 	.globl _T2CAP0H
                                    268 	.globl _T2CAP0L
                                    269 	.globl _T2CAP0
                                    270 	.globl _T2CON2
                                    271 	.globl _SBUF
                                    272 	.globl _SCON
                                    273 	.globl _TH1
                                    274 	.globl _TH0
                                    275 	.globl _TL1
                                    276 	.globl _TL0
                                    277 	.globl _TMOD
                                    278 	.globl _TCON
                                    279 	.globl _XBUS_AUX
                                    280 	.globl _PIN_FUNC
                                    281 	.globl _P5
                                    282 	.globl _P4_DIR_PU
                                    283 	.globl _P4_MOD_OC
                                    284 	.globl _P4
                                    285 	.globl _P3_DIR_PU
                                    286 	.globl _P3_MOD_OC
                                    287 	.globl _P3
                                    288 	.globl _P2_DIR_PU
                                    289 	.globl _P2_MOD_OC
                                    290 	.globl _P2
                                    291 	.globl _P1_DIR_PU
                                    292 	.globl _P1_MOD_OC
                                    293 	.globl _P1
                                    294 	.globl _P0_DIR_PU
                                    295 	.globl _P0_MOD_OC
                                    296 	.globl _P0
                                    297 	.globl _ROM_CTRL
                                    298 	.globl _ROM_DATA_HH
                                    299 	.globl _ROM_DATA_HL
                                    300 	.globl _ROM_DATA_HI
                                    301 	.globl _ROM_ADDR_H
                                    302 	.globl _ROM_ADDR_L
                                    303 	.globl _ROM_ADDR
                                    304 	.globl _GPIO_IE
                                    305 	.globl _INTX
                                    306 	.globl _IP_EX
                                    307 	.globl _IE_EX
                                    308 	.globl _IP
                                    309 	.globl _IE
                                    310 	.globl _WDOG_COUNT
                                    311 	.globl _RESET_KEEP
                                    312 	.globl _WAKE_CTRL
                                    313 	.globl _CLOCK_CFG
                                    314 	.globl _POWER_CFG
                                    315 	.globl _PCON
                                    316 	.globl _GLOBAL_CFG
                                    317 	.globl _SAFE_MOD
                                    318 	.globl _DPH
                                    319 	.globl _DPL
                                    320 	.globl _SP
                                    321 	.globl _A_INV
                                    322 	.globl _B
                                    323 	.globl _ACC
                                    324 	.globl _PSW
                                    325 	.globl _CfgFsys
                                    326 	.globl _mDelayuS
                                    327 	.globl _mDelaymS
                                    328 	.globl _mInitSTDIO
                                    329 	.globl _CH549SoftReset
                                    330 	.globl _CH549WDTModeSelect
                                    331 	.globl _CH549WDTFeed
                                    332 ;--------------------------------------------------------
                                    333 ; special function registers
                                    334 ;--------------------------------------------------------
                                    335 	.area RSEG    (ABS,DATA)
      000000                        336 	.org 0x0000
                           0000D0   337 _PSW	=	0x00d0
                           0000E0   338 _ACC	=	0x00e0
                           0000F0   339 _B	=	0x00f0
                           0000FD   340 _A_INV	=	0x00fd
                           000081   341 _SP	=	0x0081
                           000082   342 _DPL	=	0x0082
                           000083   343 _DPH	=	0x0083
                           0000A1   344 _SAFE_MOD	=	0x00a1
                           0000B1   345 _GLOBAL_CFG	=	0x00b1
                           000087   346 _PCON	=	0x0087
                           0000BA   347 _POWER_CFG	=	0x00ba
                           0000B9   348 _CLOCK_CFG	=	0x00b9
                           0000A9   349 _WAKE_CTRL	=	0x00a9
                           0000FE   350 _RESET_KEEP	=	0x00fe
                           0000FF   351 _WDOG_COUNT	=	0x00ff
                           0000A8   352 _IE	=	0x00a8
                           0000B8   353 _IP	=	0x00b8
                           0000E8   354 _IE_EX	=	0x00e8
                           0000E9   355 _IP_EX	=	0x00e9
                           0000B3   356 _INTX	=	0x00b3
                           0000B2   357 _GPIO_IE	=	0x00b2
                           008584   358 _ROM_ADDR	=	0x8584
                           000084   359 _ROM_ADDR_L	=	0x0084
                           000085   360 _ROM_ADDR_H	=	0x0085
                           008F8E   361 _ROM_DATA_HI	=	0x8f8e
                           00008E   362 _ROM_DATA_HL	=	0x008e
                           00008F   363 _ROM_DATA_HH	=	0x008f
                           000086   364 _ROM_CTRL	=	0x0086
                           000080   365 _P0	=	0x0080
                           0000C4   366 _P0_MOD_OC	=	0x00c4
                           0000C5   367 _P0_DIR_PU	=	0x00c5
                           000090   368 _P1	=	0x0090
                           000092   369 _P1_MOD_OC	=	0x0092
                           000093   370 _P1_DIR_PU	=	0x0093
                           0000A0   371 _P2	=	0x00a0
                           000094   372 _P2_MOD_OC	=	0x0094
                           000095   373 _P2_DIR_PU	=	0x0095
                           0000B0   374 _P3	=	0x00b0
                           000096   375 _P3_MOD_OC	=	0x0096
                           000097   376 _P3_DIR_PU	=	0x0097
                           0000C0   377 _P4	=	0x00c0
                           0000C2   378 _P4_MOD_OC	=	0x00c2
                           0000C3   379 _P4_DIR_PU	=	0x00c3
                           0000AB   380 _P5	=	0x00ab
                           0000AA   381 _PIN_FUNC	=	0x00aa
                           0000A2   382 _XBUS_AUX	=	0x00a2
                           000088   383 _TCON	=	0x0088
                           000089   384 _TMOD	=	0x0089
                           00008A   385 _TL0	=	0x008a
                           00008B   386 _TL1	=	0x008b
                           00008C   387 _TH0	=	0x008c
                           00008D   388 _TH1	=	0x008d
                           000098   389 _SCON	=	0x0098
                           000099   390 _SBUF	=	0x0099
                           0000C1   391 _T2CON2	=	0x00c1
                           00C7C6   392 _T2CAP0	=	0xc7c6
                           0000C6   393 _T2CAP0L	=	0x00c6
                           0000C7   394 _T2CAP0H	=	0x00c7
                           0000C8   395 _T2CON	=	0x00c8
                           0000C9   396 _T2MOD	=	0x00c9
                           00CBCA   397 _RCAP2	=	0xcbca
                           0000CA   398 _RCAP2L	=	0x00ca
                           0000CB   399 _RCAP2H	=	0x00cb
                           00CDCC   400 _T2COUNT	=	0xcdcc
                           0000CC   401 _TL2	=	0x00cc
                           0000CD   402 _TH2	=	0x00cd
                           00CFCE   403 _T2CAP1	=	0xcfce
                           0000CE   404 _T2CAP1L	=	0x00ce
                           0000CF   405 _T2CAP1H	=	0x00cf
                           00009A   406 _PWM_DATA2	=	0x009a
                           00009B   407 _PWM_DATA1	=	0x009b
                           00009C   408 _PWM_DATA0	=	0x009c
                           00009D   409 _PWM_CTRL	=	0x009d
                           00009E   410 _PWM_CK_SE	=	0x009e
                           00009F   411 _PWM_CTRL2	=	0x009f
                           0000A3   412 _PWM_DATA3	=	0x00a3
                           0000A4   413 _PWM_DATA4	=	0x00a4
                           0000A5   414 _PWM_DATA5	=	0x00a5
                           0000A6   415 _PWM_DATA6	=	0x00a6
                           0000A7   416 _PWM_DATA7	=	0x00a7
                           0000F8   417 _SPI0_STAT	=	0x00f8
                           0000F9   418 _SPI0_DATA	=	0x00f9
                           0000FA   419 _SPI0_CTRL	=	0x00fa
                           0000FB   420 _SPI0_CK_SE	=	0x00fb
                           0000FC   421 _SPI0_SETUP	=	0x00fc
                           0000BC   422 _SCON1	=	0x00bc
                           0000BD   423 _SBUF1	=	0x00bd
                           0000BE   424 _SBAUD1	=	0x00be
                           0000BF   425 _SIF1	=	0x00bf
                           0000B4   426 _SCON2	=	0x00b4
                           0000B5   427 _SBUF2	=	0x00b5
                           0000B6   428 _SBAUD2	=	0x00b6
                           0000B7   429 _SIF2	=	0x00b7
                           0000AC   430 _SCON3	=	0x00ac
                           0000AD   431 _SBUF3	=	0x00ad
                           0000AE   432 _SBAUD3	=	0x00ae
                           0000AF   433 _SIF3	=	0x00af
                           0000F1   434 _TKEY_CTRL	=	0x00f1
                           0000F2   435 _ADC_CTRL	=	0x00f2
                           0000F3   436 _ADC_CFG	=	0x00f3
                           00F5F4   437 _ADC_DAT	=	0xf5f4
                           0000F4   438 _ADC_DAT_L	=	0x00f4
                           0000F5   439 _ADC_DAT_H	=	0x00f5
                           0000F6   440 _ADC_CHAN	=	0x00f6
                           0000F7   441 _ADC_PIN	=	0x00f7
                           000091   442 _USB_C_CTRL	=	0x0091
                           0000D1   443 _UDEV_CTRL	=	0x00d1
                           0000D2   444 _UEP1_CTRL	=	0x00d2
                           0000D3   445 _UEP1_T_LEN	=	0x00d3
                           0000D4   446 _UEP2_CTRL	=	0x00d4
                           0000D5   447 _UEP2_T_LEN	=	0x00d5
                           0000D6   448 _UEP3_CTRL	=	0x00d6
                           0000D7   449 _UEP3_T_LEN	=	0x00d7
                           0000D8   450 _USB_INT_FG	=	0x00d8
                           0000D9   451 _USB_INT_ST	=	0x00d9
                           0000DA   452 _USB_MIS_ST	=	0x00da
                           0000DB   453 _USB_RX_LEN	=	0x00db
                           0000DC   454 _UEP0_CTRL	=	0x00dc
                           0000DD   455 _UEP0_T_LEN	=	0x00dd
                           0000DE   456 _UEP4_CTRL	=	0x00de
                           0000DF   457 _UEP4_T_LEN	=	0x00df
                           0000E1   458 _USB_INT_EN	=	0x00e1
                           0000E2   459 _USB_CTRL	=	0x00e2
                           0000E3   460 _USB_DEV_AD	=	0x00e3
                           00E5E4   461 _UEP2_DMA	=	0xe5e4
                           0000E4   462 _UEP2_DMA_L	=	0x00e4
                           0000E5   463 _UEP2_DMA_H	=	0x00e5
                           00E7E6   464 _UEP3_DMA	=	0xe7e6
                           0000E6   465 _UEP3_DMA_L	=	0x00e6
                           0000E7   466 _UEP3_DMA_H	=	0x00e7
                           0000EA   467 _UEP4_1_MOD	=	0x00ea
                           0000EB   468 _UEP2_3_MOD	=	0x00eb
                           00EDEC   469 _UEP0_DMA	=	0xedec
                           0000EC   470 _UEP0_DMA_L	=	0x00ec
                           0000ED   471 _UEP0_DMA_H	=	0x00ed
                           00EFEE   472 _UEP1_DMA	=	0xefee
                           0000EE   473 _UEP1_DMA_L	=	0x00ee
                           0000EF   474 _UEP1_DMA_H	=	0x00ef
                                    475 ;--------------------------------------------------------
                                    476 ; special function bits
                                    477 ;--------------------------------------------------------
                                    478 	.area RSEG    (ABS,DATA)
      000000                        479 	.org 0x0000
                           0000D7   480 _CY	=	0x00d7
                           0000D6   481 _AC	=	0x00d6
                           0000D5   482 _F0	=	0x00d5
                           0000D4   483 _RS1	=	0x00d4
                           0000D3   484 _RS0	=	0x00d3
                           0000D2   485 _OV	=	0x00d2
                           0000D1   486 _F1	=	0x00d1
                           0000D0   487 _P	=	0x00d0
                           0000AF   488 _EA	=	0x00af
                           0000AE   489 _E_DIS	=	0x00ae
                           0000AD   490 _ET2	=	0x00ad
                           0000AC   491 _ES	=	0x00ac
                           0000AB   492 _ET1	=	0x00ab
                           0000AA   493 _EX1	=	0x00aa
                           0000A9   494 _ET0	=	0x00a9
                           0000A8   495 _EX0	=	0x00a8
                           0000BF   496 _PH_FLAG	=	0x00bf
                           0000BE   497 _PL_FLAG	=	0x00be
                           0000BD   498 _PT2	=	0x00bd
                           0000BC   499 _PS	=	0x00bc
                           0000BB   500 _PT1	=	0x00bb
                           0000BA   501 _PX1	=	0x00ba
                           0000B9   502 _PT0	=	0x00b9
                           0000B8   503 _PX0	=	0x00b8
                           0000EF   504 _IE_WDOG	=	0x00ef
                           0000EE   505 _IE_GPIO	=	0x00ee
                           0000ED   506 _IE_PWMX	=	0x00ed
                           0000ED   507 _IE_UART3	=	0x00ed
                           0000EC   508 _IE_UART1	=	0x00ec
                           0000EB   509 _IE_ADC	=	0x00eb
                           0000EB   510 _IE_UART2	=	0x00eb
                           0000EA   511 _IE_USB	=	0x00ea
                           0000E9   512 _IE_INT3	=	0x00e9
                           0000E8   513 _IE_SPI0	=	0x00e8
                           000087   514 _P0_7	=	0x0087
                           000086   515 _P0_6	=	0x0086
                           000085   516 _P0_5	=	0x0085
                           000084   517 _P0_4	=	0x0084
                           000083   518 _P0_3	=	0x0083
                           000082   519 _P0_2	=	0x0082
                           000081   520 _P0_1	=	0x0081
                           000080   521 _P0_0	=	0x0080
                           000087   522 _TXD3	=	0x0087
                           000087   523 _AIN15	=	0x0087
                           000086   524 _RXD3	=	0x0086
                           000086   525 _AIN14	=	0x0086
                           000085   526 _TXD2	=	0x0085
                           000085   527 _AIN13	=	0x0085
                           000084   528 _RXD2	=	0x0084
                           000084   529 _AIN12	=	0x0084
                           000083   530 _TXD_	=	0x0083
                           000083   531 _AIN11	=	0x0083
                           000082   532 _RXD_	=	0x0082
                           000082   533 _AIN10	=	0x0082
                           000081   534 _AIN9	=	0x0081
                           000080   535 _AIN8	=	0x0080
                           000097   536 _P1_7	=	0x0097
                           000096   537 _P1_6	=	0x0096
                           000095   538 _P1_5	=	0x0095
                           000094   539 _P1_4	=	0x0094
                           000093   540 _P1_3	=	0x0093
                           000092   541 _P1_2	=	0x0092
                           000091   542 _P1_1	=	0x0091
                           000090   543 _P1_0	=	0x0090
                           000097   544 _SCK	=	0x0097
                           000097   545 _TXD1_	=	0x0097
                           000097   546 _AIN7	=	0x0097
                           000096   547 _MISO	=	0x0096
                           000096   548 _RXD1_	=	0x0096
                           000096   549 _VBUS	=	0x0096
                           000096   550 _AIN6	=	0x0096
                           000095   551 _MOSI	=	0x0095
                           000095   552 _PWM0_	=	0x0095
                           000095   553 _UCC2	=	0x0095
                           000095   554 _AIN5	=	0x0095
                           000094   555 _SCS	=	0x0094
                           000094   556 _UCC1	=	0x0094
                           000094   557 _AIN4	=	0x0094
                           000093   558 _AIN3	=	0x0093
                           000092   559 _AIN2	=	0x0092
                           000091   560 _T2EX	=	0x0091
                           000091   561 _CAP2	=	0x0091
                           000091   562 _AIN1	=	0x0091
                           000090   563 _T2	=	0x0090
                           000090   564 _CAP1	=	0x0090
                           000090   565 _AIN0	=	0x0090
                           0000A7   566 _P2_7	=	0x00a7
                           0000A6   567 _P2_6	=	0x00a6
                           0000A5   568 _P2_5	=	0x00a5
                           0000A4   569 _P2_4	=	0x00a4
                           0000A3   570 _P2_3	=	0x00a3
                           0000A2   571 _P2_2	=	0x00a2
                           0000A1   572 _P2_1	=	0x00a1
                           0000A0   573 _P2_0	=	0x00a0
                           0000A7   574 _PWM7	=	0x00a7
                           0000A7   575 _TXD1	=	0x00a7
                           0000A6   576 _PWM6	=	0x00a6
                           0000A6   577 _RXD1	=	0x00a6
                           0000A5   578 _PWM0	=	0x00a5
                           0000A5   579 _T2EX_	=	0x00a5
                           0000A5   580 _CAP2_	=	0x00a5
                           0000A4   581 _PWM1	=	0x00a4
                           0000A4   582 _T2_	=	0x00a4
                           0000A4   583 _CAP1_	=	0x00a4
                           0000A3   584 _PWM2	=	0x00a3
                           0000A2   585 _PWM3	=	0x00a2
                           0000A2   586 _INT0_	=	0x00a2
                           0000A1   587 _PWM4	=	0x00a1
                           0000A0   588 _PWM5	=	0x00a0
                           0000B7   589 _P3_7	=	0x00b7
                           0000B6   590 _P3_6	=	0x00b6
                           0000B5   591 _P3_5	=	0x00b5
                           0000B4   592 _P3_4	=	0x00b4
                           0000B3   593 _P3_3	=	0x00b3
                           0000B2   594 _P3_2	=	0x00b2
                           0000B1   595 _P3_1	=	0x00b1
                           0000B0   596 _P3_0	=	0x00b0
                           0000B7   597 _INT3	=	0x00b7
                           0000B6   598 _CAP0	=	0x00b6
                           0000B5   599 _T1	=	0x00b5
                           0000B4   600 _T0	=	0x00b4
                           0000B3   601 _INT1	=	0x00b3
                           0000B2   602 _INT0	=	0x00b2
                           0000B1   603 _TXD	=	0x00b1
                           0000B0   604 _RXD	=	0x00b0
                           0000C6   605 _P4_6	=	0x00c6
                           0000C5   606 _P4_5	=	0x00c5
                           0000C4   607 _P4_4	=	0x00c4
                           0000C3   608 _P4_3	=	0x00c3
                           0000C2   609 _P4_2	=	0x00c2
                           0000C1   610 _P4_1	=	0x00c1
                           0000C0   611 _P4_0	=	0x00c0
                           0000C7   612 _XO	=	0x00c7
                           0000C6   613 _XI	=	0x00c6
                           00008F   614 _TF1	=	0x008f
                           00008E   615 _TR1	=	0x008e
                           00008D   616 _TF0	=	0x008d
                           00008C   617 _TR0	=	0x008c
                           00008B   618 _IE1	=	0x008b
                           00008A   619 _IT1	=	0x008a
                           000089   620 _IE0	=	0x0089
                           000088   621 _IT0	=	0x0088
                           00009F   622 _SM0	=	0x009f
                           00009E   623 _SM1	=	0x009e
                           00009D   624 _SM2	=	0x009d
                           00009C   625 _REN	=	0x009c
                           00009B   626 _TB8	=	0x009b
                           00009A   627 _RB8	=	0x009a
                           000099   628 _TI	=	0x0099
                           000098   629 _RI	=	0x0098
                           0000CF   630 _TF2	=	0x00cf
                           0000CF   631 _CAP1F	=	0x00cf
                           0000CE   632 _EXF2	=	0x00ce
                           0000CD   633 _RCLK	=	0x00cd
                           0000CC   634 _TCLK	=	0x00cc
                           0000CB   635 _EXEN2	=	0x00cb
                           0000CA   636 _TR2	=	0x00ca
                           0000C9   637 _C_T2	=	0x00c9
                           0000C8   638 _CP_RL2	=	0x00c8
                           0000FF   639 _S0_FST_ACT	=	0x00ff
                           0000FE   640 _S0_IF_OV	=	0x00fe
                           0000FD   641 _S0_IF_FIRST	=	0x00fd
                           0000FC   642 _S0_IF_BYTE	=	0x00fc
                           0000FB   643 _S0_FREE	=	0x00fb
                           0000FA   644 _S0_T_FIFO	=	0x00fa
                           0000F8   645 _S0_R_FIFO	=	0x00f8
                           0000DF   646 _U_IS_NAK	=	0x00df
                           0000DE   647 _U_TOG_OK	=	0x00de
                           0000DD   648 _U_SIE_FREE	=	0x00dd
                           0000DC   649 _UIF_FIFO_OV	=	0x00dc
                           0000DB   650 _UIF_HST_SOF	=	0x00db
                           0000DA   651 _UIF_SUSPEND	=	0x00da
                           0000D9   652 _UIF_TRANSFER	=	0x00d9
                           0000D8   653 _UIF_DETECT	=	0x00d8
                           0000D8   654 _UIF_BUS_RST	=	0x00d8
                                    655 ;--------------------------------------------------------
                                    656 ; overlayable register banks
                                    657 ;--------------------------------------------------------
                                    658 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        659 	.ds 8
                                    660 ;--------------------------------------------------------
                                    661 ; internal ram data
                                    662 ;--------------------------------------------------------
                                    663 	.area DSEG    (DATA)
                                    664 ;--------------------------------------------------------
                                    665 ; overlayable items in internal ram 
                                    666 ;--------------------------------------------------------
                                    667 	.area	OSEG    (OVR,DATA)
                                    668 	.area	OSEG    (OVR,DATA)
                                    669 	.area	OSEG    (OVR,DATA)
                                    670 	.area	OSEG    (OVR,DATA)
                                    671 ;--------------------------------------------------------
                                    672 ; indirectly addressable internal ram data
                                    673 ;--------------------------------------------------------
                                    674 	.area ISEG    (DATA)
                                    675 ;--------------------------------------------------------
                                    676 ; absolute internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area IABS    (ABS,DATA)
                                    679 	.area IABS    (ABS,DATA)
                                    680 ;--------------------------------------------------------
                                    681 ; bit data
                                    682 ;--------------------------------------------------------
                                    683 	.area BSEG    (BIT)
                                    684 ;--------------------------------------------------------
                                    685 ; paged external ram data
                                    686 ;--------------------------------------------------------
                                    687 	.area PSEG    (PAG,XDATA)
                                    688 ;--------------------------------------------------------
                                    689 ; external ram data
                                    690 ;--------------------------------------------------------
                                    691 	.area XSEG    (XDATA)
                                    692 ;--------------------------------------------------------
                                    693 ; absolute external ram data
                                    694 ;--------------------------------------------------------
                                    695 	.area XABS    (ABS,XDATA)
                                    696 ;--------------------------------------------------------
                                    697 ; external initialized ram data
                                    698 ;--------------------------------------------------------
                                    699 	.area XISEG   (XDATA)
                                    700 	.area HOME    (CODE)
                                    701 	.area GSINIT0 (CODE)
                                    702 	.area GSINIT1 (CODE)
                                    703 	.area GSINIT2 (CODE)
                                    704 	.area GSINIT3 (CODE)
                                    705 	.area GSINIT4 (CODE)
                                    706 	.area GSINIT5 (CODE)
                                    707 	.area GSINIT  (CODE)
                                    708 	.area GSFINAL (CODE)
                                    709 	.area CSEG    (CODE)
                                    710 ;--------------------------------------------------------
                                    711 ; global & static initialisations
                                    712 ;--------------------------------------------------------
                                    713 	.area HOME    (CODE)
                                    714 	.area GSINIT  (CODE)
                                    715 	.area GSFINAL (CODE)
                                    716 	.area GSINIT  (CODE)
                                    717 ;--------------------------------------------------------
                                    718 ; Home
                                    719 ;--------------------------------------------------------
                                    720 	.area HOME    (CODE)
                                    721 	.area HOME    (CODE)
                                    722 ;--------------------------------------------------------
                                    723 ; code
                                    724 ;--------------------------------------------------------
                                    725 	.area CSEG    (CODE)
                                    726 ;------------------------------------------------------------
                                    727 ;Allocation info for local variables in function 'CfgFsys'
                                    728 ;------------------------------------------------------------
                                    729 ;	source/CH549_DEBUG.c:25: void CfgFsys( )  
                                    730 ;	-----------------------------------------
                                    731 ;	 function CfgFsys
                                    732 ;	-----------------------------------------
      000241                        733 _CfgFsys:
                           000007   734 	ar7 = 0x07
                           000006   735 	ar6 = 0x06
                           000005   736 	ar5 = 0x05
                           000004   737 	ar4 = 0x04
                           000003   738 	ar3 = 0x03
                           000002   739 	ar2 = 0x02
                           000001   740 	ar1 = 0x01
                           000000   741 	ar0 = 0x00
                                    742 ;	source/CH549_DEBUG.c:33: SAFE_MOD = 0x55;
      000241 75 A1 55         [24]  743 	mov	_SAFE_MOD,#0x55
                                    744 ;	source/CH549_DEBUG.c:34: SAFE_MOD = 0xAA;
      000244 75 A1 AA         [24]  745 	mov	_SAFE_MOD,#0xaa
                                    746 ;	source/CH549_DEBUG.c:35: CLOCK_CFG |= bOSC_EN_INT;                        //使能内部晶振 
      000247 43 B9 80         [24]  747 	orl	_CLOCK_CFG,#0x80
                                    748 ;	source/CH549_DEBUG.c:36: CLOCK_CFG &= ~bOSC_EN_XT;                        //关闭外部晶振	
      00024A 53 B9 BF         [24]  749 	anl	_CLOCK_CFG,#0xbf
                                    750 ;	source/CH549_DEBUG.c:38: SAFE_MOD = 0x55;
      00024D 75 A1 55         [24]  751 	mov	_SAFE_MOD,#0x55
                                    752 ;	source/CH549_DEBUG.c:39: SAFE_MOD = 0xAA;
      000250 75 A1 AA         [24]  753 	mov	_SAFE_MOD,#0xaa
                                    754 ;	source/CH549_DEBUG.c:47: CLOCK_CFG = CLOCK_CFG & ~ MASK_SYS_CK_SEL | 0x05;  // 24MHz	
      000253 74 F8            [12]  755 	mov	a,#0xf8
      000255 55 B9            [12]  756 	anl	a,_CLOCK_CFG
      000257 44 05            [12]  757 	orl	a,#0x05
      000259 F5 B9            [12]  758 	mov	_CLOCK_CFG,a
                                    759 ;	source/CH549_DEBUG.c:64: SAFE_MOD = 0x00;
      00025B 75 A1 00         [24]  760 	mov	_SAFE_MOD,#0x00
                                    761 ;	source/CH549_DEBUG.c:65: }
      00025E 22               [24]  762 	ret
                                    763 ;------------------------------------------------------------
                                    764 ;Allocation info for local variables in function 'mDelayuS'
                                    765 ;------------------------------------------------------------
                                    766 ;n                         Allocated to registers 
                                    767 ;------------------------------------------------------------
                                    768 ;	source/CH549_DEBUG.c:74: void mDelayuS( UINT16 n )  // 以uS为单位延时
                                    769 ;	-----------------------------------------
                                    770 ;	 function mDelayuS
                                    771 ;	-----------------------------------------
      00025F                        772 _mDelayuS:
      00025F AE 82            [24]  773 	mov	r6,dpl
      000261 AF 83            [24]  774 	mov	r7,dph
                                    775 ;	source/CH549_DEBUG.c:76: while ( n ) {  // total = 12~13 Fsys cycles, 1uS @Fsys=12MHz
      000263                        776 00101$:
      000263 EE               [12]  777 	mov	a,r6
      000264 4F               [12]  778 	orl	a,r7
      000265 60 15            [24]  779 	jz	00104$
                                    780 ;	source/CH549_DEBUG.c:77: ++ SAFE_MOD;  // 2 Fsys cycles, for higher Fsys, add operation here
      000267 05 A1            [12]  781 	inc	_SAFE_MOD
                                    782 ;	source/CH549_DEBUG.c:80: ++ SAFE_MOD;
      000269 05 A1            [12]  783 	inc	_SAFE_MOD
                                    784 ;	source/CH549_DEBUG.c:83: ++ SAFE_MOD;
      00026B 05 A1            [12]  785 	inc	_SAFE_MOD
                                    786 ;	source/CH549_DEBUG.c:86: ++ SAFE_MOD;
      00026D 05 A1            [12]  787 	inc	_SAFE_MOD
                                    788 ;	source/CH549_DEBUG.c:89: ++ SAFE_MOD;
      00026F 05 A1            [12]  789 	inc	_SAFE_MOD
                                    790 ;	source/CH549_DEBUG.c:92: ++ SAFE_MOD;
      000271 05 A1            [12]  791 	inc	_SAFE_MOD
                                    792 ;	source/CH549_DEBUG.c:95: ++ SAFE_MOD;
      000273 05 A1            [12]  793 	inc	_SAFE_MOD
                                    794 ;	source/CH549_DEBUG.c:146: -- n;
      000275 1E               [12]  795 	dec	r6
      000276 BE FF 01         [24]  796 	cjne	r6,#0xff,00116$
      000279 1F               [12]  797 	dec	r7
      00027A                        798 00116$:
      00027A 80 E7            [24]  799 	sjmp	00101$
      00027C                        800 00104$:
                                    801 ;	source/CH549_DEBUG.c:148: }
      00027C 22               [24]  802 	ret
                                    803 ;------------------------------------------------------------
                                    804 ;Allocation info for local variables in function 'mDelaymS'
                                    805 ;------------------------------------------------------------
                                    806 ;n                         Allocated to registers 
                                    807 ;------------------------------------------------------------
                                    808 ;	source/CH549_DEBUG.c:157: void mDelaymS( UINT16 n )                                                  // 以mS为单位延时
                                    809 ;	-----------------------------------------
                                    810 ;	 function mDelaymS
                                    811 ;	-----------------------------------------
      00027D                        812 _mDelaymS:
      00027D AE 82            [24]  813 	mov	r6,dpl
      00027F AF 83            [24]  814 	mov	r7,dph
                                    815 ;	source/CH549_DEBUG.c:159: while ( n ) 
      000281                        816 00101$:
      000281 EE               [12]  817 	mov	a,r6
      000282 4F               [12]  818 	orl	a,r7
      000283 60 15            [24]  819 	jz	00104$
                                    820 ;	source/CH549_DEBUG.c:161: mDelayuS( 1000 );
      000285 90 03 E8         [24]  821 	mov	dptr,#0x03e8
      000288 C0 07            [24]  822 	push	ar7
      00028A C0 06            [24]  823 	push	ar6
      00028C 12 02 5F         [24]  824 	lcall	_mDelayuS
      00028F D0 06            [24]  825 	pop	ar6
      000291 D0 07            [24]  826 	pop	ar7
                                    827 ;	source/CH549_DEBUG.c:162: -- n;
      000293 1E               [12]  828 	dec	r6
      000294 BE FF 01         [24]  829 	cjne	r6,#0xff,00116$
      000297 1F               [12]  830 	dec	r7
      000298                        831 00116$:
      000298 80 E7            [24]  832 	sjmp	00101$
      00029A                        833 00104$:
                                    834 ;	source/CH549_DEBUG.c:164: }                                         
      00029A 22               [24]  835 	ret
                                    836 ;------------------------------------------------------------
                                    837 ;Allocation info for local variables in function 'CH549UART0Alter'
                                    838 ;------------------------------------------------------------
                                    839 ;	source/CH549_DEBUG.c:173: void CH549UART0Alter()
                                    840 ;	-----------------------------------------
                                    841 ;	 function CH549UART0Alter
                                    842 ;	-----------------------------------------
      00029B                        843 _CH549UART0Alter:
                                    844 ;	source/CH549_DEBUG.c:175: P0_MOD_OC |= (3<<2);                                                   //准双向模式
      00029B 43 C4 0C         [24]  845 	orl	_P0_MOD_OC,#0x0c
                                    846 ;	source/CH549_DEBUG.c:176: P0_DIR_PU |= (3<<2);
      00029E 43 C5 0C         [24]  847 	orl	_P0_DIR_PU,#0x0c
                                    848 ;	source/CH549_DEBUG.c:177: PIN_FUNC |= bUART0_PIN_X;                                              //开启引脚复用功能
      0002A1 43 AA 10         [24]  849 	orl	_PIN_FUNC,#0x10
                                    850 ;	source/CH549_DEBUG.c:178: }
      0002A4 22               [24]  851 	ret
                                    852 ;------------------------------------------------------------
                                    853 ;Allocation info for local variables in function 'mInitSTDIO'
                                    854 ;------------------------------------------------------------
                                    855 ;x                         Allocated to registers 
                                    856 ;x2                        Allocated to registers 
                                    857 ;------------------------------------------------------------
                                    858 ;	source/CH549_DEBUG.c:188: void mInitSTDIO( )
                                    859 ;	-----------------------------------------
                                    860 ;	 function mInitSTDIO
                                    861 ;	-----------------------------------------
      0002A5                        862 _mInitSTDIO:
                                    863 ;	source/CH549_DEBUG.c:193: SM0 = 0;
                                    864 ;	assignBit
      0002A5 C2 9F            [12]  865 	clr	_SM0
                                    866 ;	source/CH549_DEBUG.c:194: SM1 = 1;
                                    867 ;	assignBit
      0002A7 D2 9E            [12]  868 	setb	_SM1
                                    869 ;	source/CH549_DEBUG.c:195: SM2 = 0;                                                                   //串口0使用模式1
                                    870 ;	assignBit
      0002A9 C2 9D            [12]  871 	clr	_SM2
                                    872 ;	source/CH549_DEBUG.c:197: RCLK = 0;                                                                  //UART0接收时钟
                                    873 ;	assignBit
      0002AB C2 CD            [12]  874 	clr	_RCLK
                                    875 ;	source/CH549_DEBUG.c:198: TCLK = 0;                                                                  //UART0发送时钟
                                    876 ;	assignBit
      0002AD C2 CC            [12]  877 	clr	_TCLK
                                    878 ;	source/CH549_DEBUG.c:199: PCON |= SMOD;
      0002AF 43 87 80         [24]  879 	orl	_PCON,#0x80
                                    880 ;	source/CH549_DEBUG.c:205: TMOD = TMOD & ~ bT1_GATE & ~ bT1_CT & ~ MASK_T1_MOD | bT1_M1;              //0X20，Timer1作为8位自动重载定时器
      0002B2 74 0F            [12]  881 	mov	a,#0x0f
      0002B4 55 89            [12]  882 	anl	a,_TMOD
      0002B6 44 20            [12]  883 	orl	a,#0x20
      0002B8 F5 89            [12]  884 	mov	_TMOD,a
                                    885 ;	source/CH549_DEBUG.c:206: T2MOD = T2MOD | bTMR_CLK | bT1_CLK;                                        //Timer1时钟选择
      0002BA 43 C9 A0         [24]  886 	orl	_T2MOD,#0xa0
                                    887 ;	source/CH549_DEBUG.c:207: TH1 = 0-x;                                                                 //12MHz晶振,buad/12为实际需设置波特率
      0002BD 75 8D F3         [24]  888 	mov	_TH1,#0xf3
                                    889 ;	source/CH549_DEBUG.c:208: TR1 = 1;                                                                   //启动定时器1
                                    890 ;	assignBit
      0002C0 D2 8E            [12]  891 	setb	_TR1
                                    892 ;	source/CH549_DEBUG.c:209: TI = 1;
                                    893 ;	assignBit
      0002C2 D2 99            [12]  894 	setb	_TI
                                    895 ;	source/CH549_DEBUG.c:210: REN = 1;                                                                   //串口0接收使能
                                    896 ;	assignBit
      0002C4 D2 9C            [12]  897 	setb	_REN
                                    898 ;	source/CH549_DEBUG.c:211: }
      0002C6 22               [24]  899 	ret
                                    900 ;------------------------------------------------------------
                                    901 ;Allocation info for local variables in function 'CH549SoftReset'
                                    902 ;------------------------------------------------------------
                                    903 ;	source/CH549_DEBUG.c:220: void CH549SoftReset( )
                                    904 ;	-----------------------------------------
                                    905 ;	 function CH549SoftReset
                                    906 ;	-----------------------------------------
      0002C7                        907 _CH549SoftReset:
                                    908 ;	source/CH549_DEBUG.c:222: SAFE_MOD = 0x55;
      0002C7 75 A1 55         [24]  909 	mov	_SAFE_MOD,#0x55
                                    910 ;	source/CH549_DEBUG.c:223: SAFE_MOD = 0xAA;
      0002CA 75 A1 AA         [24]  911 	mov	_SAFE_MOD,#0xaa
                                    912 ;	source/CH549_DEBUG.c:224: GLOBAL_CFG |= bSW_RESET;
      0002CD 43 B1 10         [24]  913 	orl	_GLOBAL_CFG,#0x10
                                    914 ;	source/CH549_DEBUG.c:225: }
      0002D0 22               [24]  915 	ret
                                    916 ;------------------------------------------------------------
                                    917 ;Allocation info for local variables in function 'CH549WDTModeSelect'
                                    918 ;------------------------------------------------------------
                                    919 ;mode                      Allocated to registers r7 
                                    920 ;------------------------------------------------------------
                                    921 ;	source/CH549_DEBUG.c:236: void CH549WDTModeSelect(UINT8 mode)
                                    922 ;	-----------------------------------------
                                    923 ;	 function CH549WDTModeSelect
                                    924 ;	-----------------------------------------
      0002D1                        925 _CH549WDTModeSelect:
      0002D1 AF 82            [24]  926 	mov	r7,dpl
                                    927 ;	source/CH549_DEBUG.c:238: SAFE_MOD = 0x55;
      0002D3 75 A1 55         [24]  928 	mov	_SAFE_MOD,#0x55
                                    929 ;	source/CH549_DEBUG.c:239: SAFE_MOD = 0xaa;                                                             //进入安全模式
      0002D6 75 A1 AA         [24]  930 	mov	_SAFE_MOD,#0xaa
                                    931 ;	source/CH549_DEBUG.c:240: if(mode){
      0002D9 EF               [12]  932 	mov	a,r7
      0002DA 60 05            [24]  933 	jz	00102$
                                    934 ;	source/CH549_DEBUG.c:241: GLOBAL_CFG |= bWDOG_EN;                                                    //启动看门狗复位
      0002DC 43 B1 01         [24]  935 	orl	_GLOBAL_CFG,#0x01
      0002DF 80 03            [24]  936 	sjmp	00103$
      0002E1                        937 00102$:
                                    938 ;	source/CH549_DEBUG.c:243: else GLOBAL_CFG &= ~bWDOG_EN;	                                              //启动看门狗仅仅作为定时器
      0002E1 53 B1 FE         [24]  939 	anl	_GLOBAL_CFG,#0xfe
      0002E4                        940 00103$:
                                    941 ;	source/CH549_DEBUG.c:244: SAFE_MOD = 0x00;                                                             //退出安全模式
      0002E4 75 A1 00         [24]  942 	mov	_SAFE_MOD,#0x00
                                    943 ;	source/CH549_DEBUG.c:245: WDOG_COUNT = 0;                                                              //看门狗赋初值
      0002E7 75 FF 00         [24]  944 	mov	_WDOG_COUNT,#0x00
                                    945 ;	source/CH549_DEBUG.c:246: }
      0002EA 22               [24]  946 	ret
                                    947 ;------------------------------------------------------------
                                    948 ;Allocation info for local variables in function 'CH549WDTFeed'
                                    949 ;------------------------------------------------------------
                                    950 ;tim                       Allocated to registers 
                                    951 ;------------------------------------------------------------
                                    952 ;	source/CH549_DEBUG.c:257: void CH549WDTFeed(UINT8 tim)
                                    953 ;	-----------------------------------------
                                    954 ;	 function CH549WDTFeed
                                    955 ;	-----------------------------------------
      0002EB                        956 _CH549WDTFeed:
      0002EB 85 82 FF         [24]  957 	mov	_WDOG_COUNT,dpl
                                    958 ;	source/CH549_DEBUG.c:259: WDOG_COUNT = tim;                                                             //看门狗计数器赋值	
                                    959 ;	source/CH549_DEBUG.c:260: }
      0002EE 22               [24]  960 	ret
                                    961 	.area CSEG    (CODE)
                                    962 	.area CONST   (CODE)
                                    963 	.area XINIT   (CODE)
                                    964 	.area CABS    (ABS,CODE)
