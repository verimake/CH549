                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_UART
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _UIF_BUS_RST
                                     12 	.globl _UIF_DETECT
                                     13 	.globl _UIF_TRANSFER
                                     14 	.globl _UIF_SUSPEND
                                     15 	.globl _UIF_HST_SOF
                                     16 	.globl _UIF_FIFO_OV
                                     17 	.globl _U_SIE_FREE
                                     18 	.globl _U_TOG_OK
                                     19 	.globl _U_IS_NAK
                                     20 	.globl _S0_R_FIFO
                                     21 	.globl _S0_T_FIFO
                                     22 	.globl _S0_FREE
                                     23 	.globl _S0_IF_BYTE
                                     24 	.globl _S0_IF_FIRST
                                     25 	.globl _S0_IF_OV
                                     26 	.globl _S0_FST_ACT
                                     27 	.globl _CP_RL2
                                     28 	.globl _C_T2
                                     29 	.globl _TR2
                                     30 	.globl _EXEN2
                                     31 	.globl _TCLK
                                     32 	.globl _RCLK
                                     33 	.globl _EXF2
                                     34 	.globl _CAP1F
                                     35 	.globl _TF2
                                     36 	.globl _RI
                                     37 	.globl _TI
                                     38 	.globl _RB8
                                     39 	.globl _TB8
                                     40 	.globl _REN
                                     41 	.globl _SM2
                                     42 	.globl _SM1
                                     43 	.globl _SM0
                                     44 	.globl _IT0
                                     45 	.globl _IE0
                                     46 	.globl _IT1
                                     47 	.globl _IE1
                                     48 	.globl _TR0
                                     49 	.globl _TF0
                                     50 	.globl _TR1
                                     51 	.globl _TF1
                                     52 	.globl _XI
                                     53 	.globl _XO
                                     54 	.globl _P4_0
                                     55 	.globl _P4_1
                                     56 	.globl _P4_2
                                     57 	.globl _P4_3
                                     58 	.globl _P4_4
                                     59 	.globl _P4_5
                                     60 	.globl _P4_6
                                     61 	.globl _RXD
                                     62 	.globl _TXD
                                     63 	.globl _INT0
                                     64 	.globl _INT1
                                     65 	.globl _T0
                                     66 	.globl _T1
                                     67 	.globl _CAP0
                                     68 	.globl _INT3
                                     69 	.globl _P3_0
                                     70 	.globl _P3_1
                                     71 	.globl _P3_2
                                     72 	.globl _P3_3
                                     73 	.globl _P3_4
                                     74 	.globl _P3_5
                                     75 	.globl _P3_6
                                     76 	.globl _P3_7
                                     77 	.globl _PWM5
                                     78 	.globl _PWM4
                                     79 	.globl _INT0_
                                     80 	.globl _PWM3
                                     81 	.globl _PWM2
                                     82 	.globl _CAP1_
                                     83 	.globl _T2_
                                     84 	.globl _PWM1
                                     85 	.globl _CAP2_
                                     86 	.globl _T2EX_
                                     87 	.globl _PWM0
                                     88 	.globl _RXD1
                                     89 	.globl _PWM6
                                     90 	.globl _TXD1
                                     91 	.globl _PWM7
                                     92 	.globl _P2_0
                                     93 	.globl _P2_1
                                     94 	.globl _P2_2
                                     95 	.globl _P2_3
                                     96 	.globl _P2_4
                                     97 	.globl _P2_5
                                     98 	.globl _P2_6
                                     99 	.globl _P2_7
                                    100 	.globl _AIN0
                                    101 	.globl _CAP1
                                    102 	.globl _T2
                                    103 	.globl _AIN1
                                    104 	.globl _CAP2
                                    105 	.globl _T2EX
                                    106 	.globl _AIN2
                                    107 	.globl _AIN3
                                    108 	.globl _AIN4
                                    109 	.globl _UCC1
                                    110 	.globl _SCS
                                    111 	.globl _AIN5
                                    112 	.globl _UCC2
                                    113 	.globl _PWM0_
                                    114 	.globl _MOSI
                                    115 	.globl _AIN6
                                    116 	.globl _VBUS
                                    117 	.globl _RXD1_
                                    118 	.globl _MISO
                                    119 	.globl _AIN7
                                    120 	.globl _TXD1_
                                    121 	.globl _SCK
                                    122 	.globl _P1_0
                                    123 	.globl _P1_1
                                    124 	.globl _P1_2
                                    125 	.globl _P1_3
                                    126 	.globl _P1_4
                                    127 	.globl _P1_5
                                    128 	.globl _P1_6
                                    129 	.globl _P1_7
                                    130 	.globl _AIN8
                                    131 	.globl _AIN9
                                    132 	.globl _AIN10
                                    133 	.globl _RXD_
                                    134 	.globl _AIN11
                                    135 	.globl _TXD_
                                    136 	.globl _AIN12
                                    137 	.globl _RXD2
                                    138 	.globl _AIN13
                                    139 	.globl _TXD2
                                    140 	.globl _AIN14
                                    141 	.globl _RXD3
                                    142 	.globl _AIN15
                                    143 	.globl _TXD3
                                    144 	.globl _P0_0
                                    145 	.globl _P0_1
                                    146 	.globl _P0_2
                                    147 	.globl _P0_3
                                    148 	.globl _P0_4
                                    149 	.globl _P0_5
                                    150 	.globl _P0_6
                                    151 	.globl _P0_7
                                    152 	.globl _IE_SPI0
                                    153 	.globl _IE_INT3
                                    154 	.globl _IE_USB
                                    155 	.globl _IE_UART2
                                    156 	.globl _IE_ADC
                                    157 	.globl _IE_UART1
                                    158 	.globl _IE_UART3
                                    159 	.globl _IE_PWMX
                                    160 	.globl _IE_GPIO
                                    161 	.globl _IE_WDOG
                                    162 	.globl _PX0
                                    163 	.globl _PT0
                                    164 	.globl _PX1
                                    165 	.globl _PT1
                                    166 	.globl _PS
                                    167 	.globl _PT2
                                    168 	.globl _PL_FLAG
                                    169 	.globl _PH_FLAG
                                    170 	.globl _EX0
                                    171 	.globl _ET0
                                    172 	.globl _EX1
                                    173 	.globl _ET1
                                    174 	.globl _ES
                                    175 	.globl _ET2
                                    176 	.globl _E_DIS
                                    177 	.globl _EA
                                    178 	.globl _P
                                    179 	.globl _F1
                                    180 	.globl _OV
                                    181 	.globl _RS0
                                    182 	.globl _RS1
                                    183 	.globl _F0
                                    184 	.globl _AC
                                    185 	.globl _CY
                                    186 	.globl _UEP1_DMA_H
                                    187 	.globl _UEP1_DMA_L
                                    188 	.globl _UEP1_DMA
                                    189 	.globl _UEP0_DMA_H
                                    190 	.globl _UEP0_DMA_L
                                    191 	.globl _UEP0_DMA
                                    192 	.globl _UEP2_3_MOD
                                    193 	.globl _UEP4_1_MOD
                                    194 	.globl _UEP3_DMA_H
                                    195 	.globl _UEP3_DMA_L
                                    196 	.globl _UEP3_DMA
                                    197 	.globl _UEP2_DMA_H
                                    198 	.globl _UEP2_DMA_L
                                    199 	.globl _UEP2_DMA
                                    200 	.globl _USB_DEV_AD
                                    201 	.globl _USB_CTRL
                                    202 	.globl _USB_INT_EN
                                    203 	.globl _UEP4_T_LEN
                                    204 	.globl _UEP4_CTRL
                                    205 	.globl _UEP0_T_LEN
                                    206 	.globl _UEP0_CTRL
                                    207 	.globl _USB_RX_LEN
                                    208 	.globl _USB_MIS_ST
                                    209 	.globl _USB_INT_ST
                                    210 	.globl _USB_INT_FG
                                    211 	.globl _UEP3_T_LEN
                                    212 	.globl _UEP3_CTRL
                                    213 	.globl _UEP2_T_LEN
                                    214 	.globl _UEP2_CTRL
                                    215 	.globl _UEP1_T_LEN
                                    216 	.globl _UEP1_CTRL
                                    217 	.globl _UDEV_CTRL
                                    218 	.globl _USB_C_CTRL
                                    219 	.globl _ADC_PIN
                                    220 	.globl _ADC_CHAN
                                    221 	.globl _ADC_DAT_H
                                    222 	.globl _ADC_DAT_L
                                    223 	.globl _ADC_DAT
                                    224 	.globl _ADC_CFG
                                    225 	.globl _ADC_CTRL
                                    226 	.globl _TKEY_CTRL
                                    227 	.globl _SIF3
                                    228 	.globl _SBAUD3
                                    229 	.globl _SBUF3
                                    230 	.globl _SCON3
                                    231 	.globl _SIF2
                                    232 	.globl _SBAUD2
                                    233 	.globl _SBUF2
                                    234 	.globl _SCON2
                                    235 	.globl _SIF1
                                    236 	.globl _SBAUD1
                                    237 	.globl _SBUF1
                                    238 	.globl _SCON1
                                    239 	.globl _SPI0_SETUP
                                    240 	.globl _SPI0_CK_SE
                                    241 	.globl _SPI0_CTRL
                                    242 	.globl _SPI0_DATA
                                    243 	.globl _SPI0_STAT
                                    244 	.globl _PWM_DATA7
                                    245 	.globl _PWM_DATA6
                                    246 	.globl _PWM_DATA5
                                    247 	.globl _PWM_DATA4
                                    248 	.globl _PWM_DATA3
                                    249 	.globl _PWM_CTRL2
                                    250 	.globl _PWM_CK_SE
                                    251 	.globl _PWM_CTRL
                                    252 	.globl _PWM_DATA0
                                    253 	.globl _PWM_DATA1
                                    254 	.globl _PWM_DATA2
                                    255 	.globl _T2CAP1H
                                    256 	.globl _T2CAP1L
                                    257 	.globl _T2CAP1
                                    258 	.globl _TH2
                                    259 	.globl _TL2
                                    260 	.globl _T2COUNT
                                    261 	.globl _RCAP2H
                                    262 	.globl _RCAP2L
                                    263 	.globl _RCAP2
                                    264 	.globl _T2MOD
                                    265 	.globl _T2CON
                                    266 	.globl _T2CAP0H
                                    267 	.globl _T2CAP0L
                                    268 	.globl _T2CAP0
                                    269 	.globl _T2CON2
                                    270 	.globl _SBUF
                                    271 	.globl _SCON
                                    272 	.globl _TH1
                                    273 	.globl _TH0
                                    274 	.globl _TL1
                                    275 	.globl _TL0
                                    276 	.globl _TMOD
                                    277 	.globl _TCON
                                    278 	.globl _XBUS_AUX
                                    279 	.globl _PIN_FUNC
                                    280 	.globl _P5
                                    281 	.globl _P4_DIR_PU
                                    282 	.globl _P4_MOD_OC
                                    283 	.globl _P4
                                    284 	.globl _P3_DIR_PU
                                    285 	.globl _P3_MOD_OC
                                    286 	.globl _P3
                                    287 	.globl _P2_DIR_PU
                                    288 	.globl _P2_MOD_OC
                                    289 	.globl _P2
                                    290 	.globl _P1_DIR_PU
                                    291 	.globl _P1_MOD_OC
                                    292 	.globl _P1
                                    293 	.globl _P0_DIR_PU
                                    294 	.globl _P0_MOD_OC
                                    295 	.globl _P0
                                    296 	.globl _ROM_CTRL
                                    297 	.globl _ROM_DATA_HH
                                    298 	.globl _ROM_DATA_HL
                                    299 	.globl _ROM_DATA_HI
                                    300 	.globl _ROM_ADDR_H
                                    301 	.globl _ROM_ADDR_L
                                    302 	.globl _ROM_ADDR
                                    303 	.globl _GPIO_IE
                                    304 	.globl _INTX
                                    305 	.globl _IP_EX
                                    306 	.globl _IE_EX
                                    307 	.globl _IP
                                    308 	.globl _IE
                                    309 	.globl _WDOG_COUNT
                                    310 	.globl _RESET_KEEP
                                    311 	.globl _WAKE_CTRL
                                    312 	.globl _CLOCK_CFG
                                    313 	.globl _POWER_CFG
                                    314 	.globl _PCON
                                    315 	.globl _GLOBAL_CFG
                                    316 	.globl _SAFE_MOD
                                    317 	.globl _DPH
                                    318 	.globl _DPL
                                    319 	.globl _SP
                                    320 	.globl _A_INV
                                    321 	.globl _B
                                    322 	.globl _ACC
                                    323 	.globl _PSW
                                    324 	.globl _CH549UART1Init
                                    325 	.globl _CH549UART1Alter
                                    326 	.globl _CH549UART1RcvByte
                                    327 	.globl _CH549UART1SendByte
                                    328 	.globl _UART1Interrupt
                                    329 	.globl _CH549UART2Init
                                    330 	.globl _CH549UART2RcvByte
                                    331 	.globl _CH549UART2SendByte
                                    332 	.globl _UART2Interrupt
                                    333 	.globl _CH549UART3Init
                                    334 	.globl _CH549UART3RcvByte
                                    335 	.globl _CH549UART3SendByte
                                    336 	.globl _UART3Interrupt
                                    337 ;--------------------------------------------------------
                                    338 ; special function registers
                                    339 ;--------------------------------------------------------
                                    340 	.area RSEG    (ABS,DATA)
      000000                        341 	.org 0x0000
                           0000D0   342 _PSW	=	0x00d0
                           0000E0   343 _ACC	=	0x00e0
                           0000F0   344 _B	=	0x00f0
                           0000FD   345 _A_INV	=	0x00fd
                           000081   346 _SP	=	0x0081
                           000082   347 _DPL	=	0x0082
                           000083   348 _DPH	=	0x0083
                           0000A1   349 _SAFE_MOD	=	0x00a1
                           0000B1   350 _GLOBAL_CFG	=	0x00b1
                           000087   351 _PCON	=	0x0087
                           0000BA   352 _POWER_CFG	=	0x00ba
                           0000B9   353 _CLOCK_CFG	=	0x00b9
                           0000A9   354 _WAKE_CTRL	=	0x00a9
                           0000FE   355 _RESET_KEEP	=	0x00fe
                           0000FF   356 _WDOG_COUNT	=	0x00ff
                           0000A8   357 _IE	=	0x00a8
                           0000B8   358 _IP	=	0x00b8
                           0000E8   359 _IE_EX	=	0x00e8
                           0000E9   360 _IP_EX	=	0x00e9
                           0000B3   361 _INTX	=	0x00b3
                           0000B2   362 _GPIO_IE	=	0x00b2
                           008584   363 _ROM_ADDR	=	0x8584
                           000084   364 _ROM_ADDR_L	=	0x0084
                           000085   365 _ROM_ADDR_H	=	0x0085
                           008F8E   366 _ROM_DATA_HI	=	0x8f8e
                           00008E   367 _ROM_DATA_HL	=	0x008e
                           00008F   368 _ROM_DATA_HH	=	0x008f
                           000086   369 _ROM_CTRL	=	0x0086
                           000080   370 _P0	=	0x0080
                           0000C4   371 _P0_MOD_OC	=	0x00c4
                           0000C5   372 _P0_DIR_PU	=	0x00c5
                           000090   373 _P1	=	0x0090
                           000092   374 _P1_MOD_OC	=	0x0092
                           000093   375 _P1_DIR_PU	=	0x0093
                           0000A0   376 _P2	=	0x00a0
                           000094   377 _P2_MOD_OC	=	0x0094
                           000095   378 _P2_DIR_PU	=	0x0095
                           0000B0   379 _P3	=	0x00b0
                           000096   380 _P3_MOD_OC	=	0x0096
                           000097   381 _P3_DIR_PU	=	0x0097
                           0000C0   382 _P4	=	0x00c0
                           0000C2   383 _P4_MOD_OC	=	0x00c2
                           0000C3   384 _P4_DIR_PU	=	0x00c3
                           0000AB   385 _P5	=	0x00ab
                           0000AA   386 _PIN_FUNC	=	0x00aa
                           0000A2   387 _XBUS_AUX	=	0x00a2
                           000088   388 _TCON	=	0x0088
                           000089   389 _TMOD	=	0x0089
                           00008A   390 _TL0	=	0x008a
                           00008B   391 _TL1	=	0x008b
                           00008C   392 _TH0	=	0x008c
                           00008D   393 _TH1	=	0x008d
                           000098   394 _SCON	=	0x0098
                           000099   395 _SBUF	=	0x0099
                           0000C1   396 _T2CON2	=	0x00c1
                           00C7C6   397 _T2CAP0	=	0xc7c6
                           0000C6   398 _T2CAP0L	=	0x00c6
                           0000C7   399 _T2CAP0H	=	0x00c7
                           0000C8   400 _T2CON	=	0x00c8
                           0000C9   401 _T2MOD	=	0x00c9
                           00CBCA   402 _RCAP2	=	0xcbca
                           0000CA   403 _RCAP2L	=	0x00ca
                           0000CB   404 _RCAP2H	=	0x00cb
                           00CDCC   405 _T2COUNT	=	0xcdcc
                           0000CC   406 _TL2	=	0x00cc
                           0000CD   407 _TH2	=	0x00cd
                           00CFCE   408 _T2CAP1	=	0xcfce
                           0000CE   409 _T2CAP1L	=	0x00ce
                           0000CF   410 _T2CAP1H	=	0x00cf
                           00009A   411 _PWM_DATA2	=	0x009a
                           00009B   412 _PWM_DATA1	=	0x009b
                           00009C   413 _PWM_DATA0	=	0x009c
                           00009D   414 _PWM_CTRL	=	0x009d
                           00009E   415 _PWM_CK_SE	=	0x009e
                           00009F   416 _PWM_CTRL2	=	0x009f
                           0000A3   417 _PWM_DATA3	=	0x00a3
                           0000A4   418 _PWM_DATA4	=	0x00a4
                           0000A5   419 _PWM_DATA5	=	0x00a5
                           0000A6   420 _PWM_DATA6	=	0x00a6
                           0000A7   421 _PWM_DATA7	=	0x00a7
                           0000F8   422 _SPI0_STAT	=	0x00f8
                           0000F9   423 _SPI0_DATA	=	0x00f9
                           0000FA   424 _SPI0_CTRL	=	0x00fa
                           0000FB   425 _SPI0_CK_SE	=	0x00fb
                           0000FC   426 _SPI0_SETUP	=	0x00fc
                           0000BC   427 _SCON1	=	0x00bc
                           0000BD   428 _SBUF1	=	0x00bd
                           0000BE   429 _SBAUD1	=	0x00be
                           0000BF   430 _SIF1	=	0x00bf
                           0000B4   431 _SCON2	=	0x00b4
                           0000B5   432 _SBUF2	=	0x00b5
                           0000B6   433 _SBAUD2	=	0x00b6
                           0000B7   434 _SIF2	=	0x00b7
                           0000AC   435 _SCON3	=	0x00ac
                           0000AD   436 _SBUF3	=	0x00ad
                           0000AE   437 _SBAUD3	=	0x00ae
                           0000AF   438 _SIF3	=	0x00af
                           0000F1   439 _TKEY_CTRL	=	0x00f1
                           0000F2   440 _ADC_CTRL	=	0x00f2
                           0000F3   441 _ADC_CFG	=	0x00f3
                           00F5F4   442 _ADC_DAT	=	0xf5f4
                           0000F4   443 _ADC_DAT_L	=	0x00f4
                           0000F5   444 _ADC_DAT_H	=	0x00f5
                           0000F6   445 _ADC_CHAN	=	0x00f6
                           0000F7   446 _ADC_PIN	=	0x00f7
                           000091   447 _USB_C_CTRL	=	0x0091
                           0000D1   448 _UDEV_CTRL	=	0x00d1
                           0000D2   449 _UEP1_CTRL	=	0x00d2
                           0000D3   450 _UEP1_T_LEN	=	0x00d3
                           0000D4   451 _UEP2_CTRL	=	0x00d4
                           0000D5   452 _UEP2_T_LEN	=	0x00d5
                           0000D6   453 _UEP3_CTRL	=	0x00d6
                           0000D7   454 _UEP3_T_LEN	=	0x00d7
                           0000D8   455 _USB_INT_FG	=	0x00d8
                           0000D9   456 _USB_INT_ST	=	0x00d9
                           0000DA   457 _USB_MIS_ST	=	0x00da
                           0000DB   458 _USB_RX_LEN	=	0x00db
                           0000DC   459 _UEP0_CTRL	=	0x00dc
                           0000DD   460 _UEP0_T_LEN	=	0x00dd
                           0000DE   461 _UEP4_CTRL	=	0x00de
                           0000DF   462 _UEP4_T_LEN	=	0x00df
                           0000E1   463 _USB_INT_EN	=	0x00e1
                           0000E2   464 _USB_CTRL	=	0x00e2
                           0000E3   465 _USB_DEV_AD	=	0x00e3
                           00E5E4   466 _UEP2_DMA	=	0xe5e4
                           0000E4   467 _UEP2_DMA_L	=	0x00e4
                           0000E5   468 _UEP2_DMA_H	=	0x00e5
                           00E7E6   469 _UEP3_DMA	=	0xe7e6
                           0000E6   470 _UEP3_DMA_L	=	0x00e6
                           0000E7   471 _UEP3_DMA_H	=	0x00e7
                           0000EA   472 _UEP4_1_MOD	=	0x00ea
                           0000EB   473 _UEP2_3_MOD	=	0x00eb
                           00EDEC   474 _UEP0_DMA	=	0xedec
                           0000EC   475 _UEP0_DMA_L	=	0x00ec
                           0000ED   476 _UEP0_DMA_H	=	0x00ed
                           00EFEE   477 _UEP1_DMA	=	0xefee
                           0000EE   478 _UEP1_DMA_L	=	0x00ee
                           0000EF   479 _UEP1_DMA_H	=	0x00ef
                                    480 ;--------------------------------------------------------
                                    481 ; special function bits
                                    482 ;--------------------------------------------------------
                                    483 	.area RSEG    (ABS,DATA)
      000000                        484 	.org 0x0000
                           0000D7   485 _CY	=	0x00d7
                           0000D6   486 _AC	=	0x00d6
                           0000D5   487 _F0	=	0x00d5
                           0000D4   488 _RS1	=	0x00d4
                           0000D3   489 _RS0	=	0x00d3
                           0000D2   490 _OV	=	0x00d2
                           0000D1   491 _F1	=	0x00d1
                           0000D0   492 _P	=	0x00d0
                           0000AF   493 _EA	=	0x00af
                           0000AE   494 _E_DIS	=	0x00ae
                           0000AD   495 _ET2	=	0x00ad
                           0000AC   496 _ES	=	0x00ac
                           0000AB   497 _ET1	=	0x00ab
                           0000AA   498 _EX1	=	0x00aa
                           0000A9   499 _ET0	=	0x00a9
                           0000A8   500 _EX0	=	0x00a8
                           0000BF   501 _PH_FLAG	=	0x00bf
                           0000BE   502 _PL_FLAG	=	0x00be
                           0000BD   503 _PT2	=	0x00bd
                           0000BC   504 _PS	=	0x00bc
                           0000BB   505 _PT1	=	0x00bb
                           0000BA   506 _PX1	=	0x00ba
                           0000B9   507 _PT0	=	0x00b9
                           0000B8   508 _PX0	=	0x00b8
                           0000EF   509 _IE_WDOG	=	0x00ef
                           0000EE   510 _IE_GPIO	=	0x00ee
                           0000ED   511 _IE_PWMX	=	0x00ed
                           0000ED   512 _IE_UART3	=	0x00ed
                           0000EC   513 _IE_UART1	=	0x00ec
                           0000EB   514 _IE_ADC	=	0x00eb
                           0000EB   515 _IE_UART2	=	0x00eb
                           0000EA   516 _IE_USB	=	0x00ea
                           0000E9   517 _IE_INT3	=	0x00e9
                           0000E8   518 _IE_SPI0	=	0x00e8
                           000087   519 _P0_7	=	0x0087
                           000086   520 _P0_6	=	0x0086
                           000085   521 _P0_5	=	0x0085
                           000084   522 _P0_4	=	0x0084
                           000083   523 _P0_3	=	0x0083
                           000082   524 _P0_2	=	0x0082
                           000081   525 _P0_1	=	0x0081
                           000080   526 _P0_0	=	0x0080
                           000087   527 _TXD3	=	0x0087
                           000087   528 _AIN15	=	0x0087
                           000086   529 _RXD3	=	0x0086
                           000086   530 _AIN14	=	0x0086
                           000085   531 _TXD2	=	0x0085
                           000085   532 _AIN13	=	0x0085
                           000084   533 _RXD2	=	0x0084
                           000084   534 _AIN12	=	0x0084
                           000083   535 _TXD_	=	0x0083
                           000083   536 _AIN11	=	0x0083
                           000082   537 _RXD_	=	0x0082
                           000082   538 _AIN10	=	0x0082
                           000081   539 _AIN9	=	0x0081
                           000080   540 _AIN8	=	0x0080
                           000097   541 _P1_7	=	0x0097
                           000096   542 _P1_6	=	0x0096
                           000095   543 _P1_5	=	0x0095
                           000094   544 _P1_4	=	0x0094
                           000093   545 _P1_3	=	0x0093
                           000092   546 _P1_2	=	0x0092
                           000091   547 _P1_1	=	0x0091
                           000090   548 _P1_0	=	0x0090
                           000097   549 _SCK	=	0x0097
                           000097   550 _TXD1_	=	0x0097
                           000097   551 _AIN7	=	0x0097
                           000096   552 _MISO	=	0x0096
                           000096   553 _RXD1_	=	0x0096
                           000096   554 _VBUS	=	0x0096
                           000096   555 _AIN6	=	0x0096
                           000095   556 _MOSI	=	0x0095
                           000095   557 _PWM0_	=	0x0095
                           000095   558 _UCC2	=	0x0095
                           000095   559 _AIN5	=	0x0095
                           000094   560 _SCS	=	0x0094
                           000094   561 _UCC1	=	0x0094
                           000094   562 _AIN4	=	0x0094
                           000093   563 _AIN3	=	0x0093
                           000092   564 _AIN2	=	0x0092
                           000091   565 _T2EX	=	0x0091
                           000091   566 _CAP2	=	0x0091
                           000091   567 _AIN1	=	0x0091
                           000090   568 _T2	=	0x0090
                           000090   569 _CAP1	=	0x0090
                           000090   570 _AIN0	=	0x0090
                           0000A7   571 _P2_7	=	0x00a7
                           0000A6   572 _P2_6	=	0x00a6
                           0000A5   573 _P2_5	=	0x00a5
                           0000A4   574 _P2_4	=	0x00a4
                           0000A3   575 _P2_3	=	0x00a3
                           0000A2   576 _P2_2	=	0x00a2
                           0000A1   577 _P2_1	=	0x00a1
                           0000A0   578 _P2_0	=	0x00a0
                           0000A7   579 _PWM7	=	0x00a7
                           0000A7   580 _TXD1	=	0x00a7
                           0000A6   581 _PWM6	=	0x00a6
                           0000A6   582 _RXD1	=	0x00a6
                           0000A5   583 _PWM0	=	0x00a5
                           0000A5   584 _T2EX_	=	0x00a5
                           0000A5   585 _CAP2_	=	0x00a5
                           0000A4   586 _PWM1	=	0x00a4
                           0000A4   587 _T2_	=	0x00a4
                           0000A4   588 _CAP1_	=	0x00a4
                           0000A3   589 _PWM2	=	0x00a3
                           0000A2   590 _PWM3	=	0x00a2
                           0000A2   591 _INT0_	=	0x00a2
                           0000A1   592 _PWM4	=	0x00a1
                           0000A0   593 _PWM5	=	0x00a0
                           0000B7   594 _P3_7	=	0x00b7
                           0000B6   595 _P3_6	=	0x00b6
                           0000B5   596 _P3_5	=	0x00b5
                           0000B4   597 _P3_4	=	0x00b4
                           0000B3   598 _P3_3	=	0x00b3
                           0000B2   599 _P3_2	=	0x00b2
                           0000B1   600 _P3_1	=	0x00b1
                           0000B0   601 _P3_0	=	0x00b0
                           0000B7   602 _INT3	=	0x00b7
                           0000B6   603 _CAP0	=	0x00b6
                           0000B5   604 _T1	=	0x00b5
                           0000B4   605 _T0	=	0x00b4
                           0000B3   606 _INT1	=	0x00b3
                           0000B2   607 _INT0	=	0x00b2
                           0000B1   608 _TXD	=	0x00b1
                           0000B0   609 _RXD	=	0x00b0
                           0000C6   610 _P4_6	=	0x00c6
                           0000C5   611 _P4_5	=	0x00c5
                           0000C4   612 _P4_4	=	0x00c4
                           0000C3   613 _P4_3	=	0x00c3
                           0000C2   614 _P4_2	=	0x00c2
                           0000C1   615 _P4_1	=	0x00c1
                           0000C0   616 _P4_0	=	0x00c0
                           0000C7   617 _XO	=	0x00c7
                           0000C6   618 _XI	=	0x00c6
                           00008F   619 _TF1	=	0x008f
                           00008E   620 _TR1	=	0x008e
                           00008D   621 _TF0	=	0x008d
                           00008C   622 _TR0	=	0x008c
                           00008B   623 _IE1	=	0x008b
                           00008A   624 _IT1	=	0x008a
                           000089   625 _IE0	=	0x0089
                           000088   626 _IT0	=	0x0088
                           00009F   627 _SM0	=	0x009f
                           00009E   628 _SM1	=	0x009e
                           00009D   629 _SM2	=	0x009d
                           00009C   630 _REN	=	0x009c
                           00009B   631 _TB8	=	0x009b
                           00009A   632 _RB8	=	0x009a
                           000099   633 _TI	=	0x0099
                           000098   634 _RI	=	0x0098
                           0000CF   635 _TF2	=	0x00cf
                           0000CF   636 _CAP1F	=	0x00cf
                           0000CE   637 _EXF2	=	0x00ce
                           0000CD   638 _RCLK	=	0x00cd
                           0000CC   639 _TCLK	=	0x00cc
                           0000CB   640 _EXEN2	=	0x00cb
                           0000CA   641 _TR2	=	0x00ca
                           0000C9   642 _C_T2	=	0x00c9
                           0000C8   643 _CP_RL2	=	0x00c8
                           0000FF   644 _S0_FST_ACT	=	0x00ff
                           0000FE   645 _S0_IF_OV	=	0x00fe
                           0000FD   646 _S0_IF_FIRST	=	0x00fd
                           0000FC   647 _S0_IF_BYTE	=	0x00fc
                           0000FB   648 _S0_FREE	=	0x00fb
                           0000FA   649 _S0_T_FIFO	=	0x00fa
                           0000F8   650 _S0_R_FIFO	=	0x00f8
                           0000DF   651 _U_IS_NAK	=	0x00df
                           0000DE   652 _U_TOG_OK	=	0x00de
                           0000DD   653 _U_SIE_FREE	=	0x00dd
                           0000DC   654 _UIF_FIFO_OV	=	0x00dc
                           0000DB   655 _UIF_HST_SOF	=	0x00db
                           0000DA   656 _UIF_SUSPEND	=	0x00da
                           0000D9   657 _UIF_TRANSFER	=	0x00d9
                           0000D8   658 _UIF_DETECT	=	0x00d8
                           0000D8   659 _UIF_BUS_RST	=	0x00d8
                                    660 ;--------------------------------------------------------
                                    661 ; overlayable register banks
                                    662 ;--------------------------------------------------------
                                    663 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        664 	.ds 8
                                    665 	.area REG_BANK_1	(REL,OVR,DATA)
      000008                        666 	.ds 8
                                    667 ;--------------------------------------------------------
                                    668 ; overlayable bit register bank
                                    669 ;--------------------------------------------------------
                                    670 	.area BIT_BANK	(REL,OVR,DATA)
      000020                        671 bits:
      000020                        672 	.ds 1
                           008000   673 	b0 = bits[0]
                           008100   674 	b1 = bits[1]
                           008200   675 	b2 = bits[2]
                           008300   676 	b3 = bits[3]
                           008400   677 	b4 = bits[4]
                           008500   678 	b5 = bits[5]
                           008600   679 	b6 = bits[6]
                           008700   680 	b7 = bits[7]
                                    681 ;--------------------------------------------------------
                                    682 ; internal ram data
                                    683 ;--------------------------------------------------------
                                    684 	.area DSEG    (DATA)
                                    685 ;--------------------------------------------------------
                                    686 ; overlayable items in internal ram 
                                    687 ;--------------------------------------------------------
                                    688 	.area	OSEG    (OVR,DATA)
                                    689 	.area	OSEG    (OVR,DATA)
                                    690 	.area	OSEG    (OVR,DATA)
                                    691 ;--------------------------------------------------------
                                    692 ; indirectly addressable internal ram data
                                    693 ;--------------------------------------------------------
                                    694 	.area ISEG    (DATA)
                                    695 ;--------------------------------------------------------
                                    696 ; absolute internal ram data
                                    697 ;--------------------------------------------------------
                                    698 	.area IABS    (ABS,DATA)
                                    699 	.area IABS    (ABS,DATA)
                                    700 ;--------------------------------------------------------
                                    701 ; bit data
                                    702 ;--------------------------------------------------------
                                    703 	.area BSEG    (BIT)
                                    704 ;--------------------------------------------------------
                                    705 ; paged external ram data
                                    706 ;--------------------------------------------------------
                                    707 	.area PSEG    (PAG,XDATA)
                                    708 ;--------------------------------------------------------
                                    709 ; external ram data
                                    710 ;--------------------------------------------------------
                                    711 	.area XSEG    (XDATA)
                                    712 ;--------------------------------------------------------
                                    713 ; absolute external ram data
                                    714 ;--------------------------------------------------------
                                    715 	.area XABS    (ABS,XDATA)
                                    716 ;--------------------------------------------------------
                                    717 ; external initialized ram data
                                    718 ;--------------------------------------------------------
                                    719 	.area XISEG   (XDATA)
                                    720 	.area HOME    (CODE)
                                    721 	.area GSINIT0 (CODE)
                                    722 	.area GSINIT1 (CODE)
                                    723 	.area GSINIT2 (CODE)
                                    724 	.area GSINIT3 (CODE)
                                    725 	.area GSINIT4 (CODE)
                                    726 	.area GSINIT5 (CODE)
                                    727 	.area GSINIT  (CODE)
                                    728 	.area GSFINAL (CODE)
                                    729 	.area CSEG    (CODE)
                                    730 ;--------------------------------------------------------
                                    731 ; global & static initialisations
                                    732 ;--------------------------------------------------------
                                    733 	.area HOME    (CODE)
                                    734 	.area GSINIT  (CODE)
                                    735 	.area GSFINAL (CODE)
                                    736 	.area GSINIT  (CODE)
                                    737 ;--------------------------------------------------------
                                    738 ; Home
                                    739 ;--------------------------------------------------------
                                    740 	.area HOME    (CODE)
                                    741 	.area HOME    (CODE)
                                    742 ;--------------------------------------------------------
                                    743 ; code
                                    744 ;--------------------------------------------------------
                                    745 	.area CSEG    (CODE)
                                    746 ;------------------------------------------------------------
                                    747 ;Allocation info for local variables in function 'CH549UART1Init'
                                    748 ;------------------------------------------------------------
                                    749 ;	source/CH549_UART.c:17: void CH549UART1Init()
                                    750 ;	-----------------------------------------
                                    751 ;	 function CH549UART1Init
                                    752 ;	-----------------------------------------
      0000BD                        753 _CH549UART1Init:
                           000007   754 	ar7 = 0x07
                           000006   755 	ar6 = 0x06
                           000005   756 	ar5 = 0x05
                           000004   757 	ar4 = 0x04
                           000003   758 	ar3 = 0x03
                           000002   759 	ar2 = 0x02
                           000001   760 	ar1 = 0x01
                           000000   761 	ar0 = 0x00
                                    762 ;	source/CH549_UART.c:19: SCON1 &= ~bU1SM0;                            //选择8位数据通讯
      0000BD 53 BC 7F         [24]  763 	anl	_SCON1,#0x7f
                                    764 ;	source/CH549_UART.c:20: SCON1 |= bU1SMOD;                            //快速模式
      0000C0 43 BC 20         [24]  765 	orl	_SCON1,#0x20
                                    766 ;	source/CH549_UART.c:21: SCON1 |= bU1REN;                             //使能接收
      0000C3 43 BC 10         [24]  767 	orl	_SCON1,#0x10
                                    768 ;	source/CH549_UART.c:22: SBAUD1 = 0 - FREQ_SYS/16/UART1_BUAD;         //波特率配置
      0000C6 75 BE F3         [24]  769 	mov	_SBAUD1,#0xf3
                                    770 ;	source/CH549_UART.c:23: SIF1 = bU1TI;                                //清空发送完成标志
      0000C9 75 BF 02         [24]  771 	mov	_SIF1,#0x02
                                    772 ;	source/CH549_UART.c:25: IE_UART1 = 1;
                                    773 ;	assignBit
      0000CC D2 EC            [12]  774 	setb	_IE_UART1
                                    775 ;	source/CH549_UART.c:26: EA = 1;
                                    776 ;	assignBit
      0000CE D2 AF            [12]  777 	setb	_EA
                                    778 ;	source/CH549_UART.c:28: }
      0000D0 22               [24]  779 	ret
                                    780 ;------------------------------------------------------------
                                    781 ;Allocation info for local variables in function 'CH549UART1Alter'
                                    782 ;------------------------------------------------------------
                                    783 ;	source/CH549_UART.c:36: void CH549UART1Alter()
                                    784 ;	-----------------------------------------
                                    785 ;	 function CH549UART1Alter
                                    786 ;	-----------------------------------------
      0000D1                        787 _CH549UART1Alter:
                                    788 ;	source/CH549_UART.c:38: P1_MOD_OC |= (3<<6);                                                   //准双向模式
      0000D1 43 92 C0         [24]  789 	orl	_P1_MOD_OC,#0xc0
                                    790 ;	source/CH549_UART.c:39: P1_DIR_PU |= (3<<6);
      0000D4 43 93 C0         [24]  791 	orl	_P1_DIR_PU,#0xc0
                                    792 ;	source/CH549_UART.c:40: PIN_FUNC |= bUART1_PIN_X;                                              //开启引脚复用功能
      0000D7 43 AA 20         [24]  793 	orl	_PIN_FUNC,#0x20
                                    794 ;	source/CH549_UART.c:41: }
      0000DA 22               [24]  795 	ret
                                    796 ;------------------------------------------------------------
                                    797 ;Allocation info for local variables in function 'CH549UART1RcvByte'
                                    798 ;------------------------------------------------------------
                                    799 ;	source/CH549_UART.c:49: UINT8  CH549UART1RcvByte( )
                                    800 ;	-----------------------------------------
                                    801 ;	 function CH549UART1RcvByte
                                    802 ;	-----------------------------------------
      0000DB                        803 _CH549UART1RcvByte:
                                    804 ;	source/CH549_UART.c:51: while((SIF1&bU1RI) == 0)
      0000DB                        805 00101$:
      0000DB E5 BF            [12]  806 	mov	a,_SIF1
      0000DD 30 E0 FB         [24]  807 	jnb	acc.0,00101$
                                    808 ;	source/CH549_UART.c:55: SIF1 = bU1RI;                                                          //清除接收中断
      0000E0 75 BF 01         [24]  809 	mov	_SIF1,#0x01
                                    810 ;	source/CH549_UART.c:56: return SBUF1;
      0000E3 85 BD 82         [24]  811 	mov	dpl,_SBUF1
                                    812 ;	source/CH549_UART.c:57: }
      0000E6 22               [24]  813 	ret
                                    814 ;------------------------------------------------------------
                                    815 ;Allocation info for local variables in function 'CH549UART1SendByte'
                                    816 ;------------------------------------------------------------
                                    817 ;SendDat                   Allocated to registers 
                                    818 ;------------------------------------------------------------
                                    819 ;	source/CH549_UART.c:65: void CH549UART1SendByte(UINT8 SendDat)
                                    820 ;	-----------------------------------------
                                    821 ;	 function CH549UART1SendByte
                                    822 ;	-----------------------------------------
      0000E7                        823 _CH549UART1SendByte:
      0000E7 85 82 BD         [24]  824 	mov	_SBUF1,dpl
                                    825 ;	source/CH549_UART.c:68: while((SIF1&bU1TI) == 0)
      0000EA                        826 00101$:
      0000EA E5 BF            [12]  827 	mov	a,_SIF1
      0000EC 30 E1 FB         [24]  828 	jnb	acc.1,00101$
                                    829 ;	source/CH549_UART.c:72: SIF1 = bU1TI;                                                          //清除发送完成中断
      0000EF 75 BF 02         [24]  830 	mov	_SIF1,#0x02
                                    831 ;	source/CH549_UART.c:73: }
      0000F2 22               [24]  832 	ret
                                    833 ;------------------------------------------------------------
                                    834 ;Allocation info for local variables in function 'UART1Interrupt'
                                    835 ;------------------------------------------------------------
                                    836 ;dat                       Allocated to registers 
                                    837 ;------------------------------------------------------------
                                    838 ;	source/CH549_UART.c:79: void UART1Interrupt(void) __interrupt INT_NO_UART1 __using 1            //串口1中断服务程序,使用寄存器组1
                                    839 ;	-----------------------------------------
                                    840 ;	 function UART1Interrupt
                                    841 ;	-----------------------------------------
      0000F3                        842 _UART1Interrupt:
                           00000F   843 	ar7 = 0x0f
                           00000E   844 	ar6 = 0x0e
                           00000D   845 	ar5 = 0x0d
                           00000C   846 	ar4 = 0x0c
                           00000B   847 	ar3 = 0x0b
                           00000A   848 	ar2 = 0x0a
                           000009   849 	ar1 = 0x09
                           000008   850 	ar0 = 0x08
      0000F3 C0 20            [24]  851 	push	bits
      0000F5 C0 E0            [24]  852 	push	acc
      0000F7 C0 F0            [24]  853 	push	b
      0000F9 C0 82            [24]  854 	push	dpl
      0000FB C0 83            [24]  855 	push	dph
      0000FD C0 07            [24]  856 	push	(0+7)
      0000FF C0 06            [24]  857 	push	(0+6)
      000101 C0 05            [24]  858 	push	(0+5)
      000103 C0 04            [24]  859 	push	(0+4)
      000105 C0 03            [24]  860 	push	(0+3)
      000107 C0 02            [24]  861 	push	(0+2)
      000109 C0 01            [24]  862 	push	(0+1)
      00010B C0 00            [24]  863 	push	(0+0)
      00010D C0 D0            [24]  864 	push	psw
      00010F 75 D0 08         [24]  865 	mov	psw,#0x08
                                    866 ;	source/CH549_UART.c:82: if(SIF1&bU1RI)
      000112 E5 BF            [12]  867 	mov	a,_SIF1
      000114 30 E0 0F         [24]  868 	jnb	acc.0,00103$
                                    869 ;	source/CH549_UART.c:84: SIF1 = bU1RI;                                                     //清除接收完中断
      000117 75 BF 01         [24]  870 	mov	_SIF1,#0x01
                                    871 ;	source/CH549_UART.c:85: dat = SBUF1;
      00011A 85 BD 82         [24]  872 	mov	dpl,_SBUF1
                                    873 ;	source/CH549_UART.c:86: CH549UART1SendByte(dat);
      00011D 75 D0 00         [24]  874 	mov	psw,#0x00
      000120 12 00 E7         [24]  875 	lcall	_CH549UART1SendByte
      000123 75 D0 08         [24]  876 	mov	psw,#0x08
      000126                        877 00103$:
                                    878 ;	source/CH549_UART.c:88: }
      000126 D0 D0            [24]  879 	pop	psw
      000128 D0 00            [24]  880 	pop	(0+0)
      00012A D0 01            [24]  881 	pop	(0+1)
      00012C D0 02            [24]  882 	pop	(0+2)
      00012E D0 03            [24]  883 	pop	(0+3)
      000130 D0 04            [24]  884 	pop	(0+4)
      000132 D0 05            [24]  885 	pop	(0+5)
      000134 D0 06            [24]  886 	pop	(0+6)
      000136 D0 07            [24]  887 	pop	(0+7)
      000138 D0 83            [24]  888 	pop	dph
      00013A D0 82            [24]  889 	pop	dpl
      00013C D0 F0            [24]  890 	pop	b
      00013E D0 E0            [24]  891 	pop	acc
      000140 D0 20            [24]  892 	pop	bits
      000142 32               [24]  893 	reti
                                    894 ;------------------------------------------------------------
                                    895 ;Allocation info for local variables in function 'CH549UART2Init'
                                    896 ;------------------------------------------------------------
                                    897 ;	source/CH549_UART.c:97: void CH549UART2Init()
                                    898 ;	-----------------------------------------
                                    899 ;	 function CH549UART2Init
                                    900 ;	-----------------------------------------
      000143                        901 _CH549UART2Init:
                           000007   902 	ar7 = 0x07
                           000006   903 	ar6 = 0x06
                           000005   904 	ar5 = 0x05
                           000004   905 	ar4 = 0x04
                           000003   906 	ar3 = 0x03
                           000002   907 	ar2 = 0x02
                           000001   908 	ar1 = 0x01
                           000000   909 	ar0 = 0x00
                                    910 ;	source/CH549_UART.c:99: SCON2 &= ~bU2SM0;                            //选择8位数据通讯
      000143 53 B4 7F         [24]  911 	anl	_SCON2,#0x7f
                                    912 ;	source/CH549_UART.c:100: SCON2 |= bU2SMOD;                            //快速模式
      000146 43 B4 20         [24]  913 	orl	_SCON2,#0x20
                                    914 ;	source/CH549_UART.c:101: SCON2 |= bU2REN;                             //使能接收
      000149 43 B4 10         [24]  915 	orl	_SCON2,#0x10
                                    916 ;	source/CH549_UART.c:102: SBAUD2 = 0 - FREQ_SYS/16/UART2_BUAD;         //波特率配置
      00014C 75 B6 F3         [24]  917 	mov	_SBAUD2,#0xf3
                                    918 ;	source/CH549_UART.c:103: SIF2 = bU2TI;                                //清空发送完成标志
      00014F 75 B7 02         [24]  919 	mov	_SIF2,#0x02
                                    920 ;	source/CH549_UART.c:105: SCON2 |= bU2IE;                              //开启UART2中断，关闭ADC中断
      000152 43 B4 40         [24]  921 	orl	_SCON2,#0x40
                                    922 ;	source/CH549_UART.c:106: IE_UART2 = 1;
                                    923 ;	assignBit
      000155 D2 EB            [12]  924 	setb	_IE_UART2
                                    925 ;	source/CH549_UART.c:107: EA = 1;
                                    926 ;	assignBit
      000157 D2 AF            [12]  927 	setb	_EA
                                    928 ;	source/CH549_UART.c:109: }
      000159 22               [24]  929 	ret
                                    930 ;------------------------------------------------------------
                                    931 ;Allocation info for local variables in function 'CH549UART2RcvByte'
                                    932 ;------------------------------------------------------------
                                    933 ;	source/CH549_UART.c:117: UINT8 CH549UART2RcvByte( )
                                    934 ;	-----------------------------------------
                                    935 ;	 function CH549UART2RcvByte
                                    936 ;	-----------------------------------------
      00015A                        937 _CH549UART2RcvByte:
                                    938 ;	source/CH549_UART.c:119: while((SIF2&bU2RI) == 0)
      00015A                        939 00101$:
      00015A E5 B7            [12]  940 	mov	a,_SIF2
      00015C 30 E0 FB         [24]  941 	jnb	acc.0,00101$
                                    942 ;	source/CH549_UART.c:123: SIF2 = bU2RI;                                                          //清除接收中断
      00015F 75 B7 01         [24]  943 	mov	_SIF2,#0x01
                                    944 ;	source/CH549_UART.c:124: return SBUF2;
      000162 85 B5 82         [24]  945 	mov	dpl,_SBUF2
                                    946 ;	source/CH549_UART.c:125: }
      000165 22               [24]  947 	ret
                                    948 ;------------------------------------------------------------
                                    949 ;Allocation info for local variables in function 'CH549UART2SendByte'
                                    950 ;------------------------------------------------------------
                                    951 ;SendDat                   Allocated to registers 
                                    952 ;------------------------------------------------------------
                                    953 ;	source/CH549_UART.c:133: void CH549UART2SendByte(UINT8 SendDat)
                                    954 ;	-----------------------------------------
                                    955 ;	 function CH549UART2SendByte
                                    956 ;	-----------------------------------------
      000166                        957 _CH549UART2SendByte:
      000166 85 82 B5         [24]  958 	mov	_SBUF2,dpl
                                    959 ;	source/CH549_UART.c:136: while((SIF2&bU2TI) == 0)
      000169                        960 00101$:
      000169 E5 B7            [12]  961 	mov	a,_SIF2
      00016B 30 E1 FB         [24]  962 	jnb	acc.1,00101$
                                    963 ;	source/CH549_UART.c:140: SIF2 = bU2TI;                                                          //清除发送完成中断
      00016E 75 B7 02         [24]  964 	mov	_SIF2,#0x02
                                    965 ;	source/CH549_UART.c:141: }
      000171 22               [24]  966 	ret
                                    967 ;------------------------------------------------------------
                                    968 ;Allocation info for local variables in function 'UART2Interrupt'
                                    969 ;------------------------------------------------------------
                                    970 ;dat                       Allocated to registers 
                                    971 ;------------------------------------------------------------
                                    972 ;	source/CH549_UART.c:147: void UART2Interrupt( void ) __interrupt INT_NO_UART2  __using 1               //串口2中断服务程序,使用寄存器组1
                                    973 ;	-----------------------------------------
                                    974 ;	 function UART2Interrupt
                                    975 ;	-----------------------------------------
      000172                        976 _UART2Interrupt:
                           00000F   977 	ar7 = 0x0f
                           00000E   978 	ar6 = 0x0e
                           00000D   979 	ar5 = 0x0d
                           00000C   980 	ar4 = 0x0c
                           00000B   981 	ar3 = 0x0b
                           00000A   982 	ar2 = 0x0a
                           000009   983 	ar1 = 0x09
                           000008   984 	ar0 = 0x08
      000172 C0 20            [24]  985 	push	bits
      000174 C0 E0            [24]  986 	push	acc
      000176 C0 F0            [24]  987 	push	b
      000178 C0 82            [24]  988 	push	dpl
      00017A C0 83            [24]  989 	push	dph
      00017C C0 07            [24]  990 	push	(0+7)
      00017E C0 06            [24]  991 	push	(0+6)
      000180 C0 05            [24]  992 	push	(0+5)
      000182 C0 04            [24]  993 	push	(0+4)
      000184 C0 03            [24]  994 	push	(0+3)
      000186 C0 02            [24]  995 	push	(0+2)
      000188 C0 01            [24]  996 	push	(0+1)
      00018A C0 00            [24]  997 	push	(0+0)
      00018C C0 D0            [24]  998 	push	psw
      00018E 75 D0 08         [24]  999 	mov	psw,#0x08
                                   1000 ;	source/CH549_UART.c:150: if(SIF2&bU2RI)
      000191 E5 B7            [12] 1001 	mov	a,_SIF2
      000193 30 E0 0F         [24] 1002 	jnb	acc.0,00103$
                                   1003 ;	source/CH549_UART.c:152: SIF2 = bU2RI;                                                     //清除接收完中断
      000196 75 B7 01         [24] 1004 	mov	_SIF2,#0x01
                                   1005 ;	source/CH549_UART.c:153: dat = SBUF2;
      000199 85 B5 82         [24] 1006 	mov	dpl,_SBUF2
                                   1007 ;	source/CH549_UART.c:154: CH549UART2SendByte(dat);
      00019C 75 D0 00         [24] 1008 	mov	psw,#0x00
      00019F 12 01 66         [24] 1009 	lcall	_CH549UART2SendByte
      0001A2 75 D0 08         [24] 1010 	mov	psw,#0x08
      0001A5                       1011 00103$:
                                   1012 ;	source/CH549_UART.c:156: }
      0001A5 D0 D0            [24] 1013 	pop	psw
      0001A7 D0 00            [24] 1014 	pop	(0+0)
      0001A9 D0 01            [24] 1015 	pop	(0+1)
      0001AB D0 02            [24] 1016 	pop	(0+2)
      0001AD D0 03            [24] 1017 	pop	(0+3)
      0001AF D0 04            [24] 1018 	pop	(0+4)
      0001B1 D0 05            [24] 1019 	pop	(0+5)
      0001B3 D0 06            [24] 1020 	pop	(0+6)
      0001B5 D0 07            [24] 1021 	pop	(0+7)
      0001B7 D0 83            [24] 1022 	pop	dph
      0001B9 D0 82            [24] 1023 	pop	dpl
      0001BB D0 F0            [24] 1024 	pop	b
      0001BD D0 E0            [24] 1025 	pop	acc
      0001BF D0 20            [24] 1026 	pop	bits
      0001C1 32               [24] 1027 	reti
                                   1028 ;------------------------------------------------------------
                                   1029 ;Allocation info for local variables in function 'CH549UART3Init'
                                   1030 ;------------------------------------------------------------
                                   1031 ;	source/CH549_UART.c:165: void CH549UART3Init()
                                   1032 ;	-----------------------------------------
                                   1033 ;	 function CH549UART3Init
                                   1034 ;	-----------------------------------------
      0001C2                       1035 _CH549UART3Init:
                           000007  1036 	ar7 = 0x07
                           000006  1037 	ar6 = 0x06
                           000005  1038 	ar5 = 0x05
                           000004  1039 	ar4 = 0x04
                           000003  1040 	ar3 = 0x03
                           000002  1041 	ar2 = 0x02
                           000001  1042 	ar1 = 0x01
                           000000  1043 	ar0 = 0x00
                                   1044 ;	source/CH549_UART.c:167: SCON3 &= ~bU3SM0;                            //选择8位数据通讯
      0001C2 53 AC 7F         [24] 1045 	anl	_SCON3,#0x7f
                                   1046 ;	source/CH549_UART.c:168: SCON3 |= bU3SMOD;                            //快速模式
      0001C5 43 AC 20         [24] 1047 	orl	_SCON3,#0x20
                                   1048 ;	source/CH549_UART.c:169: SCON3 |= bU3REN;                             //使能接收
      0001C8 43 AC 10         [24] 1049 	orl	_SCON3,#0x10
                                   1050 ;	source/CH549_UART.c:170: SBAUD3 = 0 - FREQ_SYS/16/UART3_BUAD;         //波特率配置
      0001CB 75 AE F3         [24] 1051 	mov	_SBAUD3,#0xf3
                                   1052 ;	source/CH549_UART.c:171: SIF3 = bU3TI;                                //清空发送完成标志
      0001CE 75 AF 02         [24] 1053 	mov	_SIF3,#0x02
                                   1054 ;	source/CH549_UART.c:173: SCON3 |= bU3IE;                              //开启UART3中断，关闭PWM中断
      0001D1 43 AC 40         [24] 1055 	orl	_SCON3,#0x40
                                   1056 ;	source/CH549_UART.c:174: IE_UART3 = 1;
                                   1057 ;	assignBit
      0001D4 D2 ED            [12] 1058 	setb	_IE_UART3
                                   1059 ;	source/CH549_UART.c:175: EA = 1;
                                   1060 ;	assignBit
      0001D6 D2 AF            [12] 1061 	setb	_EA
                                   1062 ;	source/CH549_UART.c:177: }
      0001D8 22               [24] 1063 	ret
                                   1064 ;------------------------------------------------------------
                                   1065 ;Allocation info for local variables in function 'CH549UART3RcvByte'
                                   1066 ;------------------------------------------------------------
                                   1067 ;	source/CH549_UART.c:185: UINT8 CH549UART3RcvByte( )
                                   1068 ;	-----------------------------------------
                                   1069 ;	 function CH549UART3RcvByte
                                   1070 ;	-----------------------------------------
      0001D9                       1071 _CH549UART3RcvByte:
                                   1072 ;	source/CH549_UART.c:187: while((SIF3&bU3RI) == 0)
      0001D9                       1073 00101$:
      0001D9 E5 AF            [12] 1074 	mov	a,_SIF3
      0001DB 30 E0 FB         [24] 1075 	jnb	acc.0,00101$
                                   1076 ;	source/CH549_UART.c:191: SIF3 = bU3RI;                                                          //清除接收中断
      0001DE 75 AF 01         [24] 1077 	mov	_SIF3,#0x01
                                   1078 ;	source/CH549_UART.c:192: return SBUF3;
      0001E1 85 AD 82         [24] 1079 	mov	dpl,_SBUF3
                                   1080 ;	source/CH549_UART.c:193: }
      0001E4 22               [24] 1081 	ret
                                   1082 ;------------------------------------------------------------
                                   1083 ;Allocation info for local variables in function 'CH549UART3SendByte'
                                   1084 ;------------------------------------------------------------
                                   1085 ;SendDat                   Allocated to registers 
                                   1086 ;------------------------------------------------------------
                                   1087 ;	source/CH549_UART.c:201: void CH549UART3SendByte(UINT8 SendDat)
                                   1088 ;	-----------------------------------------
                                   1089 ;	 function CH549UART3SendByte
                                   1090 ;	-----------------------------------------
      0001E5                       1091 _CH549UART3SendByte:
      0001E5 85 82 AD         [24] 1092 	mov	_SBUF3,dpl
                                   1093 ;	source/CH549_UART.c:204: while((SIF3&bU3TI) == 0)
      0001E8                       1094 00101$:
      0001E8 E5 AF            [12] 1095 	mov	a,_SIF3
      0001EA 30 E1 FB         [24] 1096 	jnb	acc.1,00101$
                                   1097 ;	source/CH549_UART.c:208: SIF3 = bU3TI;                                                          //清除发送完成中断
      0001ED 75 AF 02         [24] 1098 	mov	_SIF3,#0x02
                                   1099 ;	source/CH549_UART.c:209: }
      0001F0 22               [24] 1100 	ret
                                   1101 ;------------------------------------------------------------
                                   1102 ;Allocation info for local variables in function 'UART3Interrupt'
                                   1103 ;------------------------------------------------------------
                                   1104 ;dat                       Allocated to registers 
                                   1105 ;------------------------------------------------------------
                                   1106 ;	source/CH549_UART.c:215: void UART3Interrupt( void ) __interrupt INT_NO_UART3 __using 1              //串口3中断服务程序,使用寄存器组1
                                   1107 ;	-----------------------------------------
                                   1108 ;	 function UART3Interrupt
                                   1109 ;	-----------------------------------------
      0001F1                       1110 _UART3Interrupt:
                           00000F  1111 	ar7 = 0x0f
                           00000E  1112 	ar6 = 0x0e
                           00000D  1113 	ar5 = 0x0d
                           00000C  1114 	ar4 = 0x0c
                           00000B  1115 	ar3 = 0x0b
                           00000A  1116 	ar2 = 0x0a
                           000009  1117 	ar1 = 0x09
                           000008  1118 	ar0 = 0x08
      0001F1 C0 20            [24] 1119 	push	bits
      0001F3 C0 E0            [24] 1120 	push	acc
      0001F5 C0 F0            [24] 1121 	push	b
      0001F7 C0 82            [24] 1122 	push	dpl
      0001F9 C0 83            [24] 1123 	push	dph
      0001FB C0 07            [24] 1124 	push	(0+7)
      0001FD C0 06            [24] 1125 	push	(0+6)
      0001FF C0 05            [24] 1126 	push	(0+5)
      000201 C0 04            [24] 1127 	push	(0+4)
      000203 C0 03            [24] 1128 	push	(0+3)
      000205 C0 02            [24] 1129 	push	(0+2)
      000207 C0 01            [24] 1130 	push	(0+1)
      000209 C0 00            [24] 1131 	push	(0+0)
      00020B C0 D0            [24] 1132 	push	psw
      00020D 75 D0 08         [24] 1133 	mov	psw,#0x08
                                   1134 ;	source/CH549_UART.c:218: if(SIF3&bU3RI)
      000210 E5 AF            [12] 1135 	mov	a,_SIF3
      000212 30 E0 0F         [24] 1136 	jnb	acc.0,00103$
                                   1137 ;	source/CH549_UART.c:220: SIF3 = bU3RI;                                                     //清除接收完中断
      000215 75 AF 01         [24] 1138 	mov	_SIF3,#0x01
                                   1139 ;	source/CH549_UART.c:221: dat = SBUF3;
      000218 85 AD 82         [24] 1140 	mov	dpl,_SBUF3
                                   1141 ;	source/CH549_UART.c:222: CH549UART3SendByte(dat);
      00021B 75 D0 00         [24] 1142 	mov	psw,#0x00
      00021E 12 01 E5         [24] 1143 	lcall	_CH549UART3SendByte
      000221 75 D0 08         [24] 1144 	mov	psw,#0x08
      000224                       1145 00103$:
                                   1146 ;	source/CH549_UART.c:224: }
      000224 D0 D0            [24] 1147 	pop	psw
      000226 D0 00            [24] 1148 	pop	(0+0)
      000228 D0 01            [24] 1149 	pop	(0+1)
      00022A D0 02            [24] 1150 	pop	(0+2)
      00022C D0 03            [24] 1151 	pop	(0+3)
      00022E D0 04            [24] 1152 	pop	(0+4)
      000230 D0 05            [24] 1153 	pop	(0+5)
      000232 D0 06            [24] 1154 	pop	(0+6)
      000234 D0 07            [24] 1155 	pop	(0+7)
      000236 D0 83            [24] 1156 	pop	dph
      000238 D0 82            [24] 1157 	pop	dpl
      00023A D0 F0            [24] 1158 	pop	b
      00023C D0 E0            [24] 1159 	pop	acc
      00023E D0 20            [24] 1160 	pop	bits
      000240 32               [24] 1161 	reti
                                   1162 	.area CSEG    (CODE)
                                   1163 	.area CONST   (CODE)
                                   1164 	.area XINIT   (CODE)
                                   1165 	.area CABS    (ABS,CODE)
