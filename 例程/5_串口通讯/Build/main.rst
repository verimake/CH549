                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _CH549UART3Init
                                     13 	.globl _CH549UART2Init
                                     14 	.globl _CH549UART1SendByte
                                     15 	.globl _CH549UART1Alter
                                     16 	.globl _CH549UART1Init
                                     17 	.globl _mDelaymS
                                     18 	.globl _CfgFsys
                                     19 	.globl _printf
                                     20 	.globl _UIF_BUS_RST
                                     21 	.globl _UIF_DETECT
                                     22 	.globl _UIF_TRANSFER
                                     23 	.globl _UIF_SUSPEND
                                     24 	.globl _UIF_HST_SOF
                                     25 	.globl _UIF_FIFO_OV
                                     26 	.globl _U_SIE_FREE
                                     27 	.globl _U_TOG_OK
                                     28 	.globl _U_IS_NAK
                                     29 	.globl _S0_R_FIFO
                                     30 	.globl _S0_T_FIFO
                                     31 	.globl _S0_FREE
                                     32 	.globl _S0_IF_BYTE
                                     33 	.globl _S0_IF_FIRST
                                     34 	.globl _S0_IF_OV
                                     35 	.globl _S0_FST_ACT
                                     36 	.globl _CP_RL2
                                     37 	.globl _C_T2
                                     38 	.globl _TR2
                                     39 	.globl _EXEN2
                                     40 	.globl _TCLK
                                     41 	.globl _RCLK
                                     42 	.globl _EXF2
                                     43 	.globl _CAP1F
                                     44 	.globl _TF2
                                     45 	.globl _RI
                                     46 	.globl _TI
                                     47 	.globl _RB8
                                     48 	.globl _TB8
                                     49 	.globl _REN
                                     50 	.globl _SM2
                                     51 	.globl _SM1
                                     52 	.globl _SM0
                                     53 	.globl _IT0
                                     54 	.globl _IE0
                                     55 	.globl _IT1
                                     56 	.globl _IE1
                                     57 	.globl _TR0
                                     58 	.globl _TF0
                                     59 	.globl _TR1
                                     60 	.globl _TF1
                                     61 	.globl _XI
                                     62 	.globl _XO
                                     63 	.globl _P4_0
                                     64 	.globl _P4_1
                                     65 	.globl _P4_2
                                     66 	.globl _P4_3
                                     67 	.globl _P4_4
                                     68 	.globl _P4_5
                                     69 	.globl _P4_6
                                     70 	.globl _RXD
                                     71 	.globl _TXD
                                     72 	.globl _INT0
                                     73 	.globl _INT1
                                     74 	.globl _T0
                                     75 	.globl _T1
                                     76 	.globl _CAP0
                                     77 	.globl _INT3
                                     78 	.globl _P3_0
                                     79 	.globl _P3_1
                                     80 	.globl _P3_2
                                     81 	.globl _P3_3
                                     82 	.globl _P3_4
                                     83 	.globl _P3_5
                                     84 	.globl _P3_6
                                     85 	.globl _P3_7
                                     86 	.globl _PWM5
                                     87 	.globl _PWM4
                                     88 	.globl _INT0_
                                     89 	.globl _PWM3
                                     90 	.globl _PWM2
                                     91 	.globl _CAP1_
                                     92 	.globl _T2_
                                     93 	.globl _PWM1
                                     94 	.globl _CAP2_
                                     95 	.globl _T2EX_
                                     96 	.globl _PWM0
                                     97 	.globl _RXD1
                                     98 	.globl _PWM6
                                     99 	.globl _TXD1
                                    100 	.globl _PWM7
                                    101 	.globl _P2_0
                                    102 	.globl _P2_1
                                    103 	.globl _P2_2
                                    104 	.globl _P2_3
                                    105 	.globl _P2_4
                                    106 	.globl _P2_5
                                    107 	.globl _P2_6
                                    108 	.globl _P2_7
                                    109 	.globl _AIN0
                                    110 	.globl _CAP1
                                    111 	.globl _T2
                                    112 	.globl _AIN1
                                    113 	.globl _CAP2
                                    114 	.globl _T2EX
                                    115 	.globl _AIN2
                                    116 	.globl _AIN3
                                    117 	.globl _AIN4
                                    118 	.globl _UCC1
                                    119 	.globl _SCS
                                    120 	.globl _AIN5
                                    121 	.globl _UCC2
                                    122 	.globl _PWM0_
                                    123 	.globl _MOSI
                                    124 	.globl _AIN6
                                    125 	.globl _VBUS
                                    126 	.globl _RXD1_
                                    127 	.globl _MISO
                                    128 	.globl _AIN7
                                    129 	.globl _TXD1_
                                    130 	.globl _SCK
                                    131 	.globl _P1_0
                                    132 	.globl _P1_1
                                    133 	.globl _P1_2
                                    134 	.globl _P1_3
                                    135 	.globl _P1_4
                                    136 	.globl _P1_5
                                    137 	.globl _P1_6
                                    138 	.globl _P1_7
                                    139 	.globl _AIN8
                                    140 	.globl _AIN9
                                    141 	.globl _AIN10
                                    142 	.globl _RXD_
                                    143 	.globl _AIN11
                                    144 	.globl _TXD_
                                    145 	.globl _AIN12
                                    146 	.globl _RXD2
                                    147 	.globl _AIN13
                                    148 	.globl _TXD2
                                    149 	.globl _AIN14
                                    150 	.globl _RXD3
                                    151 	.globl _AIN15
                                    152 	.globl _TXD3
                                    153 	.globl _P0_0
                                    154 	.globl _P0_1
                                    155 	.globl _P0_2
                                    156 	.globl _P0_3
                                    157 	.globl _P0_4
                                    158 	.globl _P0_5
                                    159 	.globl _P0_6
                                    160 	.globl _P0_7
                                    161 	.globl _IE_SPI0
                                    162 	.globl _IE_INT3
                                    163 	.globl _IE_USB
                                    164 	.globl _IE_UART2
                                    165 	.globl _IE_ADC
                                    166 	.globl _IE_UART1
                                    167 	.globl _IE_UART3
                                    168 	.globl _IE_PWMX
                                    169 	.globl _IE_GPIO
                                    170 	.globl _IE_WDOG
                                    171 	.globl _PX0
                                    172 	.globl _PT0
                                    173 	.globl _PX1
                                    174 	.globl _PT1
                                    175 	.globl _PS
                                    176 	.globl _PT2
                                    177 	.globl _PL_FLAG
                                    178 	.globl _PH_FLAG
                                    179 	.globl _EX0
                                    180 	.globl _ET0
                                    181 	.globl _EX1
                                    182 	.globl _ET1
                                    183 	.globl _ES
                                    184 	.globl _ET2
                                    185 	.globl _E_DIS
                                    186 	.globl _EA
                                    187 	.globl _P
                                    188 	.globl _F1
                                    189 	.globl _OV
                                    190 	.globl _RS0
                                    191 	.globl _RS1
                                    192 	.globl _F0
                                    193 	.globl _AC
                                    194 	.globl _CY
                                    195 	.globl _UEP1_DMA_H
                                    196 	.globl _UEP1_DMA_L
                                    197 	.globl _UEP1_DMA
                                    198 	.globl _UEP0_DMA_H
                                    199 	.globl _UEP0_DMA_L
                                    200 	.globl _UEP0_DMA
                                    201 	.globl _UEP2_3_MOD
                                    202 	.globl _UEP4_1_MOD
                                    203 	.globl _UEP3_DMA_H
                                    204 	.globl _UEP3_DMA_L
                                    205 	.globl _UEP3_DMA
                                    206 	.globl _UEP2_DMA_H
                                    207 	.globl _UEP2_DMA_L
                                    208 	.globl _UEP2_DMA
                                    209 	.globl _USB_DEV_AD
                                    210 	.globl _USB_CTRL
                                    211 	.globl _USB_INT_EN
                                    212 	.globl _UEP4_T_LEN
                                    213 	.globl _UEP4_CTRL
                                    214 	.globl _UEP0_T_LEN
                                    215 	.globl _UEP0_CTRL
                                    216 	.globl _USB_RX_LEN
                                    217 	.globl _USB_MIS_ST
                                    218 	.globl _USB_INT_ST
                                    219 	.globl _USB_INT_FG
                                    220 	.globl _UEP3_T_LEN
                                    221 	.globl _UEP3_CTRL
                                    222 	.globl _UEP2_T_LEN
                                    223 	.globl _UEP2_CTRL
                                    224 	.globl _UEP1_T_LEN
                                    225 	.globl _UEP1_CTRL
                                    226 	.globl _UDEV_CTRL
                                    227 	.globl _USB_C_CTRL
                                    228 	.globl _ADC_PIN
                                    229 	.globl _ADC_CHAN
                                    230 	.globl _ADC_DAT_H
                                    231 	.globl _ADC_DAT_L
                                    232 	.globl _ADC_DAT
                                    233 	.globl _ADC_CFG
                                    234 	.globl _ADC_CTRL
                                    235 	.globl _TKEY_CTRL
                                    236 	.globl _SIF3
                                    237 	.globl _SBAUD3
                                    238 	.globl _SBUF3
                                    239 	.globl _SCON3
                                    240 	.globl _SIF2
                                    241 	.globl _SBAUD2
                                    242 	.globl _SBUF2
                                    243 	.globl _SCON2
                                    244 	.globl _SIF1
                                    245 	.globl _SBAUD1
                                    246 	.globl _SBUF1
                                    247 	.globl _SCON1
                                    248 	.globl _SPI0_SETUP
                                    249 	.globl _SPI0_CK_SE
                                    250 	.globl _SPI0_CTRL
                                    251 	.globl _SPI0_DATA
                                    252 	.globl _SPI0_STAT
                                    253 	.globl _PWM_DATA7
                                    254 	.globl _PWM_DATA6
                                    255 	.globl _PWM_DATA5
                                    256 	.globl _PWM_DATA4
                                    257 	.globl _PWM_DATA3
                                    258 	.globl _PWM_CTRL2
                                    259 	.globl _PWM_CK_SE
                                    260 	.globl _PWM_CTRL
                                    261 	.globl _PWM_DATA0
                                    262 	.globl _PWM_DATA1
                                    263 	.globl _PWM_DATA2
                                    264 	.globl _T2CAP1H
                                    265 	.globl _T2CAP1L
                                    266 	.globl _T2CAP1
                                    267 	.globl _TH2
                                    268 	.globl _TL2
                                    269 	.globl _T2COUNT
                                    270 	.globl _RCAP2H
                                    271 	.globl _RCAP2L
                                    272 	.globl _RCAP2
                                    273 	.globl _T2MOD
                                    274 	.globl _T2CON
                                    275 	.globl _T2CAP0H
                                    276 	.globl _T2CAP0L
                                    277 	.globl _T2CAP0
                                    278 	.globl _T2CON2
                                    279 	.globl _SBUF
                                    280 	.globl _SCON
                                    281 	.globl _TH1
                                    282 	.globl _TH0
                                    283 	.globl _TL1
                                    284 	.globl _TL0
                                    285 	.globl _TMOD
                                    286 	.globl _TCON
                                    287 	.globl _XBUS_AUX
                                    288 	.globl _PIN_FUNC
                                    289 	.globl _P5
                                    290 	.globl _P4_DIR_PU
                                    291 	.globl _P4_MOD_OC
                                    292 	.globl _P4
                                    293 	.globl _P3_DIR_PU
                                    294 	.globl _P3_MOD_OC
                                    295 	.globl _P3
                                    296 	.globl _P2_DIR_PU
                                    297 	.globl _P2_MOD_OC
                                    298 	.globl _P2
                                    299 	.globl _P1_DIR_PU
                                    300 	.globl _P1_MOD_OC
                                    301 	.globl _P1
                                    302 	.globl _P0_DIR_PU
                                    303 	.globl _P0_MOD_OC
                                    304 	.globl _P0
                                    305 	.globl _ROM_CTRL
                                    306 	.globl _ROM_DATA_HH
                                    307 	.globl _ROM_DATA_HL
                                    308 	.globl _ROM_DATA_HI
                                    309 	.globl _ROM_ADDR_H
                                    310 	.globl _ROM_ADDR_L
                                    311 	.globl _ROM_ADDR
                                    312 	.globl _GPIO_IE
                                    313 	.globl _INTX
                                    314 	.globl _IP_EX
                                    315 	.globl _IE_EX
                                    316 	.globl _IP
                                    317 	.globl _IE
                                    318 	.globl _WDOG_COUNT
                                    319 	.globl _RESET_KEEP
                                    320 	.globl _WAKE_CTRL
                                    321 	.globl _CLOCK_CFG
                                    322 	.globl _POWER_CFG
                                    323 	.globl _PCON
                                    324 	.globl _GLOBAL_CFG
                                    325 	.globl _SAFE_MOD
                                    326 	.globl _DPH
                                    327 	.globl _DPL
                                    328 	.globl _SP
                                    329 	.globl _A_INV
                                    330 	.globl _B
                                    331 	.globl _ACC
                                    332 	.globl _PSW
                                    333 	.globl _putchar
                                    334 ;--------------------------------------------------------
                                    335 ; special function registers
                                    336 ;--------------------------------------------------------
                                    337 	.area RSEG    (ABS,DATA)
      000000                        338 	.org 0x0000
                           0000D0   339 _PSW	=	0x00d0
                           0000E0   340 _ACC	=	0x00e0
                           0000F0   341 _B	=	0x00f0
                           0000FD   342 _A_INV	=	0x00fd
                           000081   343 _SP	=	0x0081
                           000082   344 _DPL	=	0x0082
                           000083   345 _DPH	=	0x0083
                           0000A1   346 _SAFE_MOD	=	0x00a1
                           0000B1   347 _GLOBAL_CFG	=	0x00b1
                           000087   348 _PCON	=	0x0087
                           0000BA   349 _POWER_CFG	=	0x00ba
                           0000B9   350 _CLOCK_CFG	=	0x00b9
                           0000A9   351 _WAKE_CTRL	=	0x00a9
                           0000FE   352 _RESET_KEEP	=	0x00fe
                           0000FF   353 _WDOG_COUNT	=	0x00ff
                           0000A8   354 _IE	=	0x00a8
                           0000B8   355 _IP	=	0x00b8
                           0000E8   356 _IE_EX	=	0x00e8
                           0000E9   357 _IP_EX	=	0x00e9
                           0000B3   358 _INTX	=	0x00b3
                           0000B2   359 _GPIO_IE	=	0x00b2
                           008584   360 _ROM_ADDR	=	0x8584
                           000084   361 _ROM_ADDR_L	=	0x0084
                           000085   362 _ROM_ADDR_H	=	0x0085
                           008F8E   363 _ROM_DATA_HI	=	0x8f8e
                           00008E   364 _ROM_DATA_HL	=	0x008e
                           00008F   365 _ROM_DATA_HH	=	0x008f
                           000086   366 _ROM_CTRL	=	0x0086
                           000080   367 _P0	=	0x0080
                           0000C4   368 _P0_MOD_OC	=	0x00c4
                           0000C5   369 _P0_DIR_PU	=	0x00c5
                           000090   370 _P1	=	0x0090
                           000092   371 _P1_MOD_OC	=	0x0092
                           000093   372 _P1_DIR_PU	=	0x0093
                           0000A0   373 _P2	=	0x00a0
                           000094   374 _P2_MOD_OC	=	0x0094
                           000095   375 _P2_DIR_PU	=	0x0095
                           0000B0   376 _P3	=	0x00b0
                           000096   377 _P3_MOD_OC	=	0x0096
                           000097   378 _P3_DIR_PU	=	0x0097
                           0000C0   379 _P4	=	0x00c0
                           0000C2   380 _P4_MOD_OC	=	0x00c2
                           0000C3   381 _P4_DIR_PU	=	0x00c3
                           0000AB   382 _P5	=	0x00ab
                           0000AA   383 _PIN_FUNC	=	0x00aa
                           0000A2   384 _XBUS_AUX	=	0x00a2
                           000088   385 _TCON	=	0x0088
                           000089   386 _TMOD	=	0x0089
                           00008A   387 _TL0	=	0x008a
                           00008B   388 _TL1	=	0x008b
                           00008C   389 _TH0	=	0x008c
                           00008D   390 _TH1	=	0x008d
                           000098   391 _SCON	=	0x0098
                           000099   392 _SBUF	=	0x0099
                           0000C1   393 _T2CON2	=	0x00c1
                           00C7C6   394 _T2CAP0	=	0xc7c6
                           0000C6   395 _T2CAP0L	=	0x00c6
                           0000C7   396 _T2CAP0H	=	0x00c7
                           0000C8   397 _T2CON	=	0x00c8
                           0000C9   398 _T2MOD	=	0x00c9
                           00CBCA   399 _RCAP2	=	0xcbca
                           0000CA   400 _RCAP2L	=	0x00ca
                           0000CB   401 _RCAP2H	=	0x00cb
                           00CDCC   402 _T2COUNT	=	0xcdcc
                           0000CC   403 _TL2	=	0x00cc
                           0000CD   404 _TH2	=	0x00cd
                           00CFCE   405 _T2CAP1	=	0xcfce
                           0000CE   406 _T2CAP1L	=	0x00ce
                           0000CF   407 _T2CAP1H	=	0x00cf
                           00009A   408 _PWM_DATA2	=	0x009a
                           00009B   409 _PWM_DATA1	=	0x009b
                           00009C   410 _PWM_DATA0	=	0x009c
                           00009D   411 _PWM_CTRL	=	0x009d
                           00009E   412 _PWM_CK_SE	=	0x009e
                           00009F   413 _PWM_CTRL2	=	0x009f
                           0000A3   414 _PWM_DATA3	=	0x00a3
                           0000A4   415 _PWM_DATA4	=	0x00a4
                           0000A5   416 _PWM_DATA5	=	0x00a5
                           0000A6   417 _PWM_DATA6	=	0x00a6
                           0000A7   418 _PWM_DATA7	=	0x00a7
                           0000F8   419 _SPI0_STAT	=	0x00f8
                           0000F9   420 _SPI0_DATA	=	0x00f9
                           0000FA   421 _SPI0_CTRL	=	0x00fa
                           0000FB   422 _SPI0_CK_SE	=	0x00fb
                           0000FC   423 _SPI0_SETUP	=	0x00fc
                           0000BC   424 _SCON1	=	0x00bc
                           0000BD   425 _SBUF1	=	0x00bd
                           0000BE   426 _SBAUD1	=	0x00be
                           0000BF   427 _SIF1	=	0x00bf
                           0000B4   428 _SCON2	=	0x00b4
                           0000B5   429 _SBUF2	=	0x00b5
                           0000B6   430 _SBAUD2	=	0x00b6
                           0000B7   431 _SIF2	=	0x00b7
                           0000AC   432 _SCON3	=	0x00ac
                           0000AD   433 _SBUF3	=	0x00ad
                           0000AE   434 _SBAUD3	=	0x00ae
                           0000AF   435 _SIF3	=	0x00af
                           0000F1   436 _TKEY_CTRL	=	0x00f1
                           0000F2   437 _ADC_CTRL	=	0x00f2
                           0000F3   438 _ADC_CFG	=	0x00f3
                           00F5F4   439 _ADC_DAT	=	0xf5f4
                           0000F4   440 _ADC_DAT_L	=	0x00f4
                           0000F5   441 _ADC_DAT_H	=	0x00f5
                           0000F6   442 _ADC_CHAN	=	0x00f6
                           0000F7   443 _ADC_PIN	=	0x00f7
                           000091   444 _USB_C_CTRL	=	0x0091
                           0000D1   445 _UDEV_CTRL	=	0x00d1
                           0000D2   446 _UEP1_CTRL	=	0x00d2
                           0000D3   447 _UEP1_T_LEN	=	0x00d3
                           0000D4   448 _UEP2_CTRL	=	0x00d4
                           0000D5   449 _UEP2_T_LEN	=	0x00d5
                           0000D6   450 _UEP3_CTRL	=	0x00d6
                           0000D7   451 _UEP3_T_LEN	=	0x00d7
                           0000D8   452 _USB_INT_FG	=	0x00d8
                           0000D9   453 _USB_INT_ST	=	0x00d9
                           0000DA   454 _USB_MIS_ST	=	0x00da
                           0000DB   455 _USB_RX_LEN	=	0x00db
                           0000DC   456 _UEP0_CTRL	=	0x00dc
                           0000DD   457 _UEP0_T_LEN	=	0x00dd
                           0000DE   458 _UEP4_CTRL	=	0x00de
                           0000DF   459 _UEP4_T_LEN	=	0x00df
                           0000E1   460 _USB_INT_EN	=	0x00e1
                           0000E2   461 _USB_CTRL	=	0x00e2
                           0000E3   462 _USB_DEV_AD	=	0x00e3
                           00E5E4   463 _UEP2_DMA	=	0xe5e4
                           0000E4   464 _UEP2_DMA_L	=	0x00e4
                           0000E5   465 _UEP2_DMA_H	=	0x00e5
                           00E7E6   466 _UEP3_DMA	=	0xe7e6
                           0000E6   467 _UEP3_DMA_L	=	0x00e6
                           0000E7   468 _UEP3_DMA_H	=	0x00e7
                           0000EA   469 _UEP4_1_MOD	=	0x00ea
                           0000EB   470 _UEP2_3_MOD	=	0x00eb
                           00EDEC   471 _UEP0_DMA	=	0xedec
                           0000EC   472 _UEP0_DMA_L	=	0x00ec
                           0000ED   473 _UEP0_DMA_H	=	0x00ed
                           00EFEE   474 _UEP1_DMA	=	0xefee
                           0000EE   475 _UEP1_DMA_L	=	0x00ee
                           0000EF   476 _UEP1_DMA_H	=	0x00ef
                                    477 ;--------------------------------------------------------
                                    478 ; special function bits
                                    479 ;--------------------------------------------------------
                                    480 	.area RSEG    (ABS,DATA)
      000000                        481 	.org 0x0000
                           0000D7   482 _CY	=	0x00d7
                           0000D6   483 _AC	=	0x00d6
                           0000D5   484 _F0	=	0x00d5
                           0000D4   485 _RS1	=	0x00d4
                           0000D3   486 _RS0	=	0x00d3
                           0000D2   487 _OV	=	0x00d2
                           0000D1   488 _F1	=	0x00d1
                           0000D0   489 _P	=	0x00d0
                           0000AF   490 _EA	=	0x00af
                           0000AE   491 _E_DIS	=	0x00ae
                           0000AD   492 _ET2	=	0x00ad
                           0000AC   493 _ES	=	0x00ac
                           0000AB   494 _ET1	=	0x00ab
                           0000AA   495 _EX1	=	0x00aa
                           0000A9   496 _ET0	=	0x00a9
                           0000A8   497 _EX0	=	0x00a8
                           0000BF   498 _PH_FLAG	=	0x00bf
                           0000BE   499 _PL_FLAG	=	0x00be
                           0000BD   500 _PT2	=	0x00bd
                           0000BC   501 _PS	=	0x00bc
                           0000BB   502 _PT1	=	0x00bb
                           0000BA   503 _PX1	=	0x00ba
                           0000B9   504 _PT0	=	0x00b9
                           0000B8   505 _PX0	=	0x00b8
                           0000EF   506 _IE_WDOG	=	0x00ef
                           0000EE   507 _IE_GPIO	=	0x00ee
                           0000ED   508 _IE_PWMX	=	0x00ed
                           0000ED   509 _IE_UART3	=	0x00ed
                           0000EC   510 _IE_UART1	=	0x00ec
                           0000EB   511 _IE_ADC	=	0x00eb
                           0000EB   512 _IE_UART2	=	0x00eb
                           0000EA   513 _IE_USB	=	0x00ea
                           0000E9   514 _IE_INT3	=	0x00e9
                           0000E8   515 _IE_SPI0	=	0x00e8
                           000087   516 _P0_7	=	0x0087
                           000086   517 _P0_6	=	0x0086
                           000085   518 _P0_5	=	0x0085
                           000084   519 _P0_4	=	0x0084
                           000083   520 _P0_3	=	0x0083
                           000082   521 _P0_2	=	0x0082
                           000081   522 _P0_1	=	0x0081
                           000080   523 _P0_0	=	0x0080
                           000087   524 _TXD3	=	0x0087
                           000087   525 _AIN15	=	0x0087
                           000086   526 _RXD3	=	0x0086
                           000086   527 _AIN14	=	0x0086
                           000085   528 _TXD2	=	0x0085
                           000085   529 _AIN13	=	0x0085
                           000084   530 _RXD2	=	0x0084
                           000084   531 _AIN12	=	0x0084
                           000083   532 _TXD_	=	0x0083
                           000083   533 _AIN11	=	0x0083
                           000082   534 _RXD_	=	0x0082
                           000082   535 _AIN10	=	0x0082
                           000081   536 _AIN9	=	0x0081
                           000080   537 _AIN8	=	0x0080
                           000097   538 _P1_7	=	0x0097
                           000096   539 _P1_6	=	0x0096
                           000095   540 _P1_5	=	0x0095
                           000094   541 _P1_4	=	0x0094
                           000093   542 _P1_3	=	0x0093
                           000092   543 _P1_2	=	0x0092
                           000091   544 _P1_1	=	0x0091
                           000090   545 _P1_0	=	0x0090
                           000097   546 _SCK	=	0x0097
                           000097   547 _TXD1_	=	0x0097
                           000097   548 _AIN7	=	0x0097
                           000096   549 _MISO	=	0x0096
                           000096   550 _RXD1_	=	0x0096
                           000096   551 _VBUS	=	0x0096
                           000096   552 _AIN6	=	0x0096
                           000095   553 _MOSI	=	0x0095
                           000095   554 _PWM0_	=	0x0095
                           000095   555 _UCC2	=	0x0095
                           000095   556 _AIN5	=	0x0095
                           000094   557 _SCS	=	0x0094
                           000094   558 _UCC1	=	0x0094
                           000094   559 _AIN4	=	0x0094
                           000093   560 _AIN3	=	0x0093
                           000092   561 _AIN2	=	0x0092
                           000091   562 _T2EX	=	0x0091
                           000091   563 _CAP2	=	0x0091
                           000091   564 _AIN1	=	0x0091
                           000090   565 _T2	=	0x0090
                           000090   566 _CAP1	=	0x0090
                           000090   567 _AIN0	=	0x0090
                           0000A7   568 _P2_7	=	0x00a7
                           0000A6   569 _P2_6	=	0x00a6
                           0000A5   570 _P2_5	=	0x00a5
                           0000A4   571 _P2_4	=	0x00a4
                           0000A3   572 _P2_3	=	0x00a3
                           0000A2   573 _P2_2	=	0x00a2
                           0000A1   574 _P2_1	=	0x00a1
                           0000A0   575 _P2_0	=	0x00a0
                           0000A7   576 _PWM7	=	0x00a7
                           0000A7   577 _TXD1	=	0x00a7
                           0000A6   578 _PWM6	=	0x00a6
                           0000A6   579 _RXD1	=	0x00a6
                           0000A5   580 _PWM0	=	0x00a5
                           0000A5   581 _T2EX_	=	0x00a5
                           0000A5   582 _CAP2_	=	0x00a5
                           0000A4   583 _PWM1	=	0x00a4
                           0000A4   584 _T2_	=	0x00a4
                           0000A4   585 _CAP1_	=	0x00a4
                           0000A3   586 _PWM2	=	0x00a3
                           0000A2   587 _PWM3	=	0x00a2
                           0000A2   588 _INT0_	=	0x00a2
                           0000A1   589 _PWM4	=	0x00a1
                           0000A0   590 _PWM5	=	0x00a0
                           0000B7   591 _P3_7	=	0x00b7
                           0000B6   592 _P3_6	=	0x00b6
                           0000B5   593 _P3_5	=	0x00b5
                           0000B4   594 _P3_4	=	0x00b4
                           0000B3   595 _P3_3	=	0x00b3
                           0000B2   596 _P3_2	=	0x00b2
                           0000B1   597 _P3_1	=	0x00b1
                           0000B0   598 _P3_0	=	0x00b0
                           0000B7   599 _INT3	=	0x00b7
                           0000B6   600 _CAP0	=	0x00b6
                           0000B5   601 _T1	=	0x00b5
                           0000B4   602 _T0	=	0x00b4
                           0000B3   603 _INT1	=	0x00b3
                           0000B2   604 _INT0	=	0x00b2
                           0000B1   605 _TXD	=	0x00b1
                           0000B0   606 _RXD	=	0x00b0
                           0000C6   607 _P4_6	=	0x00c6
                           0000C5   608 _P4_5	=	0x00c5
                           0000C4   609 _P4_4	=	0x00c4
                           0000C3   610 _P4_3	=	0x00c3
                           0000C2   611 _P4_2	=	0x00c2
                           0000C1   612 _P4_1	=	0x00c1
                           0000C0   613 _P4_0	=	0x00c0
                           0000C7   614 _XO	=	0x00c7
                           0000C6   615 _XI	=	0x00c6
                           00008F   616 _TF1	=	0x008f
                           00008E   617 _TR1	=	0x008e
                           00008D   618 _TF0	=	0x008d
                           00008C   619 _TR0	=	0x008c
                           00008B   620 _IE1	=	0x008b
                           00008A   621 _IT1	=	0x008a
                           000089   622 _IE0	=	0x0089
                           000088   623 _IT0	=	0x0088
                           00009F   624 _SM0	=	0x009f
                           00009E   625 _SM1	=	0x009e
                           00009D   626 _SM2	=	0x009d
                           00009C   627 _REN	=	0x009c
                           00009B   628 _TB8	=	0x009b
                           00009A   629 _RB8	=	0x009a
                           000099   630 _TI	=	0x0099
                           000098   631 _RI	=	0x0098
                           0000CF   632 _TF2	=	0x00cf
                           0000CF   633 _CAP1F	=	0x00cf
                           0000CE   634 _EXF2	=	0x00ce
                           0000CD   635 _RCLK	=	0x00cd
                           0000CC   636 _TCLK	=	0x00cc
                           0000CB   637 _EXEN2	=	0x00cb
                           0000CA   638 _TR2	=	0x00ca
                           0000C9   639 _C_T2	=	0x00c9
                           0000C8   640 _CP_RL2	=	0x00c8
                           0000FF   641 _S0_FST_ACT	=	0x00ff
                           0000FE   642 _S0_IF_OV	=	0x00fe
                           0000FD   643 _S0_IF_FIRST	=	0x00fd
                           0000FC   644 _S0_IF_BYTE	=	0x00fc
                           0000FB   645 _S0_FREE	=	0x00fb
                           0000FA   646 _S0_T_FIFO	=	0x00fa
                           0000F8   647 _S0_R_FIFO	=	0x00f8
                           0000DF   648 _U_IS_NAK	=	0x00df
                           0000DE   649 _U_TOG_OK	=	0x00de
                           0000DD   650 _U_SIE_FREE	=	0x00dd
                           0000DC   651 _UIF_FIFO_OV	=	0x00dc
                           0000DB   652 _UIF_HST_SOF	=	0x00db
                           0000DA   653 _UIF_SUSPEND	=	0x00da
                           0000D9   654 _UIF_TRANSFER	=	0x00d9
                           0000D8   655 _UIF_DETECT	=	0x00d8
                           0000D8   656 _UIF_BUS_RST	=	0x00d8
                                    657 ;--------------------------------------------------------
                                    658 ; overlayable register banks
                                    659 ;--------------------------------------------------------
                                    660 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        661 	.ds 8
                                    662 ;--------------------------------------------------------
                                    663 ; internal ram data
                                    664 ;--------------------------------------------------------
                                    665 	.area DSEG    (DATA)
                                    666 ;--------------------------------------------------------
                                    667 ; overlayable items in internal ram 
                                    668 ;--------------------------------------------------------
                                    669 ;--------------------------------------------------------
                                    670 ; Stack segment in internal ram 
                                    671 ;--------------------------------------------------------
                                    672 	.area	SSEG
      00004E                        673 __start__stack:
      00004E                        674 	.ds	1
                                    675 
                                    676 ;--------------------------------------------------------
                                    677 ; indirectly addressable internal ram data
                                    678 ;--------------------------------------------------------
                                    679 	.area ISEG    (DATA)
                                    680 ;--------------------------------------------------------
                                    681 ; absolute internal ram data
                                    682 ;--------------------------------------------------------
                                    683 	.area IABS    (ABS,DATA)
                                    684 	.area IABS    (ABS,DATA)
                                    685 ;--------------------------------------------------------
                                    686 ; bit data
                                    687 ;--------------------------------------------------------
                                    688 	.area BSEG    (BIT)
                                    689 ;--------------------------------------------------------
                                    690 ; paged external ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area PSEG    (PAG,XDATA)
                                    693 ;--------------------------------------------------------
                                    694 ; external ram data
                                    695 ;--------------------------------------------------------
                                    696 	.area XSEG    (XDATA)
                                    697 ;--------------------------------------------------------
                                    698 ; absolute external ram data
                                    699 ;--------------------------------------------------------
                                    700 	.area XABS    (ABS,XDATA)
                                    701 ;--------------------------------------------------------
                                    702 ; external initialized ram data
                                    703 ;--------------------------------------------------------
                                    704 	.area XISEG   (XDATA)
                                    705 	.area HOME    (CODE)
                                    706 	.area GSINIT0 (CODE)
                                    707 	.area GSINIT1 (CODE)
                                    708 	.area GSINIT2 (CODE)
                                    709 	.area GSINIT3 (CODE)
                                    710 	.area GSINIT4 (CODE)
                                    711 	.area GSINIT5 (CODE)
                                    712 	.area GSINIT  (CODE)
                                    713 	.area GSFINAL (CODE)
                                    714 	.area CSEG    (CODE)
                                    715 ;--------------------------------------------------------
                                    716 ; interrupt vector 
                                    717 ;--------------------------------------------------------
                                    718 	.area HOME    (CODE)
      000000                        719 __interrupt_vect:
      000000 02 00 61         [24]  720 	ljmp	__sdcc_gsinit_startup
      000003 32               [24]  721 	reti
      000004                        722 	.ds	7
      00000B 32               [24]  723 	reti
      00000C                        724 	.ds	7
      000013 32               [24]  725 	reti
      000014                        726 	.ds	7
      00001B 32               [24]  727 	reti
      00001C                        728 	.ds	7
      000023 32               [24]  729 	reti
      000024                        730 	.ds	7
      00002B 32               [24]  731 	reti
      00002C                        732 	.ds	7
      000033 32               [24]  733 	reti
      000034                        734 	.ds	7
      00003B 32               [24]  735 	reti
      00003C                        736 	.ds	7
      000043 32               [24]  737 	reti
      000044                        738 	.ds	7
      00004B 02 01 72         [24]  739 	ljmp	_UART2Interrupt
      00004E                        740 	.ds	5
      000053 02 00 F3         [24]  741 	ljmp	_UART1Interrupt
      000056                        742 	.ds	5
      00005B 02 01 F1         [24]  743 	ljmp	_UART3Interrupt
                                    744 ;--------------------------------------------------------
                                    745 ; global & static initialisations
                                    746 ;--------------------------------------------------------
                                    747 	.area HOME    (CODE)
                                    748 	.area GSINIT  (CODE)
                                    749 	.area GSFINAL (CODE)
                                    750 	.area GSINIT  (CODE)
                                    751 	.globl __sdcc_gsinit_startup
                                    752 	.globl __sdcc_program_startup
                                    753 	.globl __start__stack
                                    754 	.globl __mcs51_genXINIT
                                    755 	.globl __mcs51_genXRAMCLEAR
                                    756 	.globl __mcs51_genRAMCLEAR
                                    757 	.area GSFINAL (CODE)
      0000BA 02 00 5E         [24]  758 	ljmp	__sdcc_program_startup
                                    759 ;--------------------------------------------------------
                                    760 ; Home
                                    761 ;--------------------------------------------------------
                                    762 	.area HOME    (CODE)
                                    763 	.area HOME    (CODE)
      00005E                        764 __sdcc_program_startup:
      00005E 02 02 EF         [24]  765 	ljmp	_main
                                    766 ;	return from main will return to caller
                                    767 ;--------------------------------------------------------
                                    768 ; code
                                    769 ;--------------------------------------------------------
                                    770 	.area CSEG    (CODE)
                                    771 ;------------------------------------------------------------
                                    772 ;Allocation info for local variables in function 'main'
                                    773 ;------------------------------------------------------------
                                    774 ;dat                       Allocated with name '_main_dat_65536_48'
                                    775 ;------------------------------------------------------------
                                    776 ;	usr/main.c:30: void main( )
                                    777 ;	-----------------------------------------
                                    778 ;	 function main
                                    779 ;	-----------------------------------------
      0002EF                        780 _main:
                           000007   781 	ar7 = 0x07
                           000006   782 	ar6 = 0x06
                           000005   783 	ar5 = 0x05
                           000004   784 	ar4 = 0x04
                           000003   785 	ar3 = 0x03
                           000002   786 	ar2 = 0x02
                           000001   787 	ar1 = 0x01
                           000000   788 	ar0 = 0x00
                                    789 ;	usr/main.c:33: CfgFsys( );                                                                //CH549时钟选择配置
      0002EF 12 02 41         [24]  790 	lcall	_CfgFsys
                                    791 ;	usr/main.c:34: mDelaymS(20);
      0002F2 90 00 14         [24]  792 	mov	dptr,#0x0014
      0002F5 12 02 7D         [24]  793 	lcall	_mDelaymS
                                    794 ;	usr/main.c:35: CH549UART1Init();                                                          //串口1初始化
      0002F8 12 00 BD         [24]  795 	lcall	_CH549UART1Init
                                    796 ;	usr/main.c:36: CH549UART1Alter();                                                         //串口1引脚映射-将串口一映射到 p1.6 和 p1.7  P1.6（UART1引脚映射————RXD1_）（P1.7 UART1引脚映射————TXD1_）
      0002FB 12 00 D1         [24]  797 	lcall	_CH549UART1Alter
                                    798 ;	usr/main.c:37: CH549UART2Init();                                                          //串口2初始化
      0002FE 12 01 43         [24]  799 	lcall	_CH549UART2Init
                                    800 ;	usr/main.c:38: CH549UART3Init();                                                          //串口3初始化
      000301 12 01 C2         [24]  801 	lcall	_CH549UART3Init
                                    802 ;	usr/main.c:39: printf("hello world\n");                                                   //串口输出：“hello world”
      000304 74 5A            [12]  803 	mov	a,#___str_0
      000306 C0 E0            [24]  804 	push	acc
      000308 74 0C            [12]  805 	mov	a,#(___str_0 >> 8)
      00030A C0 E0            [24]  806 	push	acc
      00030C 74 80            [12]  807 	mov	a,#0x80
      00030E C0 E0            [24]  808 	push	acc
      000310 12 03 57         [24]  809 	lcall	_printf
      000313 15 81            [12]  810 	dec	sp
      000315 15 81            [12]  811 	dec	sp
      000317 15 81            [12]  812 	dec	sp
                                    813 ;	usr/main.c:40: while(1);
      000319                        814 00102$:
                                    815 ;	usr/main.c:41: }
      000319 80 FE            [24]  816 	sjmp	00102$
                                    817 ;------------------------------------------------------------
                                    818 ;Allocation info for local variables in function 'putchar'
                                    819 ;------------------------------------------------------------
                                    820 ;a                         Allocated to registers r6 r7 
                                    821 ;------------------------------------------------------------
                                    822 ;	usr/main.c:48: int putchar( int a)
                                    823 ;	-----------------------------------------
                                    824 ;	 function putchar
                                    825 ;	-----------------------------------------
      00031B                        826 _putchar:
                                    827 ;	usr/main.c:50: CH549UART1SendByte(a);                                                     //printf映射到串口1输出
      00031B AE 82            [24]  828 	mov	r6,dpl
      00031D AF 83            [24]  829 	mov	r7,dph
      00031F C0 07            [24]  830 	push	ar7
      000321 C0 06            [24]  831 	push	ar6
      000323 12 00 E7         [24]  832 	lcall	_CH549UART1SendByte
      000326 D0 06            [24]  833 	pop	ar6
      000328 D0 07            [24]  834 	pop	ar7
                                    835 ;	usr/main.c:51: return(a);
      00032A 8E 82            [24]  836 	mov	dpl,r6
      00032C 8F 83            [24]  837 	mov	dph,r7
                                    838 ;	usr/main.c:52: }
      00032E 22               [24]  839 	ret
                                    840 	.area CSEG    (CODE)
                                    841 	.area CONST   (CODE)
                                    842 	.area CONST   (CODE)
      000C5A                        843 ___str_0:
      000C5A 68 65 6C 6C 6F 20 77   844 	.ascii "hello world"
             6F 72 6C 64
      000C65 0A                     845 	.db 0x0a
      000C66 00                     846 	.db 0x00
                                    847 	.area CSEG    (CODE)
                                    848 	.area XINIT   (CODE)
                                    849 	.area CABS    (ABS,CODE)
