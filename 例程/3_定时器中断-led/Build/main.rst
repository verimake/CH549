                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _Timer0
                                     12 	.globl _main
                                     13 	.globl _Timer0Init
                                     14 	.globl _UIF_BUS_RST
                                     15 	.globl _UIF_DETECT
                                     16 	.globl _UIF_TRANSFER
                                     17 	.globl _UIF_SUSPEND
                                     18 	.globl _UIF_HST_SOF
                                     19 	.globl _UIF_FIFO_OV
                                     20 	.globl _U_SIE_FREE
                                     21 	.globl _U_TOG_OK
                                     22 	.globl _U_IS_NAK
                                     23 	.globl _S0_R_FIFO
                                     24 	.globl _S0_T_FIFO
                                     25 	.globl _S0_FREE
                                     26 	.globl _S0_IF_BYTE
                                     27 	.globl _S0_IF_FIRST
                                     28 	.globl _S0_IF_OV
                                     29 	.globl _S0_FST_ACT
                                     30 	.globl _CP_RL2
                                     31 	.globl _C_T2
                                     32 	.globl _TR2
                                     33 	.globl _EXEN2
                                     34 	.globl _TCLK
                                     35 	.globl _RCLK
                                     36 	.globl _EXF2
                                     37 	.globl _CAP1F
                                     38 	.globl _TF2
                                     39 	.globl _RI
                                     40 	.globl _TI
                                     41 	.globl _RB8
                                     42 	.globl _TB8
                                     43 	.globl _REN
                                     44 	.globl _SM2
                                     45 	.globl _SM1
                                     46 	.globl _SM0
                                     47 	.globl _IT0
                                     48 	.globl _IE0
                                     49 	.globl _IT1
                                     50 	.globl _IE1
                                     51 	.globl _TR0
                                     52 	.globl _TF0
                                     53 	.globl _TR1
                                     54 	.globl _TF1
                                     55 	.globl _XI
                                     56 	.globl _XO
                                     57 	.globl _P4_0
                                     58 	.globl _P4_1
                                     59 	.globl _P4_2
                                     60 	.globl _P4_3
                                     61 	.globl _P4_4
                                     62 	.globl _P4_5
                                     63 	.globl _P4_6
                                     64 	.globl _RXD
                                     65 	.globl _TXD
                                     66 	.globl _INT0
                                     67 	.globl _INT1
                                     68 	.globl _T0
                                     69 	.globl _T1
                                     70 	.globl _CAP0
                                     71 	.globl _INT3
                                     72 	.globl _P3_0
                                     73 	.globl _P3_1
                                     74 	.globl _P3_2
                                     75 	.globl _P3_3
                                     76 	.globl _P3_4
                                     77 	.globl _P3_5
                                     78 	.globl _P3_6
                                     79 	.globl _P3_7
                                     80 	.globl _PWM5
                                     81 	.globl _PWM4
                                     82 	.globl _INT0_
                                     83 	.globl _PWM3
                                     84 	.globl _PWM2
                                     85 	.globl _CAP1_
                                     86 	.globl _T2_
                                     87 	.globl _PWM1
                                     88 	.globl _CAP2_
                                     89 	.globl _T2EX_
                                     90 	.globl _PWM0
                                     91 	.globl _RXD1
                                     92 	.globl _PWM6
                                     93 	.globl _TXD1
                                     94 	.globl _PWM7
                                     95 	.globl _P2_0
                                     96 	.globl _P2_1
                                     97 	.globl _P2_2
                                     98 	.globl _P2_3
                                     99 	.globl _P2_4
                                    100 	.globl _P2_5
                                    101 	.globl _P2_6
                                    102 	.globl _P2_7
                                    103 	.globl _AIN0
                                    104 	.globl _CAP1
                                    105 	.globl _T2
                                    106 	.globl _AIN1
                                    107 	.globl _CAP2
                                    108 	.globl _T2EX
                                    109 	.globl _AIN2
                                    110 	.globl _AIN3
                                    111 	.globl _AIN4
                                    112 	.globl _UCC1
                                    113 	.globl _SCS
                                    114 	.globl _AIN5
                                    115 	.globl _UCC2
                                    116 	.globl _PWM0_
                                    117 	.globl _MOSI
                                    118 	.globl _AIN6
                                    119 	.globl _VBUS
                                    120 	.globl _RXD1_
                                    121 	.globl _MISO
                                    122 	.globl _AIN7
                                    123 	.globl _TXD1_
                                    124 	.globl _SCK
                                    125 	.globl _P1_0
                                    126 	.globl _P1_1
                                    127 	.globl _P1_2
                                    128 	.globl _P1_3
                                    129 	.globl _P1_4
                                    130 	.globl _P1_5
                                    131 	.globl _P1_6
                                    132 	.globl _P1_7
                                    133 	.globl _AIN8
                                    134 	.globl _AIN9
                                    135 	.globl _AIN10
                                    136 	.globl _RXD_
                                    137 	.globl _AIN11
                                    138 	.globl _TXD_
                                    139 	.globl _AIN12
                                    140 	.globl _RXD2
                                    141 	.globl _AIN13
                                    142 	.globl _TXD2
                                    143 	.globl _AIN14
                                    144 	.globl _RXD3
                                    145 	.globl _AIN15
                                    146 	.globl _TXD3
                                    147 	.globl _P0_0
                                    148 	.globl _P0_1
                                    149 	.globl _P0_2
                                    150 	.globl _P0_3
                                    151 	.globl _P0_4
                                    152 	.globl _P0_5
                                    153 	.globl _P0_6
                                    154 	.globl _P0_7
                                    155 	.globl _IE_SPI0
                                    156 	.globl _IE_INT3
                                    157 	.globl _IE_USB
                                    158 	.globl _IE_UART2
                                    159 	.globl _IE_ADC
                                    160 	.globl _IE_UART1
                                    161 	.globl _IE_UART3
                                    162 	.globl _IE_PWMX
                                    163 	.globl _IE_GPIO
                                    164 	.globl _IE_WDOG
                                    165 	.globl _PX0
                                    166 	.globl _PT0
                                    167 	.globl _PX1
                                    168 	.globl _PT1
                                    169 	.globl _PS
                                    170 	.globl _PT2
                                    171 	.globl _PL_FLAG
                                    172 	.globl _PH_FLAG
                                    173 	.globl _EX0
                                    174 	.globl _ET0
                                    175 	.globl _EX1
                                    176 	.globl _ET1
                                    177 	.globl _ES
                                    178 	.globl _ET2
                                    179 	.globl _E_DIS
                                    180 	.globl _EA
                                    181 	.globl _P
                                    182 	.globl _F1
                                    183 	.globl _OV
                                    184 	.globl _RS0
                                    185 	.globl _RS1
                                    186 	.globl _F0
                                    187 	.globl _AC
                                    188 	.globl _CY
                                    189 	.globl _UEP1_DMA_H
                                    190 	.globl _UEP1_DMA_L
                                    191 	.globl _UEP1_DMA
                                    192 	.globl _UEP0_DMA_H
                                    193 	.globl _UEP0_DMA_L
                                    194 	.globl _UEP0_DMA
                                    195 	.globl _UEP2_3_MOD
                                    196 	.globl _UEP4_1_MOD
                                    197 	.globl _UEP3_DMA_H
                                    198 	.globl _UEP3_DMA_L
                                    199 	.globl _UEP3_DMA
                                    200 	.globl _UEP2_DMA_H
                                    201 	.globl _UEP2_DMA_L
                                    202 	.globl _UEP2_DMA
                                    203 	.globl _USB_DEV_AD
                                    204 	.globl _USB_CTRL
                                    205 	.globl _USB_INT_EN
                                    206 	.globl _UEP4_T_LEN
                                    207 	.globl _UEP4_CTRL
                                    208 	.globl _UEP0_T_LEN
                                    209 	.globl _UEP0_CTRL
                                    210 	.globl _USB_RX_LEN
                                    211 	.globl _USB_MIS_ST
                                    212 	.globl _USB_INT_ST
                                    213 	.globl _USB_INT_FG
                                    214 	.globl _UEP3_T_LEN
                                    215 	.globl _UEP3_CTRL
                                    216 	.globl _UEP2_T_LEN
                                    217 	.globl _UEP2_CTRL
                                    218 	.globl _UEP1_T_LEN
                                    219 	.globl _UEP1_CTRL
                                    220 	.globl _UDEV_CTRL
                                    221 	.globl _USB_C_CTRL
                                    222 	.globl _ADC_PIN
                                    223 	.globl _ADC_CHAN
                                    224 	.globl _ADC_DAT_H
                                    225 	.globl _ADC_DAT_L
                                    226 	.globl _ADC_DAT
                                    227 	.globl _ADC_CFG
                                    228 	.globl _ADC_CTRL
                                    229 	.globl _TKEY_CTRL
                                    230 	.globl _SIF3
                                    231 	.globl _SBAUD3
                                    232 	.globl _SBUF3
                                    233 	.globl _SCON3
                                    234 	.globl _SIF2
                                    235 	.globl _SBAUD2
                                    236 	.globl _SBUF2
                                    237 	.globl _SCON2
                                    238 	.globl _SIF1
                                    239 	.globl _SBAUD1
                                    240 	.globl _SBUF1
                                    241 	.globl _SCON1
                                    242 	.globl _SPI0_SETUP
                                    243 	.globl _SPI0_CK_SE
                                    244 	.globl _SPI0_CTRL
                                    245 	.globl _SPI0_DATA
                                    246 	.globl _SPI0_STAT
                                    247 	.globl _PWM_DATA7
                                    248 	.globl _PWM_DATA6
                                    249 	.globl _PWM_DATA5
                                    250 	.globl _PWM_DATA4
                                    251 	.globl _PWM_DATA3
                                    252 	.globl _PWM_CTRL2
                                    253 	.globl _PWM_CK_SE
                                    254 	.globl _PWM_CTRL
                                    255 	.globl _PWM_DATA0
                                    256 	.globl _PWM_DATA1
                                    257 	.globl _PWM_DATA2
                                    258 	.globl _T2CAP1H
                                    259 	.globl _T2CAP1L
                                    260 	.globl _T2CAP1
                                    261 	.globl _TH2
                                    262 	.globl _TL2
                                    263 	.globl _T2COUNT
                                    264 	.globl _RCAP2H
                                    265 	.globl _RCAP2L
                                    266 	.globl _RCAP2
                                    267 	.globl _T2MOD
                                    268 	.globl _T2CON
                                    269 	.globl _T2CAP0H
                                    270 	.globl _T2CAP0L
                                    271 	.globl _T2CAP0
                                    272 	.globl _T2CON2
                                    273 	.globl _SBUF
                                    274 	.globl _SCON
                                    275 	.globl _TH1
                                    276 	.globl _TH0
                                    277 	.globl _TL1
                                    278 	.globl _TL0
                                    279 	.globl _TMOD
                                    280 	.globl _TCON
                                    281 	.globl _XBUS_AUX
                                    282 	.globl _PIN_FUNC
                                    283 	.globl _P5
                                    284 	.globl _P4_DIR_PU
                                    285 	.globl _P4_MOD_OC
                                    286 	.globl _P4
                                    287 	.globl _P3_DIR_PU
                                    288 	.globl _P3_MOD_OC
                                    289 	.globl _P3
                                    290 	.globl _P2_DIR_PU
                                    291 	.globl _P2_MOD_OC
                                    292 	.globl _P2
                                    293 	.globl _P1_DIR_PU
                                    294 	.globl _P1_MOD_OC
                                    295 	.globl _P1
                                    296 	.globl _P0_DIR_PU
                                    297 	.globl _P0_MOD_OC
                                    298 	.globl _P0
                                    299 	.globl _ROM_CTRL
                                    300 	.globl _ROM_DATA_HH
                                    301 	.globl _ROM_DATA_HL
                                    302 	.globl _ROM_DATA_HI
                                    303 	.globl _ROM_ADDR_H
                                    304 	.globl _ROM_ADDR_L
                                    305 	.globl _ROM_ADDR
                                    306 	.globl _GPIO_IE
                                    307 	.globl _INTX
                                    308 	.globl _IP_EX
                                    309 	.globl _IE_EX
                                    310 	.globl _IP
                                    311 	.globl _IE
                                    312 	.globl _WDOG_COUNT
                                    313 	.globl _RESET_KEEP
                                    314 	.globl _WAKE_CTRL
                                    315 	.globl _CLOCK_CFG
                                    316 	.globl _POWER_CFG
                                    317 	.globl _PCON
                                    318 	.globl _GLOBAL_CFG
                                    319 	.globl _SAFE_MOD
                                    320 	.globl _DPH
                                    321 	.globl _DPL
                                    322 	.globl _SP
                                    323 	.globl _A_INV
                                    324 	.globl _B
                                    325 	.globl _ACC
                                    326 	.globl _PSW
                                    327 ;--------------------------------------------------------
                                    328 ; special function registers
                                    329 ;--------------------------------------------------------
                                    330 	.area RSEG    (ABS,DATA)
      000000                        331 	.org 0x0000
                           0000D0   332 _PSW	=	0x00d0
                           0000E0   333 _ACC	=	0x00e0
                           0000F0   334 _B	=	0x00f0
                           0000FD   335 _A_INV	=	0x00fd
                           000081   336 _SP	=	0x0081
                           000082   337 _DPL	=	0x0082
                           000083   338 _DPH	=	0x0083
                           0000A1   339 _SAFE_MOD	=	0x00a1
                           0000B1   340 _GLOBAL_CFG	=	0x00b1
                           000087   341 _PCON	=	0x0087
                           0000BA   342 _POWER_CFG	=	0x00ba
                           0000B9   343 _CLOCK_CFG	=	0x00b9
                           0000A9   344 _WAKE_CTRL	=	0x00a9
                           0000FE   345 _RESET_KEEP	=	0x00fe
                           0000FF   346 _WDOG_COUNT	=	0x00ff
                           0000A8   347 _IE	=	0x00a8
                           0000B8   348 _IP	=	0x00b8
                           0000E8   349 _IE_EX	=	0x00e8
                           0000E9   350 _IP_EX	=	0x00e9
                           0000B3   351 _INTX	=	0x00b3
                           0000B2   352 _GPIO_IE	=	0x00b2
                           008584   353 _ROM_ADDR	=	0x8584
                           000084   354 _ROM_ADDR_L	=	0x0084
                           000085   355 _ROM_ADDR_H	=	0x0085
                           008F8E   356 _ROM_DATA_HI	=	0x8f8e
                           00008E   357 _ROM_DATA_HL	=	0x008e
                           00008F   358 _ROM_DATA_HH	=	0x008f
                           000086   359 _ROM_CTRL	=	0x0086
                           000080   360 _P0	=	0x0080
                           0000C4   361 _P0_MOD_OC	=	0x00c4
                           0000C5   362 _P0_DIR_PU	=	0x00c5
                           000090   363 _P1	=	0x0090
                           000092   364 _P1_MOD_OC	=	0x0092
                           000093   365 _P1_DIR_PU	=	0x0093
                           0000A0   366 _P2	=	0x00a0
                           000094   367 _P2_MOD_OC	=	0x0094
                           000095   368 _P2_DIR_PU	=	0x0095
                           0000B0   369 _P3	=	0x00b0
                           000096   370 _P3_MOD_OC	=	0x0096
                           000097   371 _P3_DIR_PU	=	0x0097
                           0000C0   372 _P4	=	0x00c0
                           0000C2   373 _P4_MOD_OC	=	0x00c2
                           0000C3   374 _P4_DIR_PU	=	0x00c3
                           0000AB   375 _P5	=	0x00ab
                           0000AA   376 _PIN_FUNC	=	0x00aa
                           0000A2   377 _XBUS_AUX	=	0x00a2
                           000088   378 _TCON	=	0x0088
                           000089   379 _TMOD	=	0x0089
                           00008A   380 _TL0	=	0x008a
                           00008B   381 _TL1	=	0x008b
                           00008C   382 _TH0	=	0x008c
                           00008D   383 _TH1	=	0x008d
                           000098   384 _SCON	=	0x0098
                           000099   385 _SBUF	=	0x0099
                           0000C1   386 _T2CON2	=	0x00c1
                           00C7C6   387 _T2CAP0	=	0xc7c6
                           0000C6   388 _T2CAP0L	=	0x00c6
                           0000C7   389 _T2CAP0H	=	0x00c7
                           0000C8   390 _T2CON	=	0x00c8
                           0000C9   391 _T2MOD	=	0x00c9
                           00CBCA   392 _RCAP2	=	0xcbca
                           0000CA   393 _RCAP2L	=	0x00ca
                           0000CB   394 _RCAP2H	=	0x00cb
                           00CDCC   395 _T2COUNT	=	0xcdcc
                           0000CC   396 _TL2	=	0x00cc
                           0000CD   397 _TH2	=	0x00cd
                           00CFCE   398 _T2CAP1	=	0xcfce
                           0000CE   399 _T2CAP1L	=	0x00ce
                           0000CF   400 _T2CAP1H	=	0x00cf
                           00009A   401 _PWM_DATA2	=	0x009a
                           00009B   402 _PWM_DATA1	=	0x009b
                           00009C   403 _PWM_DATA0	=	0x009c
                           00009D   404 _PWM_CTRL	=	0x009d
                           00009E   405 _PWM_CK_SE	=	0x009e
                           00009F   406 _PWM_CTRL2	=	0x009f
                           0000A3   407 _PWM_DATA3	=	0x00a3
                           0000A4   408 _PWM_DATA4	=	0x00a4
                           0000A5   409 _PWM_DATA5	=	0x00a5
                           0000A6   410 _PWM_DATA6	=	0x00a6
                           0000A7   411 _PWM_DATA7	=	0x00a7
                           0000F8   412 _SPI0_STAT	=	0x00f8
                           0000F9   413 _SPI0_DATA	=	0x00f9
                           0000FA   414 _SPI0_CTRL	=	0x00fa
                           0000FB   415 _SPI0_CK_SE	=	0x00fb
                           0000FC   416 _SPI0_SETUP	=	0x00fc
                           0000BC   417 _SCON1	=	0x00bc
                           0000BD   418 _SBUF1	=	0x00bd
                           0000BE   419 _SBAUD1	=	0x00be
                           0000BF   420 _SIF1	=	0x00bf
                           0000B4   421 _SCON2	=	0x00b4
                           0000B5   422 _SBUF2	=	0x00b5
                           0000B6   423 _SBAUD2	=	0x00b6
                           0000B7   424 _SIF2	=	0x00b7
                           0000AC   425 _SCON3	=	0x00ac
                           0000AD   426 _SBUF3	=	0x00ad
                           0000AE   427 _SBAUD3	=	0x00ae
                           0000AF   428 _SIF3	=	0x00af
                           0000F1   429 _TKEY_CTRL	=	0x00f1
                           0000F2   430 _ADC_CTRL	=	0x00f2
                           0000F3   431 _ADC_CFG	=	0x00f3
                           00F5F4   432 _ADC_DAT	=	0xf5f4
                           0000F4   433 _ADC_DAT_L	=	0x00f4
                           0000F5   434 _ADC_DAT_H	=	0x00f5
                           0000F6   435 _ADC_CHAN	=	0x00f6
                           0000F7   436 _ADC_PIN	=	0x00f7
                           000091   437 _USB_C_CTRL	=	0x0091
                           0000D1   438 _UDEV_CTRL	=	0x00d1
                           0000D2   439 _UEP1_CTRL	=	0x00d2
                           0000D3   440 _UEP1_T_LEN	=	0x00d3
                           0000D4   441 _UEP2_CTRL	=	0x00d4
                           0000D5   442 _UEP2_T_LEN	=	0x00d5
                           0000D6   443 _UEP3_CTRL	=	0x00d6
                           0000D7   444 _UEP3_T_LEN	=	0x00d7
                           0000D8   445 _USB_INT_FG	=	0x00d8
                           0000D9   446 _USB_INT_ST	=	0x00d9
                           0000DA   447 _USB_MIS_ST	=	0x00da
                           0000DB   448 _USB_RX_LEN	=	0x00db
                           0000DC   449 _UEP0_CTRL	=	0x00dc
                           0000DD   450 _UEP0_T_LEN	=	0x00dd
                           0000DE   451 _UEP4_CTRL	=	0x00de
                           0000DF   452 _UEP4_T_LEN	=	0x00df
                           0000E1   453 _USB_INT_EN	=	0x00e1
                           0000E2   454 _USB_CTRL	=	0x00e2
                           0000E3   455 _USB_DEV_AD	=	0x00e3
                           00E5E4   456 _UEP2_DMA	=	0xe5e4
                           0000E4   457 _UEP2_DMA_L	=	0x00e4
                           0000E5   458 _UEP2_DMA_H	=	0x00e5
                           00E7E6   459 _UEP3_DMA	=	0xe7e6
                           0000E6   460 _UEP3_DMA_L	=	0x00e6
                           0000E7   461 _UEP3_DMA_H	=	0x00e7
                           0000EA   462 _UEP4_1_MOD	=	0x00ea
                           0000EB   463 _UEP2_3_MOD	=	0x00eb
                           00EDEC   464 _UEP0_DMA	=	0xedec
                           0000EC   465 _UEP0_DMA_L	=	0x00ec
                           0000ED   466 _UEP0_DMA_H	=	0x00ed
                           00EFEE   467 _UEP1_DMA	=	0xefee
                           0000EE   468 _UEP1_DMA_L	=	0x00ee
                           0000EF   469 _UEP1_DMA_H	=	0x00ef
                                    470 ;--------------------------------------------------------
                                    471 ; special function bits
                                    472 ;--------------------------------------------------------
                                    473 	.area RSEG    (ABS,DATA)
      000000                        474 	.org 0x0000
                           0000D7   475 _CY	=	0x00d7
                           0000D6   476 _AC	=	0x00d6
                           0000D5   477 _F0	=	0x00d5
                           0000D4   478 _RS1	=	0x00d4
                           0000D3   479 _RS0	=	0x00d3
                           0000D2   480 _OV	=	0x00d2
                           0000D1   481 _F1	=	0x00d1
                           0000D0   482 _P	=	0x00d0
                           0000AF   483 _EA	=	0x00af
                           0000AE   484 _E_DIS	=	0x00ae
                           0000AD   485 _ET2	=	0x00ad
                           0000AC   486 _ES	=	0x00ac
                           0000AB   487 _ET1	=	0x00ab
                           0000AA   488 _EX1	=	0x00aa
                           0000A9   489 _ET0	=	0x00a9
                           0000A8   490 _EX0	=	0x00a8
                           0000BF   491 _PH_FLAG	=	0x00bf
                           0000BE   492 _PL_FLAG	=	0x00be
                           0000BD   493 _PT2	=	0x00bd
                           0000BC   494 _PS	=	0x00bc
                           0000BB   495 _PT1	=	0x00bb
                           0000BA   496 _PX1	=	0x00ba
                           0000B9   497 _PT0	=	0x00b9
                           0000B8   498 _PX0	=	0x00b8
                           0000EF   499 _IE_WDOG	=	0x00ef
                           0000EE   500 _IE_GPIO	=	0x00ee
                           0000ED   501 _IE_PWMX	=	0x00ed
                           0000ED   502 _IE_UART3	=	0x00ed
                           0000EC   503 _IE_UART1	=	0x00ec
                           0000EB   504 _IE_ADC	=	0x00eb
                           0000EB   505 _IE_UART2	=	0x00eb
                           0000EA   506 _IE_USB	=	0x00ea
                           0000E9   507 _IE_INT3	=	0x00e9
                           0000E8   508 _IE_SPI0	=	0x00e8
                           000087   509 _P0_7	=	0x0087
                           000086   510 _P0_6	=	0x0086
                           000085   511 _P0_5	=	0x0085
                           000084   512 _P0_4	=	0x0084
                           000083   513 _P0_3	=	0x0083
                           000082   514 _P0_2	=	0x0082
                           000081   515 _P0_1	=	0x0081
                           000080   516 _P0_0	=	0x0080
                           000087   517 _TXD3	=	0x0087
                           000087   518 _AIN15	=	0x0087
                           000086   519 _RXD3	=	0x0086
                           000086   520 _AIN14	=	0x0086
                           000085   521 _TXD2	=	0x0085
                           000085   522 _AIN13	=	0x0085
                           000084   523 _RXD2	=	0x0084
                           000084   524 _AIN12	=	0x0084
                           000083   525 _TXD_	=	0x0083
                           000083   526 _AIN11	=	0x0083
                           000082   527 _RXD_	=	0x0082
                           000082   528 _AIN10	=	0x0082
                           000081   529 _AIN9	=	0x0081
                           000080   530 _AIN8	=	0x0080
                           000097   531 _P1_7	=	0x0097
                           000096   532 _P1_6	=	0x0096
                           000095   533 _P1_5	=	0x0095
                           000094   534 _P1_4	=	0x0094
                           000093   535 _P1_3	=	0x0093
                           000092   536 _P1_2	=	0x0092
                           000091   537 _P1_1	=	0x0091
                           000090   538 _P1_0	=	0x0090
                           000097   539 _SCK	=	0x0097
                           000097   540 _TXD1_	=	0x0097
                           000097   541 _AIN7	=	0x0097
                           000096   542 _MISO	=	0x0096
                           000096   543 _RXD1_	=	0x0096
                           000096   544 _VBUS	=	0x0096
                           000096   545 _AIN6	=	0x0096
                           000095   546 _MOSI	=	0x0095
                           000095   547 _PWM0_	=	0x0095
                           000095   548 _UCC2	=	0x0095
                           000095   549 _AIN5	=	0x0095
                           000094   550 _SCS	=	0x0094
                           000094   551 _UCC1	=	0x0094
                           000094   552 _AIN4	=	0x0094
                           000093   553 _AIN3	=	0x0093
                           000092   554 _AIN2	=	0x0092
                           000091   555 _T2EX	=	0x0091
                           000091   556 _CAP2	=	0x0091
                           000091   557 _AIN1	=	0x0091
                           000090   558 _T2	=	0x0090
                           000090   559 _CAP1	=	0x0090
                           000090   560 _AIN0	=	0x0090
                           0000A7   561 _P2_7	=	0x00a7
                           0000A6   562 _P2_6	=	0x00a6
                           0000A5   563 _P2_5	=	0x00a5
                           0000A4   564 _P2_4	=	0x00a4
                           0000A3   565 _P2_3	=	0x00a3
                           0000A2   566 _P2_2	=	0x00a2
                           0000A1   567 _P2_1	=	0x00a1
                           0000A0   568 _P2_0	=	0x00a0
                           0000A7   569 _PWM7	=	0x00a7
                           0000A7   570 _TXD1	=	0x00a7
                           0000A6   571 _PWM6	=	0x00a6
                           0000A6   572 _RXD1	=	0x00a6
                           0000A5   573 _PWM0	=	0x00a5
                           0000A5   574 _T2EX_	=	0x00a5
                           0000A5   575 _CAP2_	=	0x00a5
                           0000A4   576 _PWM1	=	0x00a4
                           0000A4   577 _T2_	=	0x00a4
                           0000A4   578 _CAP1_	=	0x00a4
                           0000A3   579 _PWM2	=	0x00a3
                           0000A2   580 _PWM3	=	0x00a2
                           0000A2   581 _INT0_	=	0x00a2
                           0000A1   582 _PWM4	=	0x00a1
                           0000A0   583 _PWM5	=	0x00a0
                           0000B7   584 _P3_7	=	0x00b7
                           0000B6   585 _P3_6	=	0x00b6
                           0000B5   586 _P3_5	=	0x00b5
                           0000B4   587 _P3_4	=	0x00b4
                           0000B3   588 _P3_3	=	0x00b3
                           0000B2   589 _P3_2	=	0x00b2
                           0000B1   590 _P3_1	=	0x00b1
                           0000B0   591 _P3_0	=	0x00b0
                           0000B7   592 _INT3	=	0x00b7
                           0000B6   593 _CAP0	=	0x00b6
                           0000B5   594 _T1	=	0x00b5
                           0000B4   595 _T0	=	0x00b4
                           0000B3   596 _INT1	=	0x00b3
                           0000B2   597 _INT0	=	0x00b2
                           0000B1   598 _TXD	=	0x00b1
                           0000B0   599 _RXD	=	0x00b0
                           0000C6   600 _P4_6	=	0x00c6
                           0000C5   601 _P4_5	=	0x00c5
                           0000C4   602 _P4_4	=	0x00c4
                           0000C3   603 _P4_3	=	0x00c3
                           0000C2   604 _P4_2	=	0x00c2
                           0000C1   605 _P4_1	=	0x00c1
                           0000C0   606 _P4_0	=	0x00c0
                           0000C7   607 _XO	=	0x00c7
                           0000C6   608 _XI	=	0x00c6
                           00008F   609 _TF1	=	0x008f
                           00008E   610 _TR1	=	0x008e
                           00008D   611 _TF0	=	0x008d
                           00008C   612 _TR0	=	0x008c
                           00008B   613 _IE1	=	0x008b
                           00008A   614 _IT1	=	0x008a
                           000089   615 _IE0	=	0x0089
                           000088   616 _IT0	=	0x0088
                           00009F   617 _SM0	=	0x009f
                           00009E   618 _SM1	=	0x009e
                           00009D   619 _SM2	=	0x009d
                           00009C   620 _REN	=	0x009c
                           00009B   621 _TB8	=	0x009b
                           00009A   622 _RB8	=	0x009a
                           000099   623 _TI	=	0x0099
                           000098   624 _RI	=	0x0098
                           0000CF   625 _TF2	=	0x00cf
                           0000CF   626 _CAP1F	=	0x00cf
                           0000CE   627 _EXF2	=	0x00ce
                           0000CD   628 _RCLK	=	0x00cd
                           0000CC   629 _TCLK	=	0x00cc
                           0000CB   630 _EXEN2	=	0x00cb
                           0000CA   631 _TR2	=	0x00ca
                           0000C9   632 _C_T2	=	0x00c9
                           0000C8   633 _CP_RL2	=	0x00c8
                           0000FF   634 _S0_FST_ACT	=	0x00ff
                           0000FE   635 _S0_IF_OV	=	0x00fe
                           0000FD   636 _S0_IF_FIRST	=	0x00fd
                           0000FC   637 _S0_IF_BYTE	=	0x00fc
                           0000FB   638 _S0_FREE	=	0x00fb
                           0000FA   639 _S0_T_FIFO	=	0x00fa
                           0000F8   640 _S0_R_FIFO	=	0x00f8
                           0000DF   641 _U_IS_NAK	=	0x00df
                           0000DE   642 _U_TOG_OK	=	0x00de
                           0000DD   643 _U_SIE_FREE	=	0x00dd
                           0000DC   644 _UIF_FIFO_OV	=	0x00dc
                           0000DB   645 _UIF_HST_SOF	=	0x00db
                           0000DA   646 _UIF_SUSPEND	=	0x00da
                           0000D9   647 _UIF_TRANSFER	=	0x00d9
                           0000D8   648 _UIF_DETECT	=	0x00d8
                           0000D8   649 _UIF_BUS_RST	=	0x00d8
                                    650 ;--------------------------------------------------------
                                    651 ; overlayable register banks
                                    652 ;--------------------------------------------------------
                                    653 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        654 	.ds 8
                                    655 ;--------------------------------------------------------
                                    656 ; internal ram data
                                    657 ;--------------------------------------------------------
                                    658 	.area DSEG    (DATA)
      000008                        659 _Timer0_i_65537_4:
      000008                        660 	.ds 2
                                    661 ;--------------------------------------------------------
                                    662 ; overlayable items in internal ram 
                                    663 ;--------------------------------------------------------
                                    664 ;--------------------------------------------------------
                                    665 ; Stack segment in internal ram 
                                    666 ;--------------------------------------------------------
                                    667 	.area	SSEG
      00000A                        668 __start__stack:
      00000A                        669 	.ds	1
                                    670 
                                    671 ;--------------------------------------------------------
                                    672 ; indirectly addressable internal ram data
                                    673 ;--------------------------------------------------------
                                    674 	.area ISEG    (DATA)
                                    675 ;--------------------------------------------------------
                                    676 ; absolute internal ram data
                                    677 ;--------------------------------------------------------
                                    678 	.area IABS    (ABS,DATA)
                                    679 	.area IABS    (ABS,DATA)
                                    680 ;--------------------------------------------------------
                                    681 ; bit data
                                    682 ;--------------------------------------------------------
                                    683 	.area BSEG    (BIT)
                                    684 ;--------------------------------------------------------
                                    685 ; paged external ram data
                                    686 ;--------------------------------------------------------
                                    687 	.area PSEG    (PAG,XDATA)
                                    688 ;--------------------------------------------------------
                                    689 ; external ram data
                                    690 ;--------------------------------------------------------
                                    691 	.area XSEG    (XDATA)
                                    692 ;--------------------------------------------------------
                                    693 ; absolute external ram data
                                    694 ;--------------------------------------------------------
                                    695 	.area XABS    (ABS,XDATA)
                                    696 ;--------------------------------------------------------
                                    697 ; external initialized ram data
                                    698 ;--------------------------------------------------------
                                    699 	.area XISEG   (XDATA)
                                    700 	.area HOME    (CODE)
                                    701 	.area GSINIT0 (CODE)
                                    702 	.area GSINIT1 (CODE)
                                    703 	.area GSINIT2 (CODE)
                                    704 	.area GSINIT3 (CODE)
                                    705 	.area GSINIT4 (CODE)
                                    706 	.area GSINIT5 (CODE)
                                    707 	.area GSINIT  (CODE)
                                    708 	.area GSFINAL (CODE)
                                    709 	.area CSEG    (CODE)
                                    710 ;--------------------------------------------------------
                                    711 ; interrupt vector 
                                    712 ;--------------------------------------------------------
                                    713 	.area HOME    (CODE)
      000000                        714 __interrupt_vect:
      000000 02 00 11         [24]  715 	ljmp	__sdcc_gsinit_startup
      000003 32               [24]  716 	reti
      000004                        717 	.ds	7
      00000B 02 00 82         [24]  718 	ljmp	_Timer0
                                    719 ;--------------------------------------------------------
                                    720 ; global & static initialisations
                                    721 ;--------------------------------------------------------
                                    722 	.area HOME    (CODE)
                                    723 	.area GSINIT  (CODE)
                                    724 	.area GSFINAL (CODE)
                                    725 	.area GSINIT  (CODE)
                                    726 	.globl __sdcc_gsinit_startup
                                    727 	.globl __sdcc_program_startup
                                    728 	.globl __start__stack
                                    729 	.globl __mcs51_genXINIT
                                    730 	.globl __mcs51_genXRAMCLEAR
                                    731 	.globl __mcs51_genRAMCLEAR
                                    732 	.area GSFINAL (CODE)
      00006A 02 00 0E         [24]  733 	ljmp	__sdcc_program_startup
                                    734 ;--------------------------------------------------------
                                    735 ; Home
                                    736 ;--------------------------------------------------------
                                    737 	.area HOME    (CODE)
                                    738 	.area HOME    (CODE)
      00000E                        739 __sdcc_program_startup:
      00000E 02 00 7D         [24]  740 	ljmp	_main
                                    741 ;	return from main will return to caller
                                    742 ;--------------------------------------------------------
                                    743 ; code
                                    744 ;--------------------------------------------------------
                                    745 	.area CSEG    (CODE)
                                    746 ;------------------------------------------------------------
                                    747 ;Allocation info for local variables in function 'Timer0Init'
                                    748 ;------------------------------------------------------------
                                    749 ;	usr/main.c:47: void Timer0Init()
                                    750 ;	-----------------------------------------
                                    751 ;	 function Timer0Init
                                    752 ;	-----------------------------------------
      00006D                        753 _Timer0Init:
                           000007   754 	ar7 = 0x07
                           000006   755 	ar6 = 0x06
                           000005   756 	ar5 = 0x05
                           000004   757 	ar4 = 0x04
                           000003   758 	ar3 = 0x03
                           000002   759 	ar2 = 0x02
                           000001   760 	ar1 = 0x01
                           000000   761 	ar0 = 0x00
                                    762 ;	usr/main.c:49: TMOD|=0x01;//设置定时器工作模式为模式1 TMOD|=0x01等同于TMOD=TMOD|0x01 使用或控制符防止此处的设置对前面的设置造成影响
      00006D 43 89 01         [24]  763 	orl	_TMOD,#0x01
                                    764 ;	usr/main.c:52: TH0=0XFC;
      000070 75 8C FC         [24]  765 	mov	_TH0,#0xfc
                                    766 ;	usr/main.c:54: TL0=0X18;
      000073 75 8A 18         [24]  767 	mov	_TL0,#0x18
                                    768 ;	usr/main.c:56: ET0=1;//打开T0中断
                                    769 ;	assignBit
      000076 D2 A9            [12]  770 	setb	_ET0
                                    771 ;	usr/main.c:57: EA=1;//打开总中断
                                    772 ;	assignBit
      000078 D2 AF            [12]  773 	setb	_EA
                                    774 ;	usr/main.c:58: TR0=1;//使得T0开始工作
                                    775 ;	assignBit
      00007A D2 8C            [12]  776 	setb	_TR0
                                    777 ;	usr/main.c:59: }
      00007C 22               [24]  778 	ret
                                    779 ;------------------------------------------------------------
                                    780 ;Allocation info for local variables in function 'main'
                                    781 ;------------------------------------------------------------
                                    782 ;	usr/main.c:86: void main()
                                    783 ;	-----------------------------------------
                                    784 ;	 function main
                                    785 ;	-----------------------------------------
      00007D                        786 _main:
                                    787 ;	usr/main.c:88: Timer0Init();//定时器0初始化
      00007D 12 00 6D         [24]  788 	lcall	_Timer0Init
                                    789 ;	usr/main.c:89: while(1);	   //死循环
      000080                        790 00102$:
                                    791 ;	usr/main.c:90: }
      000080 80 FE            [24]  792 	sjmp	00102$
                                    793 ;------------------------------------------------------------
                                    794 ;Allocation info for local variables in function 'Timer0'
                                    795 ;------------------------------------------------------------
                                    796 ;i                         Allocated with name '_Timer0_i_65537_4'
                                    797 ;------------------------------------------------------------
                                    798 ;	usr/main.c:104: void Timer0() __interrupt 1
                                    799 ;	-----------------------------------------
                                    800 ;	 function Timer0
                                    801 ;	-----------------------------------------
      000082                        802 _Timer0:
      000082 C0 E0            [24]  803 	push	acc
      000084 C0 D0            [24]  804 	push	psw
                                    805 ;	usr/main.c:106: TH0=0XFC;	  
      000086 75 8C FC         [24]  806 	mov	_TH0,#0xfc
                                    807 ;	usr/main.c:107: TL0=0X18;
      000089 75 8A 18         [24]  808 	mov	_TL0,#0x18
                                    809 ;	usr/main.c:110: i++;
      00008C 05 08            [12]  810 	inc	_Timer0_i_65537_4
      00008E E4               [12]  811 	clr	a
      00008F B5 08 02         [24]  812 	cjne	a,_Timer0_i_65537_4,00109$
      000092 05 09            [12]  813 	inc	(_Timer0_i_65537_4 + 1)
      000094                        814 00109$:
                                    815 ;	usr/main.c:111: if(i==1000)//每进入一次中断则是1ms，1000次则是1s，此时将led状态取反即可达到实验目的。
      000094 74 E8            [12]  816 	mov	a,#0xe8
      000096 B5 08 0C         [24]  817 	cjne	a,_Timer0_i_65537_4,00103$
      000099 74 03            [12]  818 	mov	a,#0x03
      00009B B5 09 07         [24]  819 	cjne	a,(_Timer0_i_65537_4 + 1),00103$
                                    820 ;	usr/main.c:113: i=0;
      00009E E4               [12]  821 	clr	a
      00009F F5 08            [12]  822 	mov	_Timer0_i_65537_4,a
      0000A1 F5 09            [12]  823 	mov	(_Timer0_i_65537_4 + 1),a
                                    824 ;	usr/main.c:114: led=!led;	//led状态取反
      0000A3 B2 A2            [12]  825 	cpl	_P2_2
      0000A5                        826 00103$:
                                    827 ;	usr/main.c:116: }
      0000A5 D0 D0            [24]  828 	pop	psw
      0000A7 D0 E0            [24]  829 	pop	acc
      0000A9 32               [24]  830 	reti
                                    831 ;	eliminated unneeded mov psw,# (no regs used in bank)
                                    832 ;	eliminated unneeded push/pop dpl
                                    833 ;	eliminated unneeded push/pop dph
                                    834 ;	eliminated unneeded push/pop b
                                    835 	.area CSEG    (CODE)
                                    836 	.area CONST   (CODE)
                                    837 	.area XINIT   (CODE)
                                    838 	.area CABS    (ABS,CODE)
