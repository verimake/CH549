/**
  ******************************************************************
  * @file    main.c
  * @author  甜鱼酱，xy
  * @version V1.0
  * @date    2021-5-19
  * @brief   SPI
  ******************************************************************
  * @attention
  * verimake 用于ch549例程使用SPI驱动oled屏幕
  *   oled引脚接线表
  *   CS     P1_4 //CS片选
  *   RST    P3_5 //LED复位
  *   DC     P2_7 //数据/命令控制
  *   D0     P1_7 //SCLK时钟信号 
  *   D1     P1_5 //MOSI数据
  ******************************************************************
  */
#include <CH549_sdcc.h> 	//ch549的头文件，其中定义了单片机的一些特殊功能寄存器 
#include <CH549_OLED.h>     //其中有驱动屏幕使用的函数
#include <CH549_BMP.h>      //用于显示图片的头文件
#include <CH549_DEBUG.h>    //CH549官方提供库的头文件，定义了一些关于主频，延时，串口设置，看门口，赋值设置等基础函数
#include <CH549_SPI.h>      //CH549官方提供库的头文件，定义了一些关于SPI初始化，传输数据等函数
 /********************************************************************
* TIPS:
*   oled引脚接线表
*   CS     P1_4 //CS片选
*   RST    P3_5 //LED复位
*   DC     P2_7 //数据/命令控制
*   D0     P1_7 //SCLK时钟信号 
*   D1     P1_5 //MOSI数据
*********************************************************************/

 int main(void)
 {
	CfgFsys( );
  mDelaymS(20);                                                              //调整主频，建议稍加延时等待内部时钟稳定
  SPIMasterModeSet(3);                                                       //SPI主机模式设置，模式3
  SPI_CK_SET(12);                                                            //设置spi sclk 时钟信号为12分频
	OLED_Init();			                                                         //初始化OLED  
	OLED_Clear();                                                              //将oled屏幕上内容清除
	setFontSize(16);                                                           //设置文字大小
	OLED_ShowString(0,2,"Please follow   Verimake!");
	while(1);
}

