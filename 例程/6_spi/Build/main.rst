                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _BMP2
                                     12 	.globl _BMP1
                                     13 	.globl _main
                                     14 	.globl _SPIMasterModeSet
                                     15 	.globl _mDelaymS
                                     16 	.globl _CfgFsys
                                     17 	.globl _setFontSize
                                     18 	.globl _OLED_ShowString
                                     19 	.globl _OLED_Clear
                                     20 	.globl _OLED_Init
                                     21 	.globl _UIF_BUS_RST
                                     22 	.globl _UIF_DETECT
                                     23 	.globl _UIF_TRANSFER
                                     24 	.globl _UIF_SUSPEND
                                     25 	.globl _UIF_HST_SOF
                                     26 	.globl _UIF_FIFO_OV
                                     27 	.globl _U_SIE_FREE
                                     28 	.globl _U_TOG_OK
                                     29 	.globl _U_IS_NAK
                                     30 	.globl _S0_R_FIFO
                                     31 	.globl _S0_T_FIFO
                                     32 	.globl _S0_FREE
                                     33 	.globl _S0_IF_BYTE
                                     34 	.globl _S0_IF_FIRST
                                     35 	.globl _S0_IF_OV
                                     36 	.globl _S0_FST_ACT
                                     37 	.globl _CP_RL2
                                     38 	.globl _C_T2
                                     39 	.globl _TR2
                                     40 	.globl _EXEN2
                                     41 	.globl _TCLK
                                     42 	.globl _RCLK
                                     43 	.globl _EXF2
                                     44 	.globl _CAP1F
                                     45 	.globl _TF2
                                     46 	.globl _RI
                                     47 	.globl _TI
                                     48 	.globl _RB8
                                     49 	.globl _TB8
                                     50 	.globl _REN
                                     51 	.globl _SM2
                                     52 	.globl _SM1
                                     53 	.globl _SM0
                                     54 	.globl _IT0
                                     55 	.globl _IE0
                                     56 	.globl _IT1
                                     57 	.globl _IE1
                                     58 	.globl _TR0
                                     59 	.globl _TF0
                                     60 	.globl _TR1
                                     61 	.globl _TF1
                                     62 	.globl _XI
                                     63 	.globl _XO
                                     64 	.globl _P4_0
                                     65 	.globl _P4_1
                                     66 	.globl _P4_2
                                     67 	.globl _P4_3
                                     68 	.globl _P4_4
                                     69 	.globl _P4_5
                                     70 	.globl _P4_6
                                     71 	.globl _RXD
                                     72 	.globl _TXD
                                     73 	.globl _INT0
                                     74 	.globl _INT1
                                     75 	.globl _T0
                                     76 	.globl _T1
                                     77 	.globl _CAP0
                                     78 	.globl _INT3
                                     79 	.globl _P3_0
                                     80 	.globl _P3_1
                                     81 	.globl _P3_2
                                     82 	.globl _P3_3
                                     83 	.globl _P3_4
                                     84 	.globl _P3_5
                                     85 	.globl _P3_6
                                     86 	.globl _P3_7
                                     87 	.globl _PWM5
                                     88 	.globl _PWM4
                                     89 	.globl _INT0_
                                     90 	.globl _PWM3
                                     91 	.globl _PWM2
                                     92 	.globl _CAP1_
                                     93 	.globl _T2_
                                     94 	.globl _PWM1
                                     95 	.globl _CAP2_
                                     96 	.globl _T2EX_
                                     97 	.globl _PWM0
                                     98 	.globl _RXD1
                                     99 	.globl _PWM6
                                    100 	.globl _TXD1
                                    101 	.globl _PWM7
                                    102 	.globl _P2_0
                                    103 	.globl _P2_1
                                    104 	.globl _P2_2
                                    105 	.globl _P2_3
                                    106 	.globl _P2_4
                                    107 	.globl _P2_5
                                    108 	.globl _P2_6
                                    109 	.globl _P2_7
                                    110 	.globl _AIN0
                                    111 	.globl _CAP1
                                    112 	.globl _T2
                                    113 	.globl _AIN1
                                    114 	.globl _CAP2
                                    115 	.globl _T2EX
                                    116 	.globl _AIN2
                                    117 	.globl _AIN3
                                    118 	.globl _AIN4
                                    119 	.globl _UCC1
                                    120 	.globl _SCS
                                    121 	.globl _AIN5
                                    122 	.globl _UCC2
                                    123 	.globl _PWM0_
                                    124 	.globl _MOSI
                                    125 	.globl _AIN6
                                    126 	.globl _VBUS
                                    127 	.globl _RXD1_
                                    128 	.globl _MISO
                                    129 	.globl _AIN7
                                    130 	.globl _TXD1_
                                    131 	.globl _SCK
                                    132 	.globl _P1_0
                                    133 	.globl _P1_1
                                    134 	.globl _P1_2
                                    135 	.globl _P1_3
                                    136 	.globl _P1_4
                                    137 	.globl _P1_5
                                    138 	.globl _P1_6
                                    139 	.globl _P1_7
                                    140 	.globl _AIN8
                                    141 	.globl _AIN9
                                    142 	.globl _AIN10
                                    143 	.globl _RXD_
                                    144 	.globl _AIN11
                                    145 	.globl _TXD_
                                    146 	.globl _AIN12
                                    147 	.globl _RXD2
                                    148 	.globl _AIN13
                                    149 	.globl _TXD2
                                    150 	.globl _AIN14
                                    151 	.globl _RXD3
                                    152 	.globl _AIN15
                                    153 	.globl _TXD3
                                    154 	.globl _P0_0
                                    155 	.globl _P0_1
                                    156 	.globl _P0_2
                                    157 	.globl _P0_3
                                    158 	.globl _P0_4
                                    159 	.globl _P0_5
                                    160 	.globl _P0_6
                                    161 	.globl _P0_7
                                    162 	.globl _IE_SPI0
                                    163 	.globl _IE_INT3
                                    164 	.globl _IE_USB
                                    165 	.globl _IE_UART2
                                    166 	.globl _IE_ADC
                                    167 	.globl _IE_UART1
                                    168 	.globl _IE_UART3
                                    169 	.globl _IE_PWMX
                                    170 	.globl _IE_GPIO
                                    171 	.globl _IE_WDOG
                                    172 	.globl _PX0
                                    173 	.globl _PT0
                                    174 	.globl _PX1
                                    175 	.globl _PT1
                                    176 	.globl _PS
                                    177 	.globl _PT2
                                    178 	.globl _PL_FLAG
                                    179 	.globl _PH_FLAG
                                    180 	.globl _EX0
                                    181 	.globl _ET0
                                    182 	.globl _EX1
                                    183 	.globl _ET1
                                    184 	.globl _ES
                                    185 	.globl _ET2
                                    186 	.globl _E_DIS
                                    187 	.globl _EA
                                    188 	.globl _P
                                    189 	.globl _F1
                                    190 	.globl _OV
                                    191 	.globl _RS0
                                    192 	.globl _RS1
                                    193 	.globl _F0
                                    194 	.globl _AC
                                    195 	.globl _CY
                                    196 	.globl _UEP1_DMA_H
                                    197 	.globl _UEP1_DMA_L
                                    198 	.globl _UEP1_DMA
                                    199 	.globl _UEP0_DMA_H
                                    200 	.globl _UEP0_DMA_L
                                    201 	.globl _UEP0_DMA
                                    202 	.globl _UEP2_3_MOD
                                    203 	.globl _UEP4_1_MOD
                                    204 	.globl _UEP3_DMA_H
                                    205 	.globl _UEP3_DMA_L
                                    206 	.globl _UEP3_DMA
                                    207 	.globl _UEP2_DMA_H
                                    208 	.globl _UEP2_DMA_L
                                    209 	.globl _UEP2_DMA
                                    210 	.globl _USB_DEV_AD
                                    211 	.globl _USB_CTRL
                                    212 	.globl _USB_INT_EN
                                    213 	.globl _UEP4_T_LEN
                                    214 	.globl _UEP4_CTRL
                                    215 	.globl _UEP0_T_LEN
                                    216 	.globl _UEP0_CTRL
                                    217 	.globl _USB_RX_LEN
                                    218 	.globl _USB_MIS_ST
                                    219 	.globl _USB_INT_ST
                                    220 	.globl _USB_INT_FG
                                    221 	.globl _UEP3_T_LEN
                                    222 	.globl _UEP3_CTRL
                                    223 	.globl _UEP2_T_LEN
                                    224 	.globl _UEP2_CTRL
                                    225 	.globl _UEP1_T_LEN
                                    226 	.globl _UEP1_CTRL
                                    227 	.globl _UDEV_CTRL
                                    228 	.globl _USB_C_CTRL
                                    229 	.globl _ADC_PIN
                                    230 	.globl _ADC_CHAN
                                    231 	.globl _ADC_DAT_H
                                    232 	.globl _ADC_DAT_L
                                    233 	.globl _ADC_DAT
                                    234 	.globl _ADC_CFG
                                    235 	.globl _ADC_CTRL
                                    236 	.globl _TKEY_CTRL
                                    237 	.globl _SIF3
                                    238 	.globl _SBAUD3
                                    239 	.globl _SBUF3
                                    240 	.globl _SCON3
                                    241 	.globl _SIF2
                                    242 	.globl _SBAUD2
                                    243 	.globl _SBUF2
                                    244 	.globl _SCON2
                                    245 	.globl _SIF1
                                    246 	.globl _SBAUD1
                                    247 	.globl _SBUF1
                                    248 	.globl _SCON1
                                    249 	.globl _SPI0_SETUP
                                    250 	.globl _SPI0_CK_SE
                                    251 	.globl _SPI0_CTRL
                                    252 	.globl _SPI0_DATA
                                    253 	.globl _SPI0_STAT
                                    254 	.globl _PWM_DATA7
                                    255 	.globl _PWM_DATA6
                                    256 	.globl _PWM_DATA5
                                    257 	.globl _PWM_DATA4
                                    258 	.globl _PWM_DATA3
                                    259 	.globl _PWM_CTRL2
                                    260 	.globl _PWM_CK_SE
                                    261 	.globl _PWM_CTRL
                                    262 	.globl _PWM_DATA0
                                    263 	.globl _PWM_DATA1
                                    264 	.globl _PWM_DATA2
                                    265 	.globl _T2CAP1H
                                    266 	.globl _T2CAP1L
                                    267 	.globl _T2CAP1
                                    268 	.globl _TH2
                                    269 	.globl _TL2
                                    270 	.globl _T2COUNT
                                    271 	.globl _RCAP2H
                                    272 	.globl _RCAP2L
                                    273 	.globl _RCAP2
                                    274 	.globl _T2MOD
                                    275 	.globl _T2CON
                                    276 	.globl _T2CAP0H
                                    277 	.globl _T2CAP0L
                                    278 	.globl _T2CAP0
                                    279 	.globl _T2CON2
                                    280 	.globl _SBUF
                                    281 	.globl _SCON
                                    282 	.globl _TH1
                                    283 	.globl _TH0
                                    284 	.globl _TL1
                                    285 	.globl _TL0
                                    286 	.globl _TMOD
                                    287 	.globl _TCON
                                    288 	.globl _XBUS_AUX
                                    289 	.globl _PIN_FUNC
                                    290 	.globl _P5
                                    291 	.globl _P4_DIR_PU
                                    292 	.globl _P4_MOD_OC
                                    293 	.globl _P4
                                    294 	.globl _P3_DIR_PU
                                    295 	.globl _P3_MOD_OC
                                    296 	.globl _P3
                                    297 	.globl _P2_DIR_PU
                                    298 	.globl _P2_MOD_OC
                                    299 	.globl _P2
                                    300 	.globl _P1_DIR_PU
                                    301 	.globl _P1_MOD_OC
                                    302 	.globl _P1
                                    303 	.globl _P0_DIR_PU
                                    304 	.globl _P0_MOD_OC
                                    305 	.globl _P0
                                    306 	.globl _ROM_CTRL
                                    307 	.globl _ROM_DATA_HH
                                    308 	.globl _ROM_DATA_HL
                                    309 	.globl _ROM_DATA_HI
                                    310 	.globl _ROM_ADDR_H
                                    311 	.globl _ROM_ADDR_L
                                    312 	.globl _ROM_ADDR
                                    313 	.globl _GPIO_IE
                                    314 	.globl _INTX
                                    315 	.globl _IP_EX
                                    316 	.globl _IE_EX
                                    317 	.globl _IP
                                    318 	.globl _IE
                                    319 	.globl _WDOG_COUNT
                                    320 	.globl _RESET_KEEP
                                    321 	.globl _WAKE_CTRL
                                    322 	.globl _CLOCK_CFG
                                    323 	.globl _POWER_CFG
                                    324 	.globl _PCON
                                    325 	.globl _GLOBAL_CFG
                                    326 	.globl _SAFE_MOD
                                    327 	.globl _DPH
                                    328 	.globl _DPL
                                    329 	.globl _SP
                                    330 	.globl _A_INV
                                    331 	.globl _B
                                    332 	.globl _ACC
                                    333 	.globl _PSW
                                    334 ;--------------------------------------------------------
                                    335 ; special function registers
                                    336 ;--------------------------------------------------------
                                    337 	.area RSEG    (ABS,DATA)
      000000                        338 	.org 0x0000
                           0000D0   339 _PSW	=	0x00d0
                           0000E0   340 _ACC	=	0x00e0
                           0000F0   341 _B	=	0x00f0
                           0000FD   342 _A_INV	=	0x00fd
                           000081   343 _SP	=	0x0081
                           000082   344 _DPL	=	0x0082
                           000083   345 _DPH	=	0x0083
                           0000A1   346 _SAFE_MOD	=	0x00a1
                           0000B1   347 _GLOBAL_CFG	=	0x00b1
                           000087   348 _PCON	=	0x0087
                           0000BA   349 _POWER_CFG	=	0x00ba
                           0000B9   350 _CLOCK_CFG	=	0x00b9
                           0000A9   351 _WAKE_CTRL	=	0x00a9
                           0000FE   352 _RESET_KEEP	=	0x00fe
                           0000FF   353 _WDOG_COUNT	=	0x00ff
                           0000A8   354 _IE	=	0x00a8
                           0000B8   355 _IP	=	0x00b8
                           0000E8   356 _IE_EX	=	0x00e8
                           0000E9   357 _IP_EX	=	0x00e9
                           0000B3   358 _INTX	=	0x00b3
                           0000B2   359 _GPIO_IE	=	0x00b2
                           008584   360 _ROM_ADDR	=	0x8584
                           000084   361 _ROM_ADDR_L	=	0x0084
                           000085   362 _ROM_ADDR_H	=	0x0085
                           008F8E   363 _ROM_DATA_HI	=	0x8f8e
                           00008E   364 _ROM_DATA_HL	=	0x008e
                           00008F   365 _ROM_DATA_HH	=	0x008f
                           000086   366 _ROM_CTRL	=	0x0086
                           000080   367 _P0	=	0x0080
                           0000C4   368 _P0_MOD_OC	=	0x00c4
                           0000C5   369 _P0_DIR_PU	=	0x00c5
                           000090   370 _P1	=	0x0090
                           000092   371 _P1_MOD_OC	=	0x0092
                           000093   372 _P1_DIR_PU	=	0x0093
                           0000A0   373 _P2	=	0x00a0
                           000094   374 _P2_MOD_OC	=	0x0094
                           000095   375 _P2_DIR_PU	=	0x0095
                           0000B0   376 _P3	=	0x00b0
                           000096   377 _P3_MOD_OC	=	0x0096
                           000097   378 _P3_DIR_PU	=	0x0097
                           0000C0   379 _P4	=	0x00c0
                           0000C2   380 _P4_MOD_OC	=	0x00c2
                           0000C3   381 _P4_DIR_PU	=	0x00c3
                           0000AB   382 _P5	=	0x00ab
                           0000AA   383 _PIN_FUNC	=	0x00aa
                           0000A2   384 _XBUS_AUX	=	0x00a2
                           000088   385 _TCON	=	0x0088
                           000089   386 _TMOD	=	0x0089
                           00008A   387 _TL0	=	0x008a
                           00008B   388 _TL1	=	0x008b
                           00008C   389 _TH0	=	0x008c
                           00008D   390 _TH1	=	0x008d
                           000098   391 _SCON	=	0x0098
                           000099   392 _SBUF	=	0x0099
                           0000C1   393 _T2CON2	=	0x00c1
                           00C7C6   394 _T2CAP0	=	0xc7c6
                           0000C6   395 _T2CAP0L	=	0x00c6
                           0000C7   396 _T2CAP0H	=	0x00c7
                           0000C8   397 _T2CON	=	0x00c8
                           0000C9   398 _T2MOD	=	0x00c9
                           00CBCA   399 _RCAP2	=	0xcbca
                           0000CA   400 _RCAP2L	=	0x00ca
                           0000CB   401 _RCAP2H	=	0x00cb
                           00CDCC   402 _T2COUNT	=	0xcdcc
                           0000CC   403 _TL2	=	0x00cc
                           0000CD   404 _TH2	=	0x00cd
                           00CFCE   405 _T2CAP1	=	0xcfce
                           0000CE   406 _T2CAP1L	=	0x00ce
                           0000CF   407 _T2CAP1H	=	0x00cf
                           00009A   408 _PWM_DATA2	=	0x009a
                           00009B   409 _PWM_DATA1	=	0x009b
                           00009C   410 _PWM_DATA0	=	0x009c
                           00009D   411 _PWM_CTRL	=	0x009d
                           00009E   412 _PWM_CK_SE	=	0x009e
                           00009F   413 _PWM_CTRL2	=	0x009f
                           0000A3   414 _PWM_DATA3	=	0x00a3
                           0000A4   415 _PWM_DATA4	=	0x00a4
                           0000A5   416 _PWM_DATA5	=	0x00a5
                           0000A6   417 _PWM_DATA6	=	0x00a6
                           0000A7   418 _PWM_DATA7	=	0x00a7
                           0000F8   419 _SPI0_STAT	=	0x00f8
                           0000F9   420 _SPI0_DATA	=	0x00f9
                           0000FA   421 _SPI0_CTRL	=	0x00fa
                           0000FB   422 _SPI0_CK_SE	=	0x00fb
                           0000FC   423 _SPI0_SETUP	=	0x00fc
                           0000BC   424 _SCON1	=	0x00bc
                           0000BD   425 _SBUF1	=	0x00bd
                           0000BE   426 _SBAUD1	=	0x00be
                           0000BF   427 _SIF1	=	0x00bf
                           0000B4   428 _SCON2	=	0x00b4
                           0000B5   429 _SBUF2	=	0x00b5
                           0000B6   430 _SBAUD2	=	0x00b6
                           0000B7   431 _SIF2	=	0x00b7
                           0000AC   432 _SCON3	=	0x00ac
                           0000AD   433 _SBUF3	=	0x00ad
                           0000AE   434 _SBAUD3	=	0x00ae
                           0000AF   435 _SIF3	=	0x00af
                           0000F1   436 _TKEY_CTRL	=	0x00f1
                           0000F2   437 _ADC_CTRL	=	0x00f2
                           0000F3   438 _ADC_CFG	=	0x00f3
                           00F5F4   439 _ADC_DAT	=	0xf5f4
                           0000F4   440 _ADC_DAT_L	=	0x00f4
                           0000F5   441 _ADC_DAT_H	=	0x00f5
                           0000F6   442 _ADC_CHAN	=	0x00f6
                           0000F7   443 _ADC_PIN	=	0x00f7
                           000091   444 _USB_C_CTRL	=	0x0091
                           0000D1   445 _UDEV_CTRL	=	0x00d1
                           0000D2   446 _UEP1_CTRL	=	0x00d2
                           0000D3   447 _UEP1_T_LEN	=	0x00d3
                           0000D4   448 _UEP2_CTRL	=	0x00d4
                           0000D5   449 _UEP2_T_LEN	=	0x00d5
                           0000D6   450 _UEP3_CTRL	=	0x00d6
                           0000D7   451 _UEP3_T_LEN	=	0x00d7
                           0000D8   452 _USB_INT_FG	=	0x00d8
                           0000D9   453 _USB_INT_ST	=	0x00d9
                           0000DA   454 _USB_MIS_ST	=	0x00da
                           0000DB   455 _USB_RX_LEN	=	0x00db
                           0000DC   456 _UEP0_CTRL	=	0x00dc
                           0000DD   457 _UEP0_T_LEN	=	0x00dd
                           0000DE   458 _UEP4_CTRL	=	0x00de
                           0000DF   459 _UEP4_T_LEN	=	0x00df
                           0000E1   460 _USB_INT_EN	=	0x00e1
                           0000E2   461 _USB_CTRL	=	0x00e2
                           0000E3   462 _USB_DEV_AD	=	0x00e3
                           00E5E4   463 _UEP2_DMA	=	0xe5e4
                           0000E4   464 _UEP2_DMA_L	=	0x00e4
                           0000E5   465 _UEP2_DMA_H	=	0x00e5
                           00E7E6   466 _UEP3_DMA	=	0xe7e6
                           0000E6   467 _UEP3_DMA_L	=	0x00e6
                           0000E7   468 _UEP3_DMA_H	=	0x00e7
                           0000EA   469 _UEP4_1_MOD	=	0x00ea
                           0000EB   470 _UEP2_3_MOD	=	0x00eb
                           00EDEC   471 _UEP0_DMA	=	0xedec
                           0000EC   472 _UEP0_DMA_L	=	0x00ec
                           0000ED   473 _UEP0_DMA_H	=	0x00ed
                           00EFEE   474 _UEP1_DMA	=	0xefee
                           0000EE   475 _UEP1_DMA_L	=	0x00ee
                           0000EF   476 _UEP1_DMA_H	=	0x00ef
                                    477 ;--------------------------------------------------------
                                    478 ; special function bits
                                    479 ;--------------------------------------------------------
                                    480 	.area RSEG    (ABS,DATA)
      000000                        481 	.org 0x0000
                           0000D7   482 _CY	=	0x00d7
                           0000D6   483 _AC	=	0x00d6
                           0000D5   484 _F0	=	0x00d5
                           0000D4   485 _RS1	=	0x00d4
                           0000D3   486 _RS0	=	0x00d3
                           0000D2   487 _OV	=	0x00d2
                           0000D1   488 _F1	=	0x00d1
                           0000D0   489 _P	=	0x00d0
                           0000AF   490 _EA	=	0x00af
                           0000AE   491 _E_DIS	=	0x00ae
                           0000AD   492 _ET2	=	0x00ad
                           0000AC   493 _ES	=	0x00ac
                           0000AB   494 _ET1	=	0x00ab
                           0000AA   495 _EX1	=	0x00aa
                           0000A9   496 _ET0	=	0x00a9
                           0000A8   497 _EX0	=	0x00a8
                           0000BF   498 _PH_FLAG	=	0x00bf
                           0000BE   499 _PL_FLAG	=	0x00be
                           0000BD   500 _PT2	=	0x00bd
                           0000BC   501 _PS	=	0x00bc
                           0000BB   502 _PT1	=	0x00bb
                           0000BA   503 _PX1	=	0x00ba
                           0000B9   504 _PT0	=	0x00b9
                           0000B8   505 _PX0	=	0x00b8
                           0000EF   506 _IE_WDOG	=	0x00ef
                           0000EE   507 _IE_GPIO	=	0x00ee
                           0000ED   508 _IE_PWMX	=	0x00ed
                           0000ED   509 _IE_UART3	=	0x00ed
                           0000EC   510 _IE_UART1	=	0x00ec
                           0000EB   511 _IE_ADC	=	0x00eb
                           0000EB   512 _IE_UART2	=	0x00eb
                           0000EA   513 _IE_USB	=	0x00ea
                           0000E9   514 _IE_INT3	=	0x00e9
                           0000E8   515 _IE_SPI0	=	0x00e8
                           000087   516 _P0_7	=	0x0087
                           000086   517 _P0_6	=	0x0086
                           000085   518 _P0_5	=	0x0085
                           000084   519 _P0_4	=	0x0084
                           000083   520 _P0_3	=	0x0083
                           000082   521 _P0_2	=	0x0082
                           000081   522 _P0_1	=	0x0081
                           000080   523 _P0_0	=	0x0080
                           000087   524 _TXD3	=	0x0087
                           000087   525 _AIN15	=	0x0087
                           000086   526 _RXD3	=	0x0086
                           000086   527 _AIN14	=	0x0086
                           000085   528 _TXD2	=	0x0085
                           000085   529 _AIN13	=	0x0085
                           000084   530 _RXD2	=	0x0084
                           000084   531 _AIN12	=	0x0084
                           000083   532 _TXD_	=	0x0083
                           000083   533 _AIN11	=	0x0083
                           000082   534 _RXD_	=	0x0082
                           000082   535 _AIN10	=	0x0082
                           000081   536 _AIN9	=	0x0081
                           000080   537 _AIN8	=	0x0080
                           000097   538 _P1_7	=	0x0097
                           000096   539 _P1_6	=	0x0096
                           000095   540 _P1_5	=	0x0095
                           000094   541 _P1_4	=	0x0094
                           000093   542 _P1_3	=	0x0093
                           000092   543 _P1_2	=	0x0092
                           000091   544 _P1_1	=	0x0091
                           000090   545 _P1_0	=	0x0090
                           000097   546 _SCK	=	0x0097
                           000097   547 _TXD1_	=	0x0097
                           000097   548 _AIN7	=	0x0097
                           000096   549 _MISO	=	0x0096
                           000096   550 _RXD1_	=	0x0096
                           000096   551 _VBUS	=	0x0096
                           000096   552 _AIN6	=	0x0096
                           000095   553 _MOSI	=	0x0095
                           000095   554 _PWM0_	=	0x0095
                           000095   555 _UCC2	=	0x0095
                           000095   556 _AIN5	=	0x0095
                           000094   557 _SCS	=	0x0094
                           000094   558 _UCC1	=	0x0094
                           000094   559 _AIN4	=	0x0094
                           000093   560 _AIN3	=	0x0093
                           000092   561 _AIN2	=	0x0092
                           000091   562 _T2EX	=	0x0091
                           000091   563 _CAP2	=	0x0091
                           000091   564 _AIN1	=	0x0091
                           000090   565 _T2	=	0x0090
                           000090   566 _CAP1	=	0x0090
                           000090   567 _AIN0	=	0x0090
                           0000A7   568 _P2_7	=	0x00a7
                           0000A6   569 _P2_6	=	0x00a6
                           0000A5   570 _P2_5	=	0x00a5
                           0000A4   571 _P2_4	=	0x00a4
                           0000A3   572 _P2_3	=	0x00a3
                           0000A2   573 _P2_2	=	0x00a2
                           0000A1   574 _P2_1	=	0x00a1
                           0000A0   575 _P2_0	=	0x00a0
                           0000A7   576 _PWM7	=	0x00a7
                           0000A7   577 _TXD1	=	0x00a7
                           0000A6   578 _PWM6	=	0x00a6
                           0000A6   579 _RXD1	=	0x00a6
                           0000A5   580 _PWM0	=	0x00a5
                           0000A5   581 _T2EX_	=	0x00a5
                           0000A5   582 _CAP2_	=	0x00a5
                           0000A4   583 _PWM1	=	0x00a4
                           0000A4   584 _T2_	=	0x00a4
                           0000A4   585 _CAP1_	=	0x00a4
                           0000A3   586 _PWM2	=	0x00a3
                           0000A2   587 _PWM3	=	0x00a2
                           0000A2   588 _INT0_	=	0x00a2
                           0000A1   589 _PWM4	=	0x00a1
                           0000A0   590 _PWM5	=	0x00a0
                           0000B7   591 _P3_7	=	0x00b7
                           0000B6   592 _P3_6	=	0x00b6
                           0000B5   593 _P3_5	=	0x00b5
                           0000B4   594 _P3_4	=	0x00b4
                           0000B3   595 _P3_3	=	0x00b3
                           0000B2   596 _P3_2	=	0x00b2
                           0000B1   597 _P3_1	=	0x00b1
                           0000B0   598 _P3_0	=	0x00b0
                           0000B7   599 _INT3	=	0x00b7
                           0000B6   600 _CAP0	=	0x00b6
                           0000B5   601 _T1	=	0x00b5
                           0000B4   602 _T0	=	0x00b4
                           0000B3   603 _INT1	=	0x00b3
                           0000B2   604 _INT0	=	0x00b2
                           0000B1   605 _TXD	=	0x00b1
                           0000B0   606 _RXD	=	0x00b0
                           0000C6   607 _P4_6	=	0x00c6
                           0000C5   608 _P4_5	=	0x00c5
                           0000C4   609 _P4_4	=	0x00c4
                           0000C3   610 _P4_3	=	0x00c3
                           0000C2   611 _P4_2	=	0x00c2
                           0000C1   612 _P4_1	=	0x00c1
                           0000C0   613 _P4_0	=	0x00c0
                           0000C7   614 _XO	=	0x00c7
                           0000C6   615 _XI	=	0x00c6
                           00008F   616 _TF1	=	0x008f
                           00008E   617 _TR1	=	0x008e
                           00008D   618 _TF0	=	0x008d
                           00008C   619 _TR0	=	0x008c
                           00008B   620 _IE1	=	0x008b
                           00008A   621 _IT1	=	0x008a
                           000089   622 _IE0	=	0x0089
                           000088   623 _IT0	=	0x0088
                           00009F   624 _SM0	=	0x009f
                           00009E   625 _SM1	=	0x009e
                           00009D   626 _SM2	=	0x009d
                           00009C   627 _REN	=	0x009c
                           00009B   628 _TB8	=	0x009b
                           00009A   629 _RB8	=	0x009a
                           000099   630 _TI	=	0x0099
                           000098   631 _RI	=	0x0098
                           0000CF   632 _TF2	=	0x00cf
                           0000CF   633 _CAP1F	=	0x00cf
                           0000CE   634 _EXF2	=	0x00ce
                           0000CD   635 _RCLK	=	0x00cd
                           0000CC   636 _TCLK	=	0x00cc
                           0000CB   637 _EXEN2	=	0x00cb
                           0000CA   638 _TR2	=	0x00ca
                           0000C9   639 _C_T2	=	0x00c9
                           0000C8   640 _CP_RL2	=	0x00c8
                           0000FF   641 _S0_FST_ACT	=	0x00ff
                           0000FE   642 _S0_IF_OV	=	0x00fe
                           0000FD   643 _S0_IF_FIRST	=	0x00fd
                           0000FC   644 _S0_IF_BYTE	=	0x00fc
                           0000FB   645 _S0_FREE	=	0x00fb
                           0000FA   646 _S0_T_FIFO	=	0x00fa
                           0000F8   647 _S0_R_FIFO	=	0x00f8
                           0000DF   648 _U_IS_NAK	=	0x00df
                           0000DE   649 _U_TOG_OK	=	0x00de
                           0000DD   650 _U_SIE_FREE	=	0x00dd
                           0000DC   651 _UIF_FIFO_OV	=	0x00dc
                           0000DB   652 _UIF_HST_SOF	=	0x00db
                           0000DA   653 _UIF_SUSPEND	=	0x00da
                           0000D9   654 _UIF_TRANSFER	=	0x00d9
                           0000D8   655 _UIF_DETECT	=	0x00d8
                           0000D8   656 _UIF_BUS_RST	=	0x00d8
                                    657 ;--------------------------------------------------------
                                    658 ; overlayable register banks
                                    659 ;--------------------------------------------------------
                                    660 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        661 	.ds 8
                                    662 ;--------------------------------------------------------
                                    663 ; internal ram data
                                    664 ;--------------------------------------------------------
                                    665 	.area DSEG    (DATA)
                                    666 ;--------------------------------------------------------
                                    667 ; overlayable items in internal ram 
                                    668 ;--------------------------------------------------------
                                    669 ;--------------------------------------------------------
                                    670 ; Stack segment in internal ram 
                                    671 ;--------------------------------------------------------
                                    672 	.area	SSEG
      00001C                        673 __start__stack:
      00001C                        674 	.ds	1
                                    675 
                                    676 ;--------------------------------------------------------
                                    677 ; indirectly addressable internal ram data
                                    678 ;--------------------------------------------------------
                                    679 	.area ISEG    (DATA)
                                    680 ;--------------------------------------------------------
                                    681 ; absolute internal ram data
                                    682 ;--------------------------------------------------------
                                    683 	.area IABS    (ABS,DATA)
                                    684 	.area IABS    (ABS,DATA)
                                    685 ;--------------------------------------------------------
                                    686 ; bit data
                                    687 ;--------------------------------------------------------
                                    688 	.area BSEG    (BIT)
                                    689 ;--------------------------------------------------------
                                    690 ; paged external ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area PSEG    (PAG,XDATA)
                                    693 ;--------------------------------------------------------
                                    694 ; external ram data
                                    695 ;--------------------------------------------------------
                                    696 	.area XSEG    (XDATA)
                                    697 ;--------------------------------------------------------
                                    698 ; absolute external ram data
                                    699 ;--------------------------------------------------------
                                    700 	.area XABS    (ABS,XDATA)
                                    701 ;--------------------------------------------------------
                                    702 ; external initialized ram data
                                    703 ;--------------------------------------------------------
                                    704 	.area XISEG   (XDATA)
                                    705 	.area HOME    (CODE)
                                    706 	.area GSINIT0 (CODE)
                                    707 	.area GSINIT1 (CODE)
                                    708 	.area GSINIT2 (CODE)
                                    709 	.area GSINIT3 (CODE)
                                    710 	.area GSINIT4 (CODE)
                                    711 	.area GSINIT5 (CODE)
                                    712 	.area GSINIT  (CODE)
                                    713 	.area GSFINAL (CODE)
                                    714 	.area CSEG    (CODE)
                                    715 ;--------------------------------------------------------
                                    716 ; interrupt vector 
                                    717 ;--------------------------------------------------------
                                    718 	.area HOME    (CODE)
      000000                        719 __interrupt_vect:
      000000 02 00 06         [24]  720 	ljmp	__sdcc_gsinit_startup
                                    721 ;--------------------------------------------------------
                                    722 ; global & static initialisations
                                    723 ;--------------------------------------------------------
                                    724 	.area HOME    (CODE)
                                    725 	.area GSINIT  (CODE)
                                    726 	.area GSFINAL (CODE)
                                    727 	.area GSINIT  (CODE)
                                    728 	.globl __sdcc_gsinit_startup
                                    729 	.globl __sdcc_program_startup
                                    730 	.globl __start__stack
                                    731 	.globl __mcs51_genXINIT
                                    732 	.globl __mcs51_genXRAMCLEAR
                                    733 	.globl __mcs51_genRAMCLEAR
                                    734 	.area GSFINAL (CODE)
      00005F 02 00 03         [24]  735 	ljmp	__sdcc_program_startup
                                    736 ;--------------------------------------------------------
                                    737 ; Home
                                    738 ;--------------------------------------------------------
                                    739 	.area HOME    (CODE)
                                    740 	.area HOME    (CODE)
      000003                        741 __sdcc_program_startup:
      000003 02 00 62         [24]  742 	ljmp	_main
                                    743 ;	return from main will return to caller
                                    744 ;--------------------------------------------------------
                                    745 ; code
                                    746 ;--------------------------------------------------------
                                    747 	.area CSEG    (CODE)
                                    748 ;------------------------------------------------------------
                                    749 ;Allocation info for local variables in function 'main'
                                    750 ;------------------------------------------------------------
                                    751 ;	usr/main.c:34: int main(void)
                                    752 ;	-----------------------------------------
                                    753 ;	 function main
                                    754 ;	-----------------------------------------
      000062                        755 _main:
                           000007   756 	ar7 = 0x07
                           000006   757 	ar6 = 0x06
                           000005   758 	ar5 = 0x05
                           000004   759 	ar4 = 0x04
                           000003   760 	ar3 = 0x03
                           000002   761 	ar2 = 0x02
                           000001   762 	ar1 = 0x01
                           000000   763 	ar0 = 0x00
                                    764 ;	usr/main.c:36: CfgFsys( );
      000062 12 04 5C         [24]  765 	lcall	_CfgFsys
                                    766 ;	usr/main.c:37: mDelaymS(20);                                                              //调整主频，建议稍加延时等待内部时钟稳定
      000065 90 00 14         [24]  767 	mov	dptr,#0x0014
      000068 12 04 98         [24]  768 	lcall	_mDelaymS
                                    769 ;	usr/main.c:38: SPIMasterModeSet(3);                                                       //SPI主机模式设置，模式3
      00006B 75 82 03         [24]  770 	mov	dpl,#0x03
      00006E 12 05 0A         [24]  771 	lcall	_SPIMasterModeSet
                                    772 ;	usr/main.c:39: SPI_CK_SET(12);                                                            //设置spi sclk 时钟信号为12分频
      000071 75 FB 0C         [24]  773 	mov	_SPI0_CK_SE,#0x0c
                                    774 ;	usr/main.c:40: OLED_Init();			                                                         //初始化OLED  
      000074 12 01 0F         [24]  775 	lcall	_OLED_Init
                                    776 ;	usr/main.c:41: OLED_Clear();                                                              //将oled屏幕上内容清除
      000077 12 01 5D         [24]  777 	lcall	_OLED_Clear
                                    778 ;	usr/main.c:42: setFontSize(16);                                                           //设置文字大小
      00007A 75 82 10         [24]  779 	mov	dpl,#0x10
      00007D 12 00 94         [24]  780 	lcall	_setFontSize
                                    781 ;	usr/main.c:43: OLED_ShowString(0,2,"Please follow   Verimake!");
      000080 75 0F 80         [24]  782 	mov	_OLED_ShowString_PARM_3,#___str_0
      000083 75 10 0D         [24]  783 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_0 >> 8)
      000086 75 11 80         [24]  784 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      000089 75 0E 02         [24]  785 	mov	_OLED_ShowString_PARM_2,#0x02
      00008C 75 82 00         [24]  786 	mov	dpl,#0x00
      00008F 12 02 E0         [24]  787 	lcall	_OLED_ShowString
                                    788 ;	usr/main.c:44: while(1);
      000092                        789 00102$:
                                    790 ;	usr/main.c:45: }
      000092 80 FE            [24]  791 	sjmp	00102$
                                    792 	.area CSEG    (CODE)
                                    793 	.area CONST   (CODE)
      000580                        794 _BMP1:
      000580 00                     795 	.db #0x00	; 0
      000581 03                     796 	.db #0x03	; 3
      000582 05                     797 	.db #0x05	; 5
      000583 09                     798 	.db #0x09	; 9
      000584 11                     799 	.db #0x11	; 17
      000585 FF                     800 	.db #0xff	; 255
      000586 11                     801 	.db #0x11	; 17
      000587 89                     802 	.db #0x89	; 137
      000588 05                     803 	.db #0x05	; 5
      000589 C3                     804 	.db #0xc3	; 195
      00058A 00                     805 	.db #0x00	; 0
      00058B E0                     806 	.db #0xe0	; 224
      00058C 00                     807 	.db #0x00	; 0
      00058D F0                     808 	.db #0xf0	; 240
      00058E 00                     809 	.db #0x00	; 0
      00058F F8                     810 	.db #0xf8	; 248
      000590 00                     811 	.db #0x00	; 0
      000591 00                     812 	.db #0x00	; 0
      000592 00                     813 	.db #0x00	; 0
      000593 00                     814 	.db #0x00	; 0
      000594 00                     815 	.db #0x00	; 0
      000595 00                     816 	.db #0x00	; 0
      000596 00                     817 	.db #0x00	; 0
      000597 44                     818 	.db #0x44	; 68	'D'
      000598 28                     819 	.db #0x28	; 40
      000599 FF                     820 	.db #0xff	; 255
      00059A 11                     821 	.db #0x11	; 17
      00059B AA                     822 	.db #0xaa	; 170
      00059C 44                     823 	.db #0x44	; 68	'D'
      00059D 00                     824 	.db #0x00	; 0
      00059E 00                     825 	.db #0x00	; 0
      00059F 00                     826 	.db #0x00	; 0
      0005A0 00                     827 	.db #0x00	; 0
      0005A1 00                     828 	.db #0x00	; 0
      0005A2 00                     829 	.db #0x00	; 0
      0005A3 00                     830 	.db #0x00	; 0
      0005A4 00                     831 	.db #0x00	; 0
      0005A5 00                     832 	.db #0x00	; 0
      0005A6 00                     833 	.db #0x00	; 0
      0005A7 00                     834 	.db #0x00	; 0
      0005A8 00                     835 	.db #0x00	; 0
      0005A9 00                     836 	.db #0x00	; 0
      0005AA 00                     837 	.db #0x00	; 0
      0005AB 00                     838 	.db #0x00	; 0
      0005AC 00                     839 	.db #0x00	; 0
      0005AD 00                     840 	.db #0x00	; 0
      0005AE 00                     841 	.db #0x00	; 0
      0005AF 00                     842 	.db #0x00	; 0
      0005B0 00                     843 	.db #0x00	; 0
      0005B1 00                     844 	.db #0x00	; 0
      0005B2 00                     845 	.db #0x00	; 0
      0005B3 00                     846 	.db #0x00	; 0
      0005B4 00                     847 	.db #0x00	; 0
      0005B5 00                     848 	.db #0x00	; 0
      0005B6 00                     849 	.db #0x00	; 0
      0005B7 00                     850 	.db #0x00	; 0
      0005B8 00                     851 	.db #0x00	; 0
      0005B9 00                     852 	.db #0x00	; 0
      0005BA 00                     853 	.db #0x00	; 0
      0005BB 00                     854 	.db #0x00	; 0
      0005BC 00                     855 	.db #0x00	; 0
      0005BD 00                     856 	.db #0x00	; 0
      0005BE 00                     857 	.db #0x00	; 0
      0005BF 00                     858 	.db #0x00	; 0
      0005C0 00                     859 	.db #0x00	; 0
      0005C1 00                     860 	.db #0x00	; 0
      0005C2 00                     861 	.db #0x00	; 0
      0005C3 00                     862 	.db #0x00	; 0
      0005C4 00                     863 	.db #0x00	; 0
      0005C5 00                     864 	.db #0x00	; 0
      0005C6 00                     865 	.db #0x00	; 0
      0005C7 00                     866 	.db #0x00	; 0
      0005C8 00                     867 	.db #0x00	; 0
      0005C9 00                     868 	.db #0x00	; 0
      0005CA 00                     869 	.db #0x00	; 0
      0005CB 00                     870 	.db #0x00	; 0
      0005CC 00                     871 	.db #0x00	; 0
      0005CD 00                     872 	.db #0x00	; 0
      0005CE 00                     873 	.db #0x00	; 0
      0005CF 00                     874 	.db #0x00	; 0
      0005D0 00                     875 	.db #0x00	; 0
      0005D1 00                     876 	.db #0x00	; 0
      0005D2 00                     877 	.db #0x00	; 0
      0005D3 00                     878 	.db #0x00	; 0
      0005D4 00                     879 	.db #0x00	; 0
      0005D5 00                     880 	.db #0x00	; 0
      0005D6 00                     881 	.db #0x00	; 0
      0005D7 00                     882 	.db #0x00	; 0
      0005D8 00                     883 	.db #0x00	; 0
      0005D9 00                     884 	.db #0x00	; 0
      0005DA 83                     885 	.db #0x83	; 131
      0005DB 01                     886 	.db #0x01	; 1
      0005DC 38                     887 	.db #0x38	; 56	'8'
      0005DD 44                     888 	.db #0x44	; 68	'D'
      0005DE 82                     889 	.db #0x82	; 130
      0005DF 92                     890 	.db #0x92	; 146
      0005E0 92                     891 	.db #0x92	; 146
      0005E1 74                     892 	.db #0x74	; 116	't'
      0005E2 01                     893 	.db #0x01	; 1
      0005E3 83                     894 	.db #0x83	; 131
      0005E4 00                     895 	.db #0x00	; 0
      0005E5 00                     896 	.db #0x00	; 0
      0005E6 00                     897 	.db #0x00	; 0
      0005E7 00                     898 	.db #0x00	; 0
      0005E8 00                     899 	.db #0x00	; 0
      0005E9 00                     900 	.db #0x00	; 0
      0005EA 00                     901 	.db #0x00	; 0
      0005EB 7C                     902 	.db #0x7c	; 124
      0005EC 44                     903 	.db #0x44	; 68	'D'
      0005ED FF                     904 	.db #0xff	; 255
      0005EE 01                     905 	.db #0x01	; 1
      0005EF 7D                     906 	.db #0x7d	; 125
      0005F0 7D                     907 	.db #0x7d	; 125
      0005F1 7D                     908 	.db #0x7d	; 125
      0005F2 01                     909 	.db #0x01	; 1
      0005F3 7D                     910 	.db #0x7d	; 125
      0005F4 7D                     911 	.db #0x7d	; 125
      0005F5 7D                     912 	.db #0x7d	; 125
      0005F6 7D                     913 	.db #0x7d	; 125
      0005F7 01                     914 	.db #0x01	; 1
      0005F8 7D                     915 	.db #0x7d	; 125
      0005F9 7D                     916 	.db #0x7d	; 125
      0005FA 7D                     917 	.db #0x7d	; 125
      0005FB 7D                     918 	.db #0x7d	; 125
      0005FC 7D                     919 	.db #0x7d	; 125
      0005FD 01                     920 	.db #0x01	; 1
      0005FE FF                     921 	.db #0xff	; 255
      0005FF 00                     922 	.db #0x00	; 0
      000600 00                     923 	.db #0x00	; 0
      000601 00                     924 	.db #0x00	; 0
      000602 00                     925 	.db #0x00	; 0
      000603 00                     926 	.db #0x00	; 0
      000604 00                     927 	.db #0x00	; 0
      000605 01                     928 	.db #0x01	; 1
      000606 00                     929 	.db #0x00	; 0
      000607 01                     930 	.db #0x01	; 1
      000608 00                     931 	.db #0x00	; 0
      000609 01                     932 	.db #0x01	; 1
      00060A 00                     933 	.db #0x00	; 0
      00060B 01                     934 	.db #0x01	; 1
      00060C 00                     935 	.db #0x00	; 0
      00060D 01                     936 	.db #0x01	; 1
      00060E 00                     937 	.db #0x00	; 0
      00060F 01                     938 	.db #0x01	; 1
      000610 00                     939 	.db #0x00	; 0
      000611 00                     940 	.db #0x00	; 0
      000612 00                     941 	.db #0x00	; 0
      000613 00                     942 	.db #0x00	; 0
      000614 00                     943 	.db #0x00	; 0
      000615 00                     944 	.db #0x00	; 0
      000616 00                     945 	.db #0x00	; 0
      000617 00                     946 	.db #0x00	; 0
      000618 00                     947 	.db #0x00	; 0
      000619 01                     948 	.db #0x01	; 1
      00061A 01                     949 	.db #0x01	; 1
      00061B 00                     950 	.db #0x00	; 0
      00061C 00                     951 	.db #0x00	; 0
      00061D 00                     952 	.db #0x00	; 0
      00061E 00                     953 	.db #0x00	; 0
      00061F 00                     954 	.db #0x00	; 0
      000620 00                     955 	.db #0x00	; 0
      000621 00                     956 	.db #0x00	; 0
      000622 00                     957 	.db #0x00	; 0
      000623 00                     958 	.db #0x00	; 0
      000624 00                     959 	.db #0x00	; 0
      000625 00                     960 	.db #0x00	; 0
      000626 00                     961 	.db #0x00	; 0
      000627 00                     962 	.db #0x00	; 0
      000628 00                     963 	.db #0x00	; 0
      000629 00                     964 	.db #0x00	; 0
      00062A 00                     965 	.db #0x00	; 0
      00062B 00                     966 	.db #0x00	; 0
      00062C 00                     967 	.db #0x00	; 0
      00062D 00                     968 	.db #0x00	; 0
      00062E 00                     969 	.db #0x00	; 0
      00062F 00                     970 	.db #0x00	; 0
      000630 00                     971 	.db #0x00	; 0
      000631 00                     972 	.db #0x00	; 0
      000632 00                     973 	.db #0x00	; 0
      000633 00                     974 	.db #0x00	; 0
      000634 00                     975 	.db #0x00	; 0
      000635 00                     976 	.db #0x00	; 0
      000636 00                     977 	.db #0x00	; 0
      000637 00                     978 	.db #0x00	; 0
      000638 00                     979 	.db #0x00	; 0
      000639 00                     980 	.db #0x00	; 0
      00063A 00                     981 	.db #0x00	; 0
      00063B 00                     982 	.db #0x00	; 0
      00063C 00                     983 	.db #0x00	; 0
      00063D 00                     984 	.db #0x00	; 0
      00063E 00                     985 	.db #0x00	; 0
      00063F 00                     986 	.db #0x00	; 0
      000640 00                     987 	.db #0x00	; 0
      000641 00                     988 	.db #0x00	; 0
      000642 00                     989 	.db #0x00	; 0
      000643 00                     990 	.db #0x00	; 0
      000644 00                     991 	.db #0x00	; 0
      000645 00                     992 	.db #0x00	; 0
      000646 00                     993 	.db #0x00	; 0
      000647 00                     994 	.db #0x00	; 0
      000648 00                     995 	.db #0x00	; 0
      000649 00                     996 	.db #0x00	; 0
      00064A 00                     997 	.db #0x00	; 0
      00064B 00                     998 	.db #0x00	; 0
      00064C 00                     999 	.db #0x00	; 0
      00064D 00                    1000 	.db #0x00	; 0
      00064E 00                    1001 	.db #0x00	; 0
      00064F 00                    1002 	.db #0x00	; 0
      000650 00                    1003 	.db #0x00	; 0
      000651 00                    1004 	.db #0x00	; 0
      000652 00                    1005 	.db #0x00	; 0
      000653 00                    1006 	.db #0x00	; 0
      000654 00                    1007 	.db #0x00	; 0
      000655 00                    1008 	.db #0x00	; 0
      000656 00                    1009 	.db #0x00	; 0
      000657 00                    1010 	.db #0x00	; 0
      000658 00                    1011 	.db #0x00	; 0
      000659 00                    1012 	.db #0x00	; 0
      00065A 01                    1013 	.db #0x01	; 1
      00065B 01                    1014 	.db #0x01	; 1
      00065C 00                    1015 	.db #0x00	; 0
      00065D 00                    1016 	.db #0x00	; 0
      00065E 00                    1017 	.db #0x00	; 0
      00065F 00                    1018 	.db #0x00	; 0
      000660 00                    1019 	.db #0x00	; 0
      000661 00                    1020 	.db #0x00	; 0
      000662 01                    1021 	.db #0x01	; 1
      000663 01                    1022 	.db #0x01	; 1
      000664 00                    1023 	.db #0x00	; 0
      000665 00                    1024 	.db #0x00	; 0
      000666 00                    1025 	.db #0x00	; 0
      000667 00                    1026 	.db #0x00	; 0
      000668 00                    1027 	.db #0x00	; 0
      000669 00                    1028 	.db #0x00	; 0
      00066A 00                    1029 	.db #0x00	; 0
      00066B 00                    1030 	.db #0x00	; 0
      00066C 00                    1031 	.db #0x00	; 0
      00066D 01                    1032 	.db #0x01	; 1
      00066E 01                    1033 	.db #0x01	; 1
      00066F 01                    1034 	.db #0x01	; 1
      000670 01                    1035 	.db #0x01	; 1
      000671 01                    1036 	.db #0x01	; 1
      000672 01                    1037 	.db #0x01	; 1
      000673 01                    1038 	.db #0x01	; 1
      000674 01                    1039 	.db #0x01	; 1
      000675 01                    1040 	.db #0x01	; 1
      000676 01                    1041 	.db #0x01	; 1
      000677 01                    1042 	.db #0x01	; 1
      000678 01                    1043 	.db #0x01	; 1
      000679 01                    1044 	.db #0x01	; 1
      00067A 01                    1045 	.db #0x01	; 1
      00067B 01                    1046 	.db #0x01	; 1
      00067C 01                    1047 	.db #0x01	; 1
      00067D 01                    1048 	.db #0x01	; 1
      00067E 01                    1049 	.db #0x01	; 1
      00067F 00                    1050 	.db #0x00	; 0
      000680 00                    1051 	.db #0x00	; 0
      000681 00                    1052 	.db #0x00	; 0
      000682 00                    1053 	.db #0x00	; 0
      000683 00                    1054 	.db #0x00	; 0
      000684 00                    1055 	.db #0x00	; 0
      000685 00                    1056 	.db #0x00	; 0
      000686 00                    1057 	.db #0x00	; 0
      000687 00                    1058 	.db #0x00	; 0
      000688 00                    1059 	.db #0x00	; 0
      000689 00                    1060 	.db #0x00	; 0
      00068A 00                    1061 	.db #0x00	; 0
      00068B 00                    1062 	.db #0x00	; 0
      00068C 00                    1063 	.db #0x00	; 0
      00068D 00                    1064 	.db #0x00	; 0
      00068E 00                    1065 	.db #0x00	; 0
      00068F 00                    1066 	.db #0x00	; 0
      000690 00                    1067 	.db #0x00	; 0
      000691 00                    1068 	.db #0x00	; 0
      000692 00                    1069 	.db #0x00	; 0
      000693 00                    1070 	.db #0x00	; 0
      000694 00                    1071 	.db #0x00	; 0
      000695 00                    1072 	.db #0x00	; 0
      000696 00                    1073 	.db #0x00	; 0
      000697 00                    1074 	.db #0x00	; 0
      000698 00                    1075 	.db #0x00	; 0
      000699 00                    1076 	.db #0x00	; 0
      00069A 00                    1077 	.db #0x00	; 0
      00069B 00                    1078 	.db #0x00	; 0
      00069C 3F                    1079 	.db #0x3f	; 63
      00069D 3F                    1080 	.db #0x3f	; 63
      00069E 03                    1081 	.db #0x03	; 3
      00069F 03                    1082 	.db #0x03	; 3
      0006A0 F3                    1083 	.db #0xf3	; 243
      0006A1 13                    1084 	.db #0x13	; 19
      0006A2 11                    1085 	.db #0x11	; 17
      0006A3 11                    1086 	.db #0x11	; 17
      0006A4 11                    1087 	.db #0x11	; 17
      0006A5 11                    1088 	.db #0x11	; 17
      0006A6 11                    1089 	.db #0x11	; 17
      0006A7 11                    1090 	.db #0x11	; 17
      0006A8 01                    1091 	.db #0x01	; 1
      0006A9 F1                    1092 	.db #0xf1	; 241
      0006AA 11                    1093 	.db #0x11	; 17
      0006AB 61                    1094 	.db #0x61	; 97	'a'
      0006AC 81                    1095 	.db #0x81	; 129
      0006AD 01                    1096 	.db #0x01	; 1
      0006AE 01                    1097 	.db #0x01	; 1
      0006AF 01                    1098 	.db #0x01	; 1
      0006B0 81                    1099 	.db #0x81	; 129
      0006B1 61                    1100 	.db #0x61	; 97	'a'
      0006B2 11                    1101 	.db #0x11	; 17
      0006B3 F1                    1102 	.db #0xf1	; 241
      0006B4 01                    1103 	.db #0x01	; 1
      0006B5 01                    1104 	.db #0x01	; 1
      0006B6 01                    1105 	.db #0x01	; 1
      0006B7 01                    1106 	.db #0x01	; 1
      0006B8 41                    1107 	.db #0x41	; 65	'A'
      0006B9 41                    1108 	.db #0x41	; 65	'A'
      0006BA F1                    1109 	.db #0xf1	; 241
      0006BB 01                    1110 	.db #0x01	; 1
      0006BC 01                    1111 	.db #0x01	; 1
      0006BD 01                    1112 	.db #0x01	; 1
      0006BE 01                    1113 	.db #0x01	; 1
      0006BF 01                    1114 	.db #0x01	; 1
      0006C0 C1                    1115 	.db #0xc1	; 193
      0006C1 21                    1116 	.db #0x21	; 33
      0006C2 11                    1117 	.db #0x11	; 17
      0006C3 11                    1118 	.db #0x11	; 17
      0006C4 11                    1119 	.db #0x11	; 17
      0006C5 11                    1120 	.db #0x11	; 17
      0006C6 21                    1121 	.db #0x21	; 33
      0006C7 C1                    1122 	.db #0xc1	; 193
      0006C8 01                    1123 	.db #0x01	; 1
      0006C9 01                    1124 	.db #0x01	; 1
      0006CA 01                    1125 	.db #0x01	; 1
      0006CB 01                    1126 	.db #0x01	; 1
      0006CC 41                    1127 	.db #0x41	; 65	'A'
      0006CD 41                    1128 	.db #0x41	; 65	'A'
      0006CE F1                    1129 	.db #0xf1	; 241
      0006CF 01                    1130 	.db #0x01	; 1
      0006D0 01                    1131 	.db #0x01	; 1
      0006D1 01                    1132 	.db #0x01	; 1
      0006D2 01                    1133 	.db #0x01	; 1
      0006D3 01                    1134 	.db #0x01	; 1
      0006D4 01                    1135 	.db #0x01	; 1
      0006D5 01                    1136 	.db #0x01	; 1
      0006D6 01                    1137 	.db #0x01	; 1
      0006D7 01                    1138 	.db #0x01	; 1
      0006D8 01                    1139 	.db #0x01	; 1
      0006D9 11                    1140 	.db #0x11	; 17
      0006DA 11                    1141 	.db #0x11	; 17
      0006DB 11                    1142 	.db #0x11	; 17
      0006DC 11                    1143 	.db #0x11	; 17
      0006DD 11                    1144 	.db #0x11	; 17
      0006DE D3                    1145 	.db #0xd3	; 211
      0006DF 33                    1146 	.db #0x33	; 51	'3'
      0006E0 03                    1147 	.db #0x03	; 3
      0006E1 03                    1148 	.db #0x03	; 3
      0006E2 3F                    1149 	.db #0x3f	; 63
      0006E3 3F                    1150 	.db #0x3f	; 63
      0006E4 00                    1151 	.db #0x00	; 0
      0006E5 00                    1152 	.db #0x00	; 0
      0006E6 00                    1153 	.db #0x00	; 0
      0006E7 00                    1154 	.db #0x00	; 0
      0006E8 00                    1155 	.db #0x00	; 0
      0006E9 00                    1156 	.db #0x00	; 0
      0006EA 00                    1157 	.db #0x00	; 0
      0006EB 00                    1158 	.db #0x00	; 0
      0006EC 00                    1159 	.db #0x00	; 0
      0006ED 00                    1160 	.db #0x00	; 0
      0006EE 00                    1161 	.db #0x00	; 0
      0006EF 00                    1162 	.db #0x00	; 0
      0006F0 00                    1163 	.db #0x00	; 0
      0006F1 00                    1164 	.db #0x00	; 0
      0006F2 00                    1165 	.db #0x00	; 0
      0006F3 00                    1166 	.db #0x00	; 0
      0006F4 00                    1167 	.db #0x00	; 0
      0006F5 00                    1168 	.db #0x00	; 0
      0006F6 00                    1169 	.db #0x00	; 0
      0006F7 00                    1170 	.db #0x00	; 0
      0006F8 00                    1171 	.db #0x00	; 0
      0006F9 00                    1172 	.db #0x00	; 0
      0006FA 00                    1173 	.db #0x00	; 0
      0006FB 00                    1174 	.db #0x00	; 0
      0006FC 00                    1175 	.db #0x00	; 0
      0006FD 00                    1176 	.db #0x00	; 0
      0006FE 00                    1177 	.db #0x00	; 0
      0006FF 00                    1178 	.db #0x00	; 0
      000700 00                    1179 	.db #0x00	; 0
      000701 00                    1180 	.db #0x00	; 0
      000702 00                    1181 	.db #0x00	; 0
      000703 00                    1182 	.db #0x00	; 0
      000704 00                    1183 	.db #0x00	; 0
      000705 00                    1184 	.db #0x00	; 0
      000706 00                    1185 	.db #0x00	; 0
      000707 00                    1186 	.db #0x00	; 0
      000708 00                    1187 	.db #0x00	; 0
      000709 00                    1188 	.db #0x00	; 0
      00070A 00                    1189 	.db #0x00	; 0
      00070B 00                    1190 	.db #0x00	; 0
      00070C 00                    1191 	.db #0x00	; 0
      00070D 00                    1192 	.db #0x00	; 0
      00070E 00                    1193 	.db #0x00	; 0
      00070F 00                    1194 	.db #0x00	; 0
      000710 00                    1195 	.db #0x00	; 0
      000711 00                    1196 	.db #0x00	; 0
      000712 00                    1197 	.db #0x00	; 0
      000713 00                    1198 	.db #0x00	; 0
      000714 00                    1199 	.db #0x00	; 0
      000715 00                    1200 	.db #0x00	; 0
      000716 00                    1201 	.db #0x00	; 0
      000717 00                    1202 	.db #0x00	; 0
      000718 00                    1203 	.db #0x00	; 0
      000719 00                    1204 	.db #0x00	; 0
      00071A 00                    1205 	.db #0x00	; 0
      00071B 00                    1206 	.db #0x00	; 0
      00071C E0                    1207 	.db #0xe0	; 224
      00071D E0                    1208 	.db #0xe0	; 224
      00071E 00                    1209 	.db #0x00	; 0
      00071F 00                    1210 	.db #0x00	; 0
      000720 7F                    1211 	.db #0x7f	; 127
      000721 01                    1212 	.db #0x01	; 1
      000722 01                    1213 	.db #0x01	; 1
      000723 01                    1214 	.db #0x01	; 1
      000724 01                    1215 	.db #0x01	; 1
      000725 01                    1216 	.db #0x01	; 1
      000726 01                    1217 	.db #0x01	; 1
      000727 00                    1218 	.db #0x00	; 0
      000728 00                    1219 	.db #0x00	; 0
      000729 7F                    1220 	.db #0x7f	; 127
      00072A 00                    1221 	.db #0x00	; 0
      00072B 00                    1222 	.db #0x00	; 0
      00072C 01                    1223 	.db #0x01	; 1
      00072D 06                    1224 	.db #0x06	; 6
      00072E 18                    1225 	.db #0x18	; 24
      00072F 06                    1226 	.db #0x06	; 6
      000730 01                    1227 	.db #0x01	; 1
      000731 00                    1228 	.db #0x00	; 0
      000732 00                    1229 	.db #0x00	; 0
      000733 7F                    1230 	.db #0x7f	; 127
      000734 00                    1231 	.db #0x00	; 0
      000735 00                    1232 	.db #0x00	; 0
      000736 00                    1233 	.db #0x00	; 0
      000737 00                    1234 	.db #0x00	; 0
      000738 40                    1235 	.db #0x40	; 64
      000739 40                    1236 	.db #0x40	; 64
      00073A 7F                    1237 	.db #0x7f	; 127
      00073B 40                    1238 	.db #0x40	; 64
      00073C 40                    1239 	.db #0x40	; 64
      00073D 00                    1240 	.db #0x00	; 0
      00073E 00                    1241 	.db #0x00	; 0
      00073F 00                    1242 	.db #0x00	; 0
      000740 1F                    1243 	.db #0x1f	; 31
      000741 20                    1244 	.db #0x20	; 32
      000742 40                    1245 	.db #0x40	; 64
      000743 40                    1246 	.db #0x40	; 64
      000744 40                    1247 	.db #0x40	; 64
      000745 40                    1248 	.db #0x40	; 64
      000746 20                    1249 	.db #0x20	; 32
      000747 1F                    1250 	.db #0x1f	; 31
      000748 00                    1251 	.db #0x00	; 0
      000749 00                    1252 	.db #0x00	; 0
      00074A 00                    1253 	.db #0x00	; 0
      00074B 00                    1254 	.db #0x00	; 0
      00074C 40                    1255 	.db #0x40	; 64
      00074D 40                    1256 	.db #0x40	; 64
      00074E 7F                    1257 	.db #0x7f	; 127
      00074F 40                    1258 	.db #0x40	; 64
      000750 40                    1259 	.db #0x40	; 64
      000751 00                    1260 	.db #0x00	; 0
      000752 00                    1261 	.db #0x00	; 0
      000753 00                    1262 	.db #0x00	; 0
      000754 00                    1263 	.db #0x00	; 0
      000755 60                    1264 	.db #0x60	; 96
      000756 00                    1265 	.db #0x00	; 0
      000757 00                    1266 	.db #0x00	; 0
      000758 00                    1267 	.db #0x00	; 0
      000759 00                    1268 	.db #0x00	; 0
      00075A 40                    1269 	.db #0x40	; 64
      00075B 30                    1270 	.db #0x30	; 48	'0'
      00075C 0C                    1271 	.db #0x0c	; 12
      00075D 03                    1272 	.db #0x03	; 3
      00075E 00                    1273 	.db #0x00	; 0
      00075F 00                    1274 	.db #0x00	; 0
      000760 00                    1275 	.db #0x00	; 0
      000761 00                    1276 	.db #0x00	; 0
      000762 E0                    1277 	.db #0xe0	; 224
      000763 E0                    1278 	.db #0xe0	; 224
      000764 00                    1279 	.db #0x00	; 0
      000765 00                    1280 	.db #0x00	; 0
      000766 00                    1281 	.db #0x00	; 0
      000767 00                    1282 	.db #0x00	; 0
      000768 00                    1283 	.db #0x00	; 0
      000769 00                    1284 	.db #0x00	; 0
      00076A 00                    1285 	.db #0x00	; 0
      00076B 00                    1286 	.db #0x00	; 0
      00076C 00                    1287 	.db #0x00	; 0
      00076D 00                    1288 	.db #0x00	; 0
      00076E 00                    1289 	.db #0x00	; 0
      00076F 00                    1290 	.db #0x00	; 0
      000770 00                    1291 	.db #0x00	; 0
      000771 00                    1292 	.db #0x00	; 0
      000772 00                    1293 	.db #0x00	; 0
      000773 00                    1294 	.db #0x00	; 0
      000774 00                    1295 	.db #0x00	; 0
      000775 00                    1296 	.db #0x00	; 0
      000776 00                    1297 	.db #0x00	; 0
      000777 00                    1298 	.db #0x00	; 0
      000778 00                    1299 	.db #0x00	; 0
      000779 00                    1300 	.db #0x00	; 0
      00077A 00                    1301 	.db #0x00	; 0
      00077B 00                    1302 	.db #0x00	; 0
      00077C 00                    1303 	.db #0x00	; 0
      00077D 00                    1304 	.db #0x00	; 0
      00077E 00                    1305 	.db #0x00	; 0
      00077F 00                    1306 	.db #0x00	; 0
      000780 00                    1307 	.db #0x00	; 0
      000781 00                    1308 	.db #0x00	; 0
      000782 00                    1309 	.db #0x00	; 0
      000783 00                    1310 	.db #0x00	; 0
      000784 00                    1311 	.db #0x00	; 0
      000785 00                    1312 	.db #0x00	; 0
      000786 00                    1313 	.db #0x00	; 0
      000787 00                    1314 	.db #0x00	; 0
      000788 00                    1315 	.db #0x00	; 0
      000789 00                    1316 	.db #0x00	; 0
      00078A 00                    1317 	.db #0x00	; 0
      00078B 00                    1318 	.db #0x00	; 0
      00078C 00                    1319 	.db #0x00	; 0
      00078D 00                    1320 	.db #0x00	; 0
      00078E 00                    1321 	.db #0x00	; 0
      00078F 00                    1322 	.db #0x00	; 0
      000790 00                    1323 	.db #0x00	; 0
      000791 00                    1324 	.db #0x00	; 0
      000792 00                    1325 	.db #0x00	; 0
      000793 00                    1326 	.db #0x00	; 0
      000794 00                    1327 	.db #0x00	; 0
      000795 00                    1328 	.db #0x00	; 0
      000796 00                    1329 	.db #0x00	; 0
      000797 00                    1330 	.db #0x00	; 0
      000798 00                    1331 	.db #0x00	; 0
      000799 00                    1332 	.db #0x00	; 0
      00079A 00                    1333 	.db #0x00	; 0
      00079B 00                    1334 	.db #0x00	; 0
      00079C 07                    1335 	.db #0x07	; 7
      00079D 07                    1336 	.db #0x07	; 7
      00079E 06                    1337 	.db #0x06	; 6
      00079F 06                    1338 	.db #0x06	; 6
      0007A0 06                    1339 	.db #0x06	; 6
      0007A1 06                    1340 	.db #0x06	; 6
      0007A2 04                    1341 	.db #0x04	; 4
      0007A3 04                    1342 	.db #0x04	; 4
      0007A4 04                    1343 	.db #0x04	; 4
      0007A5 84                    1344 	.db #0x84	; 132
      0007A6 44                    1345 	.db #0x44	; 68	'D'
      0007A7 44                    1346 	.db #0x44	; 68	'D'
      0007A8 44                    1347 	.db #0x44	; 68	'D'
      0007A9 84                    1348 	.db #0x84	; 132
      0007AA 04                    1349 	.db #0x04	; 4
      0007AB 04                    1350 	.db #0x04	; 4
      0007AC 84                    1351 	.db #0x84	; 132
      0007AD 44                    1352 	.db #0x44	; 68	'D'
      0007AE 44                    1353 	.db #0x44	; 68	'D'
      0007AF 44                    1354 	.db #0x44	; 68	'D'
      0007B0 84                    1355 	.db #0x84	; 132
      0007B1 04                    1356 	.db #0x04	; 4
      0007B2 04                    1357 	.db #0x04	; 4
      0007B3 04                    1358 	.db #0x04	; 4
      0007B4 84                    1359 	.db #0x84	; 132
      0007B5 C4                    1360 	.db #0xc4	; 196
      0007B6 04                    1361 	.db #0x04	; 4
      0007B7 04                    1362 	.db #0x04	; 4
      0007B8 04                    1363 	.db #0x04	; 4
      0007B9 04                    1364 	.db #0x04	; 4
      0007BA 84                    1365 	.db #0x84	; 132
      0007BB 44                    1366 	.db #0x44	; 68	'D'
      0007BC 44                    1367 	.db #0x44	; 68	'D'
      0007BD 44                    1368 	.db #0x44	; 68	'D'
      0007BE 84                    1369 	.db #0x84	; 132
      0007BF 04                    1370 	.db #0x04	; 4
      0007C0 04                    1371 	.db #0x04	; 4
      0007C1 04                    1372 	.db #0x04	; 4
      0007C2 04                    1373 	.db #0x04	; 4
      0007C3 04                    1374 	.db #0x04	; 4
      0007C4 84                    1375 	.db #0x84	; 132
      0007C5 44                    1376 	.db #0x44	; 68	'D'
      0007C6 44                    1377 	.db #0x44	; 68	'D'
      0007C7 44                    1378 	.db #0x44	; 68	'D'
      0007C8 84                    1379 	.db #0x84	; 132
      0007C9 04                    1380 	.db #0x04	; 4
      0007CA 04                    1381 	.db #0x04	; 4
      0007CB 04                    1382 	.db #0x04	; 4
      0007CC 04                    1383 	.db #0x04	; 4
      0007CD 04                    1384 	.db #0x04	; 4
      0007CE 84                    1385 	.db #0x84	; 132
      0007CF 44                    1386 	.db #0x44	; 68	'D'
      0007D0 44                    1387 	.db #0x44	; 68	'D'
      0007D1 44                    1388 	.db #0x44	; 68	'D'
      0007D2 84                    1389 	.db #0x84	; 132
      0007D3 04                    1390 	.db #0x04	; 4
      0007D4 04                    1391 	.db #0x04	; 4
      0007D5 84                    1392 	.db #0x84	; 132
      0007D6 44                    1393 	.db #0x44	; 68	'D'
      0007D7 44                    1394 	.db #0x44	; 68	'D'
      0007D8 44                    1395 	.db #0x44	; 68	'D'
      0007D9 84                    1396 	.db #0x84	; 132
      0007DA 04                    1397 	.db #0x04	; 4
      0007DB 04                    1398 	.db #0x04	; 4
      0007DC 04                    1399 	.db #0x04	; 4
      0007DD 04                    1400 	.db #0x04	; 4
      0007DE 06                    1401 	.db #0x06	; 6
      0007DF 06                    1402 	.db #0x06	; 6
      0007E0 06                    1403 	.db #0x06	; 6
      0007E1 06                    1404 	.db #0x06	; 6
      0007E2 07                    1405 	.db #0x07	; 7
      0007E3 07                    1406 	.db #0x07	; 7
      0007E4 00                    1407 	.db #0x00	; 0
      0007E5 00                    1408 	.db #0x00	; 0
      0007E6 00                    1409 	.db #0x00	; 0
      0007E7 00                    1410 	.db #0x00	; 0
      0007E8 00                    1411 	.db #0x00	; 0
      0007E9 00                    1412 	.db #0x00	; 0
      0007EA 00                    1413 	.db #0x00	; 0
      0007EB 00                    1414 	.db #0x00	; 0
      0007EC 00                    1415 	.db #0x00	; 0
      0007ED 00                    1416 	.db #0x00	; 0
      0007EE 00                    1417 	.db #0x00	; 0
      0007EF 00                    1418 	.db #0x00	; 0
      0007F0 00                    1419 	.db #0x00	; 0
      0007F1 00                    1420 	.db #0x00	; 0
      0007F2 00                    1421 	.db #0x00	; 0
      0007F3 00                    1422 	.db #0x00	; 0
      0007F4 00                    1423 	.db #0x00	; 0
      0007F5 00                    1424 	.db #0x00	; 0
      0007F6 00                    1425 	.db #0x00	; 0
      0007F7 00                    1426 	.db #0x00	; 0
      0007F8 00                    1427 	.db #0x00	; 0
      0007F9 00                    1428 	.db #0x00	; 0
      0007FA 00                    1429 	.db #0x00	; 0
      0007FB 00                    1430 	.db #0x00	; 0
      0007FC 00                    1431 	.db #0x00	; 0
      0007FD 00                    1432 	.db #0x00	; 0
      0007FE 00                    1433 	.db #0x00	; 0
      0007FF 00                    1434 	.db #0x00	; 0
      000800 00                    1435 	.db #0x00	; 0
      000801 00                    1436 	.db #0x00	; 0
      000802 00                    1437 	.db #0x00	; 0
      000803 00                    1438 	.db #0x00	; 0
      000804 00                    1439 	.db #0x00	; 0
      000805 00                    1440 	.db #0x00	; 0
      000806 00                    1441 	.db #0x00	; 0
      000807 00                    1442 	.db #0x00	; 0
      000808 00                    1443 	.db #0x00	; 0
      000809 00                    1444 	.db #0x00	; 0
      00080A 00                    1445 	.db #0x00	; 0
      00080B 00                    1446 	.db #0x00	; 0
      00080C 00                    1447 	.db #0x00	; 0
      00080D 00                    1448 	.db #0x00	; 0
      00080E 00                    1449 	.db #0x00	; 0
      00080F 00                    1450 	.db #0x00	; 0
      000810 00                    1451 	.db #0x00	; 0
      000811 00                    1452 	.db #0x00	; 0
      000812 00                    1453 	.db #0x00	; 0
      000813 00                    1454 	.db #0x00	; 0
      000814 00                    1455 	.db #0x00	; 0
      000815 00                    1456 	.db #0x00	; 0
      000816 00                    1457 	.db #0x00	; 0
      000817 00                    1458 	.db #0x00	; 0
      000818 00                    1459 	.db #0x00	; 0
      000819 00                    1460 	.db #0x00	; 0
      00081A 00                    1461 	.db #0x00	; 0
      00081B 00                    1462 	.db #0x00	; 0
      00081C 00                    1463 	.db #0x00	; 0
      00081D 00                    1464 	.db #0x00	; 0
      00081E 00                    1465 	.db #0x00	; 0
      00081F 00                    1466 	.db #0x00	; 0
      000820 00                    1467 	.db #0x00	; 0
      000821 00                    1468 	.db #0x00	; 0
      000822 00                    1469 	.db #0x00	; 0
      000823 00                    1470 	.db #0x00	; 0
      000824 00                    1471 	.db #0x00	; 0
      000825 10                    1472 	.db #0x10	; 16
      000826 18                    1473 	.db #0x18	; 24
      000827 14                    1474 	.db #0x14	; 20
      000828 12                    1475 	.db #0x12	; 18
      000829 11                    1476 	.db #0x11	; 17
      00082A 00                    1477 	.db #0x00	; 0
      00082B 00                    1478 	.db #0x00	; 0
      00082C 0F                    1479 	.db #0x0f	; 15
      00082D 10                    1480 	.db #0x10	; 16
      00082E 10                    1481 	.db #0x10	; 16
      00082F 10                    1482 	.db #0x10	; 16
      000830 0F                    1483 	.db #0x0f	; 15
      000831 00                    1484 	.db #0x00	; 0
      000832 00                    1485 	.db #0x00	; 0
      000833 00                    1486 	.db #0x00	; 0
      000834 10                    1487 	.db #0x10	; 16
      000835 1F                    1488 	.db #0x1f	; 31
      000836 10                    1489 	.db #0x10	; 16
      000837 00                    1490 	.db #0x00	; 0
      000838 00                    1491 	.db #0x00	; 0
      000839 00                    1492 	.db #0x00	; 0
      00083A 08                    1493 	.db #0x08	; 8
      00083B 10                    1494 	.db #0x10	; 16
      00083C 12                    1495 	.db #0x12	; 18
      00083D 12                    1496 	.db #0x12	; 18
      00083E 0D                    1497 	.db #0x0d	; 13
      00083F 00                    1498 	.db #0x00	; 0
      000840 00                    1499 	.db #0x00	; 0
      000841 18                    1500 	.db #0x18	; 24
      000842 00                    1501 	.db #0x00	; 0
      000843 00                    1502 	.db #0x00	; 0
      000844 0D                    1503 	.db #0x0d	; 13
      000845 12                    1504 	.db #0x12	; 18
      000846 12                    1505 	.db #0x12	; 18
      000847 12                    1506 	.db #0x12	; 18
      000848 0D                    1507 	.db #0x0d	; 13
      000849 00                    1508 	.db #0x00	; 0
      00084A 00                    1509 	.db #0x00	; 0
      00084B 18                    1510 	.db #0x18	; 24
      00084C 00                    1511 	.db #0x00	; 0
      00084D 00                    1512 	.db #0x00	; 0
      00084E 10                    1513 	.db #0x10	; 16
      00084F 18                    1514 	.db #0x18	; 24
      000850 14                    1515 	.db #0x14	; 20
      000851 12                    1516 	.db #0x12	; 18
      000852 11                    1517 	.db #0x11	; 17
      000853 00                    1518 	.db #0x00	; 0
      000854 00                    1519 	.db #0x00	; 0
      000855 10                    1520 	.db #0x10	; 16
      000856 18                    1521 	.db #0x18	; 24
      000857 14                    1522 	.db #0x14	; 20
      000858 12                    1523 	.db #0x12	; 18
      000859 11                    1524 	.db #0x11	; 17
      00085A 00                    1525 	.db #0x00	; 0
      00085B 00                    1526 	.db #0x00	; 0
      00085C 00                    1527 	.db #0x00	; 0
      00085D 00                    1528 	.db #0x00	; 0
      00085E 00                    1529 	.db #0x00	; 0
      00085F 00                    1530 	.db #0x00	; 0
      000860 00                    1531 	.db #0x00	; 0
      000861 00                    1532 	.db #0x00	; 0
      000862 00                    1533 	.db #0x00	; 0
      000863 00                    1534 	.db #0x00	; 0
      000864 00                    1535 	.db #0x00	; 0
      000865 00                    1536 	.db #0x00	; 0
      000866 00                    1537 	.db #0x00	; 0
      000867 00                    1538 	.db #0x00	; 0
      000868 00                    1539 	.db #0x00	; 0
      000869 00                    1540 	.db #0x00	; 0
      00086A 00                    1541 	.db #0x00	; 0
      00086B 00                    1542 	.db #0x00	; 0
      00086C 00                    1543 	.db #0x00	; 0
      00086D 00                    1544 	.db #0x00	; 0
      00086E 00                    1545 	.db #0x00	; 0
      00086F 00                    1546 	.db #0x00	; 0
      000870 00                    1547 	.db #0x00	; 0
      000871 00                    1548 	.db #0x00	; 0
      000872 00                    1549 	.db #0x00	; 0
      000873 00                    1550 	.db #0x00	; 0
      000874 00                    1551 	.db #0x00	; 0
      000875 00                    1552 	.db #0x00	; 0
      000876 00                    1553 	.db #0x00	; 0
      000877 00                    1554 	.db #0x00	; 0
      000878 00                    1555 	.db #0x00	; 0
      000879 00                    1556 	.db #0x00	; 0
      00087A 00                    1557 	.db #0x00	; 0
      00087B 00                    1558 	.db #0x00	; 0
      00087C 00                    1559 	.db #0x00	; 0
      00087D 00                    1560 	.db #0x00	; 0
      00087E 00                    1561 	.db #0x00	; 0
      00087F 00                    1562 	.db #0x00	; 0
      000880 00                    1563 	.db #0x00	; 0
      000881 00                    1564 	.db #0x00	; 0
      000882 00                    1565 	.db #0x00	; 0
      000883 00                    1566 	.db #0x00	; 0
      000884 00                    1567 	.db #0x00	; 0
      000885 00                    1568 	.db #0x00	; 0
      000886 00                    1569 	.db #0x00	; 0
      000887 00                    1570 	.db #0x00	; 0
      000888 00                    1571 	.db #0x00	; 0
      000889 00                    1572 	.db #0x00	; 0
      00088A 00                    1573 	.db #0x00	; 0
      00088B 00                    1574 	.db #0x00	; 0
      00088C 00                    1575 	.db #0x00	; 0
      00088D 00                    1576 	.db #0x00	; 0
      00088E 00                    1577 	.db #0x00	; 0
      00088F 00                    1578 	.db #0x00	; 0
      000890 00                    1579 	.db #0x00	; 0
      000891 00                    1580 	.db #0x00	; 0
      000892 00                    1581 	.db #0x00	; 0
      000893 00                    1582 	.db #0x00	; 0
      000894 00                    1583 	.db #0x00	; 0
      000895 00                    1584 	.db #0x00	; 0
      000896 00                    1585 	.db #0x00	; 0
      000897 00                    1586 	.db #0x00	; 0
      000898 00                    1587 	.db #0x00	; 0
      000899 00                    1588 	.db #0x00	; 0
      00089A 00                    1589 	.db #0x00	; 0
      00089B 00                    1590 	.db #0x00	; 0
      00089C 00                    1591 	.db #0x00	; 0
      00089D 00                    1592 	.db #0x00	; 0
      00089E 00                    1593 	.db #0x00	; 0
      00089F 00                    1594 	.db #0x00	; 0
      0008A0 00                    1595 	.db #0x00	; 0
      0008A1 00                    1596 	.db #0x00	; 0
      0008A2 00                    1597 	.db #0x00	; 0
      0008A3 00                    1598 	.db #0x00	; 0
      0008A4 00                    1599 	.db #0x00	; 0
      0008A5 00                    1600 	.db #0x00	; 0
      0008A6 00                    1601 	.db #0x00	; 0
      0008A7 00                    1602 	.db #0x00	; 0
      0008A8 00                    1603 	.db #0x00	; 0
      0008A9 00                    1604 	.db #0x00	; 0
      0008AA 00                    1605 	.db #0x00	; 0
      0008AB 00                    1606 	.db #0x00	; 0
      0008AC 00                    1607 	.db #0x00	; 0
      0008AD 00                    1608 	.db #0x00	; 0
      0008AE 00                    1609 	.db #0x00	; 0
      0008AF 00                    1610 	.db #0x00	; 0
      0008B0 00                    1611 	.db #0x00	; 0
      0008B1 00                    1612 	.db #0x00	; 0
      0008B2 00                    1613 	.db #0x00	; 0
      0008B3 00                    1614 	.db #0x00	; 0
      0008B4 00                    1615 	.db #0x00	; 0
      0008B5 00                    1616 	.db #0x00	; 0
      0008B6 00                    1617 	.db #0x00	; 0
      0008B7 00                    1618 	.db #0x00	; 0
      0008B8 00                    1619 	.db #0x00	; 0
      0008B9 00                    1620 	.db #0x00	; 0
      0008BA 00                    1621 	.db #0x00	; 0
      0008BB 00                    1622 	.db #0x00	; 0
      0008BC 80                    1623 	.db #0x80	; 128
      0008BD 80                    1624 	.db #0x80	; 128
      0008BE 80                    1625 	.db #0x80	; 128
      0008BF 80                    1626 	.db #0x80	; 128
      0008C0 80                    1627 	.db #0x80	; 128
      0008C1 80                    1628 	.db #0x80	; 128
      0008C2 80                    1629 	.db #0x80	; 128
      0008C3 80                    1630 	.db #0x80	; 128
      0008C4 00                    1631 	.db #0x00	; 0
      0008C5 00                    1632 	.db #0x00	; 0
      0008C6 00                    1633 	.db #0x00	; 0
      0008C7 00                    1634 	.db #0x00	; 0
      0008C8 00                    1635 	.db #0x00	; 0
      0008C9 00                    1636 	.db #0x00	; 0
      0008CA 00                    1637 	.db #0x00	; 0
      0008CB 00                    1638 	.db #0x00	; 0
      0008CC 00                    1639 	.db #0x00	; 0
      0008CD 00                    1640 	.db #0x00	; 0
      0008CE 00                    1641 	.db #0x00	; 0
      0008CF 00                    1642 	.db #0x00	; 0
      0008D0 00                    1643 	.db #0x00	; 0
      0008D1 00                    1644 	.db #0x00	; 0
      0008D2 00                    1645 	.db #0x00	; 0
      0008D3 00                    1646 	.db #0x00	; 0
      0008D4 00                    1647 	.db #0x00	; 0
      0008D5 00                    1648 	.db #0x00	; 0
      0008D6 00                    1649 	.db #0x00	; 0
      0008D7 00                    1650 	.db #0x00	; 0
      0008D8 00                    1651 	.db #0x00	; 0
      0008D9 00                    1652 	.db #0x00	; 0
      0008DA 00                    1653 	.db #0x00	; 0
      0008DB 00                    1654 	.db #0x00	; 0
      0008DC 00                    1655 	.db #0x00	; 0
      0008DD 00                    1656 	.db #0x00	; 0
      0008DE 00                    1657 	.db #0x00	; 0
      0008DF 00                    1658 	.db #0x00	; 0
      0008E0 00                    1659 	.db #0x00	; 0
      0008E1 00                    1660 	.db #0x00	; 0
      0008E2 00                    1661 	.db #0x00	; 0
      0008E3 00                    1662 	.db #0x00	; 0
      0008E4 00                    1663 	.db #0x00	; 0
      0008E5 00                    1664 	.db #0x00	; 0
      0008E6 00                    1665 	.db #0x00	; 0
      0008E7 00                    1666 	.db #0x00	; 0
      0008E8 00                    1667 	.db #0x00	; 0
      0008E9 00                    1668 	.db #0x00	; 0
      0008EA 00                    1669 	.db #0x00	; 0
      0008EB 00                    1670 	.db #0x00	; 0
      0008EC 00                    1671 	.db #0x00	; 0
      0008ED 00                    1672 	.db #0x00	; 0
      0008EE 00                    1673 	.db #0x00	; 0
      0008EF 00                    1674 	.db #0x00	; 0
      0008F0 00                    1675 	.db #0x00	; 0
      0008F1 00                    1676 	.db #0x00	; 0
      0008F2 00                    1677 	.db #0x00	; 0
      0008F3 00                    1678 	.db #0x00	; 0
      0008F4 00                    1679 	.db #0x00	; 0
      0008F5 00                    1680 	.db #0x00	; 0
      0008F6 00                    1681 	.db #0x00	; 0
      0008F7 00                    1682 	.db #0x00	; 0
      0008F8 00                    1683 	.db #0x00	; 0
      0008F9 00                    1684 	.db #0x00	; 0
      0008FA 00                    1685 	.db #0x00	; 0
      0008FB 00                    1686 	.db #0x00	; 0
      0008FC 00                    1687 	.db #0x00	; 0
      0008FD 00                    1688 	.db #0x00	; 0
      0008FE 00                    1689 	.db #0x00	; 0
      0008FF 00                    1690 	.db #0x00	; 0
      000900 00                    1691 	.db #0x00	; 0
      000901 7F                    1692 	.db #0x7f	; 127
      000902 03                    1693 	.db #0x03	; 3
      000903 0C                    1694 	.db #0x0c	; 12
      000904 30                    1695 	.db #0x30	; 48	'0'
      000905 0C                    1696 	.db #0x0c	; 12
      000906 03                    1697 	.db #0x03	; 3
      000907 7F                    1698 	.db #0x7f	; 127
      000908 00                    1699 	.db #0x00	; 0
      000909 00                    1700 	.db #0x00	; 0
      00090A 38                    1701 	.db #0x38	; 56	'8'
      00090B 54                    1702 	.db #0x54	; 84	'T'
      00090C 54                    1703 	.db #0x54	; 84	'T'
      00090D 58                    1704 	.db #0x58	; 88	'X'
      00090E 00                    1705 	.db #0x00	; 0
      00090F 00                    1706 	.db #0x00	; 0
      000910 7C                    1707 	.db #0x7c	; 124
      000911 04                    1708 	.db #0x04	; 4
      000912 04                    1709 	.db #0x04	; 4
      000913 78                    1710 	.db #0x78	; 120	'x'
      000914 00                    1711 	.db #0x00	; 0
      000915 00                    1712 	.db #0x00	; 0
      000916 3C                    1713 	.db #0x3c	; 60
      000917 40                    1714 	.db #0x40	; 64
      000918 40                    1715 	.db #0x40	; 64
      000919 7C                    1716 	.db #0x7c	; 124
      00091A 00                    1717 	.db #0x00	; 0
      00091B 00                    1718 	.db #0x00	; 0
      00091C 00                    1719 	.db #0x00	; 0
      00091D 00                    1720 	.db #0x00	; 0
      00091E 00                    1721 	.db #0x00	; 0
      00091F 00                    1722 	.db #0x00	; 0
      000920 00                    1723 	.db #0x00	; 0
      000921 00                    1724 	.db #0x00	; 0
      000922 00                    1725 	.db #0x00	; 0
      000923 00                    1726 	.db #0x00	; 0
      000924 00                    1727 	.db #0x00	; 0
      000925 00                    1728 	.db #0x00	; 0
      000926 00                    1729 	.db #0x00	; 0
      000927 00                    1730 	.db #0x00	; 0
      000928 00                    1731 	.db #0x00	; 0
      000929 00                    1732 	.db #0x00	; 0
      00092A 00                    1733 	.db #0x00	; 0
      00092B 00                    1734 	.db #0x00	; 0
      00092C 00                    1735 	.db #0x00	; 0
      00092D 00                    1736 	.db #0x00	; 0
      00092E 00                    1737 	.db #0x00	; 0
      00092F 00                    1738 	.db #0x00	; 0
      000930 00                    1739 	.db #0x00	; 0
      000931 00                    1740 	.db #0x00	; 0
      000932 00                    1741 	.db #0x00	; 0
      000933 00                    1742 	.db #0x00	; 0
      000934 00                    1743 	.db #0x00	; 0
      000935 00                    1744 	.db #0x00	; 0
      000936 00                    1745 	.db #0x00	; 0
      000937 00                    1746 	.db #0x00	; 0
      000938 00                    1747 	.db #0x00	; 0
      000939 00                    1748 	.db #0x00	; 0
      00093A 00                    1749 	.db #0x00	; 0
      00093B 00                    1750 	.db #0x00	; 0
      00093C FF                    1751 	.db #0xff	; 255
      00093D AA                    1752 	.db #0xaa	; 170
      00093E AA                    1753 	.db #0xaa	; 170
      00093F AA                    1754 	.db #0xaa	; 170
      000940 28                    1755 	.db #0x28	; 40
      000941 08                    1756 	.db #0x08	; 8
      000942 00                    1757 	.db #0x00	; 0
      000943 FF                    1758 	.db #0xff	; 255
      000944 00                    1759 	.db #0x00	; 0
      000945 00                    1760 	.db #0x00	; 0
      000946 00                    1761 	.db #0x00	; 0
      000947 00                    1762 	.db #0x00	; 0
      000948 00                    1763 	.db #0x00	; 0
      000949 00                    1764 	.db #0x00	; 0
      00094A 00                    1765 	.db #0x00	; 0
      00094B 00                    1766 	.db #0x00	; 0
      00094C 00                    1767 	.db #0x00	; 0
      00094D 00                    1768 	.db #0x00	; 0
      00094E 00                    1769 	.db #0x00	; 0
      00094F 00                    1770 	.db #0x00	; 0
      000950 00                    1771 	.db #0x00	; 0
      000951 00                    1772 	.db #0x00	; 0
      000952 00                    1773 	.db #0x00	; 0
      000953 00                    1774 	.db #0x00	; 0
      000954 00                    1775 	.db #0x00	; 0
      000955 00                    1776 	.db #0x00	; 0
      000956 00                    1777 	.db #0x00	; 0
      000957 00                    1778 	.db #0x00	; 0
      000958 00                    1779 	.db #0x00	; 0
      000959 00                    1780 	.db #0x00	; 0
      00095A 00                    1781 	.db #0x00	; 0
      00095B 00                    1782 	.db #0x00	; 0
      00095C 00                    1783 	.db #0x00	; 0
      00095D 00                    1784 	.db #0x00	; 0
      00095E 00                    1785 	.db #0x00	; 0
      00095F 00                    1786 	.db #0x00	; 0
      000960 00                    1787 	.db #0x00	; 0
      000961 00                    1788 	.db #0x00	; 0
      000962 00                    1789 	.db #0x00	; 0
      000963 00                    1790 	.db #0x00	; 0
      000964 00                    1791 	.db #0x00	; 0
      000965 00                    1792 	.db #0x00	; 0
      000966 00                    1793 	.db #0x00	; 0
      000967 00                    1794 	.db #0x00	; 0
      000968 00                    1795 	.db #0x00	; 0
      000969 7F                    1796 	.db #0x7f	; 127
      00096A 03                    1797 	.db #0x03	; 3
      00096B 0C                    1798 	.db #0x0c	; 12
      00096C 30                    1799 	.db #0x30	; 48	'0'
      00096D 0C                    1800 	.db #0x0c	; 12
      00096E 03                    1801 	.db #0x03	; 3
      00096F 7F                    1802 	.db #0x7f	; 127
      000970 00                    1803 	.db #0x00	; 0
      000971 00                    1804 	.db #0x00	; 0
      000972 26                    1805 	.db #0x26	; 38
      000973 49                    1806 	.db #0x49	; 73	'I'
      000974 49                    1807 	.db #0x49	; 73	'I'
      000975 49                    1808 	.db #0x49	; 73	'I'
      000976 32                    1809 	.db #0x32	; 50	'2'
      000977 00                    1810 	.db #0x00	; 0
      000978 00                    1811 	.db #0x00	; 0
      000979 7F                    1812 	.db #0x7f	; 127
      00097A 02                    1813 	.db #0x02	; 2
      00097B 04                    1814 	.db #0x04	; 4
      00097C 08                    1815 	.db #0x08	; 8
      00097D 10                    1816 	.db #0x10	; 16
      00097E 7F                    1817 	.db #0x7f	; 127
      00097F 00                    1818 	.db #0x00	; 0
      000980                       1819 _BMP2:
      000980 00                    1820 	.db #0x00	; 0
      000981 03                    1821 	.db #0x03	; 3
      000982 05                    1822 	.db #0x05	; 5
      000983 09                    1823 	.db #0x09	; 9
      000984 11                    1824 	.db #0x11	; 17
      000985 FF                    1825 	.db #0xff	; 255
      000986 11                    1826 	.db #0x11	; 17
      000987 89                    1827 	.db #0x89	; 137
      000988 05                    1828 	.db #0x05	; 5
      000989 C3                    1829 	.db #0xc3	; 195
      00098A 00                    1830 	.db #0x00	; 0
      00098B E0                    1831 	.db #0xe0	; 224
      00098C 00                    1832 	.db #0x00	; 0
      00098D F0                    1833 	.db #0xf0	; 240
      00098E 00                    1834 	.db #0x00	; 0
      00098F F8                    1835 	.db #0xf8	; 248
      000990 00                    1836 	.db #0x00	; 0
      000991 00                    1837 	.db #0x00	; 0
      000992 00                    1838 	.db #0x00	; 0
      000993 00                    1839 	.db #0x00	; 0
      000994 00                    1840 	.db #0x00	; 0
      000995 00                    1841 	.db #0x00	; 0
      000996 00                    1842 	.db #0x00	; 0
      000997 44                    1843 	.db #0x44	; 68	'D'
      000998 28                    1844 	.db #0x28	; 40
      000999 FF                    1845 	.db #0xff	; 255
      00099A 11                    1846 	.db #0x11	; 17
      00099B AA                    1847 	.db #0xaa	; 170
      00099C 44                    1848 	.db #0x44	; 68	'D'
      00099D 00                    1849 	.db #0x00	; 0
      00099E 00                    1850 	.db #0x00	; 0
      00099F 00                    1851 	.db #0x00	; 0
      0009A0 00                    1852 	.db #0x00	; 0
      0009A1 00                    1853 	.db #0x00	; 0
      0009A2 00                    1854 	.db #0x00	; 0
      0009A3 00                    1855 	.db #0x00	; 0
      0009A4 00                    1856 	.db #0x00	; 0
      0009A5 00                    1857 	.db #0x00	; 0
      0009A6 00                    1858 	.db #0x00	; 0
      0009A7 00                    1859 	.db #0x00	; 0
      0009A8 00                    1860 	.db #0x00	; 0
      0009A9 00                    1861 	.db #0x00	; 0
      0009AA 00                    1862 	.db #0x00	; 0
      0009AB 00                    1863 	.db #0x00	; 0
      0009AC 00                    1864 	.db #0x00	; 0
      0009AD 00                    1865 	.db #0x00	; 0
      0009AE 00                    1866 	.db #0x00	; 0
      0009AF 00                    1867 	.db #0x00	; 0
      0009B0 00                    1868 	.db #0x00	; 0
      0009B1 00                    1869 	.db #0x00	; 0
      0009B2 00                    1870 	.db #0x00	; 0
      0009B3 00                    1871 	.db #0x00	; 0
      0009B4 00                    1872 	.db #0x00	; 0
      0009B5 00                    1873 	.db #0x00	; 0
      0009B6 00                    1874 	.db #0x00	; 0
      0009B7 00                    1875 	.db #0x00	; 0
      0009B8 00                    1876 	.db #0x00	; 0
      0009B9 00                    1877 	.db #0x00	; 0
      0009BA 00                    1878 	.db #0x00	; 0
      0009BB 00                    1879 	.db #0x00	; 0
      0009BC 00                    1880 	.db #0x00	; 0
      0009BD 00                    1881 	.db #0x00	; 0
      0009BE 00                    1882 	.db #0x00	; 0
      0009BF 00                    1883 	.db #0x00	; 0
      0009C0 00                    1884 	.db #0x00	; 0
      0009C1 00                    1885 	.db #0x00	; 0
      0009C2 00                    1886 	.db #0x00	; 0
      0009C3 00                    1887 	.db #0x00	; 0
      0009C4 00                    1888 	.db #0x00	; 0
      0009C5 00                    1889 	.db #0x00	; 0
      0009C6 00                    1890 	.db #0x00	; 0
      0009C7 00                    1891 	.db #0x00	; 0
      0009C8 00                    1892 	.db #0x00	; 0
      0009C9 00                    1893 	.db #0x00	; 0
      0009CA 00                    1894 	.db #0x00	; 0
      0009CB 00                    1895 	.db #0x00	; 0
      0009CC 00                    1896 	.db #0x00	; 0
      0009CD 00                    1897 	.db #0x00	; 0
      0009CE 00                    1898 	.db #0x00	; 0
      0009CF 00                    1899 	.db #0x00	; 0
      0009D0 00                    1900 	.db #0x00	; 0
      0009D1 00                    1901 	.db #0x00	; 0
      0009D2 00                    1902 	.db #0x00	; 0
      0009D3 00                    1903 	.db #0x00	; 0
      0009D4 00                    1904 	.db #0x00	; 0
      0009D5 00                    1905 	.db #0x00	; 0
      0009D6 00                    1906 	.db #0x00	; 0
      0009D7 00                    1907 	.db #0x00	; 0
      0009D8 00                    1908 	.db #0x00	; 0
      0009D9 00                    1909 	.db #0x00	; 0
      0009DA 83                    1910 	.db #0x83	; 131
      0009DB 01                    1911 	.db #0x01	; 1
      0009DC 38                    1912 	.db #0x38	; 56	'8'
      0009DD 44                    1913 	.db #0x44	; 68	'D'
      0009DE 82                    1914 	.db #0x82	; 130
      0009DF 92                    1915 	.db #0x92	; 146
      0009E0 92                    1916 	.db #0x92	; 146
      0009E1 74                    1917 	.db #0x74	; 116	't'
      0009E2 01                    1918 	.db #0x01	; 1
      0009E3 83                    1919 	.db #0x83	; 131
      0009E4 00                    1920 	.db #0x00	; 0
      0009E5 00                    1921 	.db #0x00	; 0
      0009E6 00                    1922 	.db #0x00	; 0
      0009E7 00                    1923 	.db #0x00	; 0
      0009E8 00                    1924 	.db #0x00	; 0
      0009E9 00                    1925 	.db #0x00	; 0
      0009EA 00                    1926 	.db #0x00	; 0
      0009EB 7C                    1927 	.db #0x7c	; 124
      0009EC 44                    1928 	.db #0x44	; 68	'D'
      0009ED FF                    1929 	.db #0xff	; 255
      0009EE 01                    1930 	.db #0x01	; 1
      0009EF 7D                    1931 	.db #0x7d	; 125
      0009F0 7D                    1932 	.db #0x7d	; 125
      0009F1 7D                    1933 	.db #0x7d	; 125
      0009F2 7D                    1934 	.db #0x7d	; 125
      0009F3 01                    1935 	.db #0x01	; 1
      0009F4 7D                    1936 	.db #0x7d	; 125
      0009F5 7D                    1937 	.db #0x7d	; 125
      0009F6 7D                    1938 	.db #0x7d	; 125
      0009F7 7D                    1939 	.db #0x7d	; 125
      0009F8 01                    1940 	.db #0x01	; 1
      0009F9 7D                    1941 	.db #0x7d	; 125
      0009FA 7D                    1942 	.db #0x7d	; 125
      0009FB 7D                    1943 	.db #0x7d	; 125
      0009FC 7D                    1944 	.db #0x7d	; 125
      0009FD 01                    1945 	.db #0x01	; 1
      0009FE FF                    1946 	.db #0xff	; 255
      0009FF 00                    1947 	.db #0x00	; 0
      000A00 00                    1948 	.db #0x00	; 0
      000A01 00                    1949 	.db #0x00	; 0
      000A02 00                    1950 	.db #0x00	; 0
      000A03 00                    1951 	.db #0x00	; 0
      000A04 00                    1952 	.db #0x00	; 0
      000A05 01                    1953 	.db #0x01	; 1
      000A06 00                    1954 	.db #0x00	; 0
      000A07 01                    1955 	.db #0x01	; 1
      000A08 00                    1956 	.db #0x00	; 0
      000A09 01                    1957 	.db #0x01	; 1
      000A0A 00                    1958 	.db #0x00	; 0
      000A0B 01                    1959 	.db #0x01	; 1
      000A0C 00                    1960 	.db #0x00	; 0
      000A0D 01                    1961 	.db #0x01	; 1
      000A0E 00                    1962 	.db #0x00	; 0
      000A0F 01                    1963 	.db #0x01	; 1
      000A10 00                    1964 	.db #0x00	; 0
      000A11 00                    1965 	.db #0x00	; 0
      000A12 00                    1966 	.db #0x00	; 0
      000A13 00                    1967 	.db #0x00	; 0
      000A14 00                    1968 	.db #0x00	; 0
      000A15 00                    1969 	.db #0x00	; 0
      000A16 00                    1970 	.db #0x00	; 0
      000A17 00                    1971 	.db #0x00	; 0
      000A18 00                    1972 	.db #0x00	; 0
      000A19 01                    1973 	.db #0x01	; 1
      000A1A 01                    1974 	.db #0x01	; 1
      000A1B 00                    1975 	.db #0x00	; 0
      000A1C 00                    1976 	.db #0x00	; 0
      000A1D 00                    1977 	.db #0x00	; 0
      000A1E 00                    1978 	.db #0x00	; 0
      000A1F 00                    1979 	.db #0x00	; 0
      000A20 00                    1980 	.db #0x00	; 0
      000A21 00                    1981 	.db #0x00	; 0
      000A22 00                    1982 	.db #0x00	; 0
      000A23 00                    1983 	.db #0x00	; 0
      000A24 00                    1984 	.db #0x00	; 0
      000A25 00                    1985 	.db #0x00	; 0
      000A26 00                    1986 	.db #0x00	; 0
      000A27 00                    1987 	.db #0x00	; 0
      000A28 00                    1988 	.db #0x00	; 0
      000A29 00                    1989 	.db #0x00	; 0
      000A2A 00                    1990 	.db #0x00	; 0
      000A2B 00                    1991 	.db #0x00	; 0
      000A2C 00                    1992 	.db #0x00	; 0
      000A2D 00                    1993 	.db #0x00	; 0
      000A2E 00                    1994 	.db #0x00	; 0
      000A2F 00                    1995 	.db #0x00	; 0
      000A30 00                    1996 	.db #0x00	; 0
      000A31 00                    1997 	.db #0x00	; 0
      000A32 00                    1998 	.db #0x00	; 0
      000A33 00                    1999 	.db #0x00	; 0
      000A34 00                    2000 	.db #0x00	; 0
      000A35 00                    2001 	.db #0x00	; 0
      000A36 00                    2002 	.db #0x00	; 0
      000A37 00                    2003 	.db #0x00	; 0
      000A38 00                    2004 	.db #0x00	; 0
      000A39 00                    2005 	.db #0x00	; 0
      000A3A 00                    2006 	.db #0x00	; 0
      000A3B 00                    2007 	.db #0x00	; 0
      000A3C 00                    2008 	.db #0x00	; 0
      000A3D 00                    2009 	.db #0x00	; 0
      000A3E 00                    2010 	.db #0x00	; 0
      000A3F 00                    2011 	.db #0x00	; 0
      000A40 00                    2012 	.db #0x00	; 0
      000A41 00                    2013 	.db #0x00	; 0
      000A42 00                    2014 	.db #0x00	; 0
      000A43 00                    2015 	.db #0x00	; 0
      000A44 00                    2016 	.db #0x00	; 0
      000A45 00                    2017 	.db #0x00	; 0
      000A46 00                    2018 	.db #0x00	; 0
      000A47 00                    2019 	.db #0x00	; 0
      000A48 00                    2020 	.db #0x00	; 0
      000A49 00                    2021 	.db #0x00	; 0
      000A4A 00                    2022 	.db #0x00	; 0
      000A4B 00                    2023 	.db #0x00	; 0
      000A4C 00                    2024 	.db #0x00	; 0
      000A4D 00                    2025 	.db #0x00	; 0
      000A4E 00                    2026 	.db #0x00	; 0
      000A4F 00                    2027 	.db #0x00	; 0
      000A50 00                    2028 	.db #0x00	; 0
      000A51 00                    2029 	.db #0x00	; 0
      000A52 00                    2030 	.db #0x00	; 0
      000A53 00                    2031 	.db #0x00	; 0
      000A54 00                    2032 	.db #0x00	; 0
      000A55 00                    2033 	.db #0x00	; 0
      000A56 00                    2034 	.db #0x00	; 0
      000A57 00                    2035 	.db #0x00	; 0
      000A58 00                    2036 	.db #0x00	; 0
      000A59 00                    2037 	.db #0x00	; 0
      000A5A 01                    2038 	.db #0x01	; 1
      000A5B 01                    2039 	.db #0x01	; 1
      000A5C 00                    2040 	.db #0x00	; 0
      000A5D 00                    2041 	.db #0x00	; 0
      000A5E 00                    2042 	.db #0x00	; 0
      000A5F 00                    2043 	.db #0x00	; 0
      000A60 00                    2044 	.db #0x00	; 0
      000A61 00                    2045 	.db #0x00	; 0
      000A62 01                    2046 	.db #0x01	; 1
      000A63 01                    2047 	.db #0x01	; 1
      000A64 00                    2048 	.db #0x00	; 0
      000A65 00                    2049 	.db #0x00	; 0
      000A66 00                    2050 	.db #0x00	; 0
      000A67 00                    2051 	.db #0x00	; 0
      000A68 00                    2052 	.db #0x00	; 0
      000A69 00                    2053 	.db #0x00	; 0
      000A6A 00                    2054 	.db #0x00	; 0
      000A6B 00                    2055 	.db #0x00	; 0
      000A6C 00                    2056 	.db #0x00	; 0
      000A6D 01                    2057 	.db #0x01	; 1
      000A6E 01                    2058 	.db #0x01	; 1
      000A6F 01                    2059 	.db #0x01	; 1
      000A70 01                    2060 	.db #0x01	; 1
      000A71 01                    2061 	.db #0x01	; 1
      000A72 01                    2062 	.db #0x01	; 1
      000A73 01                    2063 	.db #0x01	; 1
      000A74 01                    2064 	.db #0x01	; 1
      000A75 01                    2065 	.db #0x01	; 1
      000A76 01                    2066 	.db #0x01	; 1
      000A77 01                    2067 	.db #0x01	; 1
      000A78 01                    2068 	.db #0x01	; 1
      000A79 01                    2069 	.db #0x01	; 1
      000A7A 01                    2070 	.db #0x01	; 1
      000A7B 01                    2071 	.db #0x01	; 1
      000A7C 01                    2072 	.db #0x01	; 1
      000A7D 01                    2073 	.db #0x01	; 1
      000A7E 01                    2074 	.db #0x01	; 1
      000A7F 00                    2075 	.db #0x00	; 0
      000A80 00                    2076 	.db #0x00	; 0
      000A81 00                    2077 	.db #0x00	; 0
      000A82 00                    2078 	.db #0x00	; 0
      000A83 00                    2079 	.db #0x00	; 0
      000A84 00                    2080 	.db #0x00	; 0
      000A85 00                    2081 	.db #0x00	; 0
      000A86 00                    2082 	.db #0x00	; 0
      000A87 00                    2083 	.db #0x00	; 0
      000A88 00                    2084 	.db #0x00	; 0
      000A89 00                    2085 	.db #0x00	; 0
      000A8A 00                    2086 	.db #0x00	; 0
      000A8B 00                    2087 	.db #0x00	; 0
      000A8C 00                    2088 	.db #0x00	; 0
      000A8D 00                    2089 	.db #0x00	; 0
      000A8E 00                    2090 	.db #0x00	; 0
      000A8F 00                    2091 	.db #0x00	; 0
      000A90 00                    2092 	.db #0x00	; 0
      000A91 00                    2093 	.db #0x00	; 0
      000A92 00                    2094 	.db #0x00	; 0
      000A93 00                    2095 	.db #0x00	; 0
      000A94 00                    2096 	.db #0x00	; 0
      000A95 00                    2097 	.db #0x00	; 0
      000A96 00                    2098 	.db #0x00	; 0
      000A97 00                    2099 	.db #0x00	; 0
      000A98 00                    2100 	.db #0x00	; 0
      000A99 00                    2101 	.db #0x00	; 0
      000A9A 00                    2102 	.db #0x00	; 0
      000A9B 00                    2103 	.db #0x00	; 0
      000A9C 00                    2104 	.db #0x00	; 0
      000A9D 00                    2105 	.db #0x00	; 0
      000A9E 00                    2106 	.db #0x00	; 0
      000A9F F8                    2107 	.db #0xf8	; 248
      000AA0 08                    2108 	.db #0x08	; 8
      000AA1 08                    2109 	.db #0x08	; 8
      000AA2 08                    2110 	.db #0x08	; 8
      000AA3 08                    2111 	.db #0x08	; 8
      000AA4 08                    2112 	.db #0x08	; 8
      000AA5 08                    2113 	.db #0x08	; 8
      000AA6 08                    2114 	.db #0x08	; 8
      000AA7 00                    2115 	.db #0x00	; 0
      000AA8 F8                    2116 	.db #0xf8	; 248
      000AA9 18                    2117 	.db #0x18	; 24
      000AAA 60                    2118 	.db #0x60	; 96
      000AAB 80                    2119 	.db #0x80	; 128
      000AAC 00                    2120 	.db #0x00	; 0
      000AAD 00                    2121 	.db #0x00	; 0
      000AAE 00                    2122 	.db #0x00	; 0
      000AAF 80                    2123 	.db #0x80	; 128
      000AB0 60                    2124 	.db #0x60	; 96
      000AB1 18                    2125 	.db #0x18	; 24
      000AB2 F8                    2126 	.db #0xf8	; 248
      000AB3 00                    2127 	.db #0x00	; 0
      000AB4 00                    2128 	.db #0x00	; 0
      000AB5 00                    2129 	.db #0x00	; 0
      000AB6 20                    2130 	.db #0x20	; 32
      000AB7 20                    2131 	.db #0x20	; 32
      000AB8 F8                    2132 	.db #0xf8	; 248
      000AB9 00                    2133 	.db #0x00	; 0
      000ABA 00                    2134 	.db #0x00	; 0
      000ABB 00                    2135 	.db #0x00	; 0
      000ABC 00                    2136 	.db #0x00	; 0
      000ABD 00                    2137 	.db #0x00	; 0
      000ABE 00                    2138 	.db #0x00	; 0
      000ABF E0                    2139 	.db #0xe0	; 224
      000AC0 10                    2140 	.db #0x10	; 16
      000AC1 08                    2141 	.db #0x08	; 8
      000AC2 08                    2142 	.db #0x08	; 8
      000AC3 08                    2143 	.db #0x08	; 8
      000AC4 08                    2144 	.db #0x08	; 8
      000AC5 10                    2145 	.db #0x10	; 16
      000AC6 E0                    2146 	.db #0xe0	; 224
      000AC7 00                    2147 	.db #0x00	; 0
      000AC8 00                    2148 	.db #0x00	; 0
      000AC9 00                    2149 	.db #0x00	; 0
      000ACA 20                    2150 	.db #0x20	; 32
      000ACB 20                    2151 	.db #0x20	; 32
      000ACC F8                    2152 	.db #0xf8	; 248
      000ACD 00                    2153 	.db #0x00	; 0
      000ACE 00                    2154 	.db #0x00	; 0
      000ACF 00                    2155 	.db #0x00	; 0
      000AD0 00                    2156 	.db #0x00	; 0
      000AD1 00                    2157 	.db #0x00	; 0
      000AD2 00                    2158 	.db #0x00	; 0
      000AD3 00                    2159 	.db #0x00	; 0
      000AD4 00                    2160 	.db #0x00	; 0
      000AD5 00                    2161 	.db #0x00	; 0
      000AD6 00                    2162 	.db #0x00	; 0
      000AD7 00                    2163 	.db #0x00	; 0
      000AD8 00                    2164 	.db #0x00	; 0
      000AD9 08                    2165 	.db #0x08	; 8
      000ADA 08                    2166 	.db #0x08	; 8
      000ADB 08                    2167 	.db #0x08	; 8
      000ADC 08                    2168 	.db #0x08	; 8
      000ADD 08                    2169 	.db #0x08	; 8
      000ADE 88                    2170 	.db #0x88	; 136
      000ADF 68                    2171 	.db #0x68	; 104	'h'
      000AE0 18                    2172 	.db #0x18	; 24
      000AE1 00                    2173 	.db #0x00	; 0
      000AE2 00                    2174 	.db #0x00	; 0
      000AE3 00                    2175 	.db #0x00	; 0
      000AE4 00                    2176 	.db #0x00	; 0
      000AE5 00                    2177 	.db #0x00	; 0
      000AE6 00                    2178 	.db #0x00	; 0
      000AE7 00                    2179 	.db #0x00	; 0
      000AE8 00                    2180 	.db #0x00	; 0
      000AE9 00                    2181 	.db #0x00	; 0
      000AEA 00                    2182 	.db #0x00	; 0
      000AEB 00                    2183 	.db #0x00	; 0
      000AEC 00                    2184 	.db #0x00	; 0
      000AED 00                    2185 	.db #0x00	; 0
      000AEE 00                    2186 	.db #0x00	; 0
      000AEF 00                    2187 	.db #0x00	; 0
      000AF0 00                    2188 	.db #0x00	; 0
      000AF1 00                    2189 	.db #0x00	; 0
      000AF2 00                    2190 	.db #0x00	; 0
      000AF3 00                    2191 	.db #0x00	; 0
      000AF4 00                    2192 	.db #0x00	; 0
      000AF5 00                    2193 	.db #0x00	; 0
      000AF6 00                    2194 	.db #0x00	; 0
      000AF7 00                    2195 	.db #0x00	; 0
      000AF8 00                    2196 	.db #0x00	; 0
      000AF9 00                    2197 	.db #0x00	; 0
      000AFA 00                    2198 	.db #0x00	; 0
      000AFB 00                    2199 	.db #0x00	; 0
      000AFC 00                    2200 	.db #0x00	; 0
      000AFD 00                    2201 	.db #0x00	; 0
      000AFE 00                    2202 	.db #0x00	; 0
      000AFF 00                    2203 	.db #0x00	; 0
      000B00 00                    2204 	.db #0x00	; 0
      000B01 00                    2205 	.db #0x00	; 0
      000B02 00                    2206 	.db #0x00	; 0
      000B03 00                    2207 	.db #0x00	; 0
      000B04 00                    2208 	.db #0x00	; 0
      000B05 00                    2209 	.db #0x00	; 0
      000B06 00                    2210 	.db #0x00	; 0
      000B07 00                    2211 	.db #0x00	; 0
      000B08 00                    2212 	.db #0x00	; 0
      000B09 00                    2213 	.db #0x00	; 0
      000B0A 00                    2214 	.db #0x00	; 0
      000B0B 00                    2215 	.db #0x00	; 0
      000B0C 00                    2216 	.db #0x00	; 0
      000B0D 00                    2217 	.db #0x00	; 0
      000B0E 00                    2218 	.db #0x00	; 0
      000B0F 00                    2219 	.db #0x00	; 0
      000B10 00                    2220 	.db #0x00	; 0
      000B11 00                    2221 	.db #0x00	; 0
      000B12 00                    2222 	.db #0x00	; 0
      000B13 00                    2223 	.db #0x00	; 0
      000B14 00                    2224 	.db #0x00	; 0
      000B15 00                    2225 	.db #0x00	; 0
      000B16 00                    2226 	.db #0x00	; 0
      000B17 00                    2227 	.db #0x00	; 0
      000B18 00                    2228 	.db #0x00	; 0
      000B19 00                    2229 	.db #0x00	; 0
      000B1A 00                    2230 	.db #0x00	; 0
      000B1B 00                    2231 	.db #0x00	; 0
      000B1C 00                    2232 	.db #0x00	; 0
      000B1D 00                    2233 	.db #0x00	; 0
      000B1E 00                    2234 	.db #0x00	; 0
      000B1F 7F                    2235 	.db #0x7f	; 127
      000B20 01                    2236 	.db #0x01	; 1
      000B21 01                    2237 	.db #0x01	; 1
      000B22 01                    2238 	.db #0x01	; 1
      000B23 01                    2239 	.db #0x01	; 1
      000B24 01                    2240 	.db #0x01	; 1
      000B25 01                    2241 	.db #0x01	; 1
      000B26 00                    2242 	.db #0x00	; 0
      000B27 00                    2243 	.db #0x00	; 0
      000B28 7F                    2244 	.db #0x7f	; 127
      000B29 00                    2245 	.db #0x00	; 0
      000B2A 00                    2246 	.db #0x00	; 0
      000B2B 01                    2247 	.db #0x01	; 1
      000B2C 06                    2248 	.db #0x06	; 6
      000B2D 18                    2249 	.db #0x18	; 24
      000B2E 06                    2250 	.db #0x06	; 6
      000B2F 01                    2251 	.db #0x01	; 1
      000B30 00                    2252 	.db #0x00	; 0
      000B31 00                    2253 	.db #0x00	; 0
      000B32 7F                    2254 	.db #0x7f	; 127
      000B33 00                    2255 	.db #0x00	; 0
      000B34 00                    2256 	.db #0x00	; 0
      000B35 00                    2257 	.db #0x00	; 0
      000B36 40                    2258 	.db #0x40	; 64
      000B37 40                    2259 	.db #0x40	; 64
      000B38 7F                    2260 	.db #0x7f	; 127
      000B39 40                    2261 	.db #0x40	; 64
      000B3A 40                    2262 	.db #0x40	; 64
      000B3B 00                    2263 	.db #0x00	; 0
      000B3C 00                    2264 	.db #0x00	; 0
      000B3D 00                    2265 	.db #0x00	; 0
      000B3E 00                    2266 	.db #0x00	; 0
      000B3F 1F                    2267 	.db #0x1f	; 31
      000B40 20                    2268 	.db #0x20	; 32
      000B41 40                    2269 	.db #0x40	; 64
      000B42 40                    2270 	.db #0x40	; 64
      000B43 40                    2271 	.db #0x40	; 64
      000B44 40                    2272 	.db #0x40	; 64
      000B45 20                    2273 	.db #0x20	; 32
      000B46 1F                    2274 	.db #0x1f	; 31
      000B47 00                    2275 	.db #0x00	; 0
      000B48 00                    2276 	.db #0x00	; 0
      000B49 00                    2277 	.db #0x00	; 0
      000B4A 40                    2278 	.db #0x40	; 64
      000B4B 40                    2279 	.db #0x40	; 64
      000B4C 7F                    2280 	.db #0x7f	; 127
      000B4D 40                    2281 	.db #0x40	; 64
      000B4E 40                    2282 	.db #0x40	; 64
      000B4F 00                    2283 	.db #0x00	; 0
      000B50 00                    2284 	.db #0x00	; 0
      000B51 00                    2285 	.db #0x00	; 0
      000B52 00                    2286 	.db #0x00	; 0
      000B53 00                    2287 	.db #0x00	; 0
      000B54 60                    2288 	.db #0x60	; 96
      000B55 00                    2289 	.db #0x00	; 0
      000B56 00                    2290 	.db #0x00	; 0
      000B57 00                    2291 	.db #0x00	; 0
      000B58 00                    2292 	.db #0x00	; 0
      000B59 00                    2293 	.db #0x00	; 0
      000B5A 00                    2294 	.db #0x00	; 0
      000B5B 60                    2295 	.db #0x60	; 96
      000B5C 18                    2296 	.db #0x18	; 24
      000B5D 06                    2297 	.db #0x06	; 6
      000B5E 01                    2298 	.db #0x01	; 1
      000B5F 00                    2299 	.db #0x00	; 0
      000B60 00                    2300 	.db #0x00	; 0
      000B61 00                    2301 	.db #0x00	; 0
      000B62 00                    2302 	.db #0x00	; 0
      000B63 00                    2303 	.db #0x00	; 0
      000B64 00                    2304 	.db #0x00	; 0
      000B65 00                    2305 	.db #0x00	; 0
      000B66 00                    2306 	.db #0x00	; 0
      000B67 00                    2307 	.db #0x00	; 0
      000B68 00                    2308 	.db #0x00	; 0
      000B69 00                    2309 	.db #0x00	; 0
      000B6A 00                    2310 	.db #0x00	; 0
      000B6B 00                    2311 	.db #0x00	; 0
      000B6C 00                    2312 	.db #0x00	; 0
      000B6D 00                    2313 	.db #0x00	; 0
      000B6E 00                    2314 	.db #0x00	; 0
      000B6F 00                    2315 	.db #0x00	; 0
      000B70 00                    2316 	.db #0x00	; 0
      000B71 00                    2317 	.db #0x00	; 0
      000B72 00                    2318 	.db #0x00	; 0
      000B73 00                    2319 	.db #0x00	; 0
      000B74 00                    2320 	.db #0x00	; 0
      000B75 00                    2321 	.db #0x00	; 0
      000B76 00                    2322 	.db #0x00	; 0
      000B77 00                    2323 	.db #0x00	; 0
      000B78 00                    2324 	.db #0x00	; 0
      000B79 00                    2325 	.db #0x00	; 0
      000B7A 00                    2326 	.db #0x00	; 0
      000B7B 00                    2327 	.db #0x00	; 0
      000B7C 00                    2328 	.db #0x00	; 0
      000B7D 00                    2329 	.db #0x00	; 0
      000B7E 00                    2330 	.db #0x00	; 0
      000B7F 00                    2331 	.db #0x00	; 0
      000B80 00                    2332 	.db #0x00	; 0
      000B81 00                    2333 	.db #0x00	; 0
      000B82 00                    2334 	.db #0x00	; 0
      000B83 00                    2335 	.db #0x00	; 0
      000B84 00                    2336 	.db #0x00	; 0
      000B85 00                    2337 	.db #0x00	; 0
      000B86 00                    2338 	.db #0x00	; 0
      000B87 00                    2339 	.db #0x00	; 0
      000B88 00                    2340 	.db #0x00	; 0
      000B89 00                    2341 	.db #0x00	; 0
      000B8A 00                    2342 	.db #0x00	; 0
      000B8B 00                    2343 	.db #0x00	; 0
      000B8C 00                    2344 	.db #0x00	; 0
      000B8D 00                    2345 	.db #0x00	; 0
      000B8E 00                    2346 	.db #0x00	; 0
      000B8F 00                    2347 	.db #0x00	; 0
      000B90 00                    2348 	.db #0x00	; 0
      000B91 00                    2349 	.db #0x00	; 0
      000B92 00                    2350 	.db #0x00	; 0
      000B93 00                    2351 	.db #0x00	; 0
      000B94 00                    2352 	.db #0x00	; 0
      000B95 00                    2353 	.db #0x00	; 0
      000B96 00                    2354 	.db #0x00	; 0
      000B97 00                    2355 	.db #0x00	; 0
      000B98 00                    2356 	.db #0x00	; 0
      000B99 00                    2357 	.db #0x00	; 0
      000B9A 00                    2358 	.db #0x00	; 0
      000B9B 00                    2359 	.db #0x00	; 0
      000B9C 00                    2360 	.db #0x00	; 0
      000B9D 00                    2361 	.db #0x00	; 0
      000B9E 00                    2362 	.db #0x00	; 0
      000B9F 00                    2363 	.db #0x00	; 0
      000BA0 00                    2364 	.db #0x00	; 0
      000BA1 00                    2365 	.db #0x00	; 0
      000BA2 00                    2366 	.db #0x00	; 0
      000BA3 00                    2367 	.db #0x00	; 0
      000BA4 00                    2368 	.db #0x00	; 0
      000BA5 40                    2369 	.db #0x40	; 64
      000BA6 20                    2370 	.db #0x20	; 32
      000BA7 20                    2371 	.db #0x20	; 32
      000BA8 20                    2372 	.db #0x20	; 32
      000BA9 C0                    2373 	.db #0xc0	; 192
      000BAA 00                    2374 	.db #0x00	; 0
      000BAB 00                    2375 	.db #0x00	; 0
      000BAC E0                    2376 	.db #0xe0	; 224
      000BAD 20                    2377 	.db #0x20	; 32
      000BAE 20                    2378 	.db #0x20	; 32
      000BAF 20                    2379 	.db #0x20	; 32
      000BB0 E0                    2380 	.db #0xe0	; 224
      000BB1 00                    2381 	.db #0x00	; 0
      000BB2 00                    2382 	.db #0x00	; 0
      000BB3 00                    2383 	.db #0x00	; 0
      000BB4 40                    2384 	.db #0x40	; 64
      000BB5 E0                    2385 	.db #0xe0	; 224
      000BB6 00                    2386 	.db #0x00	; 0
      000BB7 00                    2387 	.db #0x00	; 0
      000BB8 00                    2388 	.db #0x00	; 0
      000BB9 00                    2389 	.db #0x00	; 0
      000BBA 60                    2390 	.db #0x60	; 96
      000BBB 20                    2391 	.db #0x20	; 32
      000BBC 20                    2392 	.db #0x20	; 32
      000BBD 20                    2393 	.db #0x20	; 32
      000BBE E0                    2394 	.db #0xe0	; 224
      000BBF 00                    2395 	.db #0x00	; 0
      000BC0 00                    2396 	.db #0x00	; 0
      000BC1 00                    2397 	.db #0x00	; 0
      000BC2 00                    2398 	.db #0x00	; 0
      000BC3 00                    2399 	.db #0x00	; 0
      000BC4 E0                    2400 	.db #0xe0	; 224
      000BC5 20                    2401 	.db #0x20	; 32
      000BC6 20                    2402 	.db #0x20	; 32
      000BC7 20                    2403 	.db #0x20	; 32
      000BC8 E0                    2404 	.db #0xe0	; 224
      000BC9 00                    2405 	.db #0x00	; 0
      000BCA 00                    2406 	.db #0x00	; 0
      000BCB 00                    2407 	.db #0x00	; 0
      000BCC 00                    2408 	.db #0x00	; 0
      000BCD 00                    2409 	.db #0x00	; 0
      000BCE 40                    2410 	.db #0x40	; 64
      000BCF 20                    2411 	.db #0x20	; 32
      000BD0 20                    2412 	.db #0x20	; 32
      000BD1 20                    2413 	.db #0x20	; 32
      000BD2 C0                    2414 	.db #0xc0	; 192
      000BD3 00                    2415 	.db #0x00	; 0
      000BD4 00                    2416 	.db #0x00	; 0
      000BD5 40                    2417 	.db #0x40	; 64
      000BD6 20                    2418 	.db #0x20	; 32
      000BD7 20                    2419 	.db #0x20	; 32
      000BD8 20                    2420 	.db #0x20	; 32
      000BD9 C0                    2421 	.db #0xc0	; 192
      000BDA 00                    2422 	.db #0x00	; 0
      000BDB 00                    2423 	.db #0x00	; 0
      000BDC 00                    2424 	.db #0x00	; 0
      000BDD 00                    2425 	.db #0x00	; 0
      000BDE 00                    2426 	.db #0x00	; 0
      000BDF 00                    2427 	.db #0x00	; 0
      000BE0 00                    2428 	.db #0x00	; 0
      000BE1 00                    2429 	.db #0x00	; 0
      000BE2 00                    2430 	.db #0x00	; 0
      000BE3 00                    2431 	.db #0x00	; 0
      000BE4 00                    2432 	.db #0x00	; 0
      000BE5 00                    2433 	.db #0x00	; 0
      000BE6 00                    2434 	.db #0x00	; 0
      000BE7 00                    2435 	.db #0x00	; 0
      000BE8 00                    2436 	.db #0x00	; 0
      000BE9 00                    2437 	.db #0x00	; 0
      000BEA 00                    2438 	.db #0x00	; 0
      000BEB 00                    2439 	.db #0x00	; 0
      000BEC 00                    2440 	.db #0x00	; 0
      000BED 00                    2441 	.db #0x00	; 0
      000BEE 00                    2442 	.db #0x00	; 0
      000BEF 00                    2443 	.db #0x00	; 0
      000BF0 00                    2444 	.db #0x00	; 0
      000BF1 00                    2445 	.db #0x00	; 0
      000BF2 00                    2446 	.db #0x00	; 0
      000BF3 00                    2447 	.db #0x00	; 0
      000BF4 00                    2448 	.db #0x00	; 0
      000BF5 00                    2449 	.db #0x00	; 0
      000BF6 00                    2450 	.db #0x00	; 0
      000BF7 00                    2451 	.db #0x00	; 0
      000BF8 00                    2452 	.db #0x00	; 0
      000BF9 00                    2453 	.db #0x00	; 0
      000BFA 00                    2454 	.db #0x00	; 0
      000BFB 00                    2455 	.db #0x00	; 0
      000BFC 00                    2456 	.db #0x00	; 0
      000BFD 00                    2457 	.db #0x00	; 0
      000BFE 00                    2458 	.db #0x00	; 0
      000BFF 00                    2459 	.db #0x00	; 0
      000C00 00                    2460 	.db #0x00	; 0
      000C01 00                    2461 	.db #0x00	; 0
      000C02 00                    2462 	.db #0x00	; 0
      000C03 00                    2463 	.db #0x00	; 0
      000C04 00                    2464 	.db #0x00	; 0
      000C05 00                    2465 	.db #0x00	; 0
      000C06 00                    2466 	.db #0x00	; 0
      000C07 00                    2467 	.db #0x00	; 0
      000C08 00                    2468 	.db #0x00	; 0
      000C09 00                    2469 	.db #0x00	; 0
      000C0A 00                    2470 	.db #0x00	; 0
      000C0B 00                    2471 	.db #0x00	; 0
      000C0C 00                    2472 	.db #0x00	; 0
      000C0D 00                    2473 	.db #0x00	; 0
      000C0E 00                    2474 	.db #0x00	; 0
      000C0F 00                    2475 	.db #0x00	; 0
      000C10 00                    2476 	.db #0x00	; 0
      000C11 00                    2477 	.db #0x00	; 0
      000C12 00                    2478 	.db #0x00	; 0
      000C13 00                    2479 	.db #0x00	; 0
      000C14 00                    2480 	.db #0x00	; 0
      000C15 00                    2481 	.db #0x00	; 0
      000C16 00                    2482 	.db #0x00	; 0
      000C17 00                    2483 	.db #0x00	; 0
      000C18 00                    2484 	.db #0x00	; 0
      000C19 00                    2485 	.db #0x00	; 0
      000C1A 00                    2486 	.db #0x00	; 0
      000C1B 00                    2487 	.db #0x00	; 0
      000C1C 00                    2488 	.db #0x00	; 0
      000C1D 00                    2489 	.db #0x00	; 0
      000C1E 00                    2490 	.db #0x00	; 0
      000C1F 00                    2491 	.db #0x00	; 0
      000C20 00                    2492 	.db #0x00	; 0
      000C21 00                    2493 	.db #0x00	; 0
      000C22 00                    2494 	.db #0x00	; 0
      000C23 00                    2495 	.db #0x00	; 0
      000C24 00                    2496 	.db #0x00	; 0
      000C25 0C                    2497 	.db #0x0c	; 12
      000C26 0A                    2498 	.db #0x0a	; 10
      000C27 0A                    2499 	.db #0x0a	; 10
      000C28 09                    2500 	.db #0x09	; 9
      000C29 0C                    2501 	.db #0x0c	; 12
      000C2A 00                    2502 	.db #0x00	; 0
      000C2B 00                    2503 	.db #0x00	; 0
      000C2C 0F                    2504 	.db #0x0f	; 15
      000C2D 08                    2505 	.db #0x08	; 8
      000C2E 08                    2506 	.db #0x08	; 8
      000C2F 08                    2507 	.db #0x08	; 8
      000C30 0F                    2508 	.db #0x0f	; 15
      000C31 00                    2509 	.db #0x00	; 0
      000C32 00                    2510 	.db #0x00	; 0
      000C33 00                    2511 	.db #0x00	; 0
      000C34 08                    2512 	.db #0x08	; 8
      000C35 0F                    2513 	.db #0x0f	; 15
      000C36 08                    2514 	.db #0x08	; 8
      000C37 00                    2515 	.db #0x00	; 0
      000C38 00                    2516 	.db #0x00	; 0
      000C39 00                    2517 	.db #0x00	; 0
      000C3A 0C                    2518 	.db #0x0c	; 12
      000C3B 08                    2519 	.db #0x08	; 8
      000C3C 09                    2520 	.db #0x09	; 9
      000C3D 09                    2521 	.db #0x09	; 9
      000C3E 0E                    2522 	.db #0x0e	; 14
      000C3F 00                    2523 	.db #0x00	; 0
      000C40 00                    2524 	.db #0x00	; 0
      000C41 0C                    2525 	.db #0x0c	; 12
      000C42 00                    2526 	.db #0x00	; 0
      000C43 00                    2527 	.db #0x00	; 0
      000C44 0F                    2528 	.db #0x0f	; 15
      000C45 09                    2529 	.db #0x09	; 9
      000C46 09                    2530 	.db #0x09	; 9
      000C47 09                    2531 	.db #0x09	; 9
      000C48 0F                    2532 	.db #0x0f	; 15
      000C49 00                    2533 	.db #0x00	; 0
      000C4A 00                    2534 	.db #0x00	; 0
      000C4B 0C                    2535 	.db #0x0c	; 12
      000C4C 00                    2536 	.db #0x00	; 0
      000C4D 00                    2537 	.db #0x00	; 0
      000C4E 0C                    2538 	.db #0x0c	; 12
      000C4F 0A                    2539 	.db #0x0a	; 10
      000C50 0A                    2540 	.db #0x0a	; 10
      000C51 09                    2541 	.db #0x09	; 9
      000C52 0C                    2542 	.db #0x0c	; 12
      000C53 00                    2543 	.db #0x00	; 0
      000C54 00                    2544 	.db #0x00	; 0
      000C55 0C                    2545 	.db #0x0c	; 12
      000C56 0A                    2546 	.db #0x0a	; 10
      000C57 0A                    2547 	.db #0x0a	; 10
      000C58 09                    2548 	.db #0x09	; 9
      000C59 0C                    2549 	.db #0x0c	; 12
      000C5A 00                    2550 	.db #0x00	; 0
      000C5B 00                    2551 	.db #0x00	; 0
      000C5C 00                    2552 	.db #0x00	; 0
      000C5D 00                    2553 	.db #0x00	; 0
      000C5E 00                    2554 	.db #0x00	; 0
      000C5F 00                    2555 	.db #0x00	; 0
      000C60 00                    2556 	.db #0x00	; 0
      000C61 00                    2557 	.db #0x00	; 0
      000C62 00                    2558 	.db #0x00	; 0
      000C63 00                    2559 	.db #0x00	; 0
      000C64 00                    2560 	.db #0x00	; 0
      000C65 00                    2561 	.db #0x00	; 0
      000C66 00                    2562 	.db #0x00	; 0
      000C67 00                    2563 	.db #0x00	; 0
      000C68 00                    2564 	.db #0x00	; 0
      000C69 00                    2565 	.db #0x00	; 0
      000C6A 00                    2566 	.db #0x00	; 0
      000C6B 00                    2567 	.db #0x00	; 0
      000C6C 00                    2568 	.db #0x00	; 0
      000C6D 00                    2569 	.db #0x00	; 0
      000C6E 00                    2570 	.db #0x00	; 0
      000C6F 00                    2571 	.db #0x00	; 0
      000C70 00                    2572 	.db #0x00	; 0
      000C71 00                    2573 	.db #0x00	; 0
      000C72 00                    2574 	.db #0x00	; 0
      000C73 00                    2575 	.db #0x00	; 0
      000C74 00                    2576 	.db #0x00	; 0
      000C75 00                    2577 	.db #0x00	; 0
      000C76 00                    2578 	.db #0x00	; 0
      000C77 00                    2579 	.db #0x00	; 0
      000C78 00                    2580 	.db #0x00	; 0
      000C79 00                    2581 	.db #0x00	; 0
      000C7A 00                    2582 	.db #0x00	; 0
      000C7B 00                    2583 	.db #0x00	; 0
      000C7C 00                    2584 	.db #0x00	; 0
      000C7D 00                    2585 	.db #0x00	; 0
      000C7E 00                    2586 	.db #0x00	; 0
      000C7F 00                    2587 	.db #0x00	; 0
      000C80 00                    2588 	.db #0x00	; 0
      000C81 00                    2589 	.db #0x00	; 0
      000C82 00                    2590 	.db #0x00	; 0
      000C83 00                    2591 	.db #0x00	; 0
      000C84 00                    2592 	.db #0x00	; 0
      000C85 00                    2593 	.db #0x00	; 0
      000C86 00                    2594 	.db #0x00	; 0
      000C87 00                    2595 	.db #0x00	; 0
      000C88 00                    2596 	.db #0x00	; 0
      000C89 00                    2597 	.db #0x00	; 0
      000C8A 00                    2598 	.db #0x00	; 0
      000C8B 00                    2599 	.db #0x00	; 0
      000C8C 00                    2600 	.db #0x00	; 0
      000C8D 00                    2601 	.db #0x00	; 0
      000C8E 00                    2602 	.db #0x00	; 0
      000C8F 00                    2603 	.db #0x00	; 0
      000C90 00                    2604 	.db #0x00	; 0
      000C91 00                    2605 	.db #0x00	; 0
      000C92 00                    2606 	.db #0x00	; 0
      000C93 00                    2607 	.db #0x00	; 0
      000C94 00                    2608 	.db #0x00	; 0
      000C95 00                    2609 	.db #0x00	; 0
      000C96 00                    2610 	.db #0x00	; 0
      000C97 00                    2611 	.db #0x00	; 0
      000C98 00                    2612 	.db #0x00	; 0
      000C99 00                    2613 	.db #0x00	; 0
      000C9A 00                    2614 	.db #0x00	; 0
      000C9B 00                    2615 	.db #0x00	; 0
      000C9C 00                    2616 	.db #0x00	; 0
      000C9D 00                    2617 	.db #0x00	; 0
      000C9E 00                    2618 	.db #0x00	; 0
      000C9F 00                    2619 	.db #0x00	; 0
      000CA0 00                    2620 	.db #0x00	; 0
      000CA1 00                    2621 	.db #0x00	; 0
      000CA2 00                    2622 	.db #0x00	; 0
      000CA3 00                    2623 	.db #0x00	; 0
      000CA4 00                    2624 	.db #0x00	; 0
      000CA5 00                    2625 	.db #0x00	; 0
      000CA6 00                    2626 	.db #0x00	; 0
      000CA7 00                    2627 	.db #0x00	; 0
      000CA8 00                    2628 	.db #0x00	; 0
      000CA9 00                    2629 	.db #0x00	; 0
      000CAA 00                    2630 	.db #0x00	; 0
      000CAB 00                    2631 	.db #0x00	; 0
      000CAC 00                    2632 	.db #0x00	; 0
      000CAD 00                    2633 	.db #0x00	; 0
      000CAE 00                    2634 	.db #0x00	; 0
      000CAF 00                    2635 	.db #0x00	; 0
      000CB0 00                    2636 	.db #0x00	; 0
      000CB1 00                    2637 	.db #0x00	; 0
      000CB2 00                    2638 	.db #0x00	; 0
      000CB3 00                    2639 	.db #0x00	; 0
      000CB4 00                    2640 	.db #0x00	; 0
      000CB5 00                    2641 	.db #0x00	; 0
      000CB6 00                    2642 	.db #0x00	; 0
      000CB7 00                    2643 	.db #0x00	; 0
      000CB8 00                    2644 	.db #0x00	; 0
      000CB9 00                    2645 	.db #0x00	; 0
      000CBA 00                    2646 	.db #0x00	; 0
      000CBB 00                    2647 	.db #0x00	; 0
      000CBC 80                    2648 	.db #0x80	; 128
      000CBD 80                    2649 	.db #0x80	; 128
      000CBE 80                    2650 	.db #0x80	; 128
      000CBF 80                    2651 	.db #0x80	; 128
      000CC0 80                    2652 	.db #0x80	; 128
      000CC1 80                    2653 	.db #0x80	; 128
      000CC2 80                    2654 	.db #0x80	; 128
      000CC3 80                    2655 	.db #0x80	; 128
      000CC4 00                    2656 	.db #0x00	; 0
      000CC5 00                    2657 	.db #0x00	; 0
      000CC6 00                    2658 	.db #0x00	; 0
      000CC7 00                    2659 	.db #0x00	; 0
      000CC8 00                    2660 	.db #0x00	; 0
      000CC9 00                    2661 	.db #0x00	; 0
      000CCA 00                    2662 	.db #0x00	; 0
      000CCB 00                    2663 	.db #0x00	; 0
      000CCC 00                    2664 	.db #0x00	; 0
      000CCD 00                    2665 	.db #0x00	; 0
      000CCE 00                    2666 	.db #0x00	; 0
      000CCF 00                    2667 	.db #0x00	; 0
      000CD0 00                    2668 	.db #0x00	; 0
      000CD1 00                    2669 	.db #0x00	; 0
      000CD2 00                    2670 	.db #0x00	; 0
      000CD3 00                    2671 	.db #0x00	; 0
      000CD4 00                    2672 	.db #0x00	; 0
      000CD5 00                    2673 	.db #0x00	; 0
      000CD6 00                    2674 	.db #0x00	; 0
      000CD7 00                    2675 	.db #0x00	; 0
      000CD8 00                    2676 	.db #0x00	; 0
      000CD9 00                    2677 	.db #0x00	; 0
      000CDA 00                    2678 	.db #0x00	; 0
      000CDB 00                    2679 	.db #0x00	; 0
      000CDC 00                    2680 	.db #0x00	; 0
      000CDD 00                    2681 	.db #0x00	; 0
      000CDE 00                    2682 	.db #0x00	; 0
      000CDF 00                    2683 	.db #0x00	; 0
      000CE0 00                    2684 	.db #0x00	; 0
      000CE1 00                    2685 	.db #0x00	; 0
      000CE2 00                    2686 	.db #0x00	; 0
      000CE3 00                    2687 	.db #0x00	; 0
      000CE4 00                    2688 	.db #0x00	; 0
      000CE5 00                    2689 	.db #0x00	; 0
      000CE6 00                    2690 	.db #0x00	; 0
      000CE7 00                    2691 	.db #0x00	; 0
      000CE8 00                    2692 	.db #0x00	; 0
      000CE9 00                    2693 	.db #0x00	; 0
      000CEA 00                    2694 	.db #0x00	; 0
      000CEB 00                    2695 	.db #0x00	; 0
      000CEC 00                    2696 	.db #0x00	; 0
      000CED 00                    2697 	.db #0x00	; 0
      000CEE 00                    2698 	.db #0x00	; 0
      000CEF 00                    2699 	.db #0x00	; 0
      000CF0 00                    2700 	.db #0x00	; 0
      000CF1 00                    2701 	.db #0x00	; 0
      000CF2 00                    2702 	.db #0x00	; 0
      000CF3 00                    2703 	.db #0x00	; 0
      000CF4 00                    2704 	.db #0x00	; 0
      000CF5 00                    2705 	.db #0x00	; 0
      000CF6 00                    2706 	.db #0x00	; 0
      000CF7 00                    2707 	.db #0x00	; 0
      000CF8 00                    2708 	.db #0x00	; 0
      000CF9 00                    2709 	.db #0x00	; 0
      000CFA 00                    2710 	.db #0x00	; 0
      000CFB 00                    2711 	.db #0x00	; 0
      000CFC 00                    2712 	.db #0x00	; 0
      000CFD 00                    2713 	.db #0x00	; 0
      000CFE 00                    2714 	.db #0x00	; 0
      000CFF 00                    2715 	.db #0x00	; 0
      000D00 00                    2716 	.db #0x00	; 0
      000D01 7F                    2717 	.db #0x7f	; 127
      000D02 03                    2718 	.db #0x03	; 3
      000D03 0C                    2719 	.db #0x0c	; 12
      000D04 30                    2720 	.db #0x30	; 48	'0'
      000D05 0C                    2721 	.db #0x0c	; 12
      000D06 03                    2722 	.db #0x03	; 3
      000D07 7F                    2723 	.db #0x7f	; 127
      000D08 00                    2724 	.db #0x00	; 0
      000D09 00                    2725 	.db #0x00	; 0
      000D0A 38                    2726 	.db #0x38	; 56	'8'
      000D0B 54                    2727 	.db #0x54	; 84	'T'
      000D0C 54                    2728 	.db #0x54	; 84	'T'
      000D0D 58                    2729 	.db #0x58	; 88	'X'
      000D0E 00                    2730 	.db #0x00	; 0
      000D0F 00                    2731 	.db #0x00	; 0
      000D10 7C                    2732 	.db #0x7c	; 124
      000D11 04                    2733 	.db #0x04	; 4
      000D12 04                    2734 	.db #0x04	; 4
      000D13 78                    2735 	.db #0x78	; 120	'x'
      000D14 00                    2736 	.db #0x00	; 0
      000D15 00                    2737 	.db #0x00	; 0
      000D16 3C                    2738 	.db #0x3c	; 60
      000D17 40                    2739 	.db #0x40	; 64
      000D18 40                    2740 	.db #0x40	; 64
      000D19 7C                    2741 	.db #0x7c	; 124
      000D1A 00                    2742 	.db #0x00	; 0
      000D1B 00                    2743 	.db #0x00	; 0
      000D1C 00                    2744 	.db #0x00	; 0
      000D1D 00                    2745 	.db #0x00	; 0
      000D1E 00                    2746 	.db #0x00	; 0
      000D1F 00                    2747 	.db #0x00	; 0
      000D20 00                    2748 	.db #0x00	; 0
      000D21 00                    2749 	.db #0x00	; 0
      000D22 00                    2750 	.db #0x00	; 0
      000D23 00                    2751 	.db #0x00	; 0
      000D24 00                    2752 	.db #0x00	; 0
      000D25 00                    2753 	.db #0x00	; 0
      000D26 00                    2754 	.db #0x00	; 0
      000D27 00                    2755 	.db #0x00	; 0
      000D28 00                    2756 	.db #0x00	; 0
      000D29 00                    2757 	.db #0x00	; 0
      000D2A 00                    2758 	.db #0x00	; 0
      000D2B 00                    2759 	.db #0x00	; 0
      000D2C 00                    2760 	.db #0x00	; 0
      000D2D 00                    2761 	.db #0x00	; 0
      000D2E 00                    2762 	.db #0x00	; 0
      000D2F 00                    2763 	.db #0x00	; 0
      000D30 00                    2764 	.db #0x00	; 0
      000D31 00                    2765 	.db #0x00	; 0
      000D32 00                    2766 	.db #0x00	; 0
      000D33 00                    2767 	.db #0x00	; 0
      000D34 00                    2768 	.db #0x00	; 0
      000D35 00                    2769 	.db #0x00	; 0
      000D36 00                    2770 	.db #0x00	; 0
      000D37 00                    2771 	.db #0x00	; 0
      000D38 00                    2772 	.db #0x00	; 0
      000D39 00                    2773 	.db #0x00	; 0
      000D3A 00                    2774 	.db #0x00	; 0
      000D3B 00                    2775 	.db #0x00	; 0
      000D3C FF                    2776 	.db #0xff	; 255
      000D3D AA                    2777 	.db #0xaa	; 170
      000D3E AA                    2778 	.db #0xaa	; 170
      000D3F AA                    2779 	.db #0xaa	; 170
      000D40 28                    2780 	.db #0x28	; 40
      000D41 08                    2781 	.db #0x08	; 8
      000D42 00                    2782 	.db #0x00	; 0
      000D43 FF                    2783 	.db #0xff	; 255
      000D44 00                    2784 	.db #0x00	; 0
      000D45 00                    2785 	.db #0x00	; 0
      000D46 00                    2786 	.db #0x00	; 0
      000D47 00                    2787 	.db #0x00	; 0
      000D48 00                    2788 	.db #0x00	; 0
      000D49 00                    2789 	.db #0x00	; 0
      000D4A 00                    2790 	.db #0x00	; 0
      000D4B 00                    2791 	.db #0x00	; 0
      000D4C 00                    2792 	.db #0x00	; 0
      000D4D 00                    2793 	.db #0x00	; 0
      000D4E 00                    2794 	.db #0x00	; 0
      000D4F 00                    2795 	.db #0x00	; 0
      000D50 00                    2796 	.db #0x00	; 0
      000D51 00                    2797 	.db #0x00	; 0
      000D52 00                    2798 	.db #0x00	; 0
      000D53 00                    2799 	.db #0x00	; 0
      000D54 00                    2800 	.db #0x00	; 0
      000D55 00                    2801 	.db #0x00	; 0
      000D56 00                    2802 	.db #0x00	; 0
      000D57 00                    2803 	.db #0x00	; 0
      000D58 00                    2804 	.db #0x00	; 0
      000D59 00                    2805 	.db #0x00	; 0
      000D5A 00                    2806 	.db #0x00	; 0
      000D5B 00                    2807 	.db #0x00	; 0
      000D5C 00                    2808 	.db #0x00	; 0
      000D5D 00                    2809 	.db #0x00	; 0
      000D5E 00                    2810 	.db #0x00	; 0
      000D5F 00                    2811 	.db #0x00	; 0
      000D60 00                    2812 	.db #0x00	; 0
      000D61 00                    2813 	.db #0x00	; 0
      000D62 00                    2814 	.db #0x00	; 0
      000D63 00                    2815 	.db #0x00	; 0
      000D64 00                    2816 	.db #0x00	; 0
      000D65 00                    2817 	.db #0x00	; 0
      000D66 00                    2818 	.db #0x00	; 0
      000D67 00                    2819 	.db #0x00	; 0
      000D68 00                    2820 	.db #0x00	; 0
      000D69 7F                    2821 	.db #0x7f	; 127
      000D6A 03                    2822 	.db #0x03	; 3
      000D6B 0C                    2823 	.db #0x0c	; 12
      000D6C 30                    2824 	.db #0x30	; 48	'0'
      000D6D 0C                    2825 	.db #0x0c	; 12
      000D6E 03                    2826 	.db #0x03	; 3
      000D6F 7F                    2827 	.db #0x7f	; 127
      000D70 00                    2828 	.db #0x00	; 0
      000D71 00                    2829 	.db #0x00	; 0
      000D72 26                    2830 	.db #0x26	; 38
      000D73 49                    2831 	.db #0x49	; 73	'I'
      000D74 49                    2832 	.db #0x49	; 73	'I'
      000D75 49                    2833 	.db #0x49	; 73	'I'
      000D76 32                    2834 	.db #0x32	; 50	'2'
      000D77 00                    2835 	.db #0x00	; 0
      000D78 00                    2836 	.db #0x00	; 0
      000D79 7F                    2837 	.db #0x7f	; 127
      000D7A 02                    2838 	.db #0x02	; 2
      000D7B 04                    2839 	.db #0x04	; 4
      000D7C 08                    2840 	.db #0x08	; 8
      000D7D 10                    2841 	.db #0x10	; 16
      000D7E 7F                    2842 	.db #0x7f	; 127
      000D7F 00                    2843 	.db #0x00	; 0
                                   2844 	.area CONST   (CODE)
      000D80                       2845 ___str_0:
      000D80 50 6C 65 61 73 65 20  2846 	.ascii "Please follow   Verimake!"
             66 6F 6C 6C 6F 77 20
             20 20 56 65 72 69 6D
             61 6B 65 21
      000D99 00                    2847 	.db 0x00
                                   2848 	.area CSEG    (CODE)
                                   2849 	.area XINIT   (CODE)
                                   2850 	.area CABS    (ABS,CODE)
