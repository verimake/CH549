                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _main
                                     12 	.globl _delay
                                     13 	.globl _UIF_BUS_RST
                                     14 	.globl _UIF_DETECT
                                     15 	.globl _UIF_TRANSFER
                                     16 	.globl _UIF_SUSPEND
                                     17 	.globl _UIF_HST_SOF
                                     18 	.globl _UIF_FIFO_OV
                                     19 	.globl _U_SIE_FREE
                                     20 	.globl _U_TOG_OK
                                     21 	.globl _U_IS_NAK
                                     22 	.globl _S0_R_FIFO
                                     23 	.globl _S0_T_FIFO
                                     24 	.globl _S0_FREE
                                     25 	.globl _S0_IF_BYTE
                                     26 	.globl _S0_IF_FIRST
                                     27 	.globl _S0_IF_OV
                                     28 	.globl _S0_FST_ACT
                                     29 	.globl _CP_RL2
                                     30 	.globl _C_T2
                                     31 	.globl _TR2
                                     32 	.globl _EXEN2
                                     33 	.globl _TCLK
                                     34 	.globl _RCLK
                                     35 	.globl _EXF2
                                     36 	.globl _CAP1F
                                     37 	.globl _TF2
                                     38 	.globl _RI
                                     39 	.globl _TI
                                     40 	.globl _RB8
                                     41 	.globl _TB8
                                     42 	.globl _REN
                                     43 	.globl _SM2
                                     44 	.globl _SM1
                                     45 	.globl _SM0
                                     46 	.globl _IT0
                                     47 	.globl _IE0
                                     48 	.globl _IT1
                                     49 	.globl _IE1
                                     50 	.globl _TR0
                                     51 	.globl _TF0
                                     52 	.globl _TR1
                                     53 	.globl _TF1
                                     54 	.globl _XI
                                     55 	.globl _XO
                                     56 	.globl _P4_0
                                     57 	.globl _P4_1
                                     58 	.globl _P4_2
                                     59 	.globl _P4_3
                                     60 	.globl _P4_4
                                     61 	.globl _P4_5
                                     62 	.globl _P4_6
                                     63 	.globl _RXD
                                     64 	.globl _TXD
                                     65 	.globl _INT0
                                     66 	.globl _INT1
                                     67 	.globl _T0
                                     68 	.globl _T1
                                     69 	.globl _CAP0
                                     70 	.globl _INT3
                                     71 	.globl _P3_0
                                     72 	.globl _P3_1
                                     73 	.globl _P3_2
                                     74 	.globl _P3_3
                                     75 	.globl _P3_4
                                     76 	.globl _P3_5
                                     77 	.globl _P3_6
                                     78 	.globl _P3_7
                                     79 	.globl _PWM5
                                     80 	.globl _PWM4
                                     81 	.globl _INT0_
                                     82 	.globl _PWM3
                                     83 	.globl _PWM2
                                     84 	.globl _CAP1_
                                     85 	.globl _T2_
                                     86 	.globl _PWM1
                                     87 	.globl _CAP2_
                                     88 	.globl _T2EX_
                                     89 	.globl _PWM0
                                     90 	.globl _RXD1
                                     91 	.globl _PWM6
                                     92 	.globl _TXD1
                                     93 	.globl _PWM7
                                     94 	.globl _P2_0
                                     95 	.globl _P2_1
                                     96 	.globl _P2_2
                                     97 	.globl _P2_3
                                     98 	.globl _P2_4
                                     99 	.globl _P2_5
                                    100 	.globl _P2_6
                                    101 	.globl _P2_7
                                    102 	.globl _AIN0
                                    103 	.globl _CAP1
                                    104 	.globl _T2
                                    105 	.globl _AIN1
                                    106 	.globl _CAP2
                                    107 	.globl _T2EX
                                    108 	.globl _AIN2
                                    109 	.globl _AIN3
                                    110 	.globl _AIN4
                                    111 	.globl _UCC1
                                    112 	.globl _SCS
                                    113 	.globl _AIN5
                                    114 	.globl _UCC2
                                    115 	.globl _PWM0_
                                    116 	.globl _MOSI
                                    117 	.globl _AIN6
                                    118 	.globl _VBUS
                                    119 	.globl _RXD1_
                                    120 	.globl _MISO
                                    121 	.globl _AIN7
                                    122 	.globl _TXD1_
                                    123 	.globl _SCK
                                    124 	.globl _P1_0
                                    125 	.globl _P1_1
                                    126 	.globl _P1_2
                                    127 	.globl _P1_3
                                    128 	.globl _P1_4
                                    129 	.globl _P1_5
                                    130 	.globl _P1_6
                                    131 	.globl _P1_7
                                    132 	.globl _AIN8
                                    133 	.globl _AIN9
                                    134 	.globl _AIN10
                                    135 	.globl _RXD_
                                    136 	.globl _AIN11
                                    137 	.globl _TXD_
                                    138 	.globl _AIN12
                                    139 	.globl _RXD2
                                    140 	.globl _AIN13
                                    141 	.globl _TXD2
                                    142 	.globl _AIN14
                                    143 	.globl _RXD3
                                    144 	.globl _AIN15
                                    145 	.globl _TXD3
                                    146 	.globl _P0_0
                                    147 	.globl _P0_1
                                    148 	.globl _P0_2
                                    149 	.globl _P0_3
                                    150 	.globl _P0_4
                                    151 	.globl _P0_5
                                    152 	.globl _P0_6
                                    153 	.globl _P0_7
                                    154 	.globl _IE_SPI0
                                    155 	.globl _IE_INT3
                                    156 	.globl _IE_USB
                                    157 	.globl _IE_UART2
                                    158 	.globl _IE_ADC
                                    159 	.globl _IE_UART1
                                    160 	.globl _IE_UART3
                                    161 	.globl _IE_PWMX
                                    162 	.globl _IE_GPIO
                                    163 	.globl _IE_WDOG
                                    164 	.globl _PX0
                                    165 	.globl _PT0
                                    166 	.globl _PX1
                                    167 	.globl _PT1
                                    168 	.globl _PS
                                    169 	.globl _PT2
                                    170 	.globl _PL_FLAG
                                    171 	.globl _PH_FLAG
                                    172 	.globl _EX0
                                    173 	.globl _ET0
                                    174 	.globl _EX1
                                    175 	.globl _ET1
                                    176 	.globl _ES
                                    177 	.globl _ET2
                                    178 	.globl _E_DIS
                                    179 	.globl _EA
                                    180 	.globl _P
                                    181 	.globl _F1
                                    182 	.globl _OV
                                    183 	.globl _RS0
                                    184 	.globl _RS1
                                    185 	.globl _F0
                                    186 	.globl _AC
                                    187 	.globl _CY
                                    188 	.globl _UEP1_DMA_H
                                    189 	.globl _UEP1_DMA_L
                                    190 	.globl _UEP1_DMA
                                    191 	.globl _UEP0_DMA_H
                                    192 	.globl _UEP0_DMA_L
                                    193 	.globl _UEP0_DMA
                                    194 	.globl _UEP2_3_MOD
                                    195 	.globl _UEP4_1_MOD
                                    196 	.globl _UEP3_DMA_H
                                    197 	.globl _UEP3_DMA_L
                                    198 	.globl _UEP3_DMA
                                    199 	.globl _UEP2_DMA_H
                                    200 	.globl _UEP2_DMA_L
                                    201 	.globl _UEP2_DMA
                                    202 	.globl _USB_DEV_AD
                                    203 	.globl _USB_CTRL
                                    204 	.globl _USB_INT_EN
                                    205 	.globl _UEP4_T_LEN
                                    206 	.globl _UEP4_CTRL
                                    207 	.globl _UEP0_T_LEN
                                    208 	.globl _UEP0_CTRL
                                    209 	.globl _USB_RX_LEN
                                    210 	.globl _USB_MIS_ST
                                    211 	.globl _USB_INT_ST
                                    212 	.globl _USB_INT_FG
                                    213 	.globl _UEP3_T_LEN
                                    214 	.globl _UEP3_CTRL
                                    215 	.globl _UEP2_T_LEN
                                    216 	.globl _UEP2_CTRL
                                    217 	.globl _UEP1_T_LEN
                                    218 	.globl _UEP1_CTRL
                                    219 	.globl _UDEV_CTRL
                                    220 	.globl _USB_C_CTRL
                                    221 	.globl _ADC_PIN
                                    222 	.globl _ADC_CHAN
                                    223 	.globl _ADC_DAT_H
                                    224 	.globl _ADC_DAT_L
                                    225 	.globl _ADC_DAT
                                    226 	.globl _ADC_CFG
                                    227 	.globl _ADC_CTRL
                                    228 	.globl _TKEY_CTRL
                                    229 	.globl _SIF3
                                    230 	.globl _SBAUD3
                                    231 	.globl _SBUF3
                                    232 	.globl _SCON3
                                    233 	.globl _SIF2
                                    234 	.globl _SBAUD2
                                    235 	.globl _SBUF2
                                    236 	.globl _SCON2
                                    237 	.globl _SIF1
                                    238 	.globl _SBAUD1
                                    239 	.globl _SBUF1
                                    240 	.globl _SCON1
                                    241 	.globl _SPI0_SETUP
                                    242 	.globl _SPI0_CK_SE
                                    243 	.globl _SPI0_CTRL
                                    244 	.globl _SPI0_DATA
                                    245 	.globl _SPI0_STAT
                                    246 	.globl _PWM_DATA7
                                    247 	.globl _PWM_DATA6
                                    248 	.globl _PWM_DATA5
                                    249 	.globl _PWM_DATA4
                                    250 	.globl _PWM_DATA3
                                    251 	.globl _PWM_CTRL2
                                    252 	.globl _PWM_CK_SE
                                    253 	.globl _PWM_CTRL
                                    254 	.globl _PWM_DATA0
                                    255 	.globl _PWM_DATA1
                                    256 	.globl _PWM_DATA2
                                    257 	.globl _T2CAP1H
                                    258 	.globl _T2CAP1L
                                    259 	.globl _T2CAP1
                                    260 	.globl _TH2
                                    261 	.globl _TL2
                                    262 	.globl _T2COUNT
                                    263 	.globl _RCAP2H
                                    264 	.globl _RCAP2L
                                    265 	.globl _RCAP2
                                    266 	.globl _T2MOD
                                    267 	.globl _T2CON
                                    268 	.globl _T2CAP0H
                                    269 	.globl _T2CAP0L
                                    270 	.globl _T2CAP0
                                    271 	.globl _T2CON2
                                    272 	.globl _SBUF
                                    273 	.globl _SCON
                                    274 	.globl _TH1
                                    275 	.globl _TH0
                                    276 	.globl _TL1
                                    277 	.globl _TL0
                                    278 	.globl _TMOD
                                    279 	.globl _TCON
                                    280 	.globl _XBUS_AUX
                                    281 	.globl _PIN_FUNC
                                    282 	.globl _P5
                                    283 	.globl _P4_DIR_PU
                                    284 	.globl _P4_MOD_OC
                                    285 	.globl _P4
                                    286 	.globl _P3_DIR_PU
                                    287 	.globl _P3_MOD_OC
                                    288 	.globl _P3
                                    289 	.globl _P2_DIR_PU
                                    290 	.globl _P2_MOD_OC
                                    291 	.globl _P2
                                    292 	.globl _P1_DIR_PU
                                    293 	.globl _P1_MOD_OC
                                    294 	.globl _P1
                                    295 	.globl _P0_DIR_PU
                                    296 	.globl _P0_MOD_OC
                                    297 	.globl _P0
                                    298 	.globl _ROM_CTRL
                                    299 	.globl _ROM_DATA_HH
                                    300 	.globl _ROM_DATA_HL
                                    301 	.globl _ROM_DATA_HI
                                    302 	.globl _ROM_ADDR_H
                                    303 	.globl _ROM_ADDR_L
                                    304 	.globl _ROM_ADDR
                                    305 	.globl _GPIO_IE
                                    306 	.globl _INTX
                                    307 	.globl _IP_EX
                                    308 	.globl _IE_EX
                                    309 	.globl _IP
                                    310 	.globl _IE
                                    311 	.globl _WDOG_COUNT
                                    312 	.globl _RESET_KEEP
                                    313 	.globl _WAKE_CTRL
                                    314 	.globl _CLOCK_CFG
                                    315 	.globl _POWER_CFG
                                    316 	.globl _PCON
                                    317 	.globl _GLOBAL_CFG
                                    318 	.globl _SAFE_MOD
                                    319 	.globl _DPH
                                    320 	.globl _DPL
                                    321 	.globl _SP
                                    322 	.globl _A_INV
                                    323 	.globl _B
                                    324 	.globl _ACC
                                    325 	.globl _PSW
                                    326 ;--------------------------------------------------------
                                    327 ; special function registers
                                    328 ;--------------------------------------------------------
                                    329 	.area RSEG    (ABS,DATA)
      000000                        330 	.org 0x0000
                           0000D0   331 _PSW	=	0x00d0
                           0000E0   332 _ACC	=	0x00e0
                           0000F0   333 _B	=	0x00f0
                           0000FD   334 _A_INV	=	0x00fd
                           000081   335 _SP	=	0x0081
                           000082   336 _DPL	=	0x0082
                           000083   337 _DPH	=	0x0083
                           0000A1   338 _SAFE_MOD	=	0x00a1
                           0000B1   339 _GLOBAL_CFG	=	0x00b1
                           000087   340 _PCON	=	0x0087
                           0000BA   341 _POWER_CFG	=	0x00ba
                           0000B9   342 _CLOCK_CFG	=	0x00b9
                           0000A9   343 _WAKE_CTRL	=	0x00a9
                           0000FE   344 _RESET_KEEP	=	0x00fe
                           0000FF   345 _WDOG_COUNT	=	0x00ff
                           0000A8   346 _IE	=	0x00a8
                           0000B8   347 _IP	=	0x00b8
                           0000E8   348 _IE_EX	=	0x00e8
                           0000E9   349 _IP_EX	=	0x00e9
                           0000B3   350 _INTX	=	0x00b3
                           0000B2   351 _GPIO_IE	=	0x00b2
                           008584   352 _ROM_ADDR	=	0x8584
                           000084   353 _ROM_ADDR_L	=	0x0084
                           000085   354 _ROM_ADDR_H	=	0x0085
                           008F8E   355 _ROM_DATA_HI	=	0x8f8e
                           00008E   356 _ROM_DATA_HL	=	0x008e
                           00008F   357 _ROM_DATA_HH	=	0x008f
                           000086   358 _ROM_CTRL	=	0x0086
                           000080   359 _P0	=	0x0080
                           0000C4   360 _P0_MOD_OC	=	0x00c4
                           0000C5   361 _P0_DIR_PU	=	0x00c5
                           000090   362 _P1	=	0x0090
                           000092   363 _P1_MOD_OC	=	0x0092
                           000093   364 _P1_DIR_PU	=	0x0093
                           0000A0   365 _P2	=	0x00a0
                           000094   366 _P2_MOD_OC	=	0x0094
                           000095   367 _P2_DIR_PU	=	0x0095
                           0000B0   368 _P3	=	0x00b0
                           000096   369 _P3_MOD_OC	=	0x0096
                           000097   370 _P3_DIR_PU	=	0x0097
                           0000C0   371 _P4	=	0x00c0
                           0000C2   372 _P4_MOD_OC	=	0x00c2
                           0000C3   373 _P4_DIR_PU	=	0x00c3
                           0000AB   374 _P5	=	0x00ab
                           0000AA   375 _PIN_FUNC	=	0x00aa
                           0000A2   376 _XBUS_AUX	=	0x00a2
                           000088   377 _TCON	=	0x0088
                           000089   378 _TMOD	=	0x0089
                           00008A   379 _TL0	=	0x008a
                           00008B   380 _TL1	=	0x008b
                           00008C   381 _TH0	=	0x008c
                           00008D   382 _TH1	=	0x008d
                           000098   383 _SCON	=	0x0098
                           000099   384 _SBUF	=	0x0099
                           0000C1   385 _T2CON2	=	0x00c1
                           00C7C6   386 _T2CAP0	=	0xc7c6
                           0000C6   387 _T2CAP0L	=	0x00c6
                           0000C7   388 _T2CAP0H	=	0x00c7
                           0000C8   389 _T2CON	=	0x00c8
                           0000C9   390 _T2MOD	=	0x00c9
                           00CBCA   391 _RCAP2	=	0xcbca
                           0000CA   392 _RCAP2L	=	0x00ca
                           0000CB   393 _RCAP2H	=	0x00cb
                           00CDCC   394 _T2COUNT	=	0xcdcc
                           0000CC   395 _TL2	=	0x00cc
                           0000CD   396 _TH2	=	0x00cd
                           00CFCE   397 _T2CAP1	=	0xcfce
                           0000CE   398 _T2CAP1L	=	0x00ce
                           0000CF   399 _T2CAP1H	=	0x00cf
                           00009A   400 _PWM_DATA2	=	0x009a
                           00009B   401 _PWM_DATA1	=	0x009b
                           00009C   402 _PWM_DATA0	=	0x009c
                           00009D   403 _PWM_CTRL	=	0x009d
                           00009E   404 _PWM_CK_SE	=	0x009e
                           00009F   405 _PWM_CTRL2	=	0x009f
                           0000A3   406 _PWM_DATA3	=	0x00a3
                           0000A4   407 _PWM_DATA4	=	0x00a4
                           0000A5   408 _PWM_DATA5	=	0x00a5
                           0000A6   409 _PWM_DATA6	=	0x00a6
                           0000A7   410 _PWM_DATA7	=	0x00a7
                           0000F8   411 _SPI0_STAT	=	0x00f8
                           0000F9   412 _SPI0_DATA	=	0x00f9
                           0000FA   413 _SPI0_CTRL	=	0x00fa
                           0000FB   414 _SPI0_CK_SE	=	0x00fb
                           0000FC   415 _SPI0_SETUP	=	0x00fc
                           0000BC   416 _SCON1	=	0x00bc
                           0000BD   417 _SBUF1	=	0x00bd
                           0000BE   418 _SBAUD1	=	0x00be
                           0000BF   419 _SIF1	=	0x00bf
                           0000B4   420 _SCON2	=	0x00b4
                           0000B5   421 _SBUF2	=	0x00b5
                           0000B6   422 _SBAUD2	=	0x00b6
                           0000B7   423 _SIF2	=	0x00b7
                           0000AC   424 _SCON3	=	0x00ac
                           0000AD   425 _SBUF3	=	0x00ad
                           0000AE   426 _SBAUD3	=	0x00ae
                           0000AF   427 _SIF3	=	0x00af
                           0000F1   428 _TKEY_CTRL	=	0x00f1
                           0000F2   429 _ADC_CTRL	=	0x00f2
                           0000F3   430 _ADC_CFG	=	0x00f3
                           00F5F4   431 _ADC_DAT	=	0xf5f4
                           0000F4   432 _ADC_DAT_L	=	0x00f4
                           0000F5   433 _ADC_DAT_H	=	0x00f5
                           0000F6   434 _ADC_CHAN	=	0x00f6
                           0000F7   435 _ADC_PIN	=	0x00f7
                           000091   436 _USB_C_CTRL	=	0x0091
                           0000D1   437 _UDEV_CTRL	=	0x00d1
                           0000D2   438 _UEP1_CTRL	=	0x00d2
                           0000D3   439 _UEP1_T_LEN	=	0x00d3
                           0000D4   440 _UEP2_CTRL	=	0x00d4
                           0000D5   441 _UEP2_T_LEN	=	0x00d5
                           0000D6   442 _UEP3_CTRL	=	0x00d6
                           0000D7   443 _UEP3_T_LEN	=	0x00d7
                           0000D8   444 _USB_INT_FG	=	0x00d8
                           0000D9   445 _USB_INT_ST	=	0x00d9
                           0000DA   446 _USB_MIS_ST	=	0x00da
                           0000DB   447 _USB_RX_LEN	=	0x00db
                           0000DC   448 _UEP0_CTRL	=	0x00dc
                           0000DD   449 _UEP0_T_LEN	=	0x00dd
                           0000DE   450 _UEP4_CTRL	=	0x00de
                           0000DF   451 _UEP4_T_LEN	=	0x00df
                           0000E1   452 _USB_INT_EN	=	0x00e1
                           0000E2   453 _USB_CTRL	=	0x00e2
                           0000E3   454 _USB_DEV_AD	=	0x00e3
                           00E5E4   455 _UEP2_DMA	=	0xe5e4
                           0000E4   456 _UEP2_DMA_L	=	0x00e4
                           0000E5   457 _UEP2_DMA_H	=	0x00e5
                           00E7E6   458 _UEP3_DMA	=	0xe7e6
                           0000E6   459 _UEP3_DMA_L	=	0x00e6
                           0000E7   460 _UEP3_DMA_H	=	0x00e7
                           0000EA   461 _UEP4_1_MOD	=	0x00ea
                           0000EB   462 _UEP2_3_MOD	=	0x00eb
                           00EDEC   463 _UEP0_DMA	=	0xedec
                           0000EC   464 _UEP0_DMA_L	=	0x00ec
                           0000ED   465 _UEP0_DMA_H	=	0x00ed
                           00EFEE   466 _UEP1_DMA	=	0xefee
                           0000EE   467 _UEP1_DMA_L	=	0x00ee
                           0000EF   468 _UEP1_DMA_H	=	0x00ef
                                    469 ;--------------------------------------------------------
                                    470 ; special function bits
                                    471 ;--------------------------------------------------------
                                    472 	.area RSEG    (ABS,DATA)
      000000                        473 	.org 0x0000
                           0000D7   474 _CY	=	0x00d7
                           0000D6   475 _AC	=	0x00d6
                           0000D5   476 _F0	=	0x00d5
                           0000D4   477 _RS1	=	0x00d4
                           0000D3   478 _RS0	=	0x00d3
                           0000D2   479 _OV	=	0x00d2
                           0000D1   480 _F1	=	0x00d1
                           0000D0   481 _P	=	0x00d0
                           0000AF   482 _EA	=	0x00af
                           0000AE   483 _E_DIS	=	0x00ae
                           0000AD   484 _ET2	=	0x00ad
                           0000AC   485 _ES	=	0x00ac
                           0000AB   486 _ET1	=	0x00ab
                           0000AA   487 _EX1	=	0x00aa
                           0000A9   488 _ET0	=	0x00a9
                           0000A8   489 _EX0	=	0x00a8
                           0000BF   490 _PH_FLAG	=	0x00bf
                           0000BE   491 _PL_FLAG	=	0x00be
                           0000BD   492 _PT2	=	0x00bd
                           0000BC   493 _PS	=	0x00bc
                           0000BB   494 _PT1	=	0x00bb
                           0000BA   495 _PX1	=	0x00ba
                           0000B9   496 _PT0	=	0x00b9
                           0000B8   497 _PX0	=	0x00b8
                           0000EF   498 _IE_WDOG	=	0x00ef
                           0000EE   499 _IE_GPIO	=	0x00ee
                           0000ED   500 _IE_PWMX	=	0x00ed
                           0000ED   501 _IE_UART3	=	0x00ed
                           0000EC   502 _IE_UART1	=	0x00ec
                           0000EB   503 _IE_ADC	=	0x00eb
                           0000EB   504 _IE_UART2	=	0x00eb
                           0000EA   505 _IE_USB	=	0x00ea
                           0000E9   506 _IE_INT3	=	0x00e9
                           0000E8   507 _IE_SPI0	=	0x00e8
                           000087   508 _P0_7	=	0x0087
                           000086   509 _P0_6	=	0x0086
                           000085   510 _P0_5	=	0x0085
                           000084   511 _P0_4	=	0x0084
                           000083   512 _P0_3	=	0x0083
                           000082   513 _P0_2	=	0x0082
                           000081   514 _P0_1	=	0x0081
                           000080   515 _P0_0	=	0x0080
                           000087   516 _TXD3	=	0x0087
                           000087   517 _AIN15	=	0x0087
                           000086   518 _RXD3	=	0x0086
                           000086   519 _AIN14	=	0x0086
                           000085   520 _TXD2	=	0x0085
                           000085   521 _AIN13	=	0x0085
                           000084   522 _RXD2	=	0x0084
                           000084   523 _AIN12	=	0x0084
                           000083   524 _TXD_	=	0x0083
                           000083   525 _AIN11	=	0x0083
                           000082   526 _RXD_	=	0x0082
                           000082   527 _AIN10	=	0x0082
                           000081   528 _AIN9	=	0x0081
                           000080   529 _AIN8	=	0x0080
                           000097   530 _P1_7	=	0x0097
                           000096   531 _P1_6	=	0x0096
                           000095   532 _P1_5	=	0x0095
                           000094   533 _P1_4	=	0x0094
                           000093   534 _P1_3	=	0x0093
                           000092   535 _P1_2	=	0x0092
                           000091   536 _P1_1	=	0x0091
                           000090   537 _P1_0	=	0x0090
                           000097   538 _SCK	=	0x0097
                           000097   539 _TXD1_	=	0x0097
                           000097   540 _AIN7	=	0x0097
                           000096   541 _MISO	=	0x0096
                           000096   542 _RXD1_	=	0x0096
                           000096   543 _VBUS	=	0x0096
                           000096   544 _AIN6	=	0x0096
                           000095   545 _MOSI	=	0x0095
                           000095   546 _PWM0_	=	0x0095
                           000095   547 _UCC2	=	0x0095
                           000095   548 _AIN5	=	0x0095
                           000094   549 _SCS	=	0x0094
                           000094   550 _UCC1	=	0x0094
                           000094   551 _AIN4	=	0x0094
                           000093   552 _AIN3	=	0x0093
                           000092   553 _AIN2	=	0x0092
                           000091   554 _T2EX	=	0x0091
                           000091   555 _CAP2	=	0x0091
                           000091   556 _AIN1	=	0x0091
                           000090   557 _T2	=	0x0090
                           000090   558 _CAP1	=	0x0090
                           000090   559 _AIN0	=	0x0090
                           0000A7   560 _P2_7	=	0x00a7
                           0000A6   561 _P2_6	=	0x00a6
                           0000A5   562 _P2_5	=	0x00a5
                           0000A4   563 _P2_4	=	0x00a4
                           0000A3   564 _P2_3	=	0x00a3
                           0000A2   565 _P2_2	=	0x00a2
                           0000A1   566 _P2_1	=	0x00a1
                           0000A0   567 _P2_0	=	0x00a0
                           0000A7   568 _PWM7	=	0x00a7
                           0000A7   569 _TXD1	=	0x00a7
                           0000A6   570 _PWM6	=	0x00a6
                           0000A6   571 _RXD1	=	0x00a6
                           0000A5   572 _PWM0	=	0x00a5
                           0000A5   573 _T2EX_	=	0x00a5
                           0000A5   574 _CAP2_	=	0x00a5
                           0000A4   575 _PWM1	=	0x00a4
                           0000A4   576 _T2_	=	0x00a4
                           0000A4   577 _CAP1_	=	0x00a4
                           0000A3   578 _PWM2	=	0x00a3
                           0000A2   579 _PWM3	=	0x00a2
                           0000A2   580 _INT0_	=	0x00a2
                           0000A1   581 _PWM4	=	0x00a1
                           0000A0   582 _PWM5	=	0x00a0
                           0000B7   583 _P3_7	=	0x00b7
                           0000B6   584 _P3_6	=	0x00b6
                           0000B5   585 _P3_5	=	0x00b5
                           0000B4   586 _P3_4	=	0x00b4
                           0000B3   587 _P3_3	=	0x00b3
                           0000B2   588 _P3_2	=	0x00b2
                           0000B1   589 _P3_1	=	0x00b1
                           0000B0   590 _P3_0	=	0x00b0
                           0000B7   591 _INT3	=	0x00b7
                           0000B6   592 _CAP0	=	0x00b6
                           0000B5   593 _T1	=	0x00b5
                           0000B4   594 _T0	=	0x00b4
                           0000B3   595 _INT1	=	0x00b3
                           0000B2   596 _INT0	=	0x00b2
                           0000B1   597 _TXD	=	0x00b1
                           0000B0   598 _RXD	=	0x00b0
                           0000C6   599 _P4_6	=	0x00c6
                           0000C5   600 _P4_5	=	0x00c5
                           0000C4   601 _P4_4	=	0x00c4
                           0000C3   602 _P4_3	=	0x00c3
                           0000C2   603 _P4_2	=	0x00c2
                           0000C1   604 _P4_1	=	0x00c1
                           0000C0   605 _P4_0	=	0x00c0
                           0000C7   606 _XO	=	0x00c7
                           0000C6   607 _XI	=	0x00c6
                           00008F   608 _TF1	=	0x008f
                           00008E   609 _TR1	=	0x008e
                           00008D   610 _TF0	=	0x008d
                           00008C   611 _TR0	=	0x008c
                           00008B   612 _IE1	=	0x008b
                           00008A   613 _IT1	=	0x008a
                           000089   614 _IE0	=	0x0089
                           000088   615 _IT0	=	0x0088
                           00009F   616 _SM0	=	0x009f
                           00009E   617 _SM1	=	0x009e
                           00009D   618 _SM2	=	0x009d
                           00009C   619 _REN	=	0x009c
                           00009B   620 _TB8	=	0x009b
                           00009A   621 _RB8	=	0x009a
                           000099   622 _TI	=	0x0099
                           000098   623 _RI	=	0x0098
                           0000CF   624 _TF2	=	0x00cf
                           0000CF   625 _CAP1F	=	0x00cf
                           0000CE   626 _EXF2	=	0x00ce
                           0000CD   627 _RCLK	=	0x00cd
                           0000CC   628 _TCLK	=	0x00cc
                           0000CB   629 _EXEN2	=	0x00cb
                           0000CA   630 _TR2	=	0x00ca
                           0000C9   631 _C_T2	=	0x00c9
                           0000C8   632 _CP_RL2	=	0x00c8
                           0000FF   633 _S0_FST_ACT	=	0x00ff
                           0000FE   634 _S0_IF_OV	=	0x00fe
                           0000FD   635 _S0_IF_FIRST	=	0x00fd
                           0000FC   636 _S0_IF_BYTE	=	0x00fc
                           0000FB   637 _S0_FREE	=	0x00fb
                           0000FA   638 _S0_T_FIFO	=	0x00fa
                           0000F8   639 _S0_R_FIFO	=	0x00f8
                           0000DF   640 _U_IS_NAK	=	0x00df
                           0000DE   641 _U_TOG_OK	=	0x00de
                           0000DD   642 _U_SIE_FREE	=	0x00dd
                           0000DC   643 _UIF_FIFO_OV	=	0x00dc
                           0000DB   644 _UIF_HST_SOF	=	0x00db
                           0000DA   645 _UIF_SUSPEND	=	0x00da
                           0000D9   646 _UIF_TRANSFER	=	0x00d9
                           0000D8   647 _UIF_DETECT	=	0x00d8
                           0000D8   648 _UIF_BUS_RST	=	0x00d8
                                    649 ;--------------------------------------------------------
                                    650 ; overlayable register banks
                                    651 ;--------------------------------------------------------
                                    652 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        653 	.ds 8
                                    654 ;--------------------------------------------------------
                                    655 ; internal ram data
                                    656 ;--------------------------------------------------------
                                    657 	.area DSEG    (DATA)
                                    658 ;--------------------------------------------------------
                                    659 ; overlayable items in internal ram 
                                    660 ;--------------------------------------------------------
                                    661 	.area	OSEG    (OVR,DATA)
                                    662 ;--------------------------------------------------------
                                    663 ; Stack segment in internal ram 
                                    664 ;--------------------------------------------------------
                                    665 	.area	SSEG
      000008                        666 __start__stack:
      000008                        667 	.ds	1
                                    668 
                                    669 ;--------------------------------------------------------
                                    670 ; indirectly addressable internal ram data
                                    671 ;--------------------------------------------------------
                                    672 	.area ISEG    (DATA)
                                    673 ;--------------------------------------------------------
                                    674 ; absolute internal ram data
                                    675 ;--------------------------------------------------------
                                    676 	.area IABS    (ABS,DATA)
                                    677 	.area IABS    (ABS,DATA)
                                    678 ;--------------------------------------------------------
                                    679 ; bit data
                                    680 ;--------------------------------------------------------
                                    681 	.area BSEG    (BIT)
                                    682 ;--------------------------------------------------------
                                    683 ; paged external ram data
                                    684 ;--------------------------------------------------------
                                    685 	.area PSEG    (PAG,XDATA)
                                    686 ;--------------------------------------------------------
                                    687 ; external ram data
                                    688 ;--------------------------------------------------------
                                    689 	.area XSEG    (XDATA)
                                    690 ;--------------------------------------------------------
                                    691 ; absolute external ram data
                                    692 ;--------------------------------------------------------
                                    693 	.area XABS    (ABS,XDATA)
                                    694 ;--------------------------------------------------------
                                    695 ; external initialized ram data
                                    696 ;--------------------------------------------------------
                                    697 	.area XISEG   (XDATA)
                                    698 	.area HOME    (CODE)
                                    699 	.area GSINIT0 (CODE)
                                    700 	.area GSINIT1 (CODE)
                                    701 	.area GSINIT2 (CODE)
                                    702 	.area GSINIT3 (CODE)
                                    703 	.area GSINIT4 (CODE)
                                    704 	.area GSINIT5 (CODE)
                                    705 	.area GSINIT  (CODE)
                                    706 	.area GSFINAL (CODE)
                                    707 	.area CSEG    (CODE)
                                    708 ;--------------------------------------------------------
                                    709 ; interrupt vector 
                                    710 ;--------------------------------------------------------
                                    711 	.area HOME    (CODE)
      000000                        712 __interrupt_vect:
      000000 02 00 06         [24]  713 	ljmp	__sdcc_gsinit_startup
                                    714 ;--------------------------------------------------------
                                    715 ; global & static initialisations
                                    716 ;--------------------------------------------------------
                                    717 	.area HOME    (CODE)
                                    718 	.area GSINIT  (CODE)
                                    719 	.area GSFINAL (CODE)
                                    720 	.area GSINIT  (CODE)
                                    721 	.globl __sdcc_gsinit_startup
                                    722 	.globl __sdcc_program_startup
                                    723 	.globl __start__stack
                                    724 	.globl __mcs51_genXINIT
                                    725 	.globl __mcs51_genXRAMCLEAR
                                    726 	.globl __mcs51_genRAMCLEAR
                                    727 	.area GSFINAL (CODE)
      00005F 02 00 03         [24]  728 	ljmp	__sdcc_program_startup
                                    729 ;--------------------------------------------------------
                                    730 ; Home
                                    731 ;--------------------------------------------------------
                                    732 	.area HOME    (CODE)
                                    733 	.area HOME    (CODE)
      000003                        734 __sdcc_program_startup:
      000003 02 00 74         [24]  735 	ljmp	_main
                                    736 ;	return from main will return to caller
                                    737 ;--------------------------------------------------------
                                    738 ; code
                                    739 ;--------------------------------------------------------
                                    740 	.area CSEG    (CODE)
                                    741 ;------------------------------------------------------------
                                    742 ;Allocation info for local variables in function 'delay'
                                    743 ;------------------------------------------------------------
                                    744 ;i                         Allocated to registers 
                                    745 ;------------------------------------------------------------
                                    746 ;	usr/main.c:24: void delay(int i)
                                    747 ;	-----------------------------------------
                                    748 ;	 function delay
                                    749 ;	-----------------------------------------
      000062                        750 _delay:
                           000007   751 	ar7 = 0x07
                           000006   752 	ar6 = 0x06
                           000005   753 	ar5 = 0x05
                           000004   754 	ar4 = 0x04
                           000003   755 	ar3 = 0x03
                           000002   756 	ar2 = 0x02
                           000001   757 	ar1 = 0x01
                           000000   758 	ar0 = 0x00
      000062 AE 82            [24]  759 	mov	r6,dpl
      000064 AF 83            [24]  760 	mov	r7,dph
                                    761 ;	usr/main.c:26: while(i--);	
      000066                        762 00101$:
      000066 8E 04            [24]  763 	mov	ar4,r6
      000068 8F 05            [24]  764 	mov	ar5,r7
      00006A 1E               [12]  765 	dec	r6
      00006B BE FF 01         [24]  766 	cjne	r6,#0xff,00111$
      00006E 1F               [12]  767 	dec	r7
      00006F                        768 00111$:
      00006F EC               [12]  769 	mov	a,r4
      000070 4D               [12]  770 	orl	a,r5
      000071 70 F3            [24]  771 	jnz	00101$
                                    772 ;	usr/main.c:27: }
      000073 22               [24]  773 	ret
                                    774 ;------------------------------------------------------------
                                    775 ;Allocation info for local variables in function 'main'
                                    776 ;------------------------------------------------------------
                                    777 ;	usr/main.c:35: void main()
                                    778 ;	-----------------------------------------
                                    779 ;	 function main
                                    780 ;	-----------------------------------------
      000074                        781 _main:
                                    782 ;	usr/main.c:37: while(1)
      000074                        783 00102$:
                                    784 ;	usr/main.c:39: led =!led;    //p2.2输出取反
      000074 B2 A2            [12]  785 	cpl	_P2_2
                                    786 ;	usr/main.c:40: delay(50000);
      000076 90 C3 50         [24]  787 	mov	dptr,#0xc350
      000079 12 00 62         [24]  788 	lcall	_delay
                                    789 ;	usr/main.c:43: } 
      00007C 80 F6            [24]  790 	sjmp	00102$
                                    791 	.area CSEG    (CODE)
                                    792 	.area CONST   (CODE)
                                    793 	.area XINIT   (CODE)
                                    794 	.area CABS    (ABS,CODE)
