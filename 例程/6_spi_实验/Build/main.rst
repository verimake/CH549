                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module main
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _BMP2
                                     12 	.globl _BMP1
                                     13 	.globl _main
                                     14 	.globl _CH549SPIMasterWrite
                                     15 	.globl _SPIMasterModeSet
                                     16 	.globl _mDelaymS
                                     17 	.globl _CfgFsys
                                     18 	.globl _setFontSize
                                     19 	.globl _OLED_ShowString
                                     20 	.globl _OLED_Clear
                                     21 	.globl _OLED_Init
                                     22 	.globl _UIF_BUS_RST
                                     23 	.globl _UIF_DETECT
                                     24 	.globl _UIF_TRANSFER
                                     25 	.globl _UIF_SUSPEND
                                     26 	.globl _UIF_HST_SOF
                                     27 	.globl _UIF_FIFO_OV
                                     28 	.globl _U_SIE_FREE
                                     29 	.globl _U_TOG_OK
                                     30 	.globl _U_IS_NAK
                                     31 	.globl _S0_R_FIFO
                                     32 	.globl _S0_T_FIFO
                                     33 	.globl _S0_FREE
                                     34 	.globl _S0_IF_BYTE
                                     35 	.globl _S0_IF_FIRST
                                     36 	.globl _S0_IF_OV
                                     37 	.globl _S0_FST_ACT
                                     38 	.globl _CP_RL2
                                     39 	.globl _C_T2
                                     40 	.globl _TR2
                                     41 	.globl _EXEN2
                                     42 	.globl _TCLK
                                     43 	.globl _RCLK
                                     44 	.globl _EXF2
                                     45 	.globl _CAP1F
                                     46 	.globl _TF2
                                     47 	.globl _RI
                                     48 	.globl _TI
                                     49 	.globl _RB8
                                     50 	.globl _TB8
                                     51 	.globl _REN
                                     52 	.globl _SM2
                                     53 	.globl _SM1
                                     54 	.globl _SM0
                                     55 	.globl _IT0
                                     56 	.globl _IE0
                                     57 	.globl _IT1
                                     58 	.globl _IE1
                                     59 	.globl _TR0
                                     60 	.globl _TF0
                                     61 	.globl _TR1
                                     62 	.globl _TF1
                                     63 	.globl _XI
                                     64 	.globl _XO
                                     65 	.globl _P4_0
                                     66 	.globl _P4_1
                                     67 	.globl _P4_2
                                     68 	.globl _P4_3
                                     69 	.globl _P4_4
                                     70 	.globl _P4_5
                                     71 	.globl _P4_6
                                     72 	.globl _RXD
                                     73 	.globl _TXD
                                     74 	.globl _INT0
                                     75 	.globl _INT1
                                     76 	.globl _T0
                                     77 	.globl _T1
                                     78 	.globl _CAP0
                                     79 	.globl _INT3
                                     80 	.globl _P3_0
                                     81 	.globl _P3_1
                                     82 	.globl _P3_2
                                     83 	.globl _P3_3
                                     84 	.globl _P3_4
                                     85 	.globl _P3_5
                                     86 	.globl _P3_6
                                     87 	.globl _P3_7
                                     88 	.globl _PWM5
                                     89 	.globl _PWM4
                                     90 	.globl _INT0_
                                     91 	.globl _PWM3
                                     92 	.globl _PWM2
                                     93 	.globl _CAP1_
                                     94 	.globl _T2_
                                     95 	.globl _PWM1
                                     96 	.globl _CAP2_
                                     97 	.globl _T2EX_
                                     98 	.globl _PWM0
                                     99 	.globl _RXD1
                                    100 	.globl _PWM6
                                    101 	.globl _TXD1
                                    102 	.globl _PWM7
                                    103 	.globl _P2_0
                                    104 	.globl _P2_1
                                    105 	.globl _P2_2
                                    106 	.globl _P2_3
                                    107 	.globl _P2_4
                                    108 	.globl _P2_5
                                    109 	.globl _P2_6
                                    110 	.globl _P2_7
                                    111 	.globl _AIN0
                                    112 	.globl _CAP1
                                    113 	.globl _T2
                                    114 	.globl _AIN1
                                    115 	.globl _CAP2
                                    116 	.globl _T2EX
                                    117 	.globl _AIN2
                                    118 	.globl _AIN3
                                    119 	.globl _AIN4
                                    120 	.globl _UCC1
                                    121 	.globl _SCS
                                    122 	.globl _AIN5
                                    123 	.globl _UCC2
                                    124 	.globl _PWM0_
                                    125 	.globl _MOSI
                                    126 	.globl _AIN6
                                    127 	.globl _VBUS
                                    128 	.globl _RXD1_
                                    129 	.globl _MISO
                                    130 	.globl _AIN7
                                    131 	.globl _TXD1_
                                    132 	.globl _SCK
                                    133 	.globl _P1_0
                                    134 	.globl _P1_1
                                    135 	.globl _P1_2
                                    136 	.globl _P1_3
                                    137 	.globl _P1_4
                                    138 	.globl _P1_5
                                    139 	.globl _P1_6
                                    140 	.globl _P1_7
                                    141 	.globl _AIN8
                                    142 	.globl _AIN9
                                    143 	.globl _AIN10
                                    144 	.globl _RXD_
                                    145 	.globl _AIN11
                                    146 	.globl _TXD_
                                    147 	.globl _AIN12
                                    148 	.globl _RXD2
                                    149 	.globl _AIN13
                                    150 	.globl _TXD2
                                    151 	.globl _AIN14
                                    152 	.globl _RXD3
                                    153 	.globl _AIN15
                                    154 	.globl _TXD3
                                    155 	.globl _P0_0
                                    156 	.globl _P0_1
                                    157 	.globl _P0_2
                                    158 	.globl _P0_3
                                    159 	.globl _P0_4
                                    160 	.globl _P0_5
                                    161 	.globl _P0_6
                                    162 	.globl _P0_7
                                    163 	.globl _IE_SPI0
                                    164 	.globl _IE_INT3
                                    165 	.globl _IE_USB
                                    166 	.globl _IE_UART2
                                    167 	.globl _IE_ADC
                                    168 	.globl _IE_UART1
                                    169 	.globl _IE_UART3
                                    170 	.globl _IE_PWMX
                                    171 	.globl _IE_GPIO
                                    172 	.globl _IE_WDOG
                                    173 	.globl _PX0
                                    174 	.globl _PT0
                                    175 	.globl _PX1
                                    176 	.globl _PT1
                                    177 	.globl _PS
                                    178 	.globl _PT2
                                    179 	.globl _PL_FLAG
                                    180 	.globl _PH_FLAG
                                    181 	.globl _EX0
                                    182 	.globl _ET0
                                    183 	.globl _EX1
                                    184 	.globl _ET1
                                    185 	.globl _ES
                                    186 	.globl _ET2
                                    187 	.globl _E_DIS
                                    188 	.globl _EA
                                    189 	.globl _P
                                    190 	.globl _F1
                                    191 	.globl _OV
                                    192 	.globl _RS0
                                    193 	.globl _RS1
                                    194 	.globl _F0
                                    195 	.globl _AC
                                    196 	.globl _CY
                                    197 	.globl _UEP1_DMA_H
                                    198 	.globl _UEP1_DMA_L
                                    199 	.globl _UEP1_DMA
                                    200 	.globl _UEP0_DMA_H
                                    201 	.globl _UEP0_DMA_L
                                    202 	.globl _UEP0_DMA
                                    203 	.globl _UEP2_3_MOD
                                    204 	.globl _UEP4_1_MOD
                                    205 	.globl _UEP3_DMA_H
                                    206 	.globl _UEP3_DMA_L
                                    207 	.globl _UEP3_DMA
                                    208 	.globl _UEP2_DMA_H
                                    209 	.globl _UEP2_DMA_L
                                    210 	.globl _UEP2_DMA
                                    211 	.globl _USB_DEV_AD
                                    212 	.globl _USB_CTRL
                                    213 	.globl _USB_INT_EN
                                    214 	.globl _UEP4_T_LEN
                                    215 	.globl _UEP4_CTRL
                                    216 	.globl _UEP0_T_LEN
                                    217 	.globl _UEP0_CTRL
                                    218 	.globl _USB_RX_LEN
                                    219 	.globl _USB_MIS_ST
                                    220 	.globl _USB_INT_ST
                                    221 	.globl _USB_INT_FG
                                    222 	.globl _UEP3_T_LEN
                                    223 	.globl _UEP3_CTRL
                                    224 	.globl _UEP2_T_LEN
                                    225 	.globl _UEP2_CTRL
                                    226 	.globl _UEP1_T_LEN
                                    227 	.globl _UEP1_CTRL
                                    228 	.globl _UDEV_CTRL
                                    229 	.globl _USB_C_CTRL
                                    230 	.globl _ADC_PIN
                                    231 	.globl _ADC_CHAN
                                    232 	.globl _ADC_DAT_H
                                    233 	.globl _ADC_DAT_L
                                    234 	.globl _ADC_DAT
                                    235 	.globl _ADC_CFG
                                    236 	.globl _ADC_CTRL
                                    237 	.globl _TKEY_CTRL
                                    238 	.globl _SIF3
                                    239 	.globl _SBAUD3
                                    240 	.globl _SBUF3
                                    241 	.globl _SCON3
                                    242 	.globl _SIF2
                                    243 	.globl _SBAUD2
                                    244 	.globl _SBUF2
                                    245 	.globl _SCON2
                                    246 	.globl _SIF1
                                    247 	.globl _SBAUD1
                                    248 	.globl _SBUF1
                                    249 	.globl _SCON1
                                    250 	.globl _SPI0_SETUP
                                    251 	.globl _SPI0_CK_SE
                                    252 	.globl _SPI0_CTRL
                                    253 	.globl _SPI0_DATA
                                    254 	.globl _SPI0_STAT
                                    255 	.globl _PWM_DATA7
                                    256 	.globl _PWM_DATA6
                                    257 	.globl _PWM_DATA5
                                    258 	.globl _PWM_DATA4
                                    259 	.globl _PWM_DATA3
                                    260 	.globl _PWM_CTRL2
                                    261 	.globl _PWM_CK_SE
                                    262 	.globl _PWM_CTRL
                                    263 	.globl _PWM_DATA0
                                    264 	.globl _PWM_DATA1
                                    265 	.globl _PWM_DATA2
                                    266 	.globl _T2CAP1H
                                    267 	.globl _T2CAP1L
                                    268 	.globl _T2CAP1
                                    269 	.globl _TH2
                                    270 	.globl _TL2
                                    271 	.globl _T2COUNT
                                    272 	.globl _RCAP2H
                                    273 	.globl _RCAP2L
                                    274 	.globl _RCAP2
                                    275 	.globl _T2MOD
                                    276 	.globl _T2CON
                                    277 	.globl _T2CAP0H
                                    278 	.globl _T2CAP0L
                                    279 	.globl _T2CAP0
                                    280 	.globl _T2CON2
                                    281 	.globl _SBUF
                                    282 	.globl _SCON
                                    283 	.globl _TH1
                                    284 	.globl _TH0
                                    285 	.globl _TL1
                                    286 	.globl _TL0
                                    287 	.globl _TMOD
                                    288 	.globl _TCON
                                    289 	.globl _XBUS_AUX
                                    290 	.globl _PIN_FUNC
                                    291 	.globl _P5
                                    292 	.globl _P4_DIR_PU
                                    293 	.globl _P4_MOD_OC
                                    294 	.globl _P4
                                    295 	.globl _P3_DIR_PU
                                    296 	.globl _P3_MOD_OC
                                    297 	.globl _P3
                                    298 	.globl _P2_DIR_PU
                                    299 	.globl _P2_MOD_OC
                                    300 	.globl _P2
                                    301 	.globl _P1_DIR_PU
                                    302 	.globl _P1_MOD_OC
                                    303 	.globl _P1
                                    304 	.globl _P0_DIR_PU
                                    305 	.globl _P0_MOD_OC
                                    306 	.globl _P0
                                    307 	.globl _ROM_CTRL
                                    308 	.globl _ROM_DATA_HH
                                    309 	.globl _ROM_DATA_HL
                                    310 	.globl _ROM_DATA_HI
                                    311 	.globl _ROM_ADDR_H
                                    312 	.globl _ROM_ADDR_L
                                    313 	.globl _ROM_ADDR
                                    314 	.globl _GPIO_IE
                                    315 	.globl _INTX
                                    316 	.globl _IP_EX
                                    317 	.globl _IE_EX
                                    318 	.globl _IP
                                    319 	.globl _IE
                                    320 	.globl _WDOG_COUNT
                                    321 	.globl _RESET_KEEP
                                    322 	.globl _WAKE_CTRL
                                    323 	.globl _CLOCK_CFG
                                    324 	.globl _POWER_CFG
                                    325 	.globl _PCON
                                    326 	.globl _GLOBAL_CFG
                                    327 	.globl _SAFE_MOD
                                    328 	.globl _DPH
                                    329 	.globl _DPL
                                    330 	.globl _SP
                                    331 	.globl _A_INV
                                    332 	.globl _B
                                    333 	.globl _ACC
                                    334 	.globl _PSW
                                    335 ;--------------------------------------------------------
                                    336 ; special function registers
                                    337 ;--------------------------------------------------------
                                    338 	.area RSEG    (ABS,DATA)
      000000                        339 	.org 0x0000
                           0000D0   340 _PSW	=	0x00d0
                           0000E0   341 _ACC	=	0x00e0
                           0000F0   342 _B	=	0x00f0
                           0000FD   343 _A_INV	=	0x00fd
                           000081   344 _SP	=	0x0081
                           000082   345 _DPL	=	0x0082
                           000083   346 _DPH	=	0x0083
                           0000A1   347 _SAFE_MOD	=	0x00a1
                           0000B1   348 _GLOBAL_CFG	=	0x00b1
                           000087   349 _PCON	=	0x0087
                           0000BA   350 _POWER_CFG	=	0x00ba
                           0000B9   351 _CLOCK_CFG	=	0x00b9
                           0000A9   352 _WAKE_CTRL	=	0x00a9
                           0000FE   353 _RESET_KEEP	=	0x00fe
                           0000FF   354 _WDOG_COUNT	=	0x00ff
                           0000A8   355 _IE	=	0x00a8
                           0000B8   356 _IP	=	0x00b8
                           0000E8   357 _IE_EX	=	0x00e8
                           0000E9   358 _IP_EX	=	0x00e9
                           0000B3   359 _INTX	=	0x00b3
                           0000B2   360 _GPIO_IE	=	0x00b2
                           008584   361 _ROM_ADDR	=	0x8584
                           000084   362 _ROM_ADDR_L	=	0x0084
                           000085   363 _ROM_ADDR_H	=	0x0085
                           008F8E   364 _ROM_DATA_HI	=	0x8f8e
                           00008E   365 _ROM_DATA_HL	=	0x008e
                           00008F   366 _ROM_DATA_HH	=	0x008f
                           000086   367 _ROM_CTRL	=	0x0086
                           000080   368 _P0	=	0x0080
                           0000C4   369 _P0_MOD_OC	=	0x00c4
                           0000C5   370 _P0_DIR_PU	=	0x00c5
                           000090   371 _P1	=	0x0090
                           000092   372 _P1_MOD_OC	=	0x0092
                           000093   373 _P1_DIR_PU	=	0x0093
                           0000A0   374 _P2	=	0x00a0
                           000094   375 _P2_MOD_OC	=	0x0094
                           000095   376 _P2_DIR_PU	=	0x0095
                           0000B0   377 _P3	=	0x00b0
                           000096   378 _P3_MOD_OC	=	0x0096
                           000097   379 _P3_DIR_PU	=	0x0097
                           0000C0   380 _P4	=	0x00c0
                           0000C2   381 _P4_MOD_OC	=	0x00c2
                           0000C3   382 _P4_DIR_PU	=	0x00c3
                           0000AB   383 _P5	=	0x00ab
                           0000AA   384 _PIN_FUNC	=	0x00aa
                           0000A2   385 _XBUS_AUX	=	0x00a2
                           000088   386 _TCON	=	0x0088
                           000089   387 _TMOD	=	0x0089
                           00008A   388 _TL0	=	0x008a
                           00008B   389 _TL1	=	0x008b
                           00008C   390 _TH0	=	0x008c
                           00008D   391 _TH1	=	0x008d
                           000098   392 _SCON	=	0x0098
                           000099   393 _SBUF	=	0x0099
                           0000C1   394 _T2CON2	=	0x00c1
                           00C7C6   395 _T2CAP0	=	0xc7c6
                           0000C6   396 _T2CAP0L	=	0x00c6
                           0000C7   397 _T2CAP0H	=	0x00c7
                           0000C8   398 _T2CON	=	0x00c8
                           0000C9   399 _T2MOD	=	0x00c9
                           00CBCA   400 _RCAP2	=	0xcbca
                           0000CA   401 _RCAP2L	=	0x00ca
                           0000CB   402 _RCAP2H	=	0x00cb
                           00CDCC   403 _T2COUNT	=	0xcdcc
                           0000CC   404 _TL2	=	0x00cc
                           0000CD   405 _TH2	=	0x00cd
                           00CFCE   406 _T2CAP1	=	0xcfce
                           0000CE   407 _T2CAP1L	=	0x00ce
                           0000CF   408 _T2CAP1H	=	0x00cf
                           00009A   409 _PWM_DATA2	=	0x009a
                           00009B   410 _PWM_DATA1	=	0x009b
                           00009C   411 _PWM_DATA0	=	0x009c
                           00009D   412 _PWM_CTRL	=	0x009d
                           00009E   413 _PWM_CK_SE	=	0x009e
                           00009F   414 _PWM_CTRL2	=	0x009f
                           0000A3   415 _PWM_DATA3	=	0x00a3
                           0000A4   416 _PWM_DATA4	=	0x00a4
                           0000A5   417 _PWM_DATA5	=	0x00a5
                           0000A6   418 _PWM_DATA6	=	0x00a6
                           0000A7   419 _PWM_DATA7	=	0x00a7
                           0000F8   420 _SPI0_STAT	=	0x00f8
                           0000F9   421 _SPI0_DATA	=	0x00f9
                           0000FA   422 _SPI0_CTRL	=	0x00fa
                           0000FB   423 _SPI0_CK_SE	=	0x00fb
                           0000FC   424 _SPI0_SETUP	=	0x00fc
                           0000BC   425 _SCON1	=	0x00bc
                           0000BD   426 _SBUF1	=	0x00bd
                           0000BE   427 _SBAUD1	=	0x00be
                           0000BF   428 _SIF1	=	0x00bf
                           0000B4   429 _SCON2	=	0x00b4
                           0000B5   430 _SBUF2	=	0x00b5
                           0000B6   431 _SBAUD2	=	0x00b6
                           0000B7   432 _SIF2	=	0x00b7
                           0000AC   433 _SCON3	=	0x00ac
                           0000AD   434 _SBUF3	=	0x00ad
                           0000AE   435 _SBAUD3	=	0x00ae
                           0000AF   436 _SIF3	=	0x00af
                           0000F1   437 _TKEY_CTRL	=	0x00f1
                           0000F2   438 _ADC_CTRL	=	0x00f2
                           0000F3   439 _ADC_CFG	=	0x00f3
                           00F5F4   440 _ADC_DAT	=	0xf5f4
                           0000F4   441 _ADC_DAT_L	=	0x00f4
                           0000F5   442 _ADC_DAT_H	=	0x00f5
                           0000F6   443 _ADC_CHAN	=	0x00f6
                           0000F7   444 _ADC_PIN	=	0x00f7
                           000091   445 _USB_C_CTRL	=	0x0091
                           0000D1   446 _UDEV_CTRL	=	0x00d1
                           0000D2   447 _UEP1_CTRL	=	0x00d2
                           0000D3   448 _UEP1_T_LEN	=	0x00d3
                           0000D4   449 _UEP2_CTRL	=	0x00d4
                           0000D5   450 _UEP2_T_LEN	=	0x00d5
                           0000D6   451 _UEP3_CTRL	=	0x00d6
                           0000D7   452 _UEP3_T_LEN	=	0x00d7
                           0000D8   453 _USB_INT_FG	=	0x00d8
                           0000D9   454 _USB_INT_ST	=	0x00d9
                           0000DA   455 _USB_MIS_ST	=	0x00da
                           0000DB   456 _USB_RX_LEN	=	0x00db
                           0000DC   457 _UEP0_CTRL	=	0x00dc
                           0000DD   458 _UEP0_T_LEN	=	0x00dd
                           0000DE   459 _UEP4_CTRL	=	0x00de
                           0000DF   460 _UEP4_T_LEN	=	0x00df
                           0000E1   461 _USB_INT_EN	=	0x00e1
                           0000E2   462 _USB_CTRL	=	0x00e2
                           0000E3   463 _USB_DEV_AD	=	0x00e3
                           00E5E4   464 _UEP2_DMA	=	0xe5e4
                           0000E4   465 _UEP2_DMA_L	=	0x00e4
                           0000E5   466 _UEP2_DMA_H	=	0x00e5
                           00E7E6   467 _UEP3_DMA	=	0xe7e6
                           0000E6   468 _UEP3_DMA_L	=	0x00e6
                           0000E7   469 _UEP3_DMA_H	=	0x00e7
                           0000EA   470 _UEP4_1_MOD	=	0x00ea
                           0000EB   471 _UEP2_3_MOD	=	0x00eb
                           00EDEC   472 _UEP0_DMA	=	0xedec
                           0000EC   473 _UEP0_DMA_L	=	0x00ec
                           0000ED   474 _UEP0_DMA_H	=	0x00ed
                           00EFEE   475 _UEP1_DMA	=	0xefee
                           0000EE   476 _UEP1_DMA_L	=	0x00ee
                           0000EF   477 _UEP1_DMA_H	=	0x00ef
                                    478 ;--------------------------------------------------------
                                    479 ; special function bits
                                    480 ;--------------------------------------------------------
                                    481 	.area RSEG    (ABS,DATA)
      000000                        482 	.org 0x0000
                           0000D7   483 _CY	=	0x00d7
                           0000D6   484 _AC	=	0x00d6
                           0000D5   485 _F0	=	0x00d5
                           0000D4   486 _RS1	=	0x00d4
                           0000D3   487 _RS0	=	0x00d3
                           0000D2   488 _OV	=	0x00d2
                           0000D1   489 _F1	=	0x00d1
                           0000D0   490 _P	=	0x00d0
                           0000AF   491 _EA	=	0x00af
                           0000AE   492 _E_DIS	=	0x00ae
                           0000AD   493 _ET2	=	0x00ad
                           0000AC   494 _ES	=	0x00ac
                           0000AB   495 _ET1	=	0x00ab
                           0000AA   496 _EX1	=	0x00aa
                           0000A9   497 _ET0	=	0x00a9
                           0000A8   498 _EX0	=	0x00a8
                           0000BF   499 _PH_FLAG	=	0x00bf
                           0000BE   500 _PL_FLAG	=	0x00be
                           0000BD   501 _PT2	=	0x00bd
                           0000BC   502 _PS	=	0x00bc
                           0000BB   503 _PT1	=	0x00bb
                           0000BA   504 _PX1	=	0x00ba
                           0000B9   505 _PT0	=	0x00b9
                           0000B8   506 _PX0	=	0x00b8
                           0000EF   507 _IE_WDOG	=	0x00ef
                           0000EE   508 _IE_GPIO	=	0x00ee
                           0000ED   509 _IE_PWMX	=	0x00ed
                           0000ED   510 _IE_UART3	=	0x00ed
                           0000EC   511 _IE_UART1	=	0x00ec
                           0000EB   512 _IE_ADC	=	0x00eb
                           0000EB   513 _IE_UART2	=	0x00eb
                           0000EA   514 _IE_USB	=	0x00ea
                           0000E9   515 _IE_INT3	=	0x00e9
                           0000E8   516 _IE_SPI0	=	0x00e8
                           000087   517 _P0_7	=	0x0087
                           000086   518 _P0_6	=	0x0086
                           000085   519 _P0_5	=	0x0085
                           000084   520 _P0_4	=	0x0084
                           000083   521 _P0_3	=	0x0083
                           000082   522 _P0_2	=	0x0082
                           000081   523 _P0_1	=	0x0081
                           000080   524 _P0_0	=	0x0080
                           000087   525 _TXD3	=	0x0087
                           000087   526 _AIN15	=	0x0087
                           000086   527 _RXD3	=	0x0086
                           000086   528 _AIN14	=	0x0086
                           000085   529 _TXD2	=	0x0085
                           000085   530 _AIN13	=	0x0085
                           000084   531 _RXD2	=	0x0084
                           000084   532 _AIN12	=	0x0084
                           000083   533 _TXD_	=	0x0083
                           000083   534 _AIN11	=	0x0083
                           000082   535 _RXD_	=	0x0082
                           000082   536 _AIN10	=	0x0082
                           000081   537 _AIN9	=	0x0081
                           000080   538 _AIN8	=	0x0080
                           000097   539 _P1_7	=	0x0097
                           000096   540 _P1_6	=	0x0096
                           000095   541 _P1_5	=	0x0095
                           000094   542 _P1_4	=	0x0094
                           000093   543 _P1_3	=	0x0093
                           000092   544 _P1_2	=	0x0092
                           000091   545 _P1_1	=	0x0091
                           000090   546 _P1_0	=	0x0090
                           000097   547 _SCK	=	0x0097
                           000097   548 _TXD1_	=	0x0097
                           000097   549 _AIN7	=	0x0097
                           000096   550 _MISO	=	0x0096
                           000096   551 _RXD1_	=	0x0096
                           000096   552 _VBUS	=	0x0096
                           000096   553 _AIN6	=	0x0096
                           000095   554 _MOSI	=	0x0095
                           000095   555 _PWM0_	=	0x0095
                           000095   556 _UCC2	=	0x0095
                           000095   557 _AIN5	=	0x0095
                           000094   558 _SCS	=	0x0094
                           000094   559 _UCC1	=	0x0094
                           000094   560 _AIN4	=	0x0094
                           000093   561 _AIN3	=	0x0093
                           000092   562 _AIN2	=	0x0092
                           000091   563 _T2EX	=	0x0091
                           000091   564 _CAP2	=	0x0091
                           000091   565 _AIN1	=	0x0091
                           000090   566 _T2	=	0x0090
                           000090   567 _CAP1	=	0x0090
                           000090   568 _AIN0	=	0x0090
                           0000A7   569 _P2_7	=	0x00a7
                           0000A6   570 _P2_6	=	0x00a6
                           0000A5   571 _P2_5	=	0x00a5
                           0000A4   572 _P2_4	=	0x00a4
                           0000A3   573 _P2_3	=	0x00a3
                           0000A2   574 _P2_2	=	0x00a2
                           0000A1   575 _P2_1	=	0x00a1
                           0000A0   576 _P2_0	=	0x00a0
                           0000A7   577 _PWM7	=	0x00a7
                           0000A7   578 _TXD1	=	0x00a7
                           0000A6   579 _PWM6	=	0x00a6
                           0000A6   580 _RXD1	=	0x00a6
                           0000A5   581 _PWM0	=	0x00a5
                           0000A5   582 _T2EX_	=	0x00a5
                           0000A5   583 _CAP2_	=	0x00a5
                           0000A4   584 _PWM1	=	0x00a4
                           0000A4   585 _T2_	=	0x00a4
                           0000A4   586 _CAP1_	=	0x00a4
                           0000A3   587 _PWM2	=	0x00a3
                           0000A2   588 _PWM3	=	0x00a2
                           0000A2   589 _INT0_	=	0x00a2
                           0000A1   590 _PWM4	=	0x00a1
                           0000A0   591 _PWM5	=	0x00a0
                           0000B7   592 _P3_7	=	0x00b7
                           0000B6   593 _P3_6	=	0x00b6
                           0000B5   594 _P3_5	=	0x00b5
                           0000B4   595 _P3_4	=	0x00b4
                           0000B3   596 _P3_3	=	0x00b3
                           0000B2   597 _P3_2	=	0x00b2
                           0000B1   598 _P3_1	=	0x00b1
                           0000B0   599 _P3_0	=	0x00b0
                           0000B7   600 _INT3	=	0x00b7
                           0000B6   601 _CAP0	=	0x00b6
                           0000B5   602 _T1	=	0x00b5
                           0000B4   603 _T0	=	0x00b4
                           0000B3   604 _INT1	=	0x00b3
                           0000B2   605 _INT0	=	0x00b2
                           0000B1   606 _TXD	=	0x00b1
                           0000B0   607 _RXD	=	0x00b0
                           0000C6   608 _P4_6	=	0x00c6
                           0000C5   609 _P4_5	=	0x00c5
                           0000C4   610 _P4_4	=	0x00c4
                           0000C3   611 _P4_3	=	0x00c3
                           0000C2   612 _P4_2	=	0x00c2
                           0000C1   613 _P4_1	=	0x00c1
                           0000C0   614 _P4_0	=	0x00c0
                           0000C7   615 _XO	=	0x00c7
                           0000C6   616 _XI	=	0x00c6
                           00008F   617 _TF1	=	0x008f
                           00008E   618 _TR1	=	0x008e
                           00008D   619 _TF0	=	0x008d
                           00008C   620 _TR0	=	0x008c
                           00008B   621 _IE1	=	0x008b
                           00008A   622 _IT1	=	0x008a
                           000089   623 _IE0	=	0x0089
                           000088   624 _IT0	=	0x0088
                           00009F   625 _SM0	=	0x009f
                           00009E   626 _SM1	=	0x009e
                           00009D   627 _SM2	=	0x009d
                           00009C   628 _REN	=	0x009c
                           00009B   629 _TB8	=	0x009b
                           00009A   630 _RB8	=	0x009a
                           000099   631 _TI	=	0x0099
                           000098   632 _RI	=	0x0098
                           0000CF   633 _TF2	=	0x00cf
                           0000CF   634 _CAP1F	=	0x00cf
                           0000CE   635 _EXF2	=	0x00ce
                           0000CD   636 _RCLK	=	0x00cd
                           0000CC   637 _TCLK	=	0x00cc
                           0000CB   638 _EXEN2	=	0x00cb
                           0000CA   639 _TR2	=	0x00ca
                           0000C9   640 _C_T2	=	0x00c9
                           0000C8   641 _CP_RL2	=	0x00c8
                           0000FF   642 _S0_FST_ACT	=	0x00ff
                           0000FE   643 _S0_IF_OV	=	0x00fe
                           0000FD   644 _S0_IF_FIRST	=	0x00fd
                           0000FC   645 _S0_IF_BYTE	=	0x00fc
                           0000FB   646 _S0_FREE	=	0x00fb
                           0000FA   647 _S0_T_FIFO	=	0x00fa
                           0000F8   648 _S0_R_FIFO	=	0x00f8
                           0000DF   649 _U_IS_NAK	=	0x00df
                           0000DE   650 _U_TOG_OK	=	0x00de
                           0000DD   651 _U_SIE_FREE	=	0x00dd
                           0000DC   652 _UIF_FIFO_OV	=	0x00dc
                           0000DB   653 _UIF_HST_SOF	=	0x00db
                           0000DA   654 _UIF_SUSPEND	=	0x00da
                           0000D9   655 _UIF_TRANSFER	=	0x00d9
                           0000D8   656 _UIF_DETECT	=	0x00d8
                           0000D8   657 _UIF_BUS_RST	=	0x00d8
                                    658 ;--------------------------------------------------------
                                    659 ; overlayable register banks
                                    660 ;--------------------------------------------------------
                                    661 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        662 	.ds 8
                                    663 ;--------------------------------------------------------
                                    664 ; internal ram data
                                    665 ;--------------------------------------------------------
                                    666 	.area DSEG    (DATA)
                                    667 ;--------------------------------------------------------
                                    668 ; overlayable items in internal ram 
                                    669 ;--------------------------------------------------------
                                    670 ;--------------------------------------------------------
                                    671 ; Stack segment in internal ram 
                                    672 ;--------------------------------------------------------
                                    673 	.area	SSEG
      00001C                        674 __start__stack:
      00001C                        675 	.ds	1
                                    676 
                                    677 ;--------------------------------------------------------
                                    678 ; indirectly addressable internal ram data
                                    679 ;--------------------------------------------------------
                                    680 	.area ISEG    (DATA)
                                    681 ;--------------------------------------------------------
                                    682 ; absolute internal ram data
                                    683 ;--------------------------------------------------------
                                    684 	.area IABS    (ABS,DATA)
                                    685 	.area IABS    (ABS,DATA)
                                    686 ;--------------------------------------------------------
                                    687 ; bit data
                                    688 ;--------------------------------------------------------
                                    689 	.area BSEG    (BIT)
                                    690 ;--------------------------------------------------------
                                    691 ; paged external ram data
                                    692 ;--------------------------------------------------------
                                    693 	.area PSEG    (PAG,XDATA)
                                    694 ;--------------------------------------------------------
                                    695 ; external ram data
                                    696 ;--------------------------------------------------------
                                    697 	.area XSEG    (XDATA)
                                    698 ;--------------------------------------------------------
                                    699 ; absolute external ram data
                                    700 ;--------------------------------------------------------
                                    701 	.area XABS    (ABS,XDATA)
                                    702 ;--------------------------------------------------------
                                    703 ; external initialized ram data
                                    704 ;--------------------------------------------------------
                                    705 	.area XISEG   (XDATA)
                                    706 	.area HOME    (CODE)
                                    707 	.area GSINIT0 (CODE)
                                    708 	.area GSINIT1 (CODE)
                                    709 	.area GSINIT2 (CODE)
                                    710 	.area GSINIT3 (CODE)
                                    711 	.area GSINIT4 (CODE)
                                    712 	.area GSINIT5 (CODE)
                                    713 	.area GSINIT  (CODE)
                                    714 	.area GSFINAL (CODE)
                                    715 	.area CSEG    (CODE)
                                    716 ;--------------------------------------------------------
                                    717 ; interrupt vector 
                                    718 ;--------------------------------------------------------
                                    719 	.area HOME    (CODE)
      000000                        720 __interrupt_vect:
      000000 02 00 06         [24]  721 	ljmp	__sdcc_gsinit_startup
                                    722 ;--------------------------------------------------------
                                    723 ; global & static initialisations
                                    724 ;--------------------------------------------------------
                                    725 	.area HOME    (CODE)
                                    726 	.area GSINIT  (CODE)
                                    727 	.area GSFINAL (CODE)
                                    728 	.area GSINIT  (CODE)
                                    729 	.globl __sdcc_gsinit_startup
                                    730 	.globl __sdcc_program_startup
                                    731 	.globl __start__stack
                                    732 	.globl __mcs51_genXINIT
                                    733 	.globl __mcs51_genXRAMCLEAR
                                    734 	.globl __mcs51_genRAMCLEAR
                                    735 	.area GSFINAL (CODE)
      00005F 02 00 03         [24]  736 	ljmp	__sdcc_program_startup
                                    737 ;--------------------------------------------------------
                                    738 ; Home
                                    739 ;--------------------------------------------------------
                                    740 	.area HOME    (CODE)
                                    741 	.area HOME    (CODE)
      000003                        742 __sdcc_program_startup:
      000003 02 00 62         [24]  743 	ljmp	_main
                                    744 ;	return from main will return to caller
                                    745 ;--------------------------------------------------------
                                    746 ; code
                                    747 ;--------------------------------------------------------
                                    748 	.area CSEG    (CODE)
                                    749 ;------------------------------------------------------------
                                    750 ;Allocation info for local variables in function 'main'
                                    751 ;------------------------------------------------------------
                                    752 ;	usr/main.c:35: int main(void)
                                    753 ;	-----------------------------------------
                                    754 ;	 function main
                                    755 ;	-----------------------------------------
      000062                        756 _main:
                           000007   757 	ar7 = 0x07
                           000006   758 	ar6 = 0x06
                           000005   759 	ar5 = 0x05
                           000004   760 	ar4 = 0x04
                           000003   761 	ar3 = 0x03
                           000002   762 	ar2 = 0x02
                           000001   763 	ar1 = 0x01
                           000000   764 	ar0 = 0x00
                                    765 ;	usr/main.c:37: CfgFsys( );
      000062 12 04 69         [24]  766 	lcall	_CfgFsys
                                    767 ;	usr/main.c:38: mDelaymS(20);                                                              //调整主频，建议稍加延时等待内部时钟稳定
      000065 90 00 14         [24]  768 	mov	dptr,#0x0014
      000068 12 04 A5         [24]  769 	lcall	_mDelaymS
                                    770 ;	usr/main.c:39: SPIMasterModeSet(3);                                                       //SPI主机模式设置，模式3
      00006B 75 82 03         [24]  771 	mov	dpl,#0x03
      00006E 12 05 17         [24]  772 	lcall	_SPIMasterModeSet
                                    773 ;	usr/main.c:40: SPI_CK_SET(12);                                                            //设置spi sclk 时钟信号为12分频
      000071 75 FB 0C         [24]  774 	mov	_SPI0_CK_SE,#0x0c
                                    775 ;	usr/main.c:41: OLED_Init();			                                                         //初始化OLED  
      000074 12 01 1C         [24]  776 	lcall	_OLED_Init
                                    777 ;	usr/main.c:42: OLED_Clear();                                                              //将oled屏幕上内容清除
      000077 12 01 6A         [24]  778 	lcall	_OLED_Clear
                                    779 ;	usr/main.c:43: setFontSize(16);                                                           //设置文字大小
      00007A 75 82 10         [24]  780 	mov	dpl,#0x10
      00007D 12 00 A1         [24]  781 	lcall	_setFontSize
                                    782 ;	usr/main.c:44: OLED_ShowString(0,2,"Please follow   Verimake!");
      000080 75 0F 8D         [24]  783 	mov	_OLED_ShowString_PARM_3,#___str_0
      000083 75 10 0D         [24]  784 	mov	(_OLED_ShowString_PARM_3 + 1),#(___str_0 >> 8)
      000086 75 11 80         [24]  785 	mov	(_OLED_ShowString_PARM_3 + 2),#0x80
      000089 75 0E 02         [24]  786 	mov	_OLED_ShowString_PARM_2,#0x02
      00008C 75 82 00         [24]  787 	mov	dpl,#0x00
      00008F 12 02 ED         [24]  788 	lcall	_OLED_ShowString
                                    789 ;	usr/main.c:45: while(1)
      000092                        790 00104$:
                                    791 ;	usr/main.c:47: if(key1==0)
      000092 20 90 FD         [24]  792 	jb	_P1_0,00104$
                                    793 ;	usr/main.c:49: OLED_DC = 0;
                                    794 ;	assignBit
      000095 C2 A7            [12]  795 	clr	_P2_7
                                    796 ;	usr/main.c:50: OLED_CS = 0;
                                    797 ;	assignBit
      000097 C2 94            [12]  798 	clr	_P1_4
                                    799 ;	usr/main.c:51: CH549SPIMasterWrite(0xAE); 
      000099 75 82 AE         [24]  800 	mov	dpl,#0xae
      00009C 12 05 38         [24]  801 	lcall	_CH549SPIMasterWrite
                                    802 ;	usr/main.c:57: }
      00009F 80 F1            [24]  803 	sjmp	00104$
                                    804 	.area CSEG    (CODE)
                                    805 	.area CONST   (CODE)
      00058D                        806 _BMP1:
      00058D 00                     807 	.db #0x00	; 0
      00058E 03                     808 	.db #0x03	; 3
      00058F 05                     809 	.db #0x05	; 5
      000590 09                     810 	.db #0x09	; 9
      000591 11                     811 	.db #0x11	; 17
      000592 FF                     812 	.db #0xff	; 255
      000593 11                     813 	.db #0x11	; 17
      000594 89                     814 	.db #0x89	; 137
      000595 05                     815 	.db #0x05	; 5
      000596 C3                     816 	.db #0xc3	; 195
      000597 00                     817 	.db #0x00	; 0
      000598 E0                     818 	.db #0xe0	; 224
      000599 00                     819 	.db #0x00	; 0
      00059A F0                     820 	.db #0xf0	; 240
      00059B 00                     821 	.db #0x00	; 0
      00059C F8                     822 	.db #0xf8	; 248
      00059D 00                     823 	.db #0x00	; 0
      00059E 00                     824 	.db #0x00	; 0
      00059F 00                     825 	.db #0x00	; 0
      0005A0 00                     826 	.db #0x00	; 0
      0005A1 00                     827 	.db #0x00	; 0
      0005A2 00                     828 	.db #0x00	; 0
      0005A3 00                     829 	.db #0x00	; 0
      0005A4 44                     830 	.db #0x44	; 68	'D'
      0005A5 28                     831 	.db #0x28	; 40
      0005A6 FF                     832 	.db #0xff	; 255
      0005A7 11                     833 	.db #0x11	; 17
      0005A8 AA                     834 	.db #0xaa	; 170
      0005A9 44                     835 	.db #0x44	; 68	'D'
      0005AA 00                     836 	.db #0x00	; 0
      0005AB 00                     837 	.db #0x00	; 0
      0005AC 00                     838 	.db #0x00	; 0
      0005AD 00                     839 	.db #0x00	; 0
      0005AE 00                     840 	.db #0x00	; 0
      0005AF 00                     841 	.db #0x00	; 0
      0005B0 00                     842 	.db #0x00	; 0
      0005B1 00                     843 	.db #0x00	; 0
      0005B2 00                     844 	.db #0x00	; 0
      0005B3 00                     845 	.db #0x00	; 0
      0005B4 00                     846 	.db #0x00	; 0
      0005B5 00                     847 	.db #0x00	; 0
      0005B6 00                     848 	.db #0x00	; 0
      0005B7 00                     849 	.db #0x00	; 0
      0005B8 00                     850 	.db #0x00	; 0
      0005B9 00                     851 	.db #0x00	; 0
      0005BA 00                     852 	.db #0x00	; 0
      0005BB 00                     853 	.db #0x00	; 0
      0005BC 00                     854 	.db #0x00	; 0
      0005BD 00                     855 	.db #0x00	; 0
      0005BE 00                     856 	.db #0x00	; 0
      0005BF 00                     857 	.db #0x00	; 0
      0005C0 00                     858 	.db #0x00	; 0
      0005C1 00                     859 	.db #0x00	; 0
      0005C2 00                     860 	.db #0x00	; 0
      0005C3 00                     861 	.db #0x00	; 0
      0005C4 00                     862 	.db #0x00	; 0
      0005C5 00                     863 	.db #0x00	; 0
      0005C6 00                     864 	.db #0x00	; 0
      0005C7 00                     865 	.db #0x00	; 0
      0005C8 00                     866 	.db #0x00	; 0
      0005C9 00                     867 	.db #0x00	; 0
      0005CA 00                     868 	.db #0x00	; 0
      0005CB 00                     869 	.db #0x00	; 0
      0005CC 00                     870 	.db #0x00	; 0
      0005CD 00                     871 	.db #0x00	; 0
      0005CE 00                     872 	.db #0x00	; 0
      0005CF 00                     873 	.db #0x00	; 0
      0005D0 00                     874 	.db #0x00	; 0
      0005D1 00                     875 	.db #0x00	; 0
      0005D2 00                     876 	.db #0x00	; 0
      0005D3 00                     877 	.db #0x00	; 0
      0005D4 00                     878 	.db #0x00	; 0
      0005D5 00                     879 	.db #0x00	; 0
      0005D6 00                     880 	.db #0x00	; 0
      0005D7 00                     881 	.db #0x00	; 0
      0005D8 00                     882 	.db #0x00	; 0
      0005D9 00                     883 	.db #0x00	; 0
      0005DA 00                     884 	.db #0x00	; 0
      0005DB 00                     885 	.db #0x00	; 0
      0005DC 00                     886 	.db #0x00	; 0
      0005DD 00                     887 	.db #0x00	; 0
      0005DE 00                     888 	.db #0x00	; 0
      0005DF 00                     889 	.db #0x00	; 0
      0005E0 00                     890 	.db #0x00	; 0
      0005E1 00                     891 	.db #0x00	; 0
      0005E2 00                     892 	.db #0x00	; 0
      0005E3 00                     893 	.db #0x00	; 0
      0005E4 00                     894 	.db #0x00	; 0
      0005E5 00                     895 	.db #0x00	; 0
      0005E6 00                     896 	.db #0x00	; 0
      0005E7 83                     897 	.db #0x83	; 131
      0005E8 01                     898 	.db #0x01	; 1
      0005E9 38                     899 	.db #0x38	; 56	'8'
      0005EA 44                     900 	.db #0x44	; 68	'D'
      0005EB 82                     901 	.db #0x82	; 130
      0005EC 92                     902 	.db #0x92	; 146
      0005ED 92                     903 	.db #0x92	; 146
      0005EE 74                     904 	.db #0x74	; 116	't'
      0005EF 01                     905 	.db #0x01	; 1
      0005F0 83                     906 	.db #0x83	; 131
      0005F1 00                     907 	.db #0x00	; 0
      0005F2 00                     908 	.db #0x00	; 0
      0005F3 00                     909 	.db #0x00	; 0
      0005F4 00                     910 	.db #0x00	; 0
      0005F5 00                     911 	.db #0x00	; 0
      0005F6 00                     912 	.db #0x00	; 0
      0005F7 00                     913 	.db #0x00	; 0
      0005F8 7C                     914 	.db #0x7c	; 124
      0005F9 44                     915 	.db #0x44	; 68	'D'
      0005FA FF                     916 	.db #0xff	; 255
      0005FB 01                     917 	.db #0x01	; 1
      0005FC 7D                     918 	.db #0x7d	; 125
      0005FD 7D                     919 	.db #0x7d	; 125
      0005FE 7D                     920 	.db #0x7d	; 125
      0005FF 01                     921 	.db #0x01	; 1
      000600 7D                     922 	.db #0x7d	; 125
      000601 7D                     923 	.db #0x7d	; 125
      000602 7D                     924 	.db #0x7d	; 125
      000603 7D                     925 	.db #0x7d	; 125
      000604 01                     926 	.db #0x01	; 1
      000605 7D                     927 	.db #0x7d	; 125
      000606 7D                     928 	.db #0x7d	; 125
      000607 7D                     929 	.db #0x7d	; 125
      000608 7D                     930 	.db #0x7d	; 125
      000609 7D                     931 	.db #0x7d	; 125
      00060A 01                     932 	.db #0x01	; 1
      00060B FF                     933 	.db #0xff	; 255
      00060C 00                     934 	.db #0x00	; 0
      00060D 00                     935 	.db #0x00	; 0
      00060E 00                     936 	.db #0x00	; 0
      00060F 00                     937 	.db #0x00	; 0
      000610 00                     938 	.db #0x00	; 0
      000611 00                     939 	.db #0x00	; 0
      000612 01                     940 	.db #0x01	; 1
      000613 00                     941 	.db #0x00	; 0
      000614 01                     942 	.db #0x01	; 1
      000615 00                     943 	.db #0x00	; 0
      000616 01                     944 	.db #0x01	; 1
      000617 00                     945 	.db #0x00	; 0
      000618 01                     946 	.db #0x01	; 1
      000619 00                     947 	.db #0x00	; 0
      00061A 01                     948 	.db #0x01	; 1
      00061B 00                     949 	.db #0x00	; 0
      00061C 01                     950 	.db #0x01	; 1
      00061D 00                     951 	.db #0x00	; 0
      00061E 00                     952 	.db #0x00	; 0
      00061F 00                     953 	.db #0x00	; 0
      000620 00                     954 	.db #0x00	; 0
      000621 00                     955 	.db #0x00	; 0
      000622 00                     956 	.db #0x00	; 0
      000623 00                     957 	.db #0x00	; 0
      000624 00                     958 	.db #0x00	; 0
      000625 00                     959 	.db #0x00	; 0
      000626 01                     960 	.db #0x01	; 1
      000627 01                     961 	.db #0x01	; 1
      000628 00                     962 	.db #0x00	; 0
      000629 00                     963 	.db #0x00	; 0
      00062A 00                     964 	.db #0x00	; 0
      00062B 00                     965 	.db #0x00	; 0
      00062C 00                     966 	.db #0x00	; 0
      00062D 00                     967 	.db #0x00	; 0
      00062E 00                     968 	.db #0x00	; 0
      00062F 00                     969 	.db #0x00	; 0
      000630 00                     970 	.db #0x00	; 0
      000631 00                     971 	.db #0x00	; 0
      000632 00                     972 	.db #0x00	; 0
      000633 00                     973 	.db #0x00	; 0
      000634 00                     974 	.db #0x00	; 0
      000635 00                     975 	.db #0x00	; 0
      000636 00                     976 	.db #0x00	; 0
      000637 00                     977 	.db #0x00	; 0
      000638 00                     978 	.db #0x00	; 0
      000639 00                     979 	.db #0x00	; 0
      00063A 00                     980 	.db #0x00	; 0
      00063B 00                     981 	.db #0x00	; 0
      00063C 00                     982 	.db #0x00	; 0
      00063D 00                     983 	.db #0x00	; 0
      00063E 00                     984 	.db #0x00	; 0
      00063F 00                     985 	.db #0x00	; 0
      000640 00                     986 	.db #0x00	; 0
      000641 00                     987 	.db #0x00	; 0
      000642 00                     988 	.db #0x00	; 0
      000643 00                     989 	.db #0x00	; 0
      000644 00                     990 	.db #0x00	; 0
      000645 00                     991 	.db #0x00	; 0
      000646 00                     992 	.db #0x00	; 0
      000647 00                     993 	.db #0x00	; 0
      000648 00                     994 	.db #0x00	; 0
      000649 00                     995 	.db #0x00	; 0
      00064A 00                     996 	.db #0x00	; 0
      00064B 00                     997 	.db #0x00	; 0
      00064C 00                     998 	.db #0x00	; 0
      00064D 00                     999 	.db #0x00	; 0
      00064E 00                    1000 	.db #0x00	; 0
      00064F 00                    1001 	.db #0x00	; 0
      000650 00                    1002 	.db #0x00	; 0
      000651 00                    1003 	.db #0x00	; 0
      000652 00                    1004 	.db #0x00	; 0
      000653 00                    1005 	.db #0x00	; 0
      000654 00                    1006 	.db #0x00	; 0
      000655 00                    1007 	.db #0x00	; 0
      000656 00                    1008 	.db #0x00	; 0
      000657 00                    1009 	.db #0x00	; 0
      000658 00                    1010 	.db #0x00	; 0
      000659 00                    1011 	.db #0x00	; 0
      00065A 00                    1012 	.db #0x00	; 0
      00065B 00                    1013 	.db #0x00	; 0
      00065C 00                    1014 	.db #0x00	; 0
      00065D 00                    1015 	.db #0x00	; 0
      00065E 00                    1016 	.db #0x00	; 0
      00065F 00                    1017 	.db #0x00	; 0
      000660 00                    1018 	.db #0x00	; 0
      000661 00                    1019 	.db #0x00	; 0
      000662 00                    1020 	.db #0x00	; 0
      000663 00                    1021 	.db #0x00	; 0
      000664 00                    1022 	.db #0x00	; 0
      000665 00                    1023 	.db #0x00	; 0
      000666 00                    1024 	.db #0x00	; 0
      000667 01                    1025 	.db #0x01	; 1
      000668 01                    1026 	.db #0x01	; 1
      000669 00                    1027 	.db #0x00	; 0
      00066A 00                    1028 	.db #0x00	; 0
      00066B 00                    1029 	.db #0x00	; 0
      00066C 00                    1030 	.db #0x00	; 0
      00066D 00                    1031 	.db #0x00	; 0
      00066E 00                    1032 	.db #0x00	; 0
      00066F 01                    1033 	.db #0x01	; 1
      000670 01                    1034 	.db #0x01	; 1
      000671 00                    1035 	.db #0x00	; 0
      000672 00                    1036 	.db #0x00	; 0
      000673 00                    1037 	.db #0x00	; 0
      000674 00                    1038 	.db #0x00	; 0
      000675 00                    1039 	.db #0x00	; 0
      000676 00                    1040 	.db #0x00	; 0
      000677 00                    1041 	.db #0x00	; 0
      000678 00                    1042 	.db #0x00	; 0
      000679 00                    1043 	.db #0x00	; 0
      00067A 01                    1044 	.db #0x01	; 1
      00067B 01                    1045 	.db #0x01	; 1
      00067C 01                    1046 	.db #0x01	; 1
      00067D 01                    1047 	.db #0x01	; 1
      00067E 01                    1048 	.db #0x01	; 1
      00067F 01                    1049 	.db #0x01	; 1
      000680 01                    1050 	.db #0x01	; 1
      000681 01                    1051 	.db #0x01	; 1
      000682 01                    1052 	.db #0x01	; 1
      000683 01                    1053 	.db #0x01	; 1
      000684 01                    1054 	.db #0x01	; 1
      000685 01                    1055 	.db #0x01	; 1
      000686 01                    1056 	.db #0x01	; 1
      000687 01                    1057 	.db #0x01	; 1
      000688 01                    1058 	.db #0x01	; 1
      000689 01                    1059 	.db #0x01	; 1
      00068A 01                    1060 	.db #0x01	; 1
      00068B 01                    1061 	.db #0x01	; 1
      00068C 00                    1062 	.db #0x00	; 0
      00068D 00                    1063 	.db #0x00	; 0
      00068E 00                    1064 	.db #0x00	; 0
      00068F 00                    1065 	.db #0x00	; 0
      000690 00                    1066 	.db #0x00	; 0
      000691 00                    1067 	.db #0x00	; 0
      000692 00                    1068 	.db #0x00	; 0
      000693 00                    1069 	.db #0x00	; 0
      000694 00                    1070 	.db #0x00	; 0
      000695 00                    1071 	.db #0x00	; 0
      000696 00                    1072 	.db #0x00	; 0
      000697 00                    1073 	.db #0x00	; 0
      000698 00                    1074 	.db #0x00	; 0
      000699 00                    1075 	.db #0x00	; 0
      00069A 00                    1076 	.db #0x00	; 0
      00069B 00                    1077 	.db #0x00	; 0
      00069C 00                    1078 	.db #0x00	; 0
      00069D 00                    1079 	.db #0x00	; 0
      00069E 00                    1080 	.db #0x00	; 0
      00069F 00                    1081 	.db #0x00	; 0
      0006A0 00                    1082 	.db #0x00	; 0
      0006A1 00                    1083 	.db #0x00	; 0
      0006A2 00                    1084 	.db #0x00	; 0
      0006A3 00                    1085 	.db #0x00	; 0
      0006A4 00                    1086 	.db #0x00	; 0
      0006A5 00                    1087 	.db #0x00	; 0
      0006A6 00                    1088 	.db #0x00	; 0
      0006A7 00                    1089 	.db #0x00	; 0
      0006A8 00                    1090 	.db #0x00	; 0
      0006A9 3F                    1091 	.db #0x3f	; 63
      0006AA 3F                    1092 	.db #0x3f	; 63
      0006AB 03                    1093 	.db #0x03	; 3
      0006AC 03                    1094 	.db #0x03	; 3
      0006AD F3                    1095 	.db #0xf3	; 243
      0006AE 13                    1096 	.db #0x13	; 19
      0006AF 11                    1097 	.db #0x11	; 17
      0006B0 11                    1098 	.db #0x11	; 17
      0006B1 11                    1099 	.db #0x11	; 17
      0006B2 11                    1100 	.db #0x11	; 17
      0006B3 11                    1101 	.db #0x11	; 17
      0006B4 11                    1102 	.db #0x11	; 17
      0006B5 01                    1103 	.db #0x01	; 1
      0006B6 F1                    1104 	.db #0xf1	; 241
      0006B7 11                    1105 	.db #0x11	; 17
      0006B8 61                    1106 	.db #0x61	; 97	'a'
      0006B9 81                    1107 	.db #0x81	; 129
      0006BA 01                    1108 	.db #0x01	; 1
      0006BB 01                    1109 	.db #0x01	; 1
      0006BC 01                    1110 	.db #0x01	; 1
      0006BD 81                    1111 	.db #0x81	; 129
      0006BE 61                    1112 	.db #0x61	; 97	'a'
      0006BF 11                    1113 	.db #0x11	; 17
      0006C0 F1                    1114 	.db #0xf1	; 241
      0006C1 01                    1115 	.db #0x01	; 1
      0006C2 01                    1116 	.db #0x01	; 1
      0006C3 01                    1117 	.db #0x01	; 1
      0006C4 01                    1118 	.db #0x01	; 1
      0006C5 41                    1119 	.db #0x41	; 65	'A'
      0006C6 41                    1120 	.db #0x41	; 65	'A'
      0006C7 F1                    1121 	.db #0xf1	; 241
      0006C8 01                    1122 	.db #0x01	; 1
      0006C9 01                    1123 	.db #0x01	; 1
      0006CA 01                    1124 	.db #0x01	; 1
      0006CB 01                    1125 	.db #0x01	; 1
      0006CC 01                    1126 	.db #0x01	; 1
      0006CD C1                    1127 	.db #0xc1	; 193
      0006CE 21                    1128 	.db #0x21	; 33
      0006CF 11                    1129 	.db #0x11	; 17
      0006D0 11                    1130 	.db #0x11	; 17
      0006D1 11                    1131 	.db #0x11	; 17
      0006D2 11                    1132 	.db #0x11	; 17
      0006D3 21                    1133 	.db #0x21	; 33
      0006D4 C1                    1134 	.db #0xc1	; 193
      0006D5 01                    1135 	.db #0x01	; 1
      0006D6 01                    1136 	.db #0x01	; 1
      0006D7 01                    1137 	.db #0x01	; 1
      0006D8 01                    1138 	.db #0x01	; 1
      0006D9 41                    1139 	.db #0x41	; 65	'A'
      0006DA 41                    1140 	.db #0x41	; 65	'A'
      0006DB F1                    1141 	.db #0xf1	; 241
      0006DC 01                    1142 	.db #0x01	; 1
      0006DD 01                    1143 	.db #0x01	; 1
      0006DE 01                    1144 	.db #0x01	; 1
      0006DF 01                    1145 	.db #0x01	; 1
      0006E0 01                    1146 	.db #0x01	; 1
      0006E1 01                    1147 	.db #0x01	; 1
      0006E2 01                    1148 	.db #0x01	; 1
      0006E3 01                    1149 	.db #0x01	; 1
      0006E4 01                    1150 	.db #0x01	; 1
      0006E5 01                    1151 	.db #0x01	; 1
      0006E6 11                    1152 	.db #0x11	; 17
      0006E7 11                    1153 	.db #0x11	; 17
      0006E8 11                    1154 	.db #0x11	; 17
      0006E9 11                    1155 	.db #0x11	; 17
      0006EA 11                    1156 	.db #0x11	; 17
      0006EB D3                    1157 	.db #0xd3	; 211
      0006EC 33                    1158 	.db #0x33	; 51	'3'
      0006ED 03                    1159 	.db #0x03	; 3
      0006EE 03                    1160 	.db #0x03	; 3
      0006EF 3F                    1161 	.db #0x3f	; 63
      0006F0 3F                    1162 	.db #0x3f	; 63
      0006F1 00                    1163 	.db #0x00	; 0
      0006F2 00                    1164 	.db #0x00	; 0
      0006F3 00                    1165 	.db #0x00	; 0
      0006F4 00                    1166 	.db #0x00	; 0
      0006F5 00                    1167 	.db #0x00	; 0
      0006F6 00                    1168 	.db #0x00	; 0
      0006F7 00                    1169 	.db #0x00	; 0
      0006F8 00                    1170 	.db #0x00	; 0
      0006F9 00                    1171 	.db #0x00	; 0
      0006FA 00                    1172 	.db #0x00	; 0
      0006FB 00                    1173 	.db #0x00	; 0
      0006FC 00                    1174 	.db #0x00	; 0
      0006FD 00                    1175 	.db #0x00	; 0
      0006FE 00                    1176 	.db #0x00	; 0
      0006FF 00                    1177 	.db #0x00	; 0
      000700 00                    1178 	.db #0x00	; 0
      000701 00                    1179 	.db #0x00	; 0
      000702 00                    1180 	.db #0x00	; 0
      000703 00                    1181 	.db #0x00	; 0
      000704 00                    1182 	.db #0x00	; 0
      000705 00                    1183 	.db #0x00	; 0
      000706 00                    1184 	.db #0x00	; 0
      000707 00                    1185 	.db #0x00	; 0
      000708 00                    1186 	.db #0x00	; 0
      000709 00                    1187 	.db #0x00	; 0
      00070A 00                    1188 	.db #0x00	; 0
      00070B 00                    1189 	.db #0x00	; 0
      00070C 00                    1190 	.db #0x00	; 0
      00070D 00                    1191 	.db #0x00	; 0
      00070E 00                    1192 	.db #0x00	; 0
      00070F 00                    1193 	.db #0x00	; 0
      000710 00                    1194 	.db #0x00	; 0
      000711 00                    1195 	.db #0x00	; 0
      000712 00                    1196 	.db #0x00	; 0
      000713 00                    1197 	.db #0x00	; 0
      000714 00                    1198 	.db #0x00	; 0
      000715 00                    1199 	.db #0x00	; 0
      000716 00                    1200 	.db #0x00	; 0
      000717 00                    1201 	.db #0x00	; 0
      000718 00                    1202 	.db #0x00	; 0
      000719 00                    1203 	.db #0x00	; 0
      00071A 00                    1204 	.db #0x00	; 0
      00071B 00                    1205 	.db #0x00	; 0
      00071C 00                    1206 	.db #0x00	; 0
      00071D 00                    1207 	.db #0x00	; 0
      00071E 00                    1208 	.db #0x00	; 0
      00071F 00                    1209 	.db #0x00	; 0
      000720 00                    1210 	.db #0x00	; 0
      000721 00                    1211 	.db #0x00	; 0
      000722 00                    1212 	.db #0x00	; 0
      000723 00                    1213 	.db #0x00	; 0
      000724 00                    1214 	.db #0x00	; 0
      000725 00                    1215 	.db #0x00	; 0
      000726 00                    1216 	.db #0x00	; 0
      000727 00                    1217 	.db #0x00	; 0
      000728 00                    1218 	.db #0x00	; 0
      000729 E0                    1219 	.db #0xe0	; 224
      00072A E0                    1220 	.db #0xe0	; 224
      00072B 00                    1221 	.db #0x00	; 0
      00072C 00                    1222 	.db #0x00	; 0
      00072D 7F                    1223 	.db #0x7f	; 127
      00072E 01                    1224 	.db #0x01	; 1
      00072F 01                    1225 	.db #0x01	; 1
      000730 01                    1226 	.db #0x01	; 1
      000731 01                    1227 	.db #0x01	; 1
      000732 01                    1228 	.db #0x01	; 1
      000733 01                    1229 	.db #0x01	; 1
      000734 00                    1230 	.db #0x00	; 0
      000735 00                    1231 	.db #0x00	; 0
      000736 7F                    1232 	.db #0x7f	; 127
      000737 00                    1233 	.db #0x00	; 0
      000738 00                    1234 	.db #0x00	; 0
      000739 01                    1235 	.db #0x01	; 1
      00073A 06                    1236 	.db #0x06	; 6
      00073B 18                    1237 	.db #0x18	; 24
      00073C 06                    1238 	.db #0x06	; 6
      00073D 01                    1239 	.db #0x01	; 1
      00073E 00                    1240 	.db #0x00	; 0
      00073F 00                    1241 	.db #0x00	; 0
      000740 7F                    1242 	.db #0x7f	; 127
      000741 00                    1243 	.db #0x00	; 0
      000742 00                    1244 	.db #0x00	; 0
      000743 00                    1245 	.db #0x00	; 0
      000744 00                    1246 	.db #0x00	; 0
      000745 40                    1247 	.db #0x40	; 64
      000746 40                    1248 	.db #0x40	; 64
      000747 7F                    1249 	.db #0x7f	; 127
      000748 40                    1250 	.db #0x40	; 64
      000749 40                    1251 	.db #0x40	; 64
      00074A 00                    1252 	.db #0x00	; 0
      00074B 00                    1253 	.db #0x00	; 0
      00074C 00                    1254 	.db #0x00	; 0
      00074D 1F                    1255 	.db #0x1f	; 31
      00074E 20                    1256 	.db #0x20	; 32
      00074F 40                    1257 	.db #0x40	; 64
      000750 40                    1258 	.db #0x40	; 64
      000751 40                    1259 	.db #0x40	; 64
      000752 40                    1260 	.db #0x40	; 64
      000753 20                    1261 	.db #0x20	; 32
      000754 1F                    1262 	.db #0x1f	; 31
      000755 00                    1263 	.db #0x00	; 0
      000756 00                    1264 	.db #0x00	; 0
      000757 00                    1265 	.db #0x00	; 0
      000758 00                    1266 	.db #0x00	; 0
      000759 40                    1267 	.db #0x40	; 64
      00075A 40                    1268 	.db #0x40	; 64
      00075B 7F                    1269 	.db #0x7f	; 127
      00075C 40                    1270 	.db #0x40	; 64
      00075D 40                    1271 	.db #0x40	; 64
      00075E 00                    1272 	.db #0x00	; 0
      00075F 00                    1273 	.db #0x00	; 0
      000760 00                    1274 	.db #0x00	; 0
      000761 00                    1275 	.db #0x00	; 0
      000762 60                    1276 	.db #0x60	; 96
      000763 00                    1277 	.db #0x00	; 0
      000764 00                    1278 	.db #0x00	; 0
      000765 00                    1279 	.db #0x00	; 0
      000766 00                    1280 	.db #0x00	; 0
      000767 40                    1281 	.db #0x40	; 64
      000768 30                    1282 	.db #0x30	; 48	'0'
      000769 0C                    1283 	.db #0x0c	; 12
      00076A 03                    1284 	.db #0x03	; 3
      00076B 00                    1285 	.db #0x00	; 0
      00076C 00                    1286 	.db #0x00	; 0
      00076D 00                    1287 	.db #0x00	; 0
      00076E 00                    1288 	.db #0x00	; 0
      00076F E0                    1289 	.db #0xe0	; 224
      000770 E0                    1290 	.db #0xe0	; 224
      000771 00                    1291 	.db #0x00	; 0
      000772 00                    1292 	.db #0x00	; 0
      000773 00                    1293 	.db #0x00	; 0
      000774 00                    1294 	.db #0x00	; 0
      000775 00                    1295 	.db #0x00	; 0
      000776 00                    1296 	.db #0x00	; 0
      000777 00                    1297 	.db #0x00	; 0
      000778 00                    1298 	.db #0x00	; 0
      000779 00                    1299 	.db #0x00	; 0
      00077A 00                    1300 	.db #0x00	; 0
      00077B 00                    1301 	.db #0x00	; 0
      00077C 00                    1302 	.db #0x00	; 0
      00077D 00                    1303 	.db #0x00	; 0
      00077E 00                    1304 	.db #0x00	; 0
      00077F 00                    1305 	.db #0x00	; 0
      000780 00                    1306 	.db #0x00	; 0
      000781 00                    1307 	.db #0x00	; 0
      000782 00                    1308 	.db #0x00	; 0
      000783 00                    1309 	.db #0x00	; 0
      000784 00                    1310 	.db #0x00	; 0
      000785 00                    1311 	.db #0x00	; 0
      000786 00                    1312 	.db #0x00	; 0
      000787 00                    1313 	.db #0x00	; 0
      000788 00                    1314 	.db #0x00	; 0
      000789 00                    1315 	.db #0x00	; 0
      00078A 00                    1316 	.db #0x00	; 0
      00078B 00                    1317 	.db #0x00	; 0
      00078C 00                    1318 	.db #0x00	; 0
      00078D 00                    1319 	.db #0x00	; 0
      00078E 00                    1320 	.db #0x00	; 0
      00078F 00                    1321 	.db #0x00	; 0
      000790 00                    1322 	.db #0x00	; 0
      000791 00                    1323 	.db #0x00	; 0
      000792 00                    1324 	.db #0x00	; 0
      000793 00                    1325 	.db #0x00	; 0
      000794 00                    1326 	.db #0x00	; 0
      000795 00                    1327 	.db #0x00	; 0
      000796 00                    1328 	.db #0x00	; 0
      000797 00                    1329 	.db #0x00	; 0
      000798 00                    1330 	.db #0x00	; 0
      000799 00                    1331 	.db #0x00	; 0
      00079A 00                    1332 	.db #0x00	; 0
      00079B 00                    1333 	.db #0x00	; 0
      00079C 00                    1334 	.db #0x00	; 0
      00079D 00                    1335 	.db #0x00	; 0
      00079E 00                    1336 	.db #0x00	; 0
      00079F 00                    1337 	.db #0x00	; 0
      0007A0 00                    1338 	.db #0x00	; 0
      0007A1 00                    1339 	.db #0x00	; 0
      0007A2 00                    1340 	.db #0x00	; 0
      0007A3 00                    1341 	.db #0x00	; 0
      0007A4 00                    1342 	.db #0x00	; 0
      0007A5 00                    1343 	.db #0x00	; 0
      0007A6 00                    1344 	.db #0x00	; 0
      0007A7 00                    1345 	.db #0x00	; 0
      0007A8 00                    1346 	.db #0x00	; 0
      0007A9 07                    1347 	.db #0x07	; 7
      0007AA 07                    1348 	.db #0x07	; 7
      0007AB 06                    1349 	.db #0x06	; 6
      0007AC 06                    1350 	.db #0x06	; 6
      0007AD 06                    1351 	.db #0x06	; 6
      0007AE 06                    1352 	.db #0x06	; 6
      0007AF 04                    1353 	.db #0x04	; 4
      0007B0 04                    1354 	.db #0x04	; 4
      0007B1 04                    1355 	.db #0x04	; 4
      0007B2 84                    1356 	.db #0x84	; 132
      0007B3 44                    1357 	.db #0x44	; 68	'D'
      0007B4 44                    1358 	.db #0x44	; 68	'D'
      0007B5 44                    1359 	.db #0x44	; 68	'D'
      0007B6 84                    1360 	.db #0x84	; 132
      0007B7 04                    1361 	.db #0x04	; 4
      0007B8 04                    1362 	.db #0x04	; 4
      0007B9 84                    1363 	.db #0x84	; 132
      0007BA 44                    1364 	.db #0x44	; 68	'D'
      0007BB 44                    1365 	.db #0x44	; 68	'D'
      0007BC 44                    1366 	.db #0x44	; 68	'D'
      0007BD 84                    1367 	.db #0x84	; 132
      0007BE 04                    1368 	.db #0x04	; 4
      0007BF 04                    1369 	.db #0x04	; 4
      0007C0 04                    1370 	.db #0x04	; 4
      0007C1 84                    1371 	.db #0x84	; 132
      0007C2 C4                    1372 	.db #0xc4	; 196
      0007C3 04                    1373 	.db #0x04	; 4
      0007C4 04                    1374 	.db #0x04	; 4
      0007C5 04                    1375 	.db #0x04	; 4
      0007C6 04                    1376 	.db #0x04	; 4
      0007C7 84                    1377 	.db #0x84	; 132
      0007C8 44                    1378 	.db #0x44	; 68	'D'
      0007C9 44                    1379 	.db #0x44	; 68	'D'
      0007CA 44                    1380 	.db #0x44	; 68	'D'
      0007CB 84                    1381 	.db #0x84	; 132
      0007CC 04                    1382 	.db #0x04	; 4
      0007CD 04                    1383 	.db #0x04	; 4
      0007CE 04                    1384 	.db #0x04	; 4
      0007CF 04                    1385 	.db #0x04	; 4
      0007D0 04                    1386 	.db #0x04	; 4
      0007D1 84                    1387 	.db #0x84	; 132
      0007D2 44                    1388 	.db #0x44	; 68	'D'
      0007D3 44                    1389 	.db #0x44	; 68	'D'
      0007D4 44                    1390 	.db #0x44	; 68	'D'
      0007D5 84                    1391 	.db #0x84	; 132
      0007D6 04                    1392 	.db #0x04	; 4
      0007D7 04                    1393 	.db #0x04	; 4
      0007D8 04                    1394 	.db #0x04	; 4
      0007D9 04                    1395 	.db #0x04	; 4
      0007DA 04                    1396 	.db #0x04	; 4
      0007DB 84                    1397 	.db #0x84	; 132
      0007DC 44                    1398 	.db #0x44	; 68	'D'
      0007DD 44                    1399 	.db #0x44	; 68	'D'
      0007DE 44                    1400 	.db #0x44	; 68	'D'
      0007DF 84                    1401 	.db #0x84	; 132
      0007E0 04                    1402 	.db #0x04	; 4
      0007E1 04                    1403 	.db #0x04	; 4
      0007E2 84                    1404 	.db #0x84	; 132
      0007E3 44                    1405 	.db #0x44	; 68	'D'
      0007E4 44                    1406 	.db #0x44	; 68	'D'
      0007E5 44                    1407 	.db #0x44	; 68	'D'
      0007E6 84                    1408 	.db #0x84	; 132
      0007E7 04                    1409 	.db #0x04	; 4
      0007E8 04                    1410 	.db #0x04	; 4
      0007E9 04                    1411 	.db #0x04	; 4
      0007EA 04                    1412 	.db #0x04	; 4
      0007EB 06                    1413 	.db #0x06	; 6
      0007EC 06                    1414 	.db #0x06	; 6
      0007ED 06                    1415 	.db #0x06	; 6
      0007EE 06                    1416 	.db #0x06	; 6
      0007EF 07                    1417 	.db #0x07	; 7
      0007F0 07                    1418 	.db #0x07	; 7
      0007F1 00                    1419 	.db #0x00	; 0
      0007F2 00                    1420 	.db #0x00	; 0
      0007F3 00                    1421 	.db #0x00	; 0
      0007F4 00                    1422 	.db #0x00	; 0
      0007F5 00                    1423 	.db #0x00	; 0
      0007F6 00                    1424 	.db #0x00	; 0
      0007F7 00                    1425 	.db #0x00	; 0
      0007F8 00                    1426 	.db #0x00	; 0
      0007F9 00                    1427 	.db #0x00	; 0
      0007FA 00                    1428 	.db #0x00	; 0
      0007FB 00                    1429 	.db #0x00	; 0
      0007FC 00                    1430 	.db #0x00	; 0
      0007FD 00                    1431 	.db #0x00	; 0
      0007FE 00                    1432 	.db #0x00	; 0
      0007FF 00                    1433 	.db #0x00	; 0
      000800 00                    1434 	.db #0x00	; 0
      000801 00                    1435 	.db #0x00	; 0
      000802 00                    1436 	.db #0x00	; 0
      000803 00                    1437 	.db #0x00	; 0
      000804 00                    1438 	.db #0x00	; 0
      000805 00                    1439 	.db #0x00	; 0
      000806 00                    1440 	.db #0x00	; 0
      000807 00                    1441 	.db #0x00	; 0
      000808 00                    1442 	.db #0x00	; 0
      000809 00                    1443 	.db #0x00	; 0
      00080A 00                    1444 	.db #0x00	; 0
      00080B 00                    1445 	.db #0x00	; 0
      00080C 00                    1446 	.db #0x00	; 0
      00080D 00                    1447 	.db #0x00	; 0
      00080E 00                    1448 	.db #0x00	; 0
      00080F 00                    1449 	.db #0x00	; 0
      000810 00                    1450 	.db #0x00	; 0
      000811 00                    1451 	.db #0x00	; 0
      000812 00                    1452 	.db #0x00	; 0
      000813 00                    1453 	.db #0x00	; 0
      000814 00                    1454 	.db #0x00	; 0
      000815 00                    1455 	.db #0x00	; 0
      000816 00                    1456 	.db #0x00	; 0
      000817 00                    1457 	.db #0x00	; 0
      000818 00                    1458 	.db #0x00	; 0
      000819 00                    1459 	.db #0x00	; 0
      00081A 00                    1460 	.db #0x00	; 0
      00081B 00                    1461 	.db #0x00	; 0
      00081C 00                    1462 	.db #0x00	; 0
      00081D 00                    1463 	.db #0x00	; 0
      00081E 00                    1464 	.db #0x00	; 0
      00081F 00                    1465 	.db #0x00	; 0
      000820 00                    1466 	.db #0x00	; 0
      000821 00                    1467 	.db #0x00	; 0
      000822 00                    1468 	.db #0x00	; 0
      000823 00                    1469 	.db #0x00	; 0
      000824 00                    1470 	.db #0x00	; 0
      000825 00                    1471 	.db #0x00	; 0
      000826 00                    1472 	.db #0x00	; 0
      000827 00                    1473 	.db #0x00	; 0
      000828 00                    1474 	.db #0x00	; 0
      000829 00                    1475 	.db #0x00	; 0
      00082A 00                    1476 	.db #0x00	; 0
      00082B 00                    1477 	.db #0x00	; 0
      00082C 00                    1478 	.db #0x00	; 0
      00082D 00                    1479 	.db #0x00	; 0
      00082E 00                    1480 	.db #0x00	; 0
      00082F 00                    1481 	.db #0x00	; 0
      000830 00                    1482 	.db #0x00	; 0
      000831 00                    1483 	.db #0x00	; 0
      000832 10                    1484 	.db #0x10	; 16
      000833 18                    1485 	.db #0x18	; 24
      000834 14                    1486 	.db #0x14	; 20
      000835 12                    1487 	.db #0x12	; 18
      000836 11                    1488 	.db #0x11	; 17
      000837 00                    1489 	.db #0x00	; 0
      000838 00                    1490 	.db #0x00	; 0
      000839 0F                    1491 	.db #0x0f	; 15
      00083A 10                    1492 	.db #0x10	; 16
      00083B 10                    1493 	.db #0x10	; 16
      00083C 10                    1494 	.db #0x10	; 16
      00083D 0F                    1495 	.db #0x0f	; 15
      00083E 00                    1496 	.db #0x00	; 0
      00083F 00                    1497 	.db #0x00	; 0
      000840 00                    1498 	.db #0x00	; 0
      000841 10                    1499 	.db #0x10	; 16
      000842 1F                    1500 	.db #0x1f	; 31
      000843 10                    1501 	.db #0x10	; 16
      000844 00                    1502 	.db #0x00	; 0
      000845 00                    1503 	.db #0x00	; 0
      000846 00                    1504 	.db #0x00	; 0
      000847 08                    1505 	.db #0x08	; 8
      000848 10                    1506 	.db #0x10	; 16
      000849 12                    1507 	.db #0x12	; 18
      00084A 12                    1508 	.db #0x12	; 18
      00084B 0D                    1509 	.db #0x0d	; 13
      00084C 00                    1510 	.db #0x00	; 0
      00084D 00                    1511 	.db #0x00	; 0
      00084E 18                    1512 	.db #0x18	; 24
      00084F 00                    1513 	.db #0x00	; 0
      000850 00                    1514 	.db #0x00	; 0
      000851 0D                    1515 	.db #0x0d	; 13
      000852 12                    1516 	.db #0x12	; 18
      000853 12                    1517 	.db #0x12	; 18
      000854 12                    1518 	.db #0x12	; 18
      000855 0D                    1519 	.db #0x0d	; 13
      000856 00                    1520 	.db #0x00	; 0
      000857 00                    1521 	.db #0x00	; 0
      000858 18                    1522 	.db #0x18	; 24
      000859 00                    1523 	.db #0x00	; 0
      00085A 00                    1524 	.db #0x00	; 0
      00085B 10                    1525 	.db #0x10	; 16
      00085C 18                    1526 	.db #0x18	; 24
      00085D 14                    1527 	.db #0x14	; 20
      00085E 12                    1528 	.db #0x12	; 18
      00085F 11                    1529 	.db #0x11	; 17
      000860 00                    1530 	.db #0x00	; 0
      000861 00                    1531 	.db #0x00	; 0
      000862 10                    1532 	.db #0x10	; 16
      000863 18                    1533 	.db #0x18	; 24
      000864 14                    1534 	.db #0x14	; 20
      000865 12                    1535 	.db #0x12	; 18
      000866 11                    1536 	.db #0x11	; 17
      000867 00                    1537 	.db #0x00	; 0
      000868 00                    1538 	.db #0x00	; 0
      000869 00                    1539 	.db #0x00	; 0
      00086A 00                    1540 	.db #0x00	; 0
      00086B 00                    1541 	.db #0x00	; 0
      00086C 00                    1542 	.db #0x00	; 0
      00086D 00                    1543 	.db #0x00	; 0
      00086E 00                    1544 	.db #0x00	; 0
      00086F 00                    1545 	.db #0x00	; 0
      000870 00                    1546 	.db #0x00	; 0
      000871 00                    1547 	.db #0x00	; 0
      000872 00                    1548 	.db #0x00	; 0
      000873 00                    1549 	.db #0x00	; 0
      000874 00                    1550 	.db #0x00	; 0
      000875 00                    1551 	.db #0x00	; 0
      000876 00                    1552 	.db #0x00	; 0
      000877 00                    1553 	.db #0x00	; 0
      000878 00                    1554 	.db #0x00	; 0
      000879 00                    1555 	.db #0x00	; 0
      00087A 00                    1556 	.db #0x00	; 0
      00087B 00                    1557 	.db #0x00	; 0
      00087C 00                    1558 	.db #0x00	; 0
      00087D 00                    1559 	.db #0x00	; 0
      00087E 00                    1560 	.db #0x00	; 0
      00087F 00                    1561 	.db #0x00	; 0
      000880 00                    1562 	.db #0x00	; 0
      000881 00                    1563 	.db #0x00	; 0
      000882 00                    1564 	.db #0x00	; 0
      000883 00                    1565 	.db #0x00	; 0
      000884 00                    1566 	.db #0x00	; 0
      000885 00                    1567 	.db #0x00	; 0
      000886 00                    1568 	.db #0x00	; 0
      000887 00                    1569 	.db #0x00	; 0
      000888 00                    1570 	.db #0x00	; 0
      000889 00                    1571 	.db #0x00	; 0
      00088A 00                    1572 	.db #0x00	; 0
      00088B 00                    1573 	.db #0x00	; 0
      00088C 00                    1574 	.db #0x00	; 0
      00088D 00                    1575 	.db #0x00	; 0
      00088E 00                    1576 	.db #0x00	; 0
      00088F 00                    1577 	.db #0x00	; 0
      000890 00                    1578 	.db #0x00	; 0
      000891 00                    1579 	.db #0x00	; 0
      000892 00                    1580 	.db #0x00	; 0
      000893 00                    1581 	.db #0x00	; 0
      000894 00                    1582 	.db #0x00	; 0
      000895 00                    1583 	.db #0x00	; 0
      000896 00                    1584 	.db #0x00	; 0
      000897 00                    1585 	.db #0x00	; 0
      000898 00                    1586 	.db #0x00	; 0
      000899 00                    1587 	.db #0x00	; 0
      00089A 00                    1588 	.db #0x00	; 0
      00089B 00                    1589 	.db #0x00	; 0
      00089C 00                    1590 	.db #0x00	; 0
      00089D 00                    1591 	.db #0x00	; 0
      00089E 00                    1592 	.db #0x00	; 0
      00089F 00                    1593 	.db #0x00	; 0
      0008A0 00                    1594 	.db #0x00	; 0
      0008A1 00                    1595 	.db #0x00	; 0
      0008A2 00                    1596 	.db #0x00	; 0
      0008A3 00                    1597 	.db #0x00	; 0
      0008A4 00                    1598 	.db #0x00	; 0
      0008A5 00                    1599 	.db #0x00	; 0
      0008A6 00                    1600 	.db #0x00	; 0
      0008A7 00                    1601 	.db #0x00	; 0
      0008A8 00                    1602 	.db #0x00	; 0
      0008A9 00                    1603 	.db #0x00	; 0
      0008AA 00                    1604 	.db #0x00	; 0
      0008AB 00                    1605 	.db #0x00	; 0
      0008AC 00                    1606 	.db #0x00	; 0
      0008AD 00                    1607 	.db #0x00	; 0
      0008AE 00                    1608 	.db #0x00	; 0
      0008AF 00                    1609 	.db #0x00	; 0
      0008B0 00                    1610 	.db #0x00	; 0
      0008B1 00                    1611 	.db #0x00	; 0
      0008B2 00                    1612 	.db #0x00	; 0
      0008B3 00                    1613 	.db #0x00	; 0
      0008B4 00                    1614 	.db #0x00	; 0
      0008B5 00                    1615 	.db #0x00	; 0
      0008B6 00                    1616 	.db #0x00	; 0
      0008B7 00                    1617 	.db #0x00	; 0
      0008B8 00                    1618 	.db #0x00	; 0
      0008B9 00                    1619 	.db #0x00	; 0
      0008BA 00                    1620 	.db #0x00	; 0
      0008BB 00                    1621 	.db #0x00	; 0
      0008BC 00                    1622 	.db #0x00	; 0
      0008BD 00                    1623 	.db #0x00	; 0
      0008BE 00                    1624 	.db #0x00	; 0
      0008BF 00                    1625 	.db #0x00	; 0
      0008C0 00                    1626 	.db #0x00	; 0
      0008C1 00                    1627 	.db #0x00	; 0
      0008C2 00                    1628 	.db #0x00	; 0
      0008C3 00                    1629 	.db #0x00	; 0
      0008C4 00                    1630 	.db #0x00	; 0
      0008C5 00                    1631 	.db #0x00	; 0
      0008C6 00                    1632 	.db #0x00	; 0
      0008C7 00                    1633 	.db #0x00	; 0
      0008C8 00                    1634 	.db #0x00	; 0
      0008C9 80                    1635 	.db #0x80	; 128
      0008CA 80                    1636 	.db #0x80	; 128
      0008CB 80                    1637 	.db #0x80	; 128
      0008CC 80                    1638 	.db #0x80	; 128
      0008CD 80                    1639 	.db #0x80	; 128
      0008CE 80                    1640 	.db #0x80	; 128
      0008CF 80                    1641 	.db #0x80	; 128
      0008D0 80                    1642 	.db #0x80	; 128
      0008D1 00                    1643 	.db #0x00	; 0
      0008D2 00                    1644 	.db #0x00	; 0
      0008D3 00                    1645 	.db #0x00	; 0
      0008D4 00                    1646 	.db #0x00	; 0
      0008D5 00                    1647 	.db #0x00	; 0
      0008D6 00                    1648 	.db #0x00	; 0
      0008D7 00                    1649 	.db #0x00	; 0
      0008D8 00                    1650 	.db #0x00	; 0
      0008D9 00                    1651 	.db #0x00	; 0
      0008DA 00                    1652 	.db #0x00	; 0
      0008DB 00                    1653 	.db #0x00	; 0
      0008DC 00                    1654 	.db #0x00	; 0
      0008DD 00                    1655 	.db #0x00	; 0
      0008DE 00                    1656 	.db #0x00	; 0
      0008DF 00                    1657 	.db #0x00	; 0
      0008E0 00                    1658 	.db #0x00	; 0
      0008E1 00                    1659 	.db #0x00	; 0
      0008E2 00                    1660 	.db #0x00	; 0
      0008E3 00                    1661 	.db #0x00	; 0
      0008E4 00                    1662 	.db #0x00	; 0
      0008E5 00                    1663 	.db #0x00	; 0
      0008E6 00                    1664 	.db #0x00	; 0
      0008E7 00                    1665 	.db #0x00	; 0
      0008E8 00                    1666 	.db #0x00	; 0
      0008E9 00                    1667 	.db #0x00	; 0
      0008EA 00                    1668 	.db #0x00	; 0
      0008EB 00                    1669 	.db #0x00	; 0
      0008EC 00                    1670 	.db #0x00	; 0
      0008ED 00                    1671 	.db #0x00	; 0
      0008EE 00                    1672 	.db #0x00	; 0
      0008EF 00                    1673 	.db #0x00	; 0
      0008F0 00                    1674 	.db #0x00	; 0
      0008F1 00                    1675 	.db #0x00	; 0
      0008F2 00                    1676 	.db #0x00	; 0
      0008F3 00                    1677 	.db #0x00	; 0
      0008F4 00                    1678 	.db #0x00	; 0
      0008F5 00                    1679 	.db #0x00	; 0
      0008F6 00                    1680 	.db #0x00	; 0
      0008F7 00                    1681 	.db #0x00	; 0
      0008F8 00                    1682 	.db #0x00	; 0
      0008F9 00                    1683 	.db #0x00	; 0
      0008FA 00                    1684 	.db #0x00	; 0
      0008FB 00                    1685 	.db #0x00	; 0
      0008FC 00                    1686 	.db #0x00	; 0
      0008FD 00                    1687 	.db #0x00	; 0
      0008FE 00                    1688 	.db #0x00	; 0
      0008FF 00                    1689 	.db #0x00	; 0
      000900 00                    1690 	.db #0x00	; 0
      000901 00                    1691 	.db #0x00	; 0
      000902 00                    1692 	.db #0x00	; 0
      000903 00                    1693 	.db #0x00	; 0
      000904 00                    1694 	.db #0x00	; 0
      000905 00                    1695 	.db #0x00	; 0
      000906 00                    1696 	.db #0x00	; 0
      000907 00                    1697 	.db #0x00	; 0
      000908 00                    1698 	.db #0x00	; 0
      000909 00                    1699 	.db #0x00	; 0
      00090A 00                    1700 	.db #0x00	; 0
      00090B 00                    1701 	.db #0x00	; 0
      00090C 00                    1702 	.db #0x00	; 0
      00090D 00                    1703 	.db #0x00	; 0
      00090E 7F                    1704 	.db #0x7f	; 127
      00090F 03                    1705 	.db #0x03	; 3
      000910 0C                    1706 	.db #0x0c	; 12
      000911 30                    1707 	.db #0x30	; 48	'0'
      000912 0C                    1708 	.db #0x0c	; 12
      000913 03                    1709 	.db #0x03	; 3
      000914 7F                    1710 	.db #0x7f	; 127
      000915 00                    1711 	.db #0x00	; 0
      000916 00                    1712 	.db #0x00	; 0
      000917 38                    1713 	.db #0x38	; 56	'8'
      000918 54                    1714 	.db #0x54	; 84	'T'
      000919 54                    1715 	.db #0x54	; 84	'T'
      00091A 58                    1716 	.db #0x58	; 88	'X'
      00091B 00                    1717 	.db #0x00	; 0
      00091C 00                    1718 	.db #0x00	; 0
      00091D 7C                    1719 	.db #0x7c	; 124
      00091E 04                    1720 	.db #0x04	; 4
      00091F 04                    1721 	.db #0x04	; 4
      000920 78                    1722 	.db #0x78	; 120	'x'
      000921 00                    1723 	.db #0x00	; 0
      000922 00                    1724 	.db #0x00	; 0
      000923 3C                    1725 	.db #0x3c	; 60
      000924 40                    1726 	.db #0x40	; 64
      000925 40                    1727 	.db #0x40	; 64
      000926 7C                    1728 	.db #0x7c	; 124
      000927 00                    1729 	.db #0x00	; 0
      000928 00                    1730 	.db #0x00	; 0
      000929 00                    1731 	.db #0x00	; 0
      00092A 00                    1732 	.db #0x00	; 0
      00092B 00                    1733 	.db #0x00	; 0
      00092C 00                    1734 	.db #0x00	; 0
      00092D 00                    1735 	.db #0x00	; 0
      00092E 00                    1736 	.db #0x00	; 0
      00092F 00                    1737 	.db #0x00	; 0
      000930 00                    1738 	.db #0x00	; 0
      000931 00                    1739 	.db #0x00	; 0
      000932 00                    1740 	.db #0x00	; 0
      000933 00                    1741 	.db #0x00	; 0
      000934 00                    1742 	.db #0x00	; 0
      000935 00                    1743 	.db #0x00	; 0
      000936 00                    1744 	.db #0x00	; 0
      000937 00                    1745 	.db #0x00	; 0
      000938 00                    1746 	.db #0x00	; 0
      000939 00                    1747 	.db #0x00	; 0
      00093A 00                    1748 	.db #0x00	; 0
      00093B 00                    1749 	.db #0x00	; 0
      00093C 00                    1750 	.db #0x00	; 0
      00093D 00                    1751 	.db #0x00	; 0
      00093E 00                    1752 	.db #0x00	; 0
      00093F 00                    1753 	.db #0x00	; 0
      000940 00                    1754 	.db #0x00	; 0
      000941 00                    1755 	.db #0x00	; 0
      000942 00                    1756 	.db #0x00	; 0
      000943 00                    1757 	.db #0x00	; 0
      000944 00                    1758 	.db #0x00	; 0
      000945 00                    1759 	.db #0x00	; 0
      000946 00                    1760 	.db #0x00	; 0
      000947 00                    1761 	.db #0x00	; 0
      000948 00                    1762 	.db #0x00	; 0
      000949 FF                    1763 	.db #0xff	; 255
      00094A AA                    1764 	.db #0xaa	; 170
      00094B AA                    1765 	.db #0xaa	; 170
      00094C AA                    1766 	.db #0xaa	; 170
      00094D 28                    1767 	.db #0x28	; 40
      00094E 08                    1768 	.db #0x08	; 8
      00094F 00                    1769 	.db #0x00	; 0
      000950 FF                    1770 	.db #0xff	; 255
      000951 00                    1771 	.db #0x00	; 0
      000952 00                    1772 	.db #0x00	; 0
      000953 00                    1773 	.db #0x00	; 0
      000954 00                    1774 	.db #0x00	; 0
      000955 00                    1775 	.db #0x00	; 0
      000956 00                    1776 	.db #0x00	; 0
      000957 00                    1777 	.db #0x00	; 0
      000958 00                    1778 	.db #0x00	; 0
      000959 00                    1779 	.db #0x00	; 0
      00095A 00                    1780 	.db #0x00	; 0
      00095B 00                    1781 	.db #0x00	; 0
      00095C 00                    1782 	.db #0x00	; 0
      00095D 00                    1783 	.db #0x00	; 0
      00095E 00                    1784 	.db #0x00	; 0
      00095F 00                    1785 	.db #0x00	; 0
      000960 00                    1786 	.db #0x00	; 0
      000961 00                    1787 	.db #0x00	; 0
      000962 00                    1788 	.db #0x00	; 0
      000963 00                    1789 	.db #0x00	; 0
      000964 00                    1790 	.db #0x00	; 0
      000965 00                    1791 	.db #0x00	; 0
      000966 00                    1792 	.db #0x00	; 0
      000967 00                    1793 	.db #0x00	; 0
      000968 00                    1794 	.db #0x00	; 0
      000969 00                    1795 	.db #0x00	; 0
      00096A 00                    1796 	.db #0x00	; 0
      00096B 00                    1797 	.db #0x00	; 0
      00096C 00                    1798 	.db #0x00	; 0
      00096D 00                    1799 	.db #0x00	; 0
      00096E 00                    1800 	.db #0x00	; 0
      00096F 00                    1801 	.db #0x00	; 0
      000970 00                    1802 	.db #0x00	; 0
      000971 00                    1803 	.db #0x00	; 0
      000972 00                    1804 	.db #0x00	; 0
      000973 00                    1805 	.db #0x00	; 0
      000974 00                    1806 	.db #0x00	; 0
      000975 00                    1807 	.db #0x00	; 0
      000976 7F                    1808 	.db #0x7f	; 127
      000977 03                    1809 	.db #0x03	; 3
      000978 0C                    1810 	.db #0x0c	; 12
      000979 30                    1811 	.db #0x30	; 48	'0'
      00097A 0C                    1812 	.db #0x0c	; 12
      00097B 03                    1813 	.db #0x03	; 3
      00097C 7F                    1814 	.db #0x7f	; 127
      00097D 00                    1815 	.db #0x00	; 0
      00097E 00                    1816 	.db #0x00	; 0
      00097F 26                    1817 	.db #0x26	; 38
      000980 49                    1818 	.db #0x49	; 73	'I'
      000981 49                    1819 	.db #0x49	; 73	'I'
      000982 49                    1820 	.db #0x49	; 73	'I'
      000983 32                    1821 	.db #0x32	; 50	'2'
      000984 00                    1822 	.db #0x00	; 0
      000985 00                    1823 	.db #0x00	; 0
      000986 7F                    1824 	.db #0x7f	; 127
      000987 02                    1825 	.db #0x02	; 2
      000988 04                    1826 	.db #0x04	; 4
      000989 08                    1827 	.db #0x08	; 8
      00098A 10                    1828 	.db #0x10	; 16
      00098B 7F                    1829 	.db #0x7f	; 127
      00098C 00                    1830 	.db #0x00	; 0
      00098D                       1831 _BMP2:
      00098D 00                    1832 	.db #0x00	; 0
      00098E 03                    1833 	.db #0x03	; 3
      00098F 05                    1834 	.db #0x05	; 5
      000990 09                    1835 	.db #0x09	; 9
      000991 11                    1836 	.db #0x11	; 17
      000992 FF                    1837 	.db #0xff	; 255
      000993 11                    1838 	.db #0x11	; 17
      000994 89                    1839 	.db #0x89	; 137
      000995 05                    1840 	.db #0x05	; 5
      000996 C3                    1841 	.db #0xc3	; 195
      000997 00                    1842 	.db #0x00	; 0
      000998 E0                    1843 	.db #0xe0	; 224
      000999 00                    1844 	.db #0x00	; 0
      00099A F0                    1845 	.db #0xf0	; 240
      00099B 00                    1846 	.db #0x00	; 0
      00099C F8                    1847 	.db #0xf8	; 248
      00099D 00                    1848 	.db #0x00	; 0
      00099E 00                    1849 	.db #0x00	; 0
      00099F 00                    1850 	.db #0x00	; 0
      0009A0 00                    1851 	.db #0x00	; 0
      0009A1 00                    1852 	.db #0x00	; 0
      0009A2 00                    1853 	.db #0x00	; 0
      0009A3 00                    1854 	.db #0x00	; 0
      0009A4 44                    1855 	.db #0x44	; 68	'D'
      0009A5 28                    1856 	.db #0x28	; 40
      0009A6 FF                    1857 	.db #0xff	; 255
      0009A7 11                    1858 	.db #0x11	; 17
      0009A8 AA                    1859 	.db #0xaa	; 170
      0009A9 44                    1860 	.db #0x44	; 68	'D'
      0009AA 00                    1861 	.db #0x00	; 0
      0009AB 00                    1862 	.db #0x00	; 0
      0009AC 00                    1863 	.db #0x00	; 0
      0009AD 00                    1864 	.db #0x00	; 0
      0009AE 00                    1865 	.db #0x00	; 0
      0009AF 00                    1866 	.db #0x00	; 0
      0009B0 00                    1867 	.db #0x00	; 0
      0009B1 00                    1868 	.db #0x00	; 0
      0009B2 00                    1869 	.db #0x00	; 0
      0009B3 00                    1870 	.db #0x00	; 0
      0009B4 00                    1871 	.db #0x00	; 0
      0009B5 00                    1872 	.db #0x00	; 0
      0009B6 00                    1873 	.db #0x00	; 0
      0009B7 00                    1874 	.db #0x00	; 0
      0009B8 00                    1875 	.db #0x00	; 0
      0009B9 00                    1876 	.db #0x00	; 0
      0009BA 00                    1877 	.db #0x00	; 0
      0009BB 00                    1878 	.db #0x00	; 0
      0009BC 00                    1879 	.db #0x00	; 0
      0009BD 00                    1880 	.db #0x00	; 0
      0009BE 00                    1881 	.db #0x00	; 0
      0009BF 00                    1882 	.db #0x00	; 0
      0009C0 00                    1883 	.db #0x00	; 0
      0009C1 00                    1884 	.db #0x00	; 0
      0009C2 00                    1885 	.db #0x00	; 0
      0009C3 00                    1886 	.db #0x00	; 0
      0009C4 00                    1887 	.db #0x00	; 0
      0009C5 00                    1888 	.db #0x00	; 0
      0009C6 00                    1889 	.db #0x00	; 0
      0009C7 00                    1890 	.db #0x00	; 0
      0009C8 00                    1891 	.db #0x00	; 0
      0009C9 00                    1892 	.db #0x00	; 0
      0009CA 00                    1893 	.db #0x00	; 0
      0009CB 00                    1894 	.db #0x00	; 0
      0009CC 00                    1895 	.db #0x00	; 0
      0009CD 00                    1896 	.db #0x00	; 0
      0009CE 00                    1897 	.db #0x00	; 0
      0009CF 00                    1898 	.db #0x00	; 0
      0009D0 00                    1899 	.db #0x00	; 0
      0009D1 00                    1900 	.db #0x00	; 0
      0009D2 00                    1901 	.db #0x00	; 0
      0009D3 00                    1902 	.db #0x00	; 0
      0009D4 00                    1903 	.db #0x00	; 0
      0009D5 00                    1904 	.db #0x00	; 0
      0009D6 00                    1905 	.db #0x00	; 0
      0009D7 00                    1906 	.db #0x00	; 0
      0009D8 00                    1907 	.db #0x00	; 0
      0009D9 00                    1908 	.db #0x00	; 0
      0009DA 00                    1909 	.db #0x00	; 0
      0009DB 00                    1910 	.db #0x00	; 0
      0009DC 00                    1911 	.db #0x00	; 0
      0009DD 00                    1912 	.db #0x00	; 0
      0009DE 00                    1913 	.db #0x00	; 0
      0009DF 00                    1914 	.db #0x00	; 0
      0009E0 00                    1915 	.db #0x00	; 0
      0009E1 00                    1916 	.db #0x00	; 0
      0009E2 00                    1917 	.db #0x00	; 0
      0009E3 00                    1918 	.db #0x00	; 0
      0009E4 00                    1919 	.db #0x00	; 0
      0009E5 00                    1920 	.db #0x00	; 0
      0009E6 00                    1921 	.db #0x00	; 0
      0009E7 83                    1922 	.db #0x83	; 131
      0009E8 01                    1923 	.db #0x01	; 1
      0009E9 38                    1924 	.db #0x38	; 56	'8'
      0009EA 44                    1925 	.db #0x44	; 68	'D'
      0009EB 82                    1926 	.db #0x82	; 130
      0009EC 92                    1927 	.db #0x92	; 146
      0009ED 92                    1928 	.db #0x92	; 146
      0009EE 74                    1929 	.db #0x74	; 116	't'
      0009EF 01                    1930 	.db #0x01	; 1
      0009F0 83                    1931 	.db #0x83	; 131
      0009F1 00                    1932 	.db #0x00	; 0
      0009F2 00                    1933 	.db #0x00	; 0
      0009F3 00                    1934 	.db #0x00	; 0
      0009F4 00                    1935 	.db #0x00	; 0
      0009F5 00                    1936 	.db #0x00	; 0
      0009F6 00                    1937 	.db #0x00	; 0
      0009F7 00                    1938 	.db #0x00	; 0
      0009F8 7C                    1939 	.db #0x7c	; 124
      0009F9 44                    1940 	.db #0x44	; 68	'D'
      0009FA FF                    1941 	.db #0xff	; 255
      0009FB 01                    1942 	.db #0x01	; 1
      0009FC 7D                    1943 	.db #0x7d	; 125
      0009FD 7D                    1944 	.db #0x7d	; 125
      0009FE 7D                    1945 	.db #0x7d	; 125
      0009FF 7D                    1946 	.db #0x7d	; 125
      000A00 01                    1947 	.db #0x01	; 1
      000A01 7D                    1948 	.db #0x7d	; 125
      000A02 7D                    1949 	.db #0x7d	; 125
      000A03 7D                    1950 	.db #0x7d	; 125
      000A04 7D                    1951 	.db #0x7d	; 125
      000A05 01                    1952 	.db #0x01	; 1
      000A06 7D                    1953 	.db #0x7d	; 125
      000A07 7D                    1954 	.db #0x7d	; 125
      000A08 7D                    1955 	.db #0x7d	; 125
      000A09 7D                    1956 	.db #0x7d	; 125
      000A0A 01                    1957 	.db #0x01	; 1
      000A0B FF                    1958 	.db #0xff	; 255
      000A0C 00                    1959 	.db #0x00	; 0
      000A0D 00                    1960 	.db #0x00	; 0
      000A0E 00                    1961 	.db #0x00	; 0
      000A0F 00                    1962 	.db #0x00	; 0
      000A10 00                    1963 	.db #0x00	; 0
      000A11 00                    1964 	.db #0x00	; 0
      000A12 01                    1965 	.db #0x01	; 1
      000A13 00                    1966 	.db #0x00	; 0
      000A14 01                    1967 	.db #0x01	; 1
      000A15 00                    1968 	.db #0x00	; 0
      000A16 01                    1969 	.db #0x01	; 1
      000A17 00                    1970 	.db #0x00	; 0
      000A18 01                    1971 	.db #0x01	; 1
      000A19 00                    1972 	.db #0x00	; 0
      000A1A 01                    1973 	.db #0x01	; 1
      000A1B 00                    1974 	.db #0x00	; 0
      000A1C 01                    1975 	.db #0x01	; 1
      000A1D 00                    1976 	.db #0x00	; 0
      000A1E 00                    1977 	.db #0x00	; 0
      000A1F 00                    1978 	.db #0x00	; 0
      000A20 00                    1979 	.db #0x00	; 0
      000A21 00                    1980 	.db #0x00	; 0
      000A22 00                    1981 	.db #0x00	; 0
      000A23 00                    1982 	.db #0x00	; 0
      000A24 00                    1983 	.db #0x00	; 0
      000A25 00                    1984 	.db #0x00	; 0
      000A26 01                    1985 	.db #0x01	; 1
      000A27 01                    1986 	.db #0x01	; 1
      000A28 00                    1987 	.db #0x00	; 0
      000A29 00                    1988 	.db #0x00	; 0
      000A2A 00                    1989 	.db #0x00	; 0
      000A2B 00                    1990 	.db #0x00	; 0
      000A2C 00                    1991 	.db #0x00	; 0
      000A2D 00                    1992 	.db #0x00	; 0
      000A2E 00                    1993 	.db #0x00	; 0
      000A2F 00                    1994 	.db #0x00	; 0
      000A30 00                    1995 	.db #0x00	; 0
      000A31 00                    1996 	.db #0x00	; 0
      000A32 00                    1997 	.db #0x00	; 0
      000A33 00                    1998 	.db #0x00	; 0
      000A34 00                    1999 	.db #0x00	; 0
      000A35 00                    2000 	.db #0x00	; 0
      000A36 00                    2001 	.db #0x00	; 0
      000A37 00                    2002 	.db #0x00	; 0
      000A38 00                    2003 	.db #0x00	; 0
      000A39 00                    2004 	.db #0x00	; 0
      000A3A 00                    2005 	.db #0x00	; 0
      000A3B 00                    2006 	.db #0x00	; 0
      000A3C 00                    2007 	.db #0x00	; 0
      000A3D 00                    2008 	.db #0x00	; 0
      000A3E 00                    2009 	.db #0x00	; 0
      000A3F 00                    2010 	.db #0x00	; 0
      000A40 00                    2011 	.db #0x00	; 0
      000A41 00                    2012 	.db #0x00	; 0
      000A42 00                    2013 	.db #0x00	; 0
      000A43 00                    2014 	.db #0x00	; 0
      000A44 00                    2015 	.db #0x00	; 0
      000A45 00                    2016 	.db #0x00	; 0
      000A46 00                    2017 	.db #0x00	; 0
      000A47 00                    2018 	.db #0x00	; 0
      000A48 00                    2019 	.db #0x00	; 0
      000A49 00                    2020 	.db #0x00	; 0
      000A4A 00                    2021 	.db #0x00	; 0
      000A4B 00                    2022 	.db #0x00	; 0
      000A4C 00                    2023 	.db #0x00	; 0
      000A4D 00                    2024 	.db #0x00	; 0
      000A4E 00                    2025 	.db #0x00	; 0
      000A4F 00                    2026 	.db #0x00	; 0
      000A50 00                    2027 	.db #0x00	; 0
      000A51 00                    2028 	.db #0x00	; 0
      000A52 00                    2029 	.db #0x00	; 0
      000A53 00                    2030 	.db #0x00	; 0
      000A54 00                    2031 	.db #0x00	; 0
      000A55 00                    2032 	.db #0x00	; 0
      000A56 00                    2033 	.db #0x00	; 0
      000A57 00                    2034 	.db #0x00	; 0
      000A58 00                    2035 	.db #0x00	; 0
      000A59 00                    2036 	.db #0x00	; 0
      000A5A 00                    2037 	.db #0x00	; 0
      000A5B 00                    2038 	.db #0x00	; 0
      000A5C 00                    2039 	.db #0x00	; 0
      000A5D 00                    2040 	.db #0x00	; 0
      000A5E 00                    2041 	.db #0x00	; 0
      000A5F 00                    2042 	.db #0x00	; 0
      000A60 00                    2043 	.db #0x00	; 0
      000A61 00                    2044 	.db #0x00	; 0
      000A62 00                    2045 	.db #0x00	; 0
      000A63 00                    2046 	.db #0x00	; 0
      000A64 00                    2047 	.db #0x00	; 0
      000A65 00                    2048 	.db #0x00	; 0
      000A66 00                    2049 	.db #0x00	; 0
      000A67 01                    2050 	.db #0x01	; 1
      000A68 01                    2051 	.db #0x01	; 1
      000A69 00                    2052 	.db #0x00	; 0
      000A6A 00                    2053 	.db #0x00	; 0
      000A6B 00                    2054 	.db #0x00	; 0
      000A6C 00                    2055 	.db #0x00	; 0
      000A6D 00                    2056 	.db #0x00	; 0
      000A6E 00                    2057 	.db #0x00	; 0
      000A6F 01                    2058 	.db #0x01	; 1
      000A70 01                    2059 	.db #0x01	; 1
      000A71 00                    2060 	.db #0x00	; 0
      000A72 00                    2061 	.db #0x00	; 0
      000A73 00                    2062 	.db #0x00	; 0
      000A74 00                    2063 	.db #0x00	; 0
      000A75 00                    2064 	.db #0x00	; 0
      000A76 00                    2065 	.db #0x00	; 0
      000A77 00                    2066 	.db #0x00	; 0
      000A78 00                    2067 	.db #0x00	; 0
      000A79 00                    2068 	.db #0x00	; 0
      000A7A 01                    2069 	.db #0x01	; 1
      000A7B 01                    2070 	.db #0x01	; 1
      000A7C 01                    2071 	.db #0x01	; 1
      000A7D 01                    2072 	.db #0x01	; 1
      000A7E 01                    2073 	.db #0x01	; 1
      000A7F 01                    2074 	.db #0x01	; 1
      000A80 01                    2075 	.db #0x01	; 1
      000A81 01                    2076 	.db #0x01	; 1
      000A82 01                    2077 	.db #0x01	; 1
      000A83 01                    2078 	.db #0x01	; 1
      000A84 01                    2079 	.db #0x01	; 1
      000A85 01                    2080 	.db #0x01	; 1
      000A86 01                    2081 	.db #0x01	; 1
      000A87 01                    2082 	.db #0x01	; 1
      000A88 01                    2083 	.db #0x01	; 1
      000A89 01                    2084 	.db #0x01	; 1
      000A8A 01                    2085 	.db #0x01	; 1
      000A8B 01                    2086 	.db #0x01	; 1
      000A8C 00                    2087 	.db #0x00	; 0
      000A8D 00                    2088 	.db #0x00	; 0
      000A8E 00                    2089 	.db #0x00	; 0
      000A8F 00                    2090 	.db #0x00	; 0
      000A90 00                    2091 	.db #0x00	; 0
      000A91 00                    2092 	.db #0x00	; 0
      000A92 00                    2093 	.db #0x00	; 0
      000A93 00                    2094 	.db #0x00	; 0
      000A94 00                    2095 	.db #0x00	; 0
      000A95 00                    2096 	.db #0x00	; 0
      000A96 00                    2097 	.db #0x00	; 0
      000A97 00                    2098 	.db #0x00	; 0
      000A98 00                    2099 	.db #0x00	; 0
      000A99 00                    2100 	.db #0x00	; 0
      000A9A 00                    2101 	.db #0x00	; 0
      000A9B 00                    2102 	.db #0x00	; 0
      000A9C 00                    2103 	.db #0x00	; 0
      000A9D 00                    2104 	.db #0x00	; 0
      000A9E 00                    2105 	.db #0x00	; 0
      000A9F 00                    2106 	.db #0x00	; 0
      000AA0 00                    2107 	.db #0x00	; 0
      000AA1 00                    2108 	.db #0x00	; 0
      000AA2 00                    2109 	.db #0x00	; 0
      000AA3 00                    2110 	.db #0x00	; 0
      000AA4 00                    2111 	.db #0x00	; 0
      000AA5 00                    2112 	.db #0x00	; 0
      000AA6 00                    2113 	.db #0x00	; 0
      000AA7 00                    2114 	.db #0x00	; 0
      000AA8 00                    2115 	.db #0x00	; 0
      000AA9 00                    2116 	.db #0x00	; 0
      000AAA 00                    2117 	.db #0x00	; 0
      000AAB 00                    2118 	.db #0x00	; 0
      000AAC F8                    2119 	.db #0xf8	; 248
      000AAD 08                    2120 	.db #0x08	; 8
      000AAE 08                    2121 	.db #0x08	; 8
      000AAF 08                    2122 	.db #0x08	; 8
      000AB0 08                    2123 	.db #0x08	; 8
      000AB1 08                    2124 	.db #0x08	; 8
      000AB2 08                    2125 	.db #0x08	; 8
      000AB3 08                    2126 	.db #0x08	; 8
      000AB4 00                    2127 	.db #0x00	; 0
      000AB5 F8                    2128 	.db #0xf8	; 248
      000AB6 18                    2129 	.db #0x18	; 24
      000AB7 60                    2130 	.db #0x60	; 96
      000AB8 80                    2131 	.db #0x80	; 128
      000AB9 00                    2132 	.db #0x00	; 0
      000ABA 00                    2133 	.db #0x00	; 0
      000ABB 00                    2134 	.db #0x00	; 0
      000ABC 80                    2135 	.db #0x80	; 128
      000ABD 60                    2136 	.db #0x60	; 96
      000ABE 18                    2137 	.db #0x18	; 24
      000ABF F8                    2138 	.db #0xf8	; 248
      000AC0 00                    2139 	.db #0x00	; 0
      000AC1 00                    2140 	.db #0x00	; 0
      000AC2 00                    2141 	.db #0x00	; 0
      000AC3 20                    2142 	.db #0x20	; 32
      000AC4 20                    2143 	.db #0x20	; 32
      000AC5 F8                    2144 	.db #0xf8	; 248
      000AC6 00                    2145 	.db #0x00	; 0
      000AC7 00                    2146 	.db #0x00	; 0
      000AC8 00                    2147 	.db #0x00	; 0
      000AC9 00                    2148 	.db #0x00	; 0
      000ACA 00                    2149 	.db #0x00	; 0
      000ACB 00                    2150 	.db #0x00	; 0
      000ACC E0                    2151 	.db #0xe0	; 224
      000ACD 10                    2152 	.db #0x10	; 16
      000ACE 08                    2153 	.db #0x08	; 8
      000ACF 08                    2154 	.db #0x08	; 8
      000AD0 08                    2155 	.db #0x08	; 8
      000AD1 08                    2156 	.db #0x08	; 8
      000AD2 10                    2157 	.db #0x10	; 16
      000AD3 E0                    2158 	.db #0xe0	; 224
      000AD4 00                    2159 	.db #0x00	; 0
      000AD5 00                    2160 	.db #0x00	; 0
      000AD6 00                    2161 	.db #0x00	; 0
      000AD7 20                    2162 	.db #0x20	; 32
      000AD8 20                    2163 	.db #0x20	; 32
      000AD9 F8                    2164 	.db #0xf8	; 248
      000ADA 00                    2165 	.db #0x00	; 0
      000ADB 00                    2166 	.db #0x00	; 0
      000ADC 00                    2167 	.db #0x00	; 0
      000ADD 00                    2168 	.db #0x00	; 0
      000ADE 00                    2169 	.db #0x00	; 0
      000ADF 00                    2170 	.db #0x00	; 0
      000AE0 00                    2171 	.db #0x00	; 0
      000AE1 00                    2172 	.db #0x00	; 0
      000AE2 00                    2173 	.db #0x00	; 0
      000AE3 00                    2174 	.db #0x00	; 0
      000AE4 00                    2175 	.db #0x00	; 0
      000AE5 00                    2176 	.db #0x00	; 0
      000AE6 08                    2177 	.db #0x08	; 8
      000AE7 08                    2178 	.db #0x08	; 8
      000AE8 08                    2179 	.db #0x08	; 8
      000AE9 08                    2180 	.db #0x08	; 8
      000AEA 08                    2181 	.db #0x08	; 8
      000AEB 88                    2182 	.db #0x88	; 136
      000AEC 68                    2183 	.db #0x68	; 104	'h'
      000AED 18                    2184 	.db #0x18	; 24
      000AEE 00                    2185 	.db #0x00	; 0
      000AEF 00                    2186 	.db #0x00	; 0
      000AF0 00                    2187 	.db #0x00	; 0
      000AF1 00                    2188 	.db #0x00	; 0
      000AF2 00                    2189 	.db #0x00	; 0
      000AF3 00                    2190 	.db #0x00	; 0
      000AF4 00                    2191 	.db #0x00	; 0
      000AF5 00                    2192 	.db #0x00	; 0
      000AF6 00                    2193 	.db #0x00	; 0
      000AF7 00                    2194 	.db #0x00	; 0
      000AF8 00                    2195 	.db #0x00	; 0
      000AF9 00                    2196 	.db #0x00	; 0
      000AFA 00                    2197 	.db #0x00	; 0
      000AFB 00                    2198 	.db #0x00	; 0
      000AFC 00                    2199 	.db #0x00	; 0
      000AFD 00                    2200 	.db #0x00	; 0
      000AFE 00                    2201 	.db #0x00	; 0
      000AFF 00                    2202 	.db #0x00	; 0
      000B00 00                    2203 	.db #0x00	; 0
      000B01 00                    2204 	.db #0x00	; 0
      000B02 00                    2205 	.db #0x00	; 0
      000B03 00                    2206 	.db #0x00	; 0
      000B04 00                    2207 	.db #0x00	; 0
      000B05 00                    2208 	.db #0x00	; 0
      000B06 00                    2209 	.db #0x00	; 0
      000B07 00                    2210 	.db #0x00	; 0
      000B08 00                    2211 	.db #0x00	; 0
      000B09 00                    2212 	.db #0x00	; 0
      000B0A 00                    2213 	.db #0x00	; 0
      000B0B 00                    2214 	.db #0x00	; 0
      000B0C 00                    2215 	.db #0x00	; 0
      000B0D 00                    2216 	.db #0x00	; 0
      000B0E 00                    2217 	.db #0x00	; 0
      000B0F 00                    2218 	.db #0x00	; 0
      000B10 00                    2219 	.db #0x00	; 0
      000B11 00                    2220 	.db #0x00	; 0
      000B12 00                    2221 	.db #0x00	; 0
      000B13 00                    2222 	.db #0x00	; 0
      000B14 00                    2223 	.db #0x00	; 0
      000B15 00                    2224 	.db #0x00	; 0
      000B16 00                    2225 	.db #0x00	; 0
      000B17 00                    2226 	.db #0x00	; 0
      000B18 00                    2227 	.db #0x00	; 0
      000B19 00                    2228 	.db #0x00	; 0
      000B1A 00                    2229 	.db #0x00	; 0
      000B1B 00                    2230 	.db #0x00	; 0
      000B1C 00                    2231 	.db #0x00	; 0
      000B1D 00                    2232 	.db #0x00	; 0
      000B1E 00                    2233 	.db #0x00	; 0
      000B1F 00                    2234 	.db #0x00	; 0
      000B20 00                    2235 	.db #0x00	; 0
      000B21 00                    2236 	.db #0x00	; 0
      000B22 00                    2237 	.db #0x00	; 0
      000B23 00                    2238 	.db #0x00	; 0
      000B24 00                    2239 	.db #0x00	; 0
      000B25 00                    2240 	.db #0x00	; 0
      000B26 00                    2241 	.db #0x00	; 0
      000B27 00                    2242 	.db #0x00	; 0
      000B28 00                    2243 	.db #0x00	; 0
      000B29 00                    2244 	.db #0x00	; 0
      000B2A 00                    2245 	.db #0x00	; 0
      000B2B 00                    2246 	.db #0x00	; 0
      000B2C 7F                    2247 	.db #0x7f	; 127
      000B2D 01                    2248 	.db #0x01	; 1
      000B2E 01                    2249 	.db #0x01	; 1
      000B2F 01                    2250 	.db #0x01	; 1
      000B30 01                    2251 	.db #0x01	; 1
      000B31 01                    2252 	.db #0x01	; 1
      000B32 01                    2253 	.db #0x01	; 1
      000B33 00                    2254 	.db #0x00	; 0
      000B34 00                    2255 	.db #0x00	; 0
      000B35 7F                    2256 	.db #0x7f	; 127
      000B36 00                    2257 	.db #0x00	; 0
      000B37 00                    2258 	.db #0x00	; 0
      000B38 01                    2259 	.db #0x01	; 1
      000B39 06                    2260 	.db #0x06	; 6
      000B3A 18                    2261 	.db #0x18	; 24
      000B3B 06                    2262 	.db #0x06	; 6
      000B3C 01                    2263 	.db #0x01	; 1
      000B3D 00                    2264 	.db #0x00	; 0
      000B3E 00                    2265 	.db #0x00	; 0
      000B3F 7F                    2266 	.db #0x7f	; 127
      000B40 00                    2267 	.db #0x00	; 0
      000B41 00                    2268 	.db #0x00	; 0
      000B42 00                    2269 	.db #0x00	; 0
      000B43 40                    2270 	.db #0x40	; 64
      000B44 40                    2271 	.db #0x40	; 64
      000B45 7F                    2272 	.db #0x7f	; 127
      000B46 40                    2273 	.db #0x40	; 64
      000B47 40                    2274 	.db #0x40	; 64
      000B48 00                    2275 	.db #0x00	; 0
      000B49 00                    2276 	.db #0x00	; 0
      000B4A 00                    2277 	.db #0x00	; 0
      000B4B 00                    2278 	.db #0x00	; 0
      000B4C 1F                    2279 	.db #0x1f	; 31
      000B4D 20                    2280 	.db #0x20	; 32
      000B4E 40                    2281 	.db #0x40	; 64
      000B4F 40                    2282 	.db #0x40	; 64
      000B50 40                    2283 	.db #0x40	; 64
      000B51 40                    2284 	.db #0x40	; 64
      000B52 20                    2285 	.db #0x20	; 32
      000B53 1F                    2286 	.db #0x1f	; 31
      000B54 00                    2287 	.db #0x00	; 0
      000B55 00                    2288 	.db #0x00	; 0
      000B56 00                    2289 	.db #0x00	; 0
      000B57 40                    2290 	.db #0x40	; 64
      000B58 40                    2291 	.db #0x40	; 64
      000B59 7F                    2292 	.db #0x7f	; 127
      000B5A 40                    2293 	.db #0x40	; 64
      000B5B 40                    2294 	.db #0x40	; 64
      000B5C 00                    2295 	.db #0x00	; 0
      000B5D 00                    2296 	.db #0x00	; 0
      000B5E 00                    2297 	.db #0x00	; 0
      000B5F 00                    2298 	.db #0x00	; 0
      000B60 00                    2299 	.db #0x00	; 0
      000B61 60                    2300 	.db #0x60	; 96
      000B62 00                    2301 	.db #0x00	; 0
      000B63 00                    2302 	.db #0x00	; 0
      000B64 00                    2303 	.db #0x00	; 0
      000B65 00                    2304 	.db #0x00	; 0
      000B66 00                    2305 	.db #0x00	; 0
      000B67 00                    2306 	.db #0x00	; 0
      000B68 60                    2307 	.db #0x60	; 96
      000B69 18                    2308 	.db #0x18	; 24
      000B6A 06                    2309 	.db #0x06	; 6
      000B6B 01                    2310 	.db #0x01	; 1
      000B6C 00                    2311 	.db #0x00	; 0
      000B6D 00                    2312 	.db #0x00	; 0
      000B6E 00                    2313 	.db #0x00	; 0
      000B6F 00                    2314 	.db #0x00	; 0
      000B70 00                    2315 	.db #0x00	; 0
      000B71 00                    2316 	.db #0x00	; 0
      000B72 00                    2317 	.db #0x00	; 0
      000B73 00                    2318 	.db #0x00	; 0
      000B74 00                    2319 	.db #0x00	; 0
      000B75 00                    2320 	.db #0x00	; 0
      000B76 00                    2321 	.db #0x00	; 0
      000B77 00                    2322 	.db #0x00	; 0
      000B78 00                    2323 	.db #0x00	; 0
      000B79 00                    2324 	.db #0x00	; 0
      000B7A 00                    2325 	.db #0x00	; 0
      000B7B 00                    2326 	.db #0x00	; 0
      000B7C 00                    2327 	.db #0x00	; 0
      000B7D 00                    2328 	.db #0x00	; 0
      000B7E 00                    2329 	.db #0x00	; 0
      000B7F 00                    2330 	.db #0x00	; 0
      000B80 00                    2331 	.db #0x00	; 0
      000B81 00                    2332 	.db #0x00	; 0
      000B82 00                    2333 	.db #0x00	; 0
      000B83 00                    2334 	.db #0x00	; 0
      000B84 00                    2335 	.db #0x00	; 0
      000B85 00                    2336 	.db #0x00	; 0
      000B86 00                    2337 	.db #0x00	; 0
      000B87 00                    2338 	.db #0x00	; 0
      000B88 00                    2339 	.db #0x00	; 0
      000B89 00                    2340 	.db #0x00	; 0
      000B8A 00                    2341 	.db #0x00	; 0
      000B8B 00                    2342 	.db #0x00	; 0
      000B8C 00                    2343 	.db #0x00	; 0
      000B8D 00                    2344 	.db #0x00	; 0
      000B8E 00                    2345 	.db #0x00	; 0
      000B8F 00                    2346 	.db #0x00	; 0
      000B90 00                    2347 	.db #0x00	; 0
      000B91 00                    2348 	.db #0x00	; 0
      000B92 00                    2349 	.db #0x00	; 0
      000B93 00                    2350 	.db #0x00	; 0
      000B94 00                    2351 	.db #0x00	; 0
      000B95 00                    2352 	.db #0x00	; 0
      000B96 00                    2353 	.db #0x00	; 0
      000B97 00                    2354 	.db #0x00	; 0
      000B98 00                    2355 	.db #0x00	; 0
      000B99 00                    2356 	.db #0x00	; 0
      000B9A 00                    2357 	.db #0x00	; 0
      000B9B 00                    2358 	.db #0x00	; 0
      000B9C 00                    2359 	.db #0x00	; 0
      000B9D 00                    2360 	.db #0x00	; 0
      000B9E 00                    2361 	.db #0x00	; 0
      000B9F 00                    2362 	.db #0x00	; 0
      000BA0 00                    2363 	.db #0x00	; 0
      000BA1 00                    2364 	.db #0x00	; 0
      000BA2 00                    2365 	.db #0x00	; 0
      000BA3 00                    2366 	.db #0x00	; 0
      000BA4 00                    2367 	.db #0x00	; 0
      000BA5 00                    2368 	.db #0x00	; 0
      000BA6 00                    2369 	.db #0x00	; 0
      000BA7 00                    2370 	.db #0x00	; 0
      000BA8 00                    2371 	.db #0x00	; 0
      000BA9 00                    2372 	.db #0x00	; 0
      000BAA 00                    2373 	.db #0x00	; 0
      000BAB 00                    2374 	.db #0x00	; 0
      000BAC 00                    2375 	.db #0x00	; 0
      000BAD 00                    2376 	.db #0x00	; 0
      000BAE 00                    2377 	.db #0x00	; 0
      000BAF 00                    2378 	.db #0x00	; 0
      000BB0 00                    2379 	.db #0x00	; 0
      000BB1 00                    2380 	.db #0x00	; 0
      000BB2 40                    2381 	.db #0x40	; 64
      000BB3 20                    2382 	.db #0x20	; 32
      000BB4 20                    2383 	.db #0x20	; 32
      000BB5 20                    2384 	.db #0x20	; 32
      000BB6 C0                    2385 	.db #0xc0	; 192
      000BB7 00                    2386 	.db #0x00	; 0
      000BB8 00                    2387 	.db #0x00	; 0
      000BB9 E0                    2388 	.db #0xe0	; 224
      000BBA 20                    2389 	.db #0x20	; 32
      000BBB 20                    2390 	.db #0x20	; 32
      000BBC 20                    2391 	.db #0x20	; 32
      000BBD E0                    2392 	.db #0xe0	; 224
      000BBE 00                    2393 	.db #0x00	; 0
      000BBF 00                    2394 	.db #0x00	; 0
      000BC0 00                    2395 	.db #0x00	; 0
      000BC1 40                    2396 	.db #0x40	; 64
      000BC2 E0                    2397 	.db #0xe0	; 224
      000BC3 00                    2398 	.db #0x00	; 0
      000BC4 00                    2399 	.db #0x00	; 0
      000BC5 00                    2400 	.db #0x00	; 0
      000BC6 00                    2401 	.db #0x00	; 0
      000BC7 60                    2402 	.db #0x60	; 96
      000BC8 20                    2403 	.db #0x20	; 32
      000BC9 20                    2404 	.db #0x20	; 32
      000BCA 20                    2405 	.db #0x20	; 32
      000BCB E0                    2406 	.db #0xe0	; 224
      000BCC 00                    2407 	.db #0x00	; 0
      000BCD 00                    2408 	.db #0x00	; 0
      000BCE 00                    2409 	.db #0x00	; 0
      000BCF 00                    2410 	.db #0x00	; 0
      000BD0 00                    2411 	.db #0x00	; 0
      000BD1 E0                    2412 	.db #0xe0	; 224
      000BD2 20                    2413 	.db #0x20	; 32
      000BD3 20                    2414 	.db #0x20	; 32
      000BD4 20                    2415 	.db #0x20	; 32
      000BD5 E0                    2416 	.db #0xe0	; 224
      000BD6 00                    2417 	.db #0x00	; 0
      000BD7 00                    2418 	.db #0x00	; 0
      000BD8 00                    2419 	.db #0x00	; 0
      000BD9 00                    2420 	.db #0x00	; 0
      000BDA 00                    2421 	.db #0x00	; 0
      000BDB 40                    2422 	.db #0x40	; 64
      000BDC 20                    2423 	.db #0x20	; 32
      000BDD 20                    2424 	.db #0x20	; 32
      000BDE 20                    2425 	.db #0x20	; 32
      000BDF C0                    2426 	.db #0xc0	; 192
      000BE0 00                    2427 	.db #0x00	; 0
      000BE1 00                    2428 	.db #0x00	; 0
      000BE2 40                    2429 	.db #0x40	; 64
      000BE3 20                    2430 	.db #0x20	; 32
      000BE4 20                    2431 	.db #0x20	; 32
      000BE5 20                    2432 	.db #0x20	; 32
      000BE6 C0                    2433 	.db #0xc0	; 192
      000BE7 00                    2434 	.db #0x00	; 0
      000BE8 00                    2435 	.db #0x00	; 0
      000BE9 00                    2436 	.db #0x00	; 0
      000BEA 00                    2437 	.db #0x00	; 0
      000BEB 00                    2438 	.db #0x00	; 0
      000BEC 00                    2439 	.db #0x00	; 0
      000BED 00                    2440 	.db #0x00	; 0
      000BEE 00                    2441 	.db #0x00	; 0
      000BEF 00                    2442 	.db #0x00	; 0
      000BF0 00                    2443 	.db #0x00	; 0
      000BF1 00                    2444 	.db #0x00	; 0
      000BF2 00                    2445 	.db #0x00	; 0
      000BF3 00                    2446 	.db #0x00	; 0
      000BF4 00                    2447 	.db #0x00	; 0
      000BF5 00                    2448 	.db #0x00	; 0
      000BF6 00                    2449 	.db #0x00	; 0
      000BF7 00                    2450 	.db #0x00	; 0
      000BF8 00                    2451 	.db #0x00	; 0
      000BF9 00                    2452 	.db #0x00	; 0
      000BFA 00                    2453 	.db #0x00	; 0
      000BFB 00                    2454 	.db #0x00	; 0
      000BFC 00                    2455 	.db #0x00	; 0
      000BFD 00                    2456 	.db #0x00	; 0
      000BFE 00                    2457 	.db #0x00	; 0
      000BFF 00                    2458 	.db #0x00	; 0
      000C00 00                    2459 	.db #0x00	; 0
      000C01 00                    2460 	.db #0x00	; 0
      000C02 00                    2461 	.db #0x00	; 0
      000C03 00                    2462 	.db #0x00	; 0
      000C04 00                    2463 	.db #0x00	; 0
      000C05 00                    2464 	.db #0x00	; 0
      000C06 00                    2465 	.db #0x00	; 0
      000C07 00                    2466 	.db #0x00	; 0
      000C08 00                    2467 	.db #0x00	; 0
      000C09 00                    2468 	.db #0x00	; 0
      000C0A 00                    2469 	.db #0x00	; 0
      000C0B 00                    2470 	.db #0x00	; 0
      000C0C 00                    2471 	.db #0x00	; 0
      000C0D 00                    2472 	.db #0x00	; 0
      000C0E 00                    2473 	.db #0x00	; 0
      000C0F 00                    2474 	.db #0x00	; 0
      000C10 00                    2475 	.db #0x00	; 0
      000C11 00                    2476 	.db #0x00	; 0
      000C12 00                    2477 	.db #0x00	; 0
      000C13 00                    2478 	.db #0x00	; 0
      000C14 00                    2479 	.db #0x00	; 0
      000C15 00                    2480 	.db #0x00	; 0
      000C16 00                    2481 	.db #0x00	; 0
      000C17 00                    2482 	.db #0x00	; 0
      000C18 00                    2483 	.db #0x00	; 0
      000C19 00                    2484 	.db #0x00	; 0
      000C1A 00                    2485 	.db #0x00	; 0
      000C1B 00                    2486 	.db #0x00	; 0
      000C1C 00                    2487 	.db #0x00	; 0
      000C1D 00                    2488 	.db #0x00	; 0
      000C1E 00                    2489 	.db #0x00	; 0
      000C1F 00                    2490 	.db #0x00	; 0
      000C20 00                    2491 	.db #0x00	; 0
      000C21 00                    2492 	.db #0x00	; 0
      000C22 00                    2493 	.db #0x00	; 0
      000C23 00                    2494 	.db #0x00	; 0
      000C24 00                    2495 	.db #0x00	; 0
      000C25 00                    2496 	.db #0x00	; 0
      000C26 00                    2497 	.db #0x00	; 0
      000C27 00                    2498 	.db #0x00	; 0
      000C28 00                    2499 	.db #0x00	; 0
      000C29 00                    2500 	.db #0x00	; 0
      000C2A 00                    2501 	.db #0x00	; 0
      000C2B 00                    2502 	.db #0x00	; 0
      000C2C 00                    2503 	.db #0x00	; 0
      000C2D 00                    2504 	.db #0x00	; 0
      000C2E 00                    2505 	.db #0x00	; 0
      000C2F 00                    2506 	.db #0x00	; 0
      000C30 00                    2507 	.db #0x00	; 0
      000C31 00                    2508 	.db #0x00	; 0
      000C32 0C                    2509 	.db #0x0c	; 12
      000C33 0A                    2510 	.db #0x0a	; 10
      000C34 0A                    2511 	.db #0x0a	; 10
      000C35 09                    2512 	.db #0x09	; 9
      000C36 0C                    2513 	.db #0x0c	; 12
      000C37 00                    2514 	.db #0x00	; 0
      000C38 00                    2515 	.db #0x00	; 0
      000C39 0F                    2516 	.db #0x0f	; 15
      000C3A 08                    2517 	.db #0x08	; 8
      000C3B 08                    2518 	.db #0x08	; 8
      000C3C 08                    2519 	.db #0x08	; 8
      000C3D 0F                    2520 	.db #0x0f	; 15
      000C3E 00                    2521 	.db #0x00	; 0
      000C3F 00                    2522 	.db #0x00	; 0
      000C40 00                    2523 	.db #0x00	; 0
      000C41 08                    2524 	.db #0x08	; 8
      000C42 0F                    2525 	.db #0x0f	; 15
      000C43 08                    2526 	.db #0x08	; 8
      000C44 00                    2527 	.db #0x00	; 0
      000C45 00                    2528 	.db #0x00	; 0
      000C46 00                    2529 	.db #0x00	; 0
      000C47 0C                    2530 	.db #0x0c	; 12
      000C48 08                    2531 	.db #0x08	; 8
      000C49 09                    2532 	.db #0x09	; 9
      000C4A 09                    2533 	.db #0x09	; 9
      000C4B 0E                    2534 	.db #0x0e	; 14
      000C4C 00                    2535 	.db #0x00	; 0
      000C4D 00                    2536 	.db #0x00	; 0
      000C4E 0C                    2537 	.db #0x0c	; 12
      000C4F 00                    2538 	.db #0x00	; 0
      000C50 00                    2539 	.db #0x00	; 0
      000C51 0F                    2540 	.db #0x0f	; 15
      000C52 09                    2541 	.db #0x09	; 9
      000C53 09                    2542 	.db #0x09	; 9
      000C54 09                    2543 	.db #0x09	; 9
      000C55 0F                    2544 	.db #0x0f	; 15
      000C56 00                    2545 	.db #0x00	; 0
      000C57 00                    2546 	.db #0x00	; 0
      000C58 0C                    2547 	.db #0x0c	; 12
      000C59 00                    2548 	.db #0x00	; 0
      000C5A 00                    2549 	.db #0x00	; 0
      000C5B 0C                    2550 	.db #0x0c	; 12
      000C5C 0A                    2551 	.db #0x0a	; 10
      000C5D 0A                    2552 	.db #0x0a	; 10
      000C5E 09                    2553 	.db #0x09	; 9
      000C5F 0C                    2554 	.db #0x0c	; 12
      000C60 00                    2555 	.db #0x00	; 0
      000C61 00                    2556 	.db #0x00	; 0
      000C62 0C                    2557 	.db #0x0c	; 12
      000C63 0A                    2558 	.db #0x0a	; 10
      000C64 0A                    2559 	.db #0x0a	; 10
      000C65 09                    2560 	.db #0x09	; 9
      000C66 0C                    2561 	.db #0x0c	; 12
      000C67 00                    2562 	.db #0x00	; 0
      000C68 00                    2563 	.db #0x00	; 0
      000C69 00                    2564 	.db #0x00	; 0
      000C6A 00                    2565 	.db #0x00	; 0
      000C6B 00                    2566 	.db #0x00	; 0
      000C6C 00                    2567 	.db #0x00	; 0
      000C6D 00                    2568 	.db #0x00	; 0
      000C6E 00                    2569 	.db #0x00	; 0
      000C6F 00                    2570 	.db #0x00	; 0
      000C70 00                    2571 	.db #0x00	; 0
      000C71 00                    2572 	.db #0x00	; 0
      000C72 00                    2573 	.db #0x00	; 0
      000C73 00                    2574 	.db #0x00	; 0
      000C74 00                    2575 	.db #0x00	; 0
      000C75 00                    2576 	.db #0x00	; 0
      000C76 00                    2577 	.db #0x00	; 0
      000C77 00                    2578 	.db #0x00	; 0
      000C78 00                    2579 	.db #0x00	; 0
      000C79 00                    2580 	.db #0x00	; 0
      000C7A 00                    2581 	.db #0x00	; 0
      000C7B 00                    2582 	.db #0x00	; 0
      000C7C 00                    2583 	.db #0x00	; 0
      000C7D 00                    2584 	.db #0x00	; 0
      000C7E 00                    2585 	.db #0x00	; 0
      000C7F 00                    2586 	.db #0x00	; 0
      000C80 00                    2587 	.db #0x00	; 0
      000C81 00                    2588 	.db #0x00	; 0
      000C82 00                    2589 	.db #0x00	; 0
      000C83 00                    2590 	.db #0x00	; 0
      000C84 00                    2591 	.db #0x00	; 0
      000C85 00                    2592 	.db #0x00	; 0
      000C86 00                    2593 	.db #0x00	; 0
      000C87 00                    2594 	.db #0x00	; 0
      000C88 00                    2595 	.db #0x00	; 0
      000C89 00                    2596 	.db #0x00	; 0
      000C8A 00                    2597 	.db #0x00	; 0
      000C8B 00                    2598 	.db #0x00	; 0
      000C8C 00                    2599 	.db #0x00	; 0
      000C8D 00                    2600 	.db #0x00	; 0
      000C8E 00                    2601 	.db #0x00	; 0
      000C8F 00                    2602 	.db #0x00	; 0
      000C90 00                    2603 	.db #0x00	; 0
      000C91 00                    2604 	.db #0x00	; 0
      000C92 00                    2605 	.db #0x00	; 0
      000C93 00                    2606 	.db #0x00	; 0
      000C94 00                    2607 	.db #0x00	; 0
      000C95 00                    2608 	.db #0x00	; 0
      000C96 00                    2609 	.db #0x00	; 0
      000C97 00                    2610 	.db #0x00	; 0
      000C98 00                    2611 	.db #0x00	; 0
      000C99 00                    2612 	.db #0x00	; 0
      000C9A 00                    2613 	.db #0x00	; 0
      000C9B 00                    2614 	.db #0x00	; 0
      000C9C 00                    2615 	.db #0x00	; 0
      000C9D 00                    2616 	.db #0x00	; 0
      000C9E 00                    2617 	.db #0x00	; 0
      000C9F 00                    2618 	.db #0x00	; 0
      000CA0 00                    2619 	.db #0x00	; 0
      000CA1 00                    2620 	.db #0x00	; 0
      000CA2 00                    2621 	.db #0x00	; 0
      000CA3 00                    2622 	.db #0x00	; 0
      000CA4 00                    2623 	.db #0x00	; 0
      000CA5 00                    2624 	.db #0x00	; 0
      000CA6 00                    2625 	.db #0x00	; 0
      000CA7 00                    2626 	.db #0x00	; 0
      000CA8 00                    2627 	.db #0x00	; 0
      000CA9 00                    2628 	.db #0x00	; 0
      000CAA 00                    2629 	.db #0x00	; 0
      000CAB 00                    2630 	.db #0x00	; 0
      000CAC 00                    2631 	.db #0x00	; 0
      000CAD 00                    2632 	.db #0x00	; 0
      000CAE 00                    2633 	.db #0x00	; 0
      000CAF 00                    2634 	.db #0x00	; 0
      000CB0 00                    2635 	.db #0x00	; 0
      000CB1 00                    2636 	.db #0x00	; 0
      000CB2 00                    2637 	.db #0x00	; 0
      000CB3 00                    2638 	.db #0x00	; 0
      000CB4 00                    2639 	.db #0x00	; 0
      000CB5 00                    2640 	.db #0x00	; 0
      000CB6 00                    2641 	.db #0x00	; 0
      000CB7 00                    2642 	.db #0x00	; 0
      000CB8 00                    2643 	.db #0x00	; 0
      000CB9 00                    2644 	.db #0x00	; 0
      000CBA 00                    2645 	.db #0x00	; 0
      000CBB 00                    2646 	.db #0x00	; 0
      000CBC 00                    2647 	.db #0x00	; 0
      000CBD 00                    2648 	.db #0x00	; 0
      000CBE 00                    2649 	.db #0x00	; 0
      000CBF 00                    2650 	.db #0x00	; 0
      000CC0 00                    2651 	.db #0x00	; 0
      000CC1 00                    2652 	.db #0x00	; 0
      000CC2 00                    2653 	.db #0x00	; 0
      000CC3 00                    2654 	.db #0x00	; 0
      000CC4 00                    2655 	.db #0x00	; 0
      000CC5 00                    2656 	.db #0x00	; 0
      000CC6 00                    2657 	.db #0x00	; 0
      000CC7 00                    2658 	.db #0x00	; 0
      000CC8 00                    2659 	.db #0x00	; 0
      000CC9 80                    2660 	.db #0x80	; 128
      000CCA 80                    2661 	.db #0x80	; 128
      000CCB 80                    2662 	.db #0x80	; 128
      000CCC 80                    2663 	.db #0x80	; 128
      000CCD 80                    2664 	.db #0x80	; 128
      000CCE 80                    2665 	.db #0x80	; 128
      000CCF 80                    2666 	.db #0x80	; 128
      000CD0 80                    2667 	.db #0x80	; 128
      000CD1 00                    2668 	.db #0x00	; 0
      000CD2 00                    2669 	.db #0x00	; 0
      000CD3 00                    2670 	.db #0x00	; 0
      000CD4 00                    2671 	.db #0x00	; 0
      000CD5 00                    2672 	.db #0x00	; 0
      000CD6 00                    2673 	.db #0x00	; 0
      000CD7 00                    2674 	.db #0x00	; 0
      000CD8 00                    2675 	.db #0x00	; 0
      000CD9 00                    2676 	.db #0x00	; 0
      000CDA 00                    2677 	.db #0x00	; 0
      000CDB 00                    2678 	.db #0x00	; 0
      000CDC 00                    2679 	.db #0x00	; 0
      000CDD 00                    2680 	.db #0x00	; 0
      000CDE 00                    2681 	.db #0x00	; 0
      000CDF 00                    2682 	.db #0x00	; 0
      000CE0 00                    2683 	.db #0x00	; 0
      000CE1 00                    2684 	.db #0x00	; 0
      000CE2 00                    2685 	.db #0x00	; 0
      000CE3 00                    2686 	.db #0x00	; 0
      000CE4 00                    2687 	.db #0x00	; 0
      000CE5 00                    2688 	.db #0x00	; 0
      000CE6 00                    2689 	.db #0x00	; 0
      000CE7 00                    2690 	.db #0x00	; 0
      000CE8 00                    2691 	.db #0x00	; 0
      000CE9 00                    2692 	.db #0x00	; 0
      000CEA 00                    2693 	.db #0x00	; 0
      000CEB 00                    2694 	.db #0x00	; 0
      000CEC 00                    2695 	.db #0x00	; 0
      000CED 00                    2696 	.db #0x00	; 0
      000CEE 00                    2697 	.db #0x00	; 0
      000CEF 00                    2698 	.db #0x00	; 0
      000CF0 00                    2699 	.db #0x00	; 0
      000CF1 00                    2700 	.db #0x00	; 0
      000CF2 00                    2701 	.db #0x00	; 0
      000CF3 00                    2702 	.db #0x00	; 0
      000CF4 00                    2703 	.db #0x00	; 0
      000CF5 00                    2704 	.db #0x00	; 0
      000CF6 00                    2705 	.db #0x00	; 0
      000CF7 00                    2706 	.db #0x00	; 0
      000CF8 00                    2707 	.db #0x00	; 0
      000CF9 00                    2708 	.db #0x00	; 0
      000CFA 00                    2709 	.db #0x00	; 0
      000CFB 00                    2710 	.db #0x00	; 0
      000CFC 00                    2711 	.db #0x00	; 0
      000CFD 00                    2712 	.db #0x00	; 0
      000CFE 00                    2713 	.db #0x00	; 0
      000CFF 00                    2714 	.db #0x00	; 0
      000D00 00                    2715 	.db #0x00	; 0
      000D01 00                    2716 	.db #0x00	; 0
      000D02 00                    2717 	.db #0x00	; 0
      000D03 00                    2718 	.db #0x00	; 0
      000D04 00                    2719 	.db #0x00	; 0
      000D05 00                    2720 	.db #0x00	; 0
      000D06 00                    2721 	.db #0x00	; 0
      000D07 00                    2722 	.db #0x00	; 0
      000D08 00                    2723 	.db #0x00	; 0
      000D09 00                    2724 	.db #0x00	; 0
      000D0A 00                    2725 	.db #0x00	; 0
      000D0B 00                    2726 	.db #0x00	; 0
      000D0C 00                    2727 	.db #0x00	; 0
      000D0D 00                    2728 	.db #0x00	; 0
      000D0E 7F                    2729 	.db #0x7f	; 127
      000D0F 03                    2730 	.db #0x03	; 3
      000D10 0C                    2731 	.db #0x0c	; 12
      000D11 30                    2732 	.db #0x30	; 48	'0'
      000D12 0C                    2733 	.db #0x0c	; 12
      000D13 03                    2734 	.db #0x03	; 3
      000D14 7F                    2735 	.db #0x7f	; 127
      000D15 00                    2736 	.db #0x00	; 0
      000D16 00                    2737 	.db #0x00	; 0
      000D17 38                    2738 	.db #0x38	; 56	'8'
      000D18 54                    2739 	.db #0x54	; 84	'T'
      000D19 54                    2740 	.db #0x54	; 84	'T'
      000D1A 58                    2741 	.db #0x58	; 88	'X'
      000D1B 00                    2742 	.db #0x00	; 0
      000D1C 00                    2743 	.db #0x00	; 0
      000D1D 7C                    2744 	.db #0x7c	; 124
      000D1E 04                    2745 	.db #0x04	; 4
      000D1F 04                    2746 	.db #0x04	; 4
      000D20 78                    2747 	.db #0x78	; 120	'x'
      000D21 00                    2748 	.db #0x00	; 0
      000D22 00                    2749 	.db #0x00	; 0
      000D23 3C                    2750 	.db #0x3c	; 60
      000D24 40                    2751 	.db #0x40	; 64
      000D25 40                    2752 	.db #0x40	; 64
      000D26 7C                    2753 	.db #0x7c	; 124
      000D27 00                    2754 	.db #0x00	; 0
      000D28 00                    2755 	.db #0x00	; 0
      000D29 00                    2756 	.db #0x00	; 0
      000D2A 00                    2757 	.db #0x00	; 0
      000D2B 00                    2758 	.db #0x00	; 0
      000D2C 00                    2759 	.db #0x00	; 0
      000D2D 00                    2760 	.db #0x00	; 0
      000D2E 00                    2761 	.db #0x00	; 0
      000D2F 00                    2762 	.db #0x00	; 0
      000D30 00                    2763 	.db #0x00	; 0
      000D31 00                    2764 	.db #0x00	; 0
      000D32 00                    2765 	.db #0x00	; 0
      000D33 00                    2766 	.db #0x00	; 0
      000D34 00                    2767 	.db #0x00	; 0
      000D35 00                    2768 	.db #0x00	; 0
      000D36 00                    2769 	.db #0x00	; 0
      000D37 00                    2770 	.db #0x00	; 0
      000D38 00                    2771 	.db #0x00	; 0
      000D39 00                    2772 	.db #0x00	; 0
      000D3A 00                    2773 	.db #0x00	; 0
      000D3B 00                    2774 	.db #0x00	; 0
      000D3C 00                    2775 	.db #0x00	; 0
      000D3D 00                    2776 	.db #0x00	; 0
      000D3E 00                    2777 	.db #0x00	; 0
      000D3F 00                    2778 	.db #0x00	; 0
      000D40 00                    2779 	.db #0x00	; 0
      000D41 00                    2780 	.db #0x00	; 0
      000D42 00                    2781 	.db #0x00	; 0
      000D43 00                    2782 	.db #0x00	; 0
      000D44 00                    2783 	.db #0x00	; 0
      000D45 00                    2784 	.db #0x00	; 0
      000D46 00                    2785 	.db #0x00	; 0
      000D47 00                    2786 	.db #0x00	; 0
      000D48 00                    2787 	.db #0x00	; 0
      000D49 FF                    2788 	.db #0xff	; 255
      000D4A AA                    2789 	.db #0xaa	; 170
      000D4B AA                    2790 	.db #0xaa	; 170
      000D4C AA                    2791 	.db #0xaa	; 170
      000D4D 28                    2792 	.db #0x28	; 40
      000D4E 08                    2793 	.db #0x08	; 8
      000D4F 00                    2794 	.db #0x00	; 0
      000D50 FF                    2795 	.db #0xff	; 255
      000D51 00                    2796 	.db #0x00	; 0
      000D52 00                    2797 	.db #0x00	; 0
      000D53 00                    2798 	.db #0x00	; 0
      000D54 00                    2799 	.db #0x00	; 0
      000D55 00                    2800 	.db #0x00	; 0
      000D56 00                    2801 	.db #0x00	; 0
      000D57 00                    2802 	.db #0x00	; 0
      000D58 00                    2803 	.db #0x00	; 0
      000D59 00                    2804 	.db #0x00	; 0
      000D5A 00                    2805 	.db #0x00	; 0
      000D5B 00                    2806 	.db #0x00	; 0
      000D5C 00                    2807 	.db #0x00	; 0
      000D5D 00                    2808 	.db #0x00	; 0
      000D5E 00                    2809 	.db #0x00	; 0
      000D5F 00                    2810 	.db #0x00	; 0
      000D60 00                    2811 	.db #0x00	; 0
      000D61 00                    2812 	.db #0x00	; 0
      000D62 00                    2813 	.db #0x00	; 0
      000D63 00                    2814 	.db #0x00	; 0
      000D64 00                    2815 	.db #0x00	; 0
      000D65 00                    2816 	.db #0x00	; 0
      000D66 00                    2817 	.db #0x00	; 0
      000D67 00                    2818 	.db #0x00	; 0
      000D68 00                    2819 	.db #0x00	; 0
      000D69 00                    2820 	.db #0x00	; 0
      000D6A 00                    2821 	.db #0x00	; 0
      000D6B 00                    2822 	.db #0x00	; 0
      000D6C 00                    2823 	.db #0x00	; 0
      000D6D 00                    2824 	.db #0x00	; 0
      000D6E 00                    2825 	.db #0x00	; 0
      000D6F 00                    2826 	.db #0x00	; 0
      000D70 00                    2827 	.db #0x00	; 0
      000D71 00                    2828 	.db #0x00	; 0
      000D72 00                    2829 	.db #0x00	; 0
      000D73 00                    2830 	.db #0x00	; 0
      000D74 00                    2831 	.db #0x00	; 0
      000D75 00                    2832 	.db #0x00	; 0
      000D76 7F                    2833 	.db #0x7f	; 127
      000D77 03                    2834 	.db #0x03	; 3
      000D78 0C                    2835 	.db #0x0c	; 12
      000D79 30                    2836 	.db #0x30	; 48	'0'
      000D7A 0C                    2837 	.db #0x0c	; 12
      000D7B 03                    2838 	.db #0x03	; 3
      000D7C 7F                    2839 	.db #0x7f	; 127
      000D7D 00                    2840 	.db #0x00	; 0
      000D7E 00                    2841 	.db #0x00	; 0
      000D7F 26                    2842 	.db #0x26	; 38
      000D80 49                    2843 	.db #0x49	; 73	'I'
      000D81 49                    2844 	.db #0x49	; 73	'I'
      000D82 49                    2845 	.db #0x49	; 73	'I'
      000D83 32                    2846 	.db #0x32	; 50	'2'
      000D84 00                    2847 	.db #0x00	; 0
      000D85 00                    2848 	.db #0x00	; 0
      000D86 7F                    2849 	.db #0x7f	; 127
      000D87 02                    2850 	.db #0x02	; 2
      000D88 04                    2851 	.db #0x04	; 4
      000D89 08                    2852 	.db #0x08	; 8
      000D8A 10                    2853 	.db #0x10	; 16
      000D8B 7F                    2854 	.db #0x7f	; 127
      000D8C 00                    2855 	.db #0x00	; 0
                                   2856 	.area CONST   (CODE)
      000D8D                       2857 ___str_0:
      000D8D 50 6C 65 61 73 65 20  2858 	.ascii "Please follow   Verimake!"
             66 6F 6C 6C 6F 77 20
             20 20 56 65 72 69 6D
             61 6B 65 21
      000DA6 00                    2859 	.db 0x00
                                   2860 	.area CSEG    (CODE)
                                   2861 	.area XINIT   (CODE)
                                   2862 	.area CABS    (ABS,CODE)
