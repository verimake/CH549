                                      1 ;--------------------------------------------------------
                                      2 ; File Created by SDCC : free open source ANSI-C Compiler
                                      3 ; Version 4.0.0 #11528 (MINGW64)
                                      4 ;--------------------------------------------------------
                                      5 	.module CH549_PWM
                                      6 	.optsdcc -mmcs51 --model-small
                                      7 	
                                      8 ;--------------------------------------------------------
                                      9 ; Public variables in this module
                                     10 ;--------------------------------------------------------
                                     11 	.globl _PWM_SEL_CHANNEL_PARM_2
                                     12 	.globl _UIF_BUS_RST
                                     13 	.globl _UIF_DETECT
                                     14 	.globl _UIF_TRANSFER
                                     15 	.globl _UIF_SUSPEND
                                     16 	.globl _UIF_HST_SOF
                                     17 	.globl _UIF_FIFO_OV
                                     18 	.globl _U_SIE_FREE
                                     19 	.globl _U_TOG_OK
                                     20 	.globl _U_IS_NAK
                                     21 	.globl _S0_R_FIFO
                                     22 	.globl _S0_T_FIFO
                                     23 	.globl _S0_FREE
                                     24 	.globl _S0_IF_BYTE
                                     25 	.globl _S0_IF_FIRST
                                     26 	.globl _S0_IF_OV
                                     27 	.globl _S0_FST_ACT
                                     28 	.globl _CP_RL2
                                     29 	.globl _C_T2
                                     30 	.globl _TR2
                                     31 	.globl _EXEN2
                                     32 	.globl _TCLK
                                     33 	.globl _RCLK
                                     34 	.globl _EXF2
                                     35 	.globl _CAP1F
                                     36 	.globl _TF2
                                     37 	.globl _RI
                                     38 	.globl _TI
                                     39 	.globl _RB8
                                     40 	.globl _TB8
                                     41 	.globl _REN
                                     42 	.globl _SM2
                                     43 	.globl _SM1
                                     44 	.globl _SM0
                                     45 	.globl _IT0
                                     46 	.globl _IE0
                                     47 	.globl _IT1
                                     48 	.globl _IE1
                                     49 	.globl _TR0
                                     50 	.globl _TF0
                                     51 	.globl _TR1
                                     52 	.globl _TF1
                                     53 	.globl _XI
                                     54 	.globl _XO
                                     55 	.globl _P4_0
                                     56 	.globl _P4_1
                                     57 	.globl _P4_2
                                     58 	.globl _P4_3
                                     59 	.globl _P4_4
                                     60 	.globl _P4_5
                                     61 	.globl _P4_6
                                     62 	.globl _RXD
                                     63 	.globl _TXD
                                     64 	.globl _INT0
                                     65 	.globl _INT1
                                     66 	.globl _T0
                                     67 	.globl _T1
                                     68 	.globl _CAP0
                                     69 	.globl _INT3
                                     70 	.globl _P3_0
                                     71 	.globl _P3_1
                                     72 	.globl _P3_2
                                     73 	.globl _P3_3
                                     74 	.globl _P3_4
                                     75 	.globl _P3_5
                                     76 	.globl _P3_6
                                     77 	.globl _P3_7
                                     78 	.globl _PWM5
                                     79 	.globl _PWM4
                                     80 	.globl _INT0_
                                     81 	.globl _PWM3
                                     82 	.globl _PWM2
                                     83 	.globl _CAP1_
                                     84 	.globl _T2_
                                     85 	.globl _PWM1
                                     86 	.globl _CAP2_
                                     87 	.globl _T2EX_
                                     88 	.globl _PWM0
                                     89 	.globl _RXD1
                                     90 	.globl _PWM6
                                     91 	.globl _TXD1
                                     92 	.globl _PWM7
                                     93 	.globl _P2_0
                                     94 	.globl _P2_1
                                     95 	.globl _P2_2
                                     96 	.globl _P2_3
                                     97 	.globl _P2_4
                                     98 	.globl _P2_5
                                     99 	.globl _P2_6
                                    100 	.globl _P2_7
                                    101 	.globl _AIN0
                                    102 	.globl _CAP1
                                    103 	.globl _T2
                                    104 	.globl _AIN1
                                    105 	.globl _CAP2
                                    106 	.globl _T2EX
                                    107 	.globl _AIN2
                                    108 	.globl _AIN3
                                    109 	.globl _AIN4
                                    110 	.globl _UCC1
                                    111 	.globl _SCS
                                    112 	.globl _AIN5
                                    113 	.globl _UCC2
                                    114 	.globl _PWM0_
                                    115 	.globl _MOSI
                                    116 	.globl _AIN6
                                    117 	.globl _VBUS
                                    118 	.globl _RXD1_
                                    119 	.globl _MISO
                                    120 	.globl _AIN7
                                    121 	.globl _TXD1_
                                    122 	.globl _SCK
                                    123 	.globl _P1_0
                                    124 	.globl _P1_1
                                    125 	.globl _P1_2
                                    126 	.globl _P1_3
                                    127 	.globl _P1_4
                                    128 	.globl _P1_5
                                    129 	.globl _P1_6
                                    130 	.globl _P1_7
                                    131 	.globl _AIN8
                                    132 	.globl _AIN9
                                    133 	.globl _AIN10
                                    134 	.globl _RXD_
                                    135 	.globl _AIN11
                                    136 	.globl _TXD_
                                    137 	.globl _AIN12
                                    138 	.globl _RXD2
                                    139 	.globl _AIN13
                                    140 	.globl _TXD2
                                    141 	.globl _AIN14
                                    142 	.globl _RXD3
                                    143 	.globl _AIN15
                                    144 	.globl _TXD3
                                    145 	.globl _P0_0
                                    146 	.globl _P0_1
                                    147 	.globl _P0_2
                                    148 	.globl _P0_3
                                    149 	.globl _P0_4
                                    150 	.globl _P0_5
                                    151 	.globl _P0_6
                                    152 	.globl _P0_7
                                    153 	.globl _IE_SPI0
                                    154 	.globl _IE_INT3
                                    155 	.globl _IE_USB
                                    156 	.globl _IE_UART2
                                    157 	.globl _IE_ADC
                                    158 	.globl _IE_UART1
                                    159 	.globl _IE_UART3
                                    160 	.globl _IE_PWMX
                                    161 	.globl _IE_GPIO
                                    162 	.globl _IE_WDOG
                                    163 	.globl _PX0
                                    164 	.globl _PT0
                                    165 	.globl _PX1
                                    166 	.globl _PT1
                                    167 	.globl _PS
                                    168 	.globl _PT2
                                    169 	.globl _PL_FLAG
                                    170 	.globl _PH_FLAG
                                    171 	.globl _EX0
                                    172 	.globl _ET0
                                    173 	.globl _EX1
                                    174 	.globl _ET1
                                    175 	.globl _ES
                                    176 	.globl _ET2
                                    177 	.globl _E_DIS
                                    178 	.globl _EA
                                    179 	.globl _P
                                    180 	.globl _F1
                                    181 	.globl _OV
                                    182 	.globl _RS0
                                    183 	.globl _RS1
                                    184 	.globl _F0
                                    185 	.globl _AC
                                    186 	.globl _CY
                                    187 	.globl _UEP1_DMA_H
                                    188 	.globl _UEP1_DMA_L
                                    189 	.globl _UEP1_DMA
                                    190 	.globl _UEP0_DMA_H
                                    191 	.globl _UEP0_DMA_L
                                    192 	.globl _UEP0_DMA
                                    193 	.globl _UEP2_3_MOD
                                    194 	.globl _UEP4_1_MOD
                                    195 	.globl _UEP3_DMA_H
                                    196 	.globl _UEP3_DMA_L
                                    197 	.globl _UEP3_DMA
                                    198 	.globl _UEP2_DMA_H
                                    199 	.globl _UEP2_DMA_L
                                    200 	.globl _UEP2_DMA
                                    201 	.globl _USB_DEV_AD
                                    202 	.globl _USB_CTRL
                                    203 	.globl _USB_INT_EN
                                    204 	.globl _UEP4_T_LEN
                                    205 	.globl _UEP4_CTRL
                                    206 	.globl _UEP0_T_LEN
                                    207 	.globl _UEP0_CTRL
                                    208 	.globl _USB_RX_LEN
                                    209 	.globl _USB_MIS_ST
                                    210 	.globl _USB_INT_ST
                                    211 	.globl _USB_INT_FG
                                    212 	.globl _UEP3_T_LEN
                                    213 	.globl _UEP3_CTRL
                                    214 	.globl _UEP2_T_LEN
                                    215 	.globl _UEP2_CTRL
                                    216 	.globl _UEP1_T_LEN
                                    217 	.globl _UEP1_CTRL
                                    218 	.globl _UDEV_CTRL
                                    219 	.globl _USB_C_CTRL
                                    220 	.globl _ADC_PIN
                                    221 	.globl _ADC_CHAN
                                    222 	.globl _ADC_DAT_H
                                    223 	.globl _ADC_DAT_L
                                    224 	.globl _ADC_DAT
                                    225 	.globl _ADC_CFG
                                    226 	.globl _ADC_CTRL
                                    227 	.globl _TKEY_CTRL
                                    228 	.globl _SIF3
                                    229 	.globl _SBAUD3
                                    230 	.globl _SBUF3
                                    231 	.globl _SCON3
                                    232 	.globl _SIF2
                                    233 	.globl _SBAUD2
                                    234 	.globl _SBUF2
                                    235 	.globl _SCON2
                                    236 	.globl _SIF1
                                    237 	.globl _SBAUD1
                                    238 	.globl _SBUF1
                                    239 	.globl _SCON1
                                    240 	.globl _SPI0_SETUP
                                    241 	.globl _SPI0_CK_SE
                                    242 	.globl _SPI0_CTRL
                                    243 	.globl _SPI0_DATA
                                    244 	.globl _SPI0_STAT
                                    245 	.globl _PWM_DATA7
                                    246 	.globl _PWM_DATA6
                                    247 	.globl _PWM_DATA5
                                    248 	.globl _PWM_DATA4
                                    249 	.globl _PWM_DATA3
                                    250 	.globl _PWM_CTRL2
                                    251 	.globl _PWM_CK_SE
                                    252 	.globl _PWM_CTRL
                                    253 	.globl _PWM_DATA0
                                    254 	.globl _PWM_DATA1
                                    255 	.globl _PWM_DATA2
                                    256 	.globl _T2CAP1H
                                    257 	.globl _T2CAP1L
                                    258 	.globl _T2CAP1
                                    259 	.globl _TH2
                                    260 	.globl _TL2
                                    261 	.globl _T2COUNT
                                    262 	.globl _RCAP2H
                                    263 	.globl _RCAP2L
                                    264 	.globl _RCAP2
                                    265 	.globl _T2MOD
                                    266 	.globl _T2CON
                                    267 	.globl _T2CAP0H
                                    268 	.globl _T2CAP0L
                                    269 	.globl _T2CAP0
                                    270 	.globl _T2CON2
                                    271 	.globl _SBUF
                                    272 	.globl _SCON
                                    273 	.globl _TH1
                                    274 	.globl _TH0
                                    275 	.globl _TL1
                                    276 	.globl _TL0
                                    277 	.globl _TMOD
                                    278 	.globl _TCON
                                    279 	.globl _XBUS_AUX
                                    280 	.globl _PIN_FUNC
                                    281 	.globl _P5
                                    282 	.globl _P4_DIR_PU
                                    283 	.globl _P4_MOD_OC
                                    284 	.globl _P4
                                    285 	.globl _P3_DIR_PU
                                    286 	.globl _P3_MOD_OC
                                    287 	.globl _P3
                                    288 	.globl _P2_DIR_PU
                                    289 	.globl _P2_MOD_OC
                                    290 	.globl _P2
                                    291 	.globl _P1_DIR_PU
                                    292 	.globl _P1_MOD_OC
                                    293 	.globl _P1
                                    294 	.globl _P0_DIR_PU
                                    295 	.globl _P0_MOD_OC
                                    296 	.globl _P0
                                    297 	.globl _ROM_CTRL
                                    298 	.globl _ROM_DATA_HH
                                    299 	.globl _ROM_DATA_HL
                                    300 	.globl _ROM_DATA_HI
                                    301 	.globl _ROM_ADDR_H
                                    302 	.globl _ROM_ADDR_L
                                    303 	.globl _ROM_ADDR
                                    304 	.globl _GPIO_IE
                                    305 	.globl _INTX
                                    306 	.globl _IP_EX
                                    307 	.globl _IE_EX
                                    308 	.globl _IP
                                    309 	.globl _IE
                                    310 	.globl _WDOG_COUNT
                                    311 	.globl _RESET_KEEP
                                    312 	.globl _WAKE_CTRL
                                    313 	.globl _CLOCK_CFG
                                    314 	.globl _POWER_CFG
                                    315 	.globl _PCON
                                    316 	.globl _GLOBAL_CFG
                                    317 	.globl _SAFE_MOD
                                    318 	.globl _DPH
                                    319 	.globl _DPL
                                    320 	.globl _SP
                                    321 	.globl _A_INV
                                    322 	.globl _B
                                    323 	.globl _ACC
                                    324 	.globl _PSW
                                    325 	.globl _PWM_SEL_CHANNEL
                                    326 ;--------------------------------------------------------
                                    327 ; special function registers
                                    328 ;--------------------------------------------------------
                                    329 	.area RSEG    (ABS,DATA)
      000000                        330 	.org 0x0000
                           0000D0   331 _PSW	=	0x00d0
                           0000E0   332 _ACC	=	0x00e0
                           0000F0   333 _B	=	0x00f0
                           0000FD   334 _A_INV	=	0x00fd
                           000081   335 _SP	=	0x0081
                           000082   336 _DPL	=	0x0082
                           000083   337 _DPH	=	0x0083
                           0000A1   338 _SAFE_MOD	=	0x00a1
                           0000B1   339 _GLOBAL_CFG	=	0x00b1
                           000087   340 _PCON	=	0x0087
                           0000BA   341 _POWER_CFG	=	0x00ba
                           0000B9   342 _CLOCK_CFG	=	0x00b9
                           0000A9   343 _WAKE_CTRL	=	0x00a9
                           0000FE   344 _RESET_KEEP	=	0x00fe
                           0000FF   345 _WDOG_COUNT	=	0x00ff
                           0000A8   346 _IE	=	0x00a8
                           0000B8   347 _IP	=	0x00b8
                           0000E8   348 _IE_EX	=	0x00e8
                           0000E9   349 _IP_EX	=	0x00e9
                           0000B3   350 _INTX	=	0x00b3
                           0000B2   351 _GPIO_IE	=	0x00b2
                           008584   352 _ROM_ADDR	=	0x8584
                           000084   353 _ROM_ADDR_L	=	0x0084
                           000085   354 _ROM_ADDR_H	=	0x0085
                           008F8E   355 _ROM_DATA_HI	=	0x8f8e
                           00008E   356 _ROM_DATA_HL	=	0x008e
                           00008F   357 _ROM_DATA_HH	=	0x008f
                           000086   358 _ROM_CTRL	=	0x0086
                           000080   359 _P0	=	0x0080
                           0000C4   360 _P0_MOD_OC	=	0x00c4
                           0000C5   361 _P0_DIR_PU	=	0x00c5
                           000090   362 _P1	=	0x0090
                           000092   363 _P1_MOD_OC	=	0x0092
                           000093   364 _P1_DIR_PU	=	0x0093
                           0000A0   365 _P2	=	0x00a0
                           000094   366 _P2_MOD_OC	=	0x0094
                           000095   367 _P2_DIR_PU	=	0x0095
                           0000B0   368 _P3	=	0x00b0
                           000096   369 _P3_MOD_OC	=	0x0096
                           000097   370 _P3_DIR_PU	=	0x0097
                           0000C0   371 _P4	=	0x00c0
                           0000C2   372 _P4_MOD_OC	=	0x00c2
                           0000C3   373 _P4_DIR_PU	=	0x00c3
                           0000AB   374 _P5	=	0x00ab
                           0000AA   375 _PIN_FUNC	=	0x00aa
                           0000A2   376 _XBUS_AUX	=	0x00a2
                           000088   377 _TCON	=	0x0088
                           000089   378 _TMOD	=	0x0089
                           00008A   379 _TL0	=	0x008a
                           00008B   380 _TL1	=	0x008b
                           00008C   381 _TH0	=	0x008c
                           00008D   382 _TH1	=	0x008d
                           000098   383 _SCON	=	0x0098
                           000099   384 _SBUF	=	0x0099
                           0000C1   385 _T2CON2	=	0x00c1
                           00C7C6   386 _T2CAP0	=	0xc7c6
                           0000C6   387 _T2CAP0L	=	0x00c6
                           0000C7   388 _T2CAP0H	=	0x00c7
                           0000C8   389 _T2CON	=	0x00c8
                           0000C9   390 _T2MOD	=	0x00c9
                           00CBCA   391 _RCAP2	=	0xcbca
                           0000CA   392 _RCAP2L	=	0x00ca
                           0000CB   393 _RCAP2H	=	0x00cb
                           00CDCC   394 _T2COUNT	=	0xcdcc
                           0000CC   395 _TL2	=	0x00cc
                           0000CD   396 _TH2	=	0x00cd
                           00CFCE   397 _T2CAP1	=	0xcfce
                           0000CE   398 _T2CAP1L	=	0x00ce
                           0000CF   399 _T2CAP1H	=	0x00cf
                           00009A   400 _PWM_DATA2	=	0x009a
                           00009B   401 _PWM_DATA1	=	0x009b
                           00009C   402 _PWM_DATA0	=	0x009c
                           00009D   403 _PWM_CTRL	=	0x009d
                           00009E   404 _PWM_CK_SE	=	0x009e
                           00009F   405 _PWM_CTRL2	=	0x009f
                           0000A3   406 _PWM_DATA3	=	0x00a3
                           0000A4   407 _PWM_DATA4	=	0x00a4
                           0000A5   408 _PWM_DATA5	=	0x00a5
                           0000A6   409 _PWM_DATA6	=	0x00a6
                           0000A7   410 _PWM_DATA7	=	0x00a7
                           0000F8   411 _SPI0_STAT	=	0x00f8
                           0000F9   412 _SPI0_DATA	=	0x00f9
                           0000FA   413 _SPI0_CTRL	=	0x00fa
                           0000FB   414 _SPI0_CK_SE	=	0x00fb
                           0000FC   415 _SPI0_SETUP	=	0x00fc
                           0000BC   416 _SCON1	=	0x00bc
                           0000BD   417 _SBUF1	=	0x00bd
                           0000BE   418 _SBAUD1	=	0x00be
                           0000BF   419 _SIF1	=	0x00bf
                           0000B4   420 _SCON2	=	0x00b4
                           0000B5   421 _SBUF2	=	0x00b5
                           0000B6   422 _SBAUD2	=	0x00b6
                           0000B7   423 _SIF2	=	0x00b7
                           0000AC   424 _SCON3	=	0x00ac
                           0000AD   425 _SBUF3	=	0x00ad
                           0000AE   426 _SBAUD3	=	0x00ae
                           0000AF   427 _SIF3	=	0x00af
                           0000F1   428 _TKEY_CTRL	=	0x00f1
                           0000F2   429 _ADC_CTRL	=	0x00f2
                           0000F3   430 _ADC_CFG	=	0x00f3
                           00F5F4   431 _ADC_DAT	=	0xf5f4
                           0000F4   432 _ADC_DAT_L	=	0x00f4
                           0000F5   433 _ADC_DAT_H	=	0x00f5
                           0000F6   434 _ADC_CHAN	=	0x00f6
                           0000F7   435 _ADC_PIN	=	0x00f7
                           000091   436 _USB_C_CTRL	=	0x0091
                           0000D1   437 _UDEV_CTRL	=	0x00d1
                           0000D2   438 _UEP1_CTRL	=	0x00d2
                           0000D3   439 _UEP1_T_LEN	=	0x00d3
                           0000D4   440 _UEP2_CTRL	=	0x00d4
                           0000D5   441 _UEP2_T_LEN	=	0x00d5
                           0000D6   442 _UEP3_CTRL	=	0x00d6
                           0000D7   443 _UEP3_T_LEN	=	0x00d7
                           0000D8   444 _USB_INT_FG	=	0x00d8
                           0000D9   445 _USB_INT_ST	=	0x00d9
                           0000DA   446 _USB_MIS_ST	=	0x00da
                           0000DB   447 _USB_RX_LEN	=	0x00db
                           0000DC   448 _UEP0_CTRL	=	0x00dc
                           0000DD   449 _UEP0_T_LEN	=	0x00dd
                           0000DE   450 _UEP4_CTRL	=	0x00de
                           0000DF   451 _UEP4_T_LEN	=	0x00df
                           0000E1   452 _USB_INT_EN	=	0x00e1
                           0000E2   453 _USB_CTRL	=	0x00e2
                           0000E3   454 _USB_DEV_AD	=	0x00e3
                           00E5E4   455 _UEP2_DMA	=	0xe5e4
                           0000E4   456 _UEP2_DMA_L	=	0x00e4
                           0000E5   457 _UEP2_DMA_H	=	0x00e5
                           00E7E6   458 _UEP3_DMA	=	0xe7e6
                           0000E6   459 _UEP3_DMA_L	=	0x00e6
                           0000E7   460 _UEP3_DMA_H	=	0x00e7
                           0000EA   461 _UEP4_1_MOD	=	0x00ea
                           0000EB   462 _UEP2_3_MOD	=	0x00eb
                           00EDEC   463 _UEP0_DMA	=	0xedec
                           0000EC   464 _UEP0_DMA_L	=	0x00ec
                           0000ED   465 _UEP0_DMA_H	=	0x00ed
                           00EFEE   466 _UEP1_DMA	=	0xefee
                           0000EE   467 _UEP1_DMA_L	=	0x00ee
                           0000EF   468 _UEP1_DMA_H	=	0x00ef
                                    469 ;--------------------------------------------------------
                                    470 ; special function bits
                                    471 ;--------------------------------------------------------
                                    472 	.area RSEG    (ABS,DATA)
      000000                        473 	.org 0x0000
                           0000D7   474 _CY	=	0x00d7
                           0000D6   475 _AC	=	0x00d6
                           0000D5   476 _F0	=	0x00d5
                           0000D4   477 _RS1	=	0x00d4
                           0000D3   478 _RS0	=	0x00d3
                           0000D2   479 _OV	=	0x00d2
                           0000D1   480 _F1	=	0x00d1
                           0000D0   481 _P	=	0x00d0
                           0000AF   482 _EA	=	0x00af
                           0000AE   483 _E_DIS	=	0x00ae
                           0000AD   484 _ET2	=	0x00ad
                           0000AC   485 _ES	=	0x00ac
                           0000AB   486 _ET1	=	0x00ab
                           0000AA   487 _EX1	=	0x00aa
                           0000A9   488 _ET0	=	0x00a9
                           0000A8   489 _EX0	=	0x00a8
                           0000BF   490 _PH_FLAG	=	0x00bf
                           0000BE   491 _PL_FLAG	=	0x00be
                           0000BD   492 _PT2	=	0x00bd
                           0000BC   493 _PS	=	0x00bc
                           0000BB   494 _PT1	=	0x00bb
                           0000BA   495 _PX1	=	0x00ba
                           0000B9   496 _PT0	=	0x00b9
                           0000B8   497 _PX0	=	0x00b8
                           0000EF   498 _IE_WDOG	=	0x00ef
                           0000EE   499 _IE_GPIO	=	0x00ee
                           0000ED   500 _IE_PWMX	=	0x00ed
                           0000ED   501 _IE_UART3	=	0x00ed
                           0000EC   502 _IE_UART1	=	0x00ec
                           0000EB   503 _IE_ADC	=	0x00eb
                           0000EB   504 _IE_UART2	=	0x00eb
                           0000EA   505 _IE_USB	=	0x00ea
                           0000E9   506 _IE_INT3	=	0x00e9
                           0000E8   507 _IE_SPI0	=	0x00e8
                           000087   508 _P0_7	=	0x0087
                           000086   509 _P0_6	=	0x0086
                           000085   510 _P0_5	=	0x0085
                           000084   511 _P0_4	=	0x0084
                           000083   512 _P0_3	=	0x0083
                           000082   513 _P0_2	=	0x0082
                           000081   514 _P0_1	=	0x0081
                           000080   515 _P0_0	=	0x0080
                           000087   516 _TXD3	=	0x0087
                           000087   517 _AIN15	=	0x0087
                           000086   518 _RXD3	=	0x0086
                           000086   519 _AIN14	=	0x0086
                           000085   520 _TXD2	=	0x0085
                           000085   521 _AIN13	=	0x0085
                           000084   522 _RXD2	=	0x0084
                           000084   523 _AIN12	=	0x0084
                           000083   524 _TXD_	=	0x0083
                           000083   525 _AIN11	=	0x0083
                           000082   526 _RXD_	=	0x0082
                           000082   527 _AIN10	=	0x0082
                           000081   528 _AIN9	=	0x0081
                           000080   529 _AIN8	=	0x0080
                           000097   530 _P1_7	=	0x0097
                           000096   531 _P1_6	=	0x0096
                           000095   532 _P1_5	=	0x0095
                           000094   533 _P1_4	=	0x0094
                           000093   534 _P1_3	=	0x0093
                           000092   535 _P1_2	=	0x0092
                           000091   536 _P1_1	=	0x0091
                           000090   537 _P1_0	=	0x0090
                           000097   538 _SCK	=	0x0097
                           000097   539 _TXD1_	=	0x0097
                           000097   540 _AIN7	=	0x0097
                           000096   541 _MISO	=	0x0096
                           000096   542 _RXD1_	=	0x0096
                           000096   543 _VBUS	=	0x0096
                           000096   544 _AIN6	=	0x0096
                           000095   545 _MOSI	=	0x0095
                           000095   546 _PWM0_	=	0x0095
                           000095   547 _UCC2	=	0x0095
                           000095   548 _AIN5	=	0x0095
                           000094   549 _SCS	=	0x0094
                           000094   550 _UCC1	=	0x0094
                           000094   551 _AIN4	=	0x0094
                           000093   552 _AIN3	=	0x0093
                           000092   553 _AIN2	=	0x0092
                           000091   554 _T2EX	=	0x0091
                           000091   555 _CAP2	=	0x0091
                           000091   556 _AIN1	=	0x0091
                           000090   557 _T2	=	0x0090
                           000090   558 _CAP1	=	0x0090
                           000090   559 _AIN0	=	0x0090
                           0000A7   560 _P2_7	=	0x00a7
                           0000A6   561 _P2_6	=	0x00a6
                           0000A5   562 _P2_5	=	0x00a5
                           0000A4   563 _P2_4	=	0x00a4
                           0000A3   564 _P2_3	=	0x00a3
                           0000A2   565 _P2_2	=	0x00a2
                           0000A1   566 _P2_1	=	0x00a1
                           0000A0   567 _P2_0	=	0x00a0
                           0000A7   568 _PWM7	=	0x00a7
                           0000A7   569 _TXD1	=	0x00a7
                           0000A6   570 _PWM6	=	0x00a6
                           0000A6   571 _RXD1	=	0x00a6
                           0000A5   572 _PWM0	=	0x00a5
                           0000A5   573 _T2EX_	=	0x00a5
                           0000A5   574 _CAP2_	=	0x00a5
                           0000A4   575 _PWM1	=	0x00a4
                           0000A4   576 _T2_	=	0x00a4
                           0000A4   577 _CAP1_	=	0x00a4
                           0000A3   578 _PWM2	=	0x00a3
                           0000A2   579 _PWM3	=	0x00a2
                           0000A2   580 _INT0_	=	0x00a2
                           0000A1   581 _PWM4	=	0x00a1
                           0000A0   582 _PWM5	=	0x00a0
                           0000B7   583 _P3_7	=	0x00b7
                           0000B6   584 _P3_6	=	0x00b6
                           0000B5   585 _P3_5	=	0x00b5
                           0000B4   586 _P3_4	=	0x00b4
                           0000B3   587 _P3_3	=	0x00b3
                           0000B2   588 _P3_2	=	0x00b2
                           0000B1   589 _P3_1	=	0x00b1
                           0000B0   590 _P3_0	=	0x00b0
                           0000B7   591 _INT3	=	0x00b7
                           0000B6   592 _CAP0	=	0x00b6
                           0000B5   593 _T1	=	0x00b5
                           0000B4   594 _T0	=	0x00b4
                           0000B3   595 _INT1	=	0x00b3
                           0000B2   596 _INT0	=	0x00b2
                           0000B1   597 _TXD	=	0x00b1
                           0000B0   598 _RXD	=	0x00b0
                           0000C6   599 _P4_6	=	0x00c6
                           0000C5   600 _P4_5	=	0x00c5
                           0000C4   601 _P4_4	=	0x00c4
                           0000C3   602 _P4_3	=	0x00c3
                           0000C2   603 _P4_2	=	0x00c2
                           0000C1   604 _P4_1	=	0x00c1
                           0000C0   605 _P4_0	=	0x00c0
                           0000C7   606 _XO	=	0x00c7
                           0000C6   607 _XI	=	0x00c6
                           00008F   608 _TF1	=	0x008f
                           00008E   609 _TR1	=	0x008e
                           00008D   610 _TF0	=	0x008d
                           00008C   611 _TR0	=	0x008c
                           00008B   612 _IE1	=	0x008b
                           00008A   613 _IT1	=	0x008a
                           000089   614 _IE0	=	0x0089
                           000088   615 _IT0	=	0x0088
                           00009F   616 _SM0	=	0x009f
                           00009E   617 _SM1	=	0x009e
                           00009D   618 _SM2	=	0x009d
                           00009C   619 _REN	=	0x009c
                           00009B   620 _TB8	=	0x009b
                           00009A   621 _RB8	=	0x009a
                           000099   622 _TI	=	0x0099
                           000098   623 _RI	=	0x0098
                           0000CF   624 _TF2	=	0x00cf
                           0000CF   625 _CAP1F	=	0x00cf
                           0000CE   626 _EXF2	=	0x00ce
                           0000CD   627 _RCLK	=	0x00cd
                           0000CC   628 _TCLK	=	0x00cc
                           0000CB   629 _EXEN2	=	0x00cb
                           0000CA   630 _TR2	=	0x00ca
                           0000C9   631 _C_T2	=	0x00c9
                           0000C8   632 _CP_RL2	=	0x00c8
                           0000FF   633 _S0_FST_ACT	=	0x00ff
                           0000FE   634 _S0_IF_OV	=	0x00fe
                           0000FD   635 _S0_IF_FIRST	=	0x00fd
                           0000FC   636 _S0_IF_BYTE	=	0x00fc
                           0000FB   637 _S0_FREE	=	0x00fb
                           0000FA   638 _S0_T_FIFO	=	0x00fa
                           0000F8   639 _S0_R_FIFO	=	0x00f8
                           0000DF   640 _U_IS_NAK	=	0x00df
                           0000DE   641 _U_TOG_OK	=	0x00de
                           0000DD   642 _U_SIE_FREE	=	0x00dd
                           0000DC   643 _UIF_FIFO_OV	=	0x00dc
                           0000DB   644 _UIF_HST_SOF	=	0x00db
                           0000DA   645 _UIF_SUSPEND	=	0x00da
                           0000D9   646 _UIF_TRANSFER	=	0x00d9
                           0000D8   647 _UIF_DETECT	=	0x00d8
                           0000D8   648 _UIF_BUS_RST	=	0x00d8
                                    649 ;--------------------------------------------------------
                                    650 ; overlayable register banks
                                    651 ;--------------------------------------------------------
                                    652 	.area REG_BANK_0	(REL,OVR,DATA)
      000000                        653 	.ds 8
                                    654 ;--------------------------------------------------------
                                    655 ; internal ram data
                                    656 ;--------------------------------------------------------
                                    657 	.area DSEG    (DATA)
                                    658 ;--------------------------------------------------------
                                    659 ; overlayable items in internal ram 
                                    660 ;--------------------------------------------------------
                                    661 	.area	OSEG    (OVR,DATA)
      00001B                        662 _PWM_SEL_CHANNEL_PARM_2:
      00001B                        663 	.ds 1
                                    664 ;--------------------------------------------------------
                                    665 ; indirectly addressable internal ram data
                                    666 ;--------------------------------------------------------
                                    667 	.area ISEG    (DATA)
                                    668 ;--------------------------------------------------------
                                    669 ; absolute internal ram data
                                    670 ;--------------------------------------------------------
                                    671 	.area IABS    (ABS,DATA)
                                    672 	.area IABS    (ABS,DATA)
                                    673 ;--------------------------------------------------------
                                    674 ; bit data
                                    675 ;--------------------------------------------------------
                                    676 	.area BSEG    (BIT)
                                    677 ;--------------------------------------------------------
                                    678 ; paged external ram data
                                    679 ;--------------------------------------------------------
                                    680 	.area PSEG    (PAG,XDATA)
                                    681 ;--------------------------------------------------------
                                    682 ; external ram data
                                    683 ;--------------------------------------------------------
                                    684 	.area XSEG    (XDATA)
                                    685 ;--------------------------------------------------------
                                    686 ; absolute external ram data
                                    687 ;--------------------------------------------------------
                                    688 	.area XABS    (ABS,XDATA)
                                    689 ;--------------------------------------------------------
                                    690 ; external initialized ram data
                                    691 ;--------------------------------------------------------
                                    692 	.area XISEG   (XDATA)
                                    693 	.area HOME    (CODE)
                                    694 	.area GSINIT0 (CODE)
                                    695 	.area GSINIT1 (CODE)
                                    696 	.area GSINIT2 (CODE)
                                    697 	.area GSINIT3 (CODE)
                                    698 	.area GSINIT4 (CODE)
                                    699 	.area GSINIT5 (CODE)
                                    700 	.area GSINIT  (CODE)
                                    701 	.area GSFINAL (CODE)
                                    702 	.area CSEG    (CODE)
                                    703 ;--------------------------------------------------------
                                    704 ; global & static initialisations
                                    705 ;--------------------------------------------------------
                                    706 	.area HOME    (CODE)
                                    707 	.area GSINIT  (CODE)
                                    708 	.area GSFINAL (CODE)
                                    709 	.area GSINIT  (CODE)
                                    710 ;--------------------------------------------------------
                                    711 ; Home
                                    712 ;--------------------------------------------------------
                                    713 	.area HOME    (CODE)
                                    714 	.area HOME    (CODE)
                                    715 ;--------------------------------------------------------
                                    716 ; code
                                    717 ;--------------------------------------------------------
                                    718 	.area CSEG    (CODE)
                                    719 ;------------------------------------------------------------
                                    720 ;Allocation info for local variables in function 'PWM_SEL_CHANNEL'
                                    721 ;------------------------------------------------------------
                                    722 ;NewState                  Allocated with name '_PWM_SEL_CHANNEL_PARM_2'
                                    723 ;Channel                   Allocated to registers r7 
                                    724 ;i                         Allocated to registers r6 
                                    725 ;------------------------------------------------------------
                                    726 ;	source/CH549_PWM.c:27: void PWM_SEL_CHANNEL(UINT8 Channel,UINT8 NewState)
                                    727 ;	-----------------------------------------
                                    728 ;	 function PWM_SEL_CHANNEL
                                    729 ;	-----------------------------------------
      0007D6                        730 _PWM_SEL_CHANNEL:
                           000007   731 	ar7 = 0x07
                           000006   732 	ar6 = 0x06
                           000005   733 	ar5 = 0x05
                           000004   734 	ar4 = 0x04
                           000003   735 	ar3 = 0x03
                           000002   736 	ar2 = 0x02
                           000001   737 	ar1 = 0x01
                           000000   738 	ar0 = 0x00
      0007D6 AF 82            [24]  739 	mov	r7,dpl
                                    740 ;	source/CH549_PWM.c:31: if(NewState == Enable)                    //输出开启
      0007D8 74 01            [12]  741 	mov	a,#0x01
      0007DA B5 1B 74         [24]  742 	cjne	a,_PWM_SEL_CHANNEL_PARM_2,00117$
                                    743 ;	source/CH549_PWM.c:33: PWM_CTRL &= ~bPWM_CLR_ALL;
      0007DD 53 9D FD         [24]  744 	anl	_PWM_CTRL,#0xfd
                                    745 ;	source/CH549_PWM.c:34: if(Channel&CH0)
      0007E0 EF               [12]  746 	mov	a,r7
      0007E1 30 E0 03         [24]  747 	jnb	acc.0,00102$
                                    748 ;	source/CH549_PWM.c:36: PWM_CTRL |= bPWM0_OUT_EN;
      0007E4 43 9D 04         [24]  749 	orl	_PWM_CTRL,#0x04
      0007E7                        750 00102$:
                                    751 ;	source/CH549_PWM.c:38: if(Channel&CH1)
      0007E7 EF               [12]  752 	mov	a,r7
      0007E8 30 E1 03         [24]  753 	jnb	acc.1,00104$
                                    754 ;	source/CH549_PWM.c:40: PWM_CTRL |= bPWM1_OUT_EN;
      0007EB 43 9D 08         [24]  755 	orl	_PWM_CTRL,#0x08
      0007EE                        756 00104$:
                                    757 ;	source/CH549_PWM.c:42: PWM_CTRL2 = (Channel>>2);
      0007EE EF               [12]  758 	mov	a,r7
      0007EF 03               [12]  759 	rr	a
      0007F0 03               [12]  760 	rr	a
      0007F1 54 3F            [12]  761 	anl	a,#0x3f
      0007F3 F5 9F            [12]  762 	mov	_PWM_CTRL2,a
                                    763 ;	source/CH549_PWM.c:44: for(i=0; i!=6; i++)
      0007F5 7E 00            [12]  764 	mov	r6,#0x00
      0007F7                        765 00119$:
                                    766 ;	source/CH549_PWM.c:46: if(Channel & (1<<i))
      0007F7 8E F0            [24]  767 	mov	b,r6
      0007F9 05 F0            [12]  768 	inc	b
      0007FB 7C 01            [12]  769 	mov	r4,#0x01
      0007FD 7D 00            [12]  770 	mov	r5,#0x00
      0007FF 80 06            [24]  771 	sjmp	00169$
      000801                        772 00168$:
      000801 EC               [12]  773 	mov	a,r4
      000802 2C               [12]  774 	add	a,r4
      000803 FC               [12]  775 	mov	r4,a
      000804 ED               [12]  776 	mov	a,r5
      000805 33               [12]  777 	rlc	a
      000806 FD               [12]  778 	mov	r5,a
      000807                        779 00169$:
      000807 D5 F0 F7         [24]  780 	djnz	b,00168$
      00080A 8F 02            [24]  781 	mov	ar2,r7
      00080C 7B 00            [12]  782 	mov	r3,#0x00
      00080E EA               [12]  783 	mov	a,r2
      00080F 52 04            [12]  784 	anl	ar4,a
      000811 EB               [12]  785 	mov	a,r3
      000812 52 05            [12]  786 	anl	ar5,a
      000814 EC               [12]  787 	mov	a,r4
      000815 4D               [12]  788 	orl	a,r5
      000816 60 20            [24]  789 	jz	00120$
                                    790 ;	source/CH549_PWM.c:48: P2_MOD_OC &= ~(1<<(5-i));
      000818 8E 05            [24]  791 	mov	ar5,r6
      00081A 74 05            [12]  792 	mov	a,#0x05
      00081C C3               [12]  793 	clr	c
      00081D 9D               [12]  794 	subb	a,r5
      00081E F5 F0            [12]  795 	mov	b,a
      000820 05 F0            [12]  796 	inc	b
      000822 74 01            [12]  797 	mov	a,#0x01
      000824 80 02            [24]  798 	sjmp	00173$
      000826                        799 00171$:
      000826 25 E0            [12]  800 	add	a,acc
      000828                        801 00173$:
      000828 D5 F0 FB         [24]  802 	djnz	b,00171$
      00082B FD               [12]  803 	mov	r5,a
      00082C F4               [12]  804 	cpl	a
      00082D AB 94            [24]  805 	mov	r3,_P2_MOD_OC
      00082F 5B               [12]  806 	anl	a,r3
      000830 F5 94            [12]  807 	mov	_P2_MOD_OC,a
                                    808 ;	source/CH549_PWM.c:49: P2_DIR_PU |= (1<<(5-i));
      000832 AC 95            [24]  809 	mov	r4,_P2_DIR_PU
      000834 ED               [12]  810 	mov	a,r5
      000835 4C               [12]  811 	orl	a,r4
      000836 F5 95            [12]  812 	mov	_P2_DIR_PU,a
      000838                        813 00120$:
                                    814 ;	source/CH549_PWM.c:44: for(i=0; i!=6; i++)
      000838 0E               [12]  815 	inc	r6
      000839 BE 06 BB         [24]  816 	cjne	r6,#0x06,00119$
                                    817 ;	source/CH549_PWM.c:52: if(Channel&CH6)
      00083C EF               [12]  818 	mov	a,r7
      00083D 30 E6 06         [24]  819 	jnb	acc.6,00109$
                                    820 ;	source/CH549_PWM.c:54: P2_MOD_OC &= ~CH6;
      000840 53 94 BF         [24]  821 	anl	_P2_MOD_OC,#0xbf
                                    822 ;	source/CH549_PWM.c:55: P2_DIR_PU |= CH6;
      000843 43 95 40         [24]  823 	orl	_P2_DIR_PU,#0x40
      000846                        824 00109$:
                                    825 ;	source/CH549_PWM.c:57: if(Channel&CH7)
      000846 EF               [12]  826 	mov	a,r7
      000847 30 E7 1E         [24]  827 	jnb	acc.7,00121$
                                    828 ;	source/CH549_PWM.c:59: P2_MOD_OC &= ~CH7;
      00084A 53 94 7F         [24]  829 	anl	_P2_MOD_OC,#0x7f
                                    830 ;	source/CH549_PWM.c:60: P2_DIR_PU |= CH7;
      00084D 43 95 80         [24]  831 	orl	_P2_DIR_PU,#0x80
      000850 22               [24]  832 	ret
      000851                        833 00117$:
                                    834 ;	source/CH549_PWM.c:65: if(Channel&CH0)
      000851 EF               [12]  835 	mov	a,r7
      000852 30 E0 03         [24]  836 	jnb	acc.0,00113$
                                    837 ;	source/CH549_PWM.c:67: PWM_CTRL &= ~bPWM0_OUT_EN;
      000855 53 9D FB         [24]  838 	anl	_PWM_CTRL,#0xfb
      000858                        839 00113$:
                                    840 ;	source/CH549_PWM.c:69: if(Channel&CH1)
      000858 EF               [12]  841 	mov	a,r7
      000859 30 E1 03         [24]  842 	jnb	acc.1,00115$
                                    843 ;	source/CH549_PWM.c:71: PWM_CTRL &= ~bPWM1_OUT_EN;
      00085C 53 9D F7         [24]  844 	anl	_PWM_CTRL,#0xf7
      00085F                        845 00115$:
                                    846 ;	source/CH549_PWM.c:73: PWM_CTRL2 &= ~(Channel>>2);
      00085F EF               [12]  847 	mov	a,r7
      000860 03               [12]  848 	rr	a
      000861 03               [12]  849 	rr	a
      000862 54 3F            [12]  850 	anl	a,#0x3f
      000864 F4               [12]  851 	cpl	a
      000865 FF               [12]  852 	mov	r7,a
      000866 52 9F            [12]  853 	anl	_PWM_CTRL2,a
      000868                        854 00121$:
                                    855 ;	source/CH549_PWM.c:75: }
      000868 22               [24]  856 	ret
                                    857 	.area CSEG    (CODE)
                                    858 	.area CONST   (CODE)
                                    859 	.area XINIT   (CODE)
                                    860 	.area CABS    (ABS,CODE)
