# CH549

#### 介绍  
VeriMake 制作的 51 单片机视频教程的配套材料（以沁恒 CH549 为学习平台）

#### 使用说明  
请查看我们发布在 VeriMake 论坛上的 [**51 教程 | 主索引**](https://verimake.com/topics/255) 了解教程的所有内容。